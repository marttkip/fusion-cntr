<?php

$result = '';
if($query != null)
{
	if($query->num_rows() > 0)
	{
		foreach ($query->result() as $key => $value) {
			# code...
			$message_template_id = $value->message_template_id;
			$message_template_description = $value->message_template_description;
			$message_template_code = $value->message_template_code;
			$result .='<li class="status-online">
					    <a onclick="update_message_template('.$message_template_id.')">
					      <input type="checkbox" name="message_template_id" class="menu-icon " >
					      <span class="name">'.$message_template_code.'</span>
					    </a>
					  </li>';
		}

		
	}
	else
	{
		$result .='<li class="status-online">
				    <span class="name">No templates by that name</span>
				  </li>';
	}

}
else
{
	$result .='<li class="status-online">
				    <span class="name">Kindly search using template titles</span>
				  </li>';
}

echo $result;

?>