<!-- <div id="loader" style="display: none;"></div> -->
<div class="row">
	<section class="panel">
	    <header class="panel-heading">
	            <h5 class="pull-left"><i class="icon-reorder"></i>Messages</h5>
	          <div class="widget-icons pull-right">
	          	
	          	<a onclick="compose_message()" class="btn btn-success btn-sm"> Compose Message</a>
	          	
	          </div>
	          <div class="clearfix"></div>
	    </header>
	    <div class="panel-body">
	    	<div class="col-md-12" style="margin-bottom: 10px;">
	    		<div class="pull-right">
	    			<input type="text" name="search_field" id="search_field" class="form-control" placeholder="Search messages ......" onkeyup="get_searched_emails()">
	    		</div>
	    		
	    	</div>
	    	<div class="" style="margin-top: 10px;">


                            
		    	<div id="email-list"></div>
		    </div>

		  
			
		</div>
	</section>
</div>

<script type="text/javascript">
	var config_url = document.getElementById("config_url").value;
	$(document).ready(function(){
		var config_url = document.getElementById("config_url").value;
		get_emails_view();
		 
	});
// 	jQuery(document).ready(function($) {
// 		 $('.multiselect').multiselect();
// });
	function get_emails_view()
	{

		// document.getElementById("loader").style.display = "block";
		var XMLHttpRequestObject = false;       
		 if (window.XMLHttpRequest) {
		 
		   XMLHttpRequestObject = new XMLHttpRequest();
		 } 
		   
		 else if (window.ActiveXObject) {
		   XMLHttpRequestObject = new ActiveXObject("Microsoft.XMLHTTP");
		 }
		 var config_url = $('#config_url').val();
		 var url = config_url+"messaging/mailing/email_view";
		 // alert(url);
		 if(XMLHttpRequestObject) {
		   var obj = document.getElementById("email-list");
		   XMLHttpRequestObject.open("GET", url);
		       
		   XMLHttpRequestObject.onreadystatechange = function(){
		     
		     if (XMLHttpRequestObject.readyState == 4 && XMLHttpRequestObject.status == 200) {
		       obj.innerHTML = XMLHttpRequestObject.responseText;
		      	// document.getElementById("loader").style.display = "none";
       			// get_email_list(0);
		     }
		   }
		   
		   XMLHttpRequestObject.send(null);
		 }
		 else
		 {
		 	// document.getElementById("loader").style.display = "none";
		 }

		

	}
	function get_email_list(pages)
	{
		// document.getElementById("loader").style.display = "block";
		var XMLHttpRequestObject = false;       
		 if (window.XMLHttpRequest) {
		 
		   XMLHttpRequestObject = new XMLHttpRequest();
		 } 
		   
		 else if (window.ActiveXObject) {
		   XMLHttpRequestObject = new ActiveXObject("Microsoft.XMLHTTP");

		 }
		 var config_url = $('#config_url').val();
		 var url = config_url+"messaging/mailing/emails_items/"+pages;
		 // alert(url);
		 if(XMLHttpRequestObject) {
		   var obj = document.getElementById("email-list");
		   XMLHttpRequestObject.open("GET", url);
		       
		   XMLHttpRequestObject.onreadystatechange = function(){
		     
		     if (XMLHttpRequestObject.readyState == 4 && XMLHttpRequestObject.status == 200) {
		       obj.innerHTML = XMLHttpRequestObject.responseText;
		      // document.getElementById("loader").style.display = "none";
		     }
		   }
		   
		   XMLHttpRequestObject.send(null);
		 }
		 else
		 {
		 	// document.getElementById("loader").style.display = "none";
		 }

		
	}

	function get_email_details(email_id,page)
	{
		document.getElementById("loader").style.display = "block";
		var XMLHttpRequestObject = false;       
		 if (window.XMLHttpRequest) {
		 
		   XMLHttpRequestObject = new XMLHttpRequest();
		 } 
		   
		 else if (window.ActiveXObject) {
		   XMLHttpRequestObject = new ActiveXObject("Microsoft.XMLHTTP");

		 }
		 var config_url = $('#config_url').val();
		 var url = config_url+"messaging/mailing/email_detail/"+email_id+"/"+page;
		 // alert(url);
		 if(XMLHttpRequestObject) {
		   var obj = document.getElementById("mailing-items");
		   XMLHttpRequestObject.open("GET", url);
		       
		   XMLHttpRequestObject.onreadystatechange = function(){
		     
		     if (XMLHttpRequestObject.readyState == 4 && XMLHttpRequestObject.status == 200) {
		       obj.innerHTML = XMLHttpRequestObject.responseText;
		      document.getElementById("loader").style.display = "none";
		     }
		   }
		   
		   XMLHttpRequestObject.send(null);
		 }
		 else
		 {
		 	document.getElementById("loader").style.display = "none";
		 }
	}

	function compose_email()
	{
		document.getElementById("loader").style.display = "block";
		var XMLHttpRequestObject = false;       
		 if (window.XMLHttpRequest) {
		 
		   XMLHttpRequestObject = new XMLHttpRequest();
		 } 
		   
		 else if (window.ActiveXObject) {
		   XMLHttpRequestObject = new ActiveXObject("Microsoft.XMLHTTP");
		 }
		 var config_url = $('#config_url').val();
		 var url = config_url+"messaging/mailing/compose_email_view";
		 // alert(url);
		 if(XMLHttpRequestObject) {
		   var obj = document.getElementById("mailing-items");
		   XMLHttpRequestObject.open("GET", url);
		       
		   XMLHttpRequestObject.onreadystatechange = function(){
		     
		     if (XMLHttpRequestObject.readyState == 4 && XMLHttpRequestObject.status == 200) {
		       obj.innerHTML = XMLHttpRequestObject.responseText;
		      	document.getElementById("loader").style.display = "none";
		      	// $("#rx").cleditor();
		      	tinymce.init({
				                selector: ".cleditor",
				               	height: "600"
					            });

		      	// $("#patient_id").customselect();	  


		      	
		     }
		   }
		   
		   XMLHttpRequestObject.send(null);
		 }
		 else
		 {
		 	document.getElementById("loader").style.display = "none";
		 }

	}




	function autocomplete(inp, arr) {
	  /*the autocomplete function takes two arguments,
	  the text field element and an array of possible autocompleted values:*/
	  var currentFocus;
	  /*execute a function when someone writes in the text field:*/
	  inp.addEventListener("input", function(e) {
	      var a, b, i, val = this.value;
	      /*close any already open lists of autocompleted values*/
	      closeAllLists();
	      if (!val) { return false;}
	      currentFocus = -1;
	      /*create a DIV element that will contain the items (values):*/
	      a = document.createElement("DIV");
	      a.setAttribute("id", this.id + "autocomplete-list");
	      a.setAttribute("class", "autocomplete-items");
	      /*append the DIV element as a child of the autocomplete container:*/
	      this.parentNode.appendChild(a);
	      /*for each item in the array...*/
	      for (i = 0; i < arr.length; i++) {
	        /*check if the item starts with the same letters as the text field value:*/
	        if (arr[i].substr(0, val.length).toUpperCase() == val.toUpperCase()) {
	          /*create a DIV element for each matching element:*/
	          b = document.createElement("DIV");
	          /*make the matching letters bold:*/
	          b.innerHTML = "<strong>" + arr[i].substr(0, val.length) + "</strong>";
	          b.innerHTML += arr[i].substr(val.length);
	          /*insert a input field that will hold the current array item's value:*/
	          b.innerHTML += "<input type='hidden' value='" + arr[i] + "'>";
	          /*execute a function when someone clicks on the item value (DIV element):*/
	          b.addEventListener("click", function(e) {
	              /*insert the value for the autocomplete text field:*/
	              inp.value = this.getElementsByTagName("input")[0].value;
	              // alert(inp.value);
	              /*close the list of autocompleted values,
	              (or any other open lists of autocompleted values:*/
	              closeAllLists();
	          });
	          a.appendChild(b);
	        }
	      }
	  });
	  /*execute a function presses a key on the keyboard:*/
	  inp.addEventListener("keydown", function(e) {
	      var x = document.getElementById(this.id + "autocomplete-list");
	      if (x) x = x.getElementsByTagName("div");
	      if (e.keyCode == 40) {
	        /*If the arrow DOWN key is pressed,
	        increase the currentFocus variable:*/
	        currentFocus++;
	        /*and and make the current item more visible:*/
	        addActive(x);
	      } else if (e.keyCode == 38) { //up
	        /*If the arrow UP key is pressed,
	        decrease the currentFocus variable:*/
	        currentFocus--;
	        /*and and make the current item more visible:*/
	        addActive(x);
	      } else if (e.keyCode == 13) {
	        /*If the ENTER key is pressed, prevent the form from being submitted,*/
	        e.preventDefault();
	        if (currentFocus > -1) {
	          /*and simulate a click on the "active" item:*/
	          if (x) x[currentFocus].click();
	        }
	      }
	  });
	  function addActive(x) {
	    /*a function to classify an item as "active":*/
	    if (!x) return false;
	    /*start by removing the "active" class on all items:*/
	    removeActive(x);
	    if (currentFocus >= x.length) currentFocus = 0;
	    if (currentFocus < 0) currentFocus = (x.length - 1);
	    /*add class "autocomplete-active":*/
	    x[currentFocus].classList.add("autocomplete-active");
	  }
	  function removeActive(x) {
	    /*a function to remove the "active" class from all autocomplete items:*/
	    for (var i = 0; i < x.length; i++) {
	      x[i].classList.remove("autocomplete-active");
	    }
	  }
	  function closeAllLists(elmnt) {
	    /*close all autocomplete lists in the document,
	    except the one passed as an argument:*/
	    var x = document.getElementsByClassName("autocomplete-items");
	    for (var i = 0; i < x.length; i++) {
	      if (elmnt != x[i] && elmnt != inp) {
	        x[i].parentNode.removeChild(x[i]);
	      }
	    }
	  }
	  /*execute a function when someone clicks in the document:*/
	  document.addEventListener("click", function (e) {
	      closeAllLists(e.target);
	  });
	}

	function get_patient_emails(patient_id)
	{
		var config_url = $('#config_url').val();
        var data_url_tca = config_url+"messaging/mailing/get_patient_email_detail/"+patient_id;
		// var category = $('#category:checked').val();
		var category = $("input[name='category']:checked").val();

		
        var doctor_notes_tca = "";
        $.ajax({
        type:'POST',
        url: data_url_tca,
        data:{vategory: category},
        dataType: 'text',
        success:function(data){
        	// alert(data);
        	 var data = jQuery.parseJSON(data);
        	if(data.message == 'success')
        	{
        		// alert(data.patient_email);
        		if(category == 0)
	        	{
	        		document.getElementById("email").style.display = "block";
	           		document.getElementById("email").value = data.patient_email;
	           		document.getElementById("phone_div").style.display = "block";
	           		document.getElementById("phone_number").value = data.patient_phone;
	        	}
	        	else if(category == 1)
	        	{
	        		document.getElementById("email").style.display = "block";
	           		document.getElementById("email").value = data.patient_email;
	           		document.getElementById("phone_div").style.display = "none";
	        	}
	        	else if(category == 2)
	        	{
	        		document.getElementById("email").style.display = "none";
	           		document.getElementById("phone_div").style.display = "block";
	           		document.getElementById("phone_number").value = data.patient_phone;
	        	}

        	}
        	
           
        },
        error: function(xhr, status, error) {
        //alert("XMLHttpRequest=" + xhr.responseText + "\ntextStatus=" + status + "\nerrorThrown=" + error);
        alert(error);

        }

        });
	}


	function send_patient_message()
	{
		// e.preventDefault();

		// document.getElementById("loader").style.display = "block";
		var config_url = $('#config_url').val();
	 	var url = config_url+"messaging/mailing/send_email_message";
		// alert(summary_notes);

		var category = $("input[name='category']:checked").val();

		if(category == 1)
		{

			var message = tinymce.get('rx').getContent();
			var email_from = $('#email_from').val();
			var patient_id = $('#patient_id').val();
			var subject = $('#subject').val();
			var email = $('#email').val();
			var phone_number = '';
			var sms = '';

		}
		else if(category == 2)
		{
			var message = '';
			var email_from = '';
			var subject = '';
			var email = '';
			var patient_id = $('#patient_id').val();
			var phone_number = $('#phone_number').val();
			var sms = $('#sms').val();
		}
		else if(category == 0)
		{
			var message = tinymce.get('rx').getContent();
			var email_from = $('#email_from').val();
			var patient_id = $('#patient_id').val();
			var subject = $('#subject').val();
			var email = $('#email').val();
			var phone_number = $('#phone_number').val();
			var sms = $('#sms').val();
		}
		
		// var option = document.getElementById("optionsRadios1"+appointment_id).value; 
		$.ajax({
		type:'POST',
		url: url,
		data:{patient_id: patient_id,email_from: email_from,message: message,subject: subject,email: email,phone_number: phone_number, sms: sms,category: category},
		dataType: 'text',
		// processData: false,
		// contentType: false,
		success:function(data){
		  var data = jQuery.parseJSON(data);
		 
		  // document.getElementById("loader").style.display = "none";
		  // alert(data.message);
		  if(data.message='success')
		  {
		  	// if(category < 2)
		  	// {
		  	// 	tinymce.get('rx').setContent('');
		  	// }
		  	 close_side_bar();
		  	 get_emails_view();
		  }
		  else
		  {
		  	alert(data.result);
		  }
		  
		},
		error: function(xhr, status, error) {
		 alert("XMLHttpRequest=" + xhr.responseText + "\ntextStatus=" + status + "\nerrorThrown=" + error);
			// document.getElementById("loader").style.display = "none";
		}
		});
	}


	function get_message_view(category)
	{

		if(category == 1)
		{
			document.getElementById("phone_div").style.display = "none";
			document.getElementById("sms_div").style.display = "none";

			
		}
		else if(category == 2)
		{
			document.getElementById("from_div").style.display = "none";
			document.getElementById("email").style.display = "none";
			document.getElementById("subject_div").style.display = "none";
			document.getElementById("message_div").style.display = "none";

			document.getElementById("sms_div").style.display = "block";
			document.getElementById("phone_div").style.display = "block";
		}

		else if(category == 0)
		{
			document.getElementById("from_div").style.display = "block";
			document.getElementById("email").style.display = "block";
			document.getElementById("subject_div").style.display = "block";
			document.getElementById("message_div").style.display = "block";

			document.getElementById("sms_div").style.display = "block";
			document.getElementById("phone_div").style.display = "block";
		}


	}

	function get_searched_emails()
	{

		var config_url = $('#config_url').val();
	 	var url = config_url+"messaging/mailing/email_view";
	 	var search = $('#search_field').val();
	 	// alert(search);
	 	$.ajax({
		type:'POST',
		url: url,
		data:{search: search},
		dataType: 'text',
		success:function(data){
		  // var data = jQuery.parseJSON(data);		 
		  // document.getElementById("loader").style.display = "none";	
// alert(data);
		  $( "#email-list" ).html(data);

		  
		},
		error: function(xhr, status, error) {
		 alert("XMLHttpRequest=" + xhr.responseText + "\ntextStatus=" + status + "\nerrorThrown=" + error);
			// document.getElementById("loader").style.display = "none";
			 // document.getElementById("loader").style.display = "none";	
		}
		});

		return false;

	}


	$(document).on("submit","form#searchform",function(e)
	{
		e.preventDefault();

		var search = $('#q').val();

		document.getElementById("loader").style.display = "block";
		var config_url = $('#config_url').val();
	 	var url = config_url+"messaging/mailing/emails_items/0";

	 	$.ajax({
		type:'POST',
		url: url,
		data:{search: search},
		dataType: 'text',
		success:function(data){
		  // var data = jQuery.parseJSON(data);		 
		  document.getElementById("loader").style.display = "none";	
// alert(data);
		  $( "#email-list" ).html(data);

		  
		},
		error: function(xhr, status, error) {
		 alert("XMLHttpRequest=" + xhr.responseText + "\ntextStatus=" + status + "\nerrorThrown=" + error);
			// document.getElementById("loader").style.display = "none";
			 document.getElementById("loader").style.display = "none";	
		}
		});

		return false;
	});



	function compose_message()
	{
	 
	  document.getElementById("sidebar-right").style.display = "block"; 
	  document.getElementById("existing-sidebar-div").style.display = "none"; 

	  var config_url = $('#config_url').val();
	  var data_url = config_url+"messaging/mailing/compose_message";
	  //window.alert(data_url);
	  $.ajax({
	  type:'POST',
	  url: data_url,
	  data:{appointment_id: 1},
	  dataType: 'text',
	  success:function(data){
	  //window.alert("You have successfully updated the symptoms");
	  //obj.innerHTML = XMLHttpRequestObject.responseText;
	   document.getElementById("current-sidebar-div").style.display = "block"; 
	   $("#current-sidebar-div").html(data);

	 


	    // alert(data);
	  },
	  error: function(xhr, status, error) {
	  //alert("XMLHttpRequest=" + xhr.responseText + "\ntextStatus=" + status + "\nerrorThrown=" + error);
	  alert(error);
	  }

	  });
	}
	function get_searched_patient()
	{
		var config_url = $('#config_url').val();
		var message_type = window.localStorage.getItem("message_type");
		
		//window.alert(data_url);

		var schedule_list_id = 0;
		
		if(message_type == 0)
		{
			var doctor_id = null;
			var date_from = document.getElementById("reg_date_from").value;
			var date_to = document.getElementById("reg_date_to").value;
			var lab_test = $('#search_patient').val();

			
		}
		else if(message_type == 1)
		{
			var doctor_id = document.getElementById("doctor_id").value;
			var reg_date_from = document.getElementById("date_from").value;
			var date_to = document.getElementById("date_to").value;
			var lab_test = $('#search_patient_two').val();

			window.localStorage.setItem("message_type",message_type);

			
		}
		else if(message_type == 2)
		{
			var doctor_id = document.getElementById("recall_doctor_id").value;
			var date_from = document.getElementById("recall_date_from").value;
			var date_to = document.getElementById("recall_date_to").value;
			var schedule_list_id = document.getElementById("schedule_list_id").value;
			var lab_test = $('#search_patient_three').val();

			window.localStorage.setItem("message_type",message_type);

		
		}
	
		var identifier = window.localStorage.getItem("identifier");
		var data_url = config_url+"messaging/mailing/search_patients/"+message_type+"/"+identifier;
		$.ajax({
		type:'POST',
		url: data_url,
		data:{query : lab_test,doctor_id: doctor_id,date_from:date_from,date_to: date_to,recall_list_id: schedule_list_id},
		dataType: 'text',
		success:function(data){
		//window.alert("You have successfully updated the symptoms");
		//obj.innerHTML = XMLHttpRequestObject.responseText;
		$("#searched-lab-test").html(data);
		// alert(data);
		},
		error: function(xhr, status, error) {
		//alert("XMLHttpRequest=" + xhr.responseText + "\ntextStatus=" + status + "\nerrorThrown=" + error);
		alert(error);
		}

		});
	}
	function add_lab_test(patient_id,type)
	{

		var res = confirm('Are you sure you want to message this patient ?');

		if(res)
		{	

			var config_url = $('#config_url').val();
			var data_url = config_url+"messaging/mailing/message_patient/"+patient_id;
			// window.alert(data_url);
			$.ajax({
			type:'POST',
			url: data_url,
			data:{patient_id: patient_id},
			dataType: 'text',
			success:function(data){
				//window.alert("You have successfully updated the symptoms");
				//obj.innerHTML = XMLHttpRequestObject.responseText;
				$("#current-sidebar-div").html(data);
				// $('#top-div').css('display', 'none');
				// $('#bottom-div').css('display', 'block');
				tinymce.init({
	                selector: ".cleditor",
	               	height: "150"
		            });

				// var data_url = config_url+"admin/patient_details/"+appointment_id+"/"+patient_id;
				// // window.alert(data_url);
				// $.ajax({
				// type:'POST',
				// url: data_url,
				// data:{appointment_id: appointment_id},
				// dataType: 'text',
				// success:function(data){
				// 	document.getElementById("patient_phone1"+appointment_id).value = data; 
				// },
				// error: function(xhr, status, error) {
				// //alert("XMLHttpRequest=" + xhr.responseText + "\ntextStatus=" + status + "\nerrorThrown=" + error);
				// alert(error);
				// }

				// });
			
			},
			error: function(xhr, status, error) {
			//alert("XMLHttpRequest=" + xhr.responseText + "\ntextStatus=" + status + "\nerrorThrown=" + error);
			alert(error);
			}

			});
		}

	}

	function close_side_bar()
	{
		// $('html').removeClass('sidebar-right-opened');
		document.getElementById("sidebar-right").style.display = "none"; 
		document.getElementById("current-sidebar-div").style.display = "none"; 
		document.getElementById("existing-sidebar-div").style.display = "none"; 
	}

	function schedule_appointment(message_type)
	{
		tinymce.remove();
		if(message_type ==  0)
		{
			document.getElementById("search-field").style.display = "block"; 
			document.getElementById("all-patients").style.display = "block"; 
			document.getElementById("all-appointments").style.display = "none"; 
			document.getElementById("all-recall").style.display = "none"; 
			window.localStorage.setItem("message_type",message_type);
			$("#reg_date_from").datepicker({
				format: 'yyyy-mm-dd',
			    onSelect: function(dateText) {
			      // alert("Selected date: " + dateText + ", Current Selected Value= " + this.value);
			      $(this).change(this.value);
			    }
			  }).on("change", function(dateText) {
			  	var bla = $('#reg_date_from').val();
			    updateAb(bla);
			});
			$("#reg_date_to").datepicker({
				format: 'yyyy-mm-dd',
			    onSelect: function(dateText) {
			      // alert("Selected date: " + dateText + ", Current Selected Value= " + this.value);
			      $(this).change(this.value);
			    }
			  }).on("change", function(dateText) {
			  	var bla = $('#reg_date_to').val();
			    updateAbtwo(bla);
			});

	  		tinymce.init({
	                selector: ".cleditor",
	               	height: "150"
		            });

		}
		else if(message_type == 1)
		{
			document.getElementById("search-field").style.display = "block"; 
			document.getElementById("all-patients").style.display = "none"; 
			document.getElementById("all-appointments").style.display = "block"; 
			document.getElementById("all-recall").style.display = "none"; 

			window.localStorage.setItem("message_type",message_type);

			// $('.datepicker').datepicker({
			//     format: 'yyyy-mm-dd'
			// });

			$("#date_from").datepicker({
				format: 'yyyy-mm-dd',
			    onSelect: function(dateText) {
			      // alert("Selected date: " + dateText + ", Current Selected Value= " + this.value);
			      $(this).change(this.value);
			    }
			  }).on("change", function(dateText) {
			  	var bla = $('#date_from').val();
			    updateAb(bla);
			});
			$("#date_to").datepicker({
				format: 'yyyy-mm-dd',
			    onSelect: function(dateText) {
			      // alert("Selected date: " + dateText + ", Current Selected Value= " + this.value);
			      $(this).change(this.value);
			    }
			  }).on("change", function(dateText) {
			  	var bla = $('#date_to').val();
			    updateAbtwo(bla);
			});


			

			
	  		// tinymce.init({
	    //             selector: ".cleditor",
	    //            	height: "150"
		   //          });
			
		}
		else if(message_type == 2)
		{
			document.getElementById("search-field").style.display = "block"; 
			document.getElementById("all-patients").style.display = "none"; 
			document.getElementById("all-appointments").style.display = "none"; 
			document.getElementById("all-recall").style.display = "block"; 

			window.localStorage.setItem("message_type",message_type);

			// $('.datepicker').datepicker({
			//     format: 'yyyy-mm-dd'
			// });

			$("#recall_date_from").datepicker({
				format: 'yyyy-mm-dd',
			    onSelect: function(dateText) {
			      // alert("Selected date: " + dateText + ", Current Selected Value= " + this.value);
			      $(this).change(this.value);
			    }
			  }).on("change", function(dateText) {
			  	var bla = $('#date_from').val();
			    updateAb(bla);
			});
			$("#recall_date_to").datepicker({
				format: 'yyyy-mm-dd',
			    onSelect: function(dateText) {
			      // alert("Selected date: " + dateText + ", Current Selected Value= " + this.value);
			      $(this).change(this.value);
			    }
			  }).on("change", function(dateText) {
			  	var bla = $('#date_to').val();
			    updateAbtwo(bla);
			});
			
		}

		var d = <?php echo date('ymdhmi')?>;

		window.localStorage.setItem("identifier",d);



		get_searched_templates();
		get_searched_patient();
		get_listed_patients(d,message_type);

	}



	
	function updateAb(value)
	{   

		// alert(value);
	    $('#datepicker_one').val(value);    
	    get_searched_patient();
	}
	
	function updateAbtwo(value)
	{     
	    $('#datepicker_two').val(value);  
	     get_searched_patient();  
	}

	function get_searched_templates()
	{
		var config_url = $('#config_url').val();
	 	var url = config_url+"messaging/mailing/search_templates";
	 	var search = $('#template_search').val();
	 	 document.getElementById("textarea-list").style.display = "none";	
	 	 tinymce.remove();
	 	// alert(search);
	 	$.ajax({
		type:'POST',
		url: url,
		data:{query: search},
		dataType: 'text',
		success:function(data)
		{
		  // var data = jQuery.parseJSON(data);		 
		  // document.getElementById("loader").style.display = "none";	
		  // alert(data);
			// alert(data);
		  $( "#searched-templates" ).html(data);

		  
		},
		error: function(xhr, status, error) {
		 alert("XMLHttpRequest=" + xhr.responseText + "\ntextStatus=" + status + "\nerrorThrown=" + error);
			// document.getElementById("loader").style.display = "none";
			 // document.getElementById("loader").style.display = "none";	
		}
		});

		return false;
	}
	function update_message_template(message_template_id)
	{
		var config_url = $('#config_url').val();
	 	var url = config_url+"messaging/mailing/get_message_template/"+message_template_id;
	 	var search = $('#template_search').val();
	 	// alert(search);
	 	$.ajax({
		type:'POST',
		url: url,
		data:{query: search},
		dataType: 'text',
		success:function(data)
		{
		  var data = jQuery.parseJSON(data);		 
		  document.getElementById("textarea-list").style.display = "block";	
		  // alert(data);
			// alert(data);
		  // $( "#searched-templates" ).html(data);
		  // $("input#subject").val(data.subject);
		  document.getElementById("subject").value = data.subject; 
          $("textarea#message_item_description").val(data.message);
	      	tinymce.init({
		                selector: ".cleditor",
		               	height: "150"
			            });
          // alert(data);

		  
		},
		error: function(xhr, status, error) {
		 alert("XMLHttpRequest=" + xhr.responseText + "\ntextStatus=" + status + "\nerrorThrown=" + error);
			// document.getElementById("loader").style.display = "none";
			 // document.getElementById("loader").style.display = "none";	
		}
		});

		return false;
	}


	$(document).on("submit","form#submit-message",function(e)
	{
		e.preventDefault();
		// myApp.showIndicator();
		
		var form_data = new FormData(this);

		// alert(form_data);

		var config_url = $('#config_url').val();
		var message = tinymce.get('message_item_description').getContent();
		

		var channel_id = getRadioVal( document.getElementById('submit-message'), 'channel_id' );
		// alert(channel_id);

		if(channel_id >= 0 && message != '')
		{


		
			// if()
			close_side_bar(); 

			 var url = config_url+"messaging/mailing/send_message_items";
		       $.ajax({
		       type:'POST',
		       url: url,
		       data:form_data,
		       dataType: 'text',
		       processData: false,
		       contentType: false,
		       success:function(data){
		          var data = jQuery.parseJSON(data);
		        	// alert(data);
		          	if(data.status == "success")
					{	
						
						
						// document.getElementById("add_template_note").reset();
						// $('#add_template_note').modal('hide'); 
						get_emails_view();
						alert(data.message);
						
					}
					else
					{
						alert(data.message);
					}
		       
		       },
		       error: function(xhr, status, error) {
		       alert("XMLHttpRequest=" + xhr.responseText + "\ntextStatus=" + status + "\nerrorThrown=" + error);
		       
		       }
		       });
		 }
		 else
		 {
		 	alert('Ensure you have selected the channel and typed in a message to send');
		 }
				 
	});

	function getRadioVal(form, name) {
	    var val;
	    // get list of radio buttons with specified name
	    var radios = form.elements[name];
	    
	    // loop through list of radio buttons
	    for (var i=0, len=radios.length; i<len; i++) {
	        if ( radios[i].checked ) { // radio checked?
	            val = radios[i].value; // if so, hold its value in val
	            break; // and break out of for loop
	        }
	    }
	    return val; // return value of checked radio or undefined if none checked
	}

	function add_to_list(patient_id,identifier)
	{
		var config_url = $('#config_url').val();

		var message_type = window.localStorage.getItem("message_type");

	 	var url = config_url+"messaging/mailing/add_patient_to_list/"+patient_id+"/"+identifier+"/"+message_type;
	 	
	 	$.ajax({
		type:'POST',
		url: url,
		data:{query: null},
		dataType: 'text',
		success:function(data)
		{
		  // var data = jQuery.parseJSON(data);		 
		  // document.getElementById("loader").style.display = "none";	
		  // alert(data);
			// alert(data);
		  // $( "#searched-templates" ).html(data);

		  get_listed_patients(identifier,message_type);

		  
		},
		error: function(xhr, status, error) {
		 alert("XMLHttpRequest=" + xhr.responseText + "\ntextStatus=" + status + "\nerrorThrown=" + error);
			// document.getElementById("loader").style.display = "none";
			 // document.getElementById("loader").style.display = "none";	
		}
		});

		return false;
	}

	function get_listed_patients(identifier,message_type)
	{

		var config_url = $('#config_url').val();
	 	var url = config_url+"messaging/mailing/listed_patients/"+identifier+"/"+message_type;
	 	
	 	// alert(search);
	 	$.ajax({
		type:'POST',
		url: url,
		data:{query: null},
		dataType: 'text',
		success:function(data)
		{
		  // var data = jQuery.parseJSON(data);		 
		  // document.getElementById("loader").style.display = "none";	
		  // alert(data);
			// alert(data);
		  $( "#listed-items" ).html(data);

		  
		},
		error: function(xhr, status, error) {
		 alert("XMLHttpRequest=" + xhr.responseText + "\ntextStatus=" + status + "\nerrorThrown=" + error);
			// document.getElementById("loader").style.display = "none";
			 // document.getElementById("loader").style.display = "none";	
		}
		});

		return false;

	}

	function remove_from_list(picked_id,identifier,message_type)
	{

		var message_type = window.localStorage.getItem("message_type");
		var identifier = window.localStorage.getItem("identifier");

		var config_url = $('#config_url').val();


	 	var url = config_url+"messaging/mailing/delete_from_list/"+picked_id+"/"+identifier+"/"+message_type;
	 	
	 	$.ajax({
		type:'POST',
		url: url,
		data:{query: null},
		dataType: 'text',
		success:function(data)
		{
		  // var data = jQuery.parseJSON(data);		 
		  // document.getElementById("loader").style.display = "none";	
		  // alert(data);
			// alert(data);
		  // $( "#searched-templates" ).html(data);

		  get_listed_patients(identifier,message_type);

		  
		},
		error: function(xhr, status, error) {
		 alert("XMLHttpRequest=" + xhr.responseText + "\ntextStatus=" + status + "\nerrorThrown=" + error);
			// document.getElementById("loader").style.display = "none";
			 // document.getElementById("loader").style.display = "none";	
		}
		});

		return false;

	}

	function search_items_listed_patient()
	{
		// alert('sdasjkda');
		var message_type = window.localStorage.getItem("message_type");
		var identifier = window.localStorage.getItem("identifier");
		var config_url = $('#config_url').val();
		var data_url = config_url+"messaging/mailing/search_listed_patients/"+message_type+"/"+identifier;

		var lab_test = $('#search_listed_patient').val();
		$.ajax({
		type:'POST',
		url: data_url,
		data:{query : lab_test},
		dataType: 'text',
		success:function(data){
		//window.alert("You have successfully updated the symptoms");
		//obj.innerHTML = XMLHttpRequestObject.responseText;
		$("#listed-items").html(data);
		// alert(data);
		},
		error: function(xhr, status, error) {
		//alert("XMLHttpRequest=" + xhr.responseText + "\ntextStatus=" + status + "\nerrorThrown=" + error);
		alert(error);
		}

		});

	}
	function add_all_patients()
	{
		var res = confirm('Are you sure you want to add all patients to list ?');

		if(res)
		{

			var config_url = $('#config_url').val();
			var message_type = window.localStorage.getItem("message_type");
			var identifier = window.localStorage.getItem("identifier");
		 	var url = config_url+"messaging/mailing/add_patients_to_list/"+message_type+"/"+identifier;
		 	
		 	$.ajax({
			type:'POST',
			url: url,
			data:{query: null},
			dataType: 'text',
			success:function(data)
			{
			  get_listed_patients(identifier,message_type);
			  
			},
			error: function(xhr, status, error) {
			 alert("XMLHttpRequest=" + xhr.responseText + "\ntextStatus=" + status + "\nerrorThrown=" + error);
				// document.getElementById("loader").style.display = "none";
				 // document.getElementById("loader").style.display = "none";	
			}
			});

			return false;

		}
	}



		
</script>