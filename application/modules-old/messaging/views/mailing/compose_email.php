<?php
$department_email = $this->session->userdata('department_email');

?>

<div class="inner-toolbar clearfix">
	<ul>
		<li>
			<a onclick="send_patient_note()"><i class="fa fa-paper-plane mr-2" ></i> Send</a>
		</li>
		<!-- <li>
			<a href="#"><i class="fa fa-times mr-2"></i> Discard</a>
		</li>
		<li>
			<a href="#"><i class="fa fa-paperclip mr-2"></i> Attach</a>
		</li> -->
	</ul>
</div>
<div class="inner-body mailbox-email mailbox-compose" style="margin-top: -24px !important;">
	<form class="form-horizontal form-bordered form-bordered">
		<div class="form-group">
			<label for="to" class="control-label col-md-1">Type:</label>
			<div class="col-md-10 mailbox-compose-field">
				<div class="radio">
                    <label>
                        <input id="optionsRadios1" type="radio" checked value="0" name="category" onclick="get_message_view(0)">
                        Email and SMS Only
                    </label>
                    <label>
                        <input id="optionsRadios1" type="radio" value="1" name="category" onclick="get_message_view(1)">
                        Email Only
                    </label>
                    <label>
                        <input id="optionsRadios2" type="radio"  value="2" name="category" onclick="get_message_view(2)">
                        SMS Only
                    </label>
                </div>
                        
			</div>
		</div>
		<div class="form-group" id="from_div" style="display: block;">
			<label for="to" class="control-label col-md-1">From:</label>
			<div class="col-md-10 mailbox-compose-field">
				<input id="email_from" name="email_from" type="text" class="form-control"  value="<?php echo $department_email?>" required="required" >
			</div>
		</div>
		<div class="form-group" id="to_div">
			<label for="cc" class="control-label col-md-1">To:</label>
			<div class="col-md-10 mailbox-compose-field">
				<select name="patient_id" id="patient_id" class="form-control custom-select" onchange="get_patient_emails(this.value)" required="required">
					<option value="">----Select a Patient----</option>
					<?php
											
						echo $patients;
					?>
				</select>
				<br>
				<input id="email" name="email" type="text" class="form-control" style="display: none" required="required">

			</div>
			
		</div>
		<div class="form-group" id="subject_div" style="display: block;">
			<label for="subject" class="control-label col-md-1">Subject:</label>
			<div class="col-md-10 mailbox-compose-field">
				<input id="subject" name="subject" type="text" class="form-control" required="required">
			</div>
		</div>
		<div class="form-group" id="message_div" style="display: block;">
			<label class="col-md-1 control-label">Message : </label>
			<div class="col-md-10">
				<textarea id="rx" name="rx"  class="form-control cleditor" required="required"></textarea>
			</div>
		</div>

		<div class="form-group" id="phone_div" style="display: block;">
			<label for="cc" class="control-label col-md-1">To:</label>
			<div class="col-md-10 mailbox-compose-field">
				<input id="phone_number" name="phone_number" type="text" class="form-control"  required="required">
			</div>
			
		</div>
		<div class="form-group" id="sms_div" style="display: block;">
			<label class="col-md-1 control-label">SMS Message : </label>
			<div class="col-md-10">
				<textarea id="sms" name="sms"  class="form-control " required="required"></textarea>
			</div>
		</div>
	</form>
</div>

