<div class="inner-body mailbox-folder">
<header class="mailbox-header">
	<div class="row">
		<div class="col-md-6">
			<h3 class="mailbox-title font-weight-light mt-20">			
				Messages

				<a class="item-action fa fa-refresh"  onclick="get_email_list()"></a>
			</h3>

			<span style="color: #800080"> EMAIL & SMS 	</span> :
				<span style="color: #EA4C89">EMAIL ONLY 	</span> :
				<span style="color: #0088CC">SMS ONLY </span>
			
				
		</div>
		<div class="col-md-6">
			<div class="search">
				<form id="searchform" >
					<div class="input-group">						
						<input type="text" class="form-control" name="q" id="q" placeholder="Search...">
					</div>
				</form>
			</div>
		</div>
	</div>
</header>

<div id="mailbox-email-list" class="mailbox-email-list">
	<div class="nano has-scrollbar">
		<div class="nano-content" tabindex="0" style="right: -17px;">	
				<div id="email-list"></div>
		</div>
	<div class="nano-pane" style="opacity: 1; visibility: visible;"><div class="nano-slider" style="height: 191px; transform: translate(0px, 0px);"></div></div></div>
</div>
</div>