<div id="loader" style="display: none;"></div>
 <section class="panel">
    <header class="panel-heading">
            <h5 class="pull-left"><i class="icon-reorder"></i>Add Patient</h5>
          <div class="widget-icons pull-right">
               <a href="<?php echo site_url();?>uhdc-patients" class="btn btn-success btn-sm"> Back to Patients List</a>

          </div>
          <div class="clearfix"></div>
    </header>
      <div class="panel-body">
        <div class="padd">
			<section class="content-with-menu mailbox">
				<div class="content-with-menu-container" data-mailbox="" data-mailbox-view="folder">
					<div class="inner-menu-toggle">
						<a href="#" class="inner-menu-expand" data-open="inner-menu">
							Show Menu <i class="fa fa-chevron-right"></i>
						</a>
					</div>
					
					<div id="mailing-items"></div>
				
				</div>
			</section>
		</div>
	</div>
</section>
<script type="text/javascript">
	var config_url = document.getElementById("config_url").value;
	$(document).ready(function(){
		var config_url = document.getElementById("config_url").value;
		get_emails_view();
	});

	function get_emails_view()
	{
		document.getElementById("loader").style.display = "block";
		var XMLHttpRequestObject = false;       
		 if (window.XMLHttpRequest) {
		 
		   XMLHttpRequestObject = new XMLHttpRequest();
		 } 
		   
		 else if (window.ActiveXObject) {
		   XMLHttpRequestObject = new ActiveXObject("Microsoft.XMLHTTP");
		 }
		 var config_url = $('#config_url').val();
		 var url = config_url+"messaging/mailing/email_view";
		 // alert(url);
		 if(XMLHttpRequestObject) {
		   var obj = document.getElementById("mailing-items");
		   XMLHttpRequestObject.open("GET", url);
		       
		   XMLHttpRequestObject.onreadystatechange = function(){
		     
		     if (XMLHttpRequestObject.readyState == 4 && XMLHttpRequestObject.status == 200) {
		       obj.innerHTML = XMLHttpRequestObject.responseText;
		      	document.getElementById("loader").style.display = "none";
       			get_email_list(0);
		     }
		   }
		   
		   XMLHttpRequestObject.send(null);
		 }
		 else
		 {
		 	document.getElementById("loader").style.display = "none";
		 }

		

	}
	function get_email_list(pages)
	{
		document.getElementById("loader").style.display = "block";
		var XMLHttpRequestObject = false;       
		 if (window.XMLHttpRequest) {
		 
		   XMLHttpRequestObject = new XMLHttpRequest();
		 } 
		   
		 else if (window.ActiveXObject) {
		   XMLHttpRequestObject = new ActiveXObject("Microsoft.XMLHTTP");

		 }
		 var config_url = $('#config_url').val();
		 var url = config_url+"messaging/mailing/emails_items/"+pages;
		 // alert(url);
		 if(XMLHttpRequestObject) {
		   var obj = document.getElementById("email-list");
		   XMLHttpRequestObject.open("GET", url);
		       
		   XMLHttpRequestObject.onreadystatechange = function(){
		     
		     if (XMLHttpRequestObject.readyState == 4 && XMLHttpRequestObject.status == 200) {
		       obj.innerHTML = XMLHttpRequestObject.responseText;
		      document.getElementById("loader").style.display = "none";
		     }
		   }
		   
		   XMLHttpRequestObject.send(null);
		 }
		 else
		 {
		 	document.getElementById("loader").style.display = "none";
		 }

		
	}

	function get_email_details(email_id,page)
	{
		document.getElementById("loader").style.display = "block";
		var XMLHttpRequestObject = false;       
		 if (window.XMLHttpRequest) {
		 
		   XMLHttpRequestObject = new XMLHttpRequest();
		 } 
		   
		 else if (window.ActiveXObject) {
		   XMLHttpRequestObject = new ActiveXObject("Microsoft.XMLHTTP");

		 }
		 var config_url = $('#config_url').val();
		 var url = config_url+"messaging/mailing/email_detail/"+email_id+"/"+page;
		 // alert(url);
		 if(XMLHttpRequestObject) {
		   var obj = document.getElementById("mailing-items");
		   XMLHttpRequestObject.open("GET", url);
		       
		   XMLHttpRequestObject.onreadystatechange = function(){
		     
		     if (XMLHttpRequestObject.readyState == 4 && XMLHttpRequestObject.status == 200) {
		       obj.innerHTML = XMLHttpRequestObject.responseText;
		      document.getElementById("loader").style.display = "none";
		     }
		   }
		   
		   XMLHttpRequestObject.send(null);
		 }
		 else
		 {
		 	document.getElementById("loader").style.display = "none";
		 }
	}

	function compose_email()
	{
		document.getElementById("loader").style.display = "block";
		var XMLHttpRequestObject = false;       
		 if (window.XMLHttpRequest) {
		 
		   XMLHttpRequestObject = new XMLHttpRequest();
		 } 
		   
		 else if (window.ActiveXObject) {
		   XMLHttpRequestObject = new ActiveXObject("Microsoft.XMLHTTP");
		 }
		 var config_url = $('#config_url').val();
		 var url = config_url+"messaging/mailing/compose_email_view";
		 // alert(url);
		 if(XMLHttpRequestObject) {
		   var obj = document.getElementById("mailing-items");
		   XMLHttpRequestObject.open("GET", url);
		       
		   XMLHttpRequestObject.onreadystatechange = function(){
		     
		     if (XMLHttpRequestObject.readyState == 4 && XMLHttpRequestObject.status == 200) {
		       obj.innerHTML = XMLHttpRequestObject.responseText;
		      	document.getElementById("loader").style.display = "none";
		      	// $("#rx").cleditor();
		      	tinymce.init({
				                selector: ".cleditor",
				               	height: "600"
					            });

		      	$("#patient_id").customselect();	      	

		      	
		     }
		   }
		   
		   XMLHttpRequestObject.send(null);
		 }
		 else
		 {
		 	document.getElementById("loader").style.display = "none";
		 }

	}




	function autocomplete(inp, arr) {
	  /*the autocomplete function takes two arguments,
	  the text field element and an array of possible autocompleted values:*/
	  var currentFocus;
	  /*execute a function when someone writes in the text field:*/
	  inp.addEventListener("input", function(e) {
	      var a, b, i, val = this.value;
	      /*close any already open lists of autocompleted values*/
	      closeAllLists();
	      if (!val) { return false;}
	      currentFocus = -1;
	      /*create a DIV element that will contain the items (values):*/
	      a = document.createElement("DIV");
	      a.setAttribute("id", this.id + "autocomplete-list");
	      a.setAttribute("class", "autocomplete-items");
	      /*append the DIV element as a child of the autocomplete container:*/
	      this.parentNode.appendChild(a);
	      /*for each item in the array...*/
	      for (i = 0; i < arr.length; i++) {
	        /*check if the item starts with the same letters as the text field value:*/
	        if (arr[i].substr(0, val.length).toUpperCase() == val.toUpperCase()) {
	          /*create a DIV element for each matching element:*/
	          b = document.createElement("DIV");
	          /*make the matching letters bold:*/
	          b.innerHTML = "<strong>" + arr[i].substr(0, val.length) + "</strong>";
	          b.innerHTML += arr[i].substr(val.length);
	          /*insert a input field that will hold the current array item's value:*/
	          b.innerHTML += "<input type='hidden' value='" + arr[i] + "'>";
	          /*execute a function when someone clicks on the item value (DIV element):*/
	          b.addEventListener("click", function(e) {
	              /*insert the value for the autocomplete text field:*/
	              inp.value = this.getElementsByTagName("input")[0].value;
	              // alert(inp.value);
	              /*close the list of autocompleted values,
	              (or any other open lists of autocompleted values:*/
	              closeAllLists();
	          });
	          a.appendChild(b);
	        }
	      }
	  });
	  /*execute a function presses a key on the keyboard:*/
	  inp.addEventListener("keydown", function(e) {
	      var x = document.getElementById(this.id + "autocomplete-list");
	      if (x) x = x.getElementsByTagName("div");
	      if (e.keyCode == 40) {
	        /*If the arrow DOWN key is pressed,
	        increase the currentFocus variable:*/
	        currentFocus++;
	        /*and and make the current item more visible:*/
	        addActive(x);
	      } else if (e.keyCode == 38) { //up
	        /*If the arrow UP key is pressed,
	        decrease the currentFocus variable:*/
	        currentFocus--;
	        /*and and make the current item more visible:*/
	        addActive(x);
	      } else if (e.keyCode == 13) {
	        /*If the ENTER key is pressed, prevent the form from being submitted,*/
	        e.preventDefault();
	        if (currentFocus > -1) {
	          /*and simulate a click on the "active" item:*/
	          if (x) x[currentFocus].click();
	        }
	      }
	  });
	  function addActive(x) {
	    /*a function to classify an item as "active":*/
	    if (!x) return false;
	    /*start by removing the "active" class on all items:*/
	    removeActive(x);
	    if (currentFocus >= x.length) currentFocus = 0;
	    if (currentFocus < 0) currentFocus = (x.length - 1);
	    /*add class "autocomplete-active":*/
	    x[currentFocus].classList.add("autocomplete-active");
	  }
	  function removeActive(x) {
	    /*a function to remove the "active" class from all autocomplete items:*/
	    for (var i = 0; i < x.length; i++) {
	      x[i].classList.remove("autocomplete-active");
	    }
	  }
	  function closeAllLists(elmnt) {
	    /*close all autocomplete lists in the document,
	    except the one passed as an argument:*/
	    var x = document.getElementsByClassName("autocomplete-items");
	    for (var i = 0; i < x.length; i++) {
	      if (elmnt != x[i] && elmnt != inp) {
	        x[i].parentNode.removeChild(x[i]);
	      }
	    }
	  }
	  /*execute a function when someone clicks in the document:*/
	  document.addEventListener("click", function (e) {
	      closeAllLists(e.target);
	  });
	}

	function get_patient_emails(patient_id)
	{
		var config_url = $('#config_url').val();
        var data_url_tca = config_url+"messaging/mailing/get_patient_email_detail/"+patient_id;
		// var category = $('#category:checked').val();
		var category = $("input[name='category']:checked").val();

		
        var doctor_notes_tca = "";
        $.ajax({
        type:'POST',
        url: data_url_tca,
        data:{vategory: category},
        dataType: 'text',
        success:function(data){
        	// alert(data);
        	 var data = jQuery.parseJSON(data);
        	if(data.message == 'success')
        	{
        		// alert(data.patient_email);
        		if(category == 0)
	        	{
	        		document.getElementById("email").style.display = "block";
	           		document.getElementById("email").value = data.patient_email;
	           		document.getElementById("phone_div").style.display = "block";
	           		document.getElementById("phone_number").value = data.patient_phone;
	        	}
	        	else if(category == 1)
	        	{
	        		document.getElementById("email").style.display = "block";
	           		document.getElementById("email").value = data.patient_email;
	           		document.getElementById("phone_div").style.display = "none";
	        	}
	        	else if(category == 2)
	        	{
	        		document.getElementById("email").style.display = "none";
	           		document.getElementById("phone_div").style.display = "block";
	           		document.getElementById("phone_number").value = data.patient_phone;
	        	}

        	}
        	
           
        },
        error: function(xhr, status, error) {
        //alert("XMLHttpRequest=" + xhr.responseText + "\ntextStatus=" + status + "\nerrorThrown=" + error);
        alert(error);

        }

        });
	}


	function send_patient_note()
	{

		document.getElementById("loader").style.display = "block";
		var config_url = $('#config_url').val();
	 	var url = config_url+"messaging/mailing/send_email_message";
		// alert(summary_notes);

		var category = $("input[name='category']:checked").val();

		if(category == 1)
		{

			var message = tinymce.get('rx').getContent();
			var email_from = $('#email_from').val();
			var patient_id = $('#patient_id').val();
			var subject = $('#subject').val();
			var email = $('#email').val();
			var phone_number = '';
			var sms = '';

		}
		else if(category == 2)
		{
			var message = '';
			var email_from = '';
			var subject = '';
			var email = '';
			var patient_id = $('#patient_id').val();
			var phone_number = $('#phone_number').val();
			var sms = $('#sms').val();
		}
		else if(category == 0)
		{
			var message = tinymce.get('rx').getContent();
			var email_from = $('#email_from').val();
			var patient_id = $('#patient_id').val();
			var subject = $('#subject').val();
			var email = $('#email').val();
			var phone_number = $('#phone_number').val();
			var sms = $('#sms').val();
		}
		
		// var option = document.getElementById("optionsRadios1"+appointment_id).value; 
		$.ajax({
		type:'POST',
		url: url,
		data:{patient_id: patient_id,email_from: email_from,message: message,subject: subject,email: email,phone_number: phone_number, sms: sms,category: category},
		dataType: 'text',
		// processData: false,
		// contentType: false,
		success:function(data){
		  var data = jQuery.parseJSON(data);
		 
		  document.getElementById("loader").style.display = "none";
		  // alert(data.message);
		  if(data.message='success')
		  {
		  	if(category < 2)
		  	{
		  		tinymce.get('rx').setContent('');
		  	}
		  	 
		  	 get_emails_view();
		  }
		  else
		  {
		  	alert(dara.result);
		  }
		  
		},
		error: function(xhr, status, error) {
		 alert("XMLHttpRequest=" + xhr.responseText + "\ntextStatus=" + status + "\nerrorThrown=" + error);
			// document.getElementById("loader").style.display = "none";
		}
		});
	}


	function get_message_view(category)
	{

		if(category == 1)
		{
			document.getElementById("phone_div").style.display = "none";
			document.getElementById("sms_div").style.display = "none";

			
		}
		else if(category == 2)
		{
			document.getElementById("from_div").style.display = "none";
			document.getElementById("email").style.display = "none";
			document.getElementById("subject_div").style.display = "none";
			document.getElementById("message_div").style.display = "none";

			document.getElementById("sms_div").style.display = "block";
			document.getElementById("phone_div").style.display = "block";
		}

		else if(category == 0)
		{
			document.getElementById("from_div").style.display = "block";
			document.getElementById("email").style.display = "block";
			document.getElementById("subject_div").style.display = "block";
			document.getElementById("message_div").style.display = "block";

			document.getElementById("sms_div").style.display = "block";
			document.getElementById("phone_div").style.display = "block";
		}


	}


	$(document).on("submit","form#searchform",function(e)
	{
		e.preventDefault();

		var search = $('#q').val();

		document.getElementById("loader").style.display = "block";
		var config_url = $('#config_url').val();
	 	var url = config_url+"messaging/mailing/emails_items/0";

	 	$.ajax({
		type:'POST',
		url: url,
		data:{search: search},
		dataType: 'text',
		success:function(data){
		  // var data = jQuery.parseJSON(data);		 
		  document.getElementById("loader").style.display = "none";	
// alert(data);
		  $( "#email-list" ).html(data);

		  
		},
		error: function(xhr, status, error) {
		 alert("XMLHttpRequest=" + xhr.responseText + "\ntextStatus=" + status + "\nerrorThrown=" + error);
			// document.getElementById("loader").style.display = "none";
			 document.getElementById("loader").style.display = "none";	
		}
		});

		return false;
	});

</script>