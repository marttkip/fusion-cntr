<?php
// var_dump()

// $count_visit = $visit_list->num_rows();
// $num_pages = $total_rows/$per_page;

// if($num_pages < 1)
// {
// 	$num_pages = 0;
// }
// $num_pages = round($num_pages);

// if($page==0)
// {
// 	$counted = 0;
// }
// else if($page > 0)
// {
// 	$counted = $per_page*$page;
// }
$result_view = '';
$counted = 0;
if($email_query->num_rows() > 0)
{
	
	foreach ($email_query->result() as $key => $value) {
		# code...
		$email_id = $value->email_id;
		$email_description = $value->email_description;
		$email_from = $value->email_from;
		$email_cc = $value->email_cc;
		$email_to = $value->email_to;
		$email_to = $value->email_to;
		$email_status = $value->email_status;
		$created = $value->created;
		$last_modified = $value->last_modified;
		$email_deleted = $value->email_deleted;
		$patient_name = $value->patient_name;
		$email_subject = $value->email_subject;
		$category = $value->category;
		$department_name = $value->department_name;
		$department_id = $value->department_id;
		$sms = $value->sms;
		// $email_subject = $value->email_subject;
		
		$last_modified = date('M j Y H:i A',strtotime($last_modified));


		if($email_status == 0)
		{
			$color = 'unread';
		}
		else 
		{
			$color = 'unread';

		}
		

		// if($category == 2)
		// {
		// 	$email_subject = $sms;
		// }
		// else
		// {
			$email_subject = $email_description;
		// }


		if($department_id == 0)
		{
			$department_name = '  Administration';
		}
		else
		{
			$department_name = '  '.$department_name;
		}

		if($category == 0)
		{
			$category_color = '#EA4C89';
			$tag = '<button class="btn btn-xs btn-success">Email & SMS</button>';
		}
		else if($category == 1)
		{
			$category_color = '#0088CC';
			$tag = '<button class="btn btn-xs btn-warning">SMS Only</button>';
		}
		else if($category == 2)
		{
			$category_color = '#800080';
			$tag = '<button class="btn btn-xs btn-info">Email Only</button>';
		}
		$mini_desc = strip_tags(implode(' ', array_slice(explode(' ', $email_subject), 0, 25)));	
		
		$counted++;
		$result_view .= '
						  <tr onclick="get_email_details('.$email_id.','.$page.')">
								<td style="color:'.$color.'" >
									<div class="col-sender">
										<i class="mail-label" style="border-color: '.$category_color.'"></i>
										<div class="checkbox-custom checkbox-text-primary ib">
											<input type="checkbox" id="mail'.$email_id.'">
											<label for="mail1"></label>
										</div>
										<p class="m-0 ib">'.$patient_name.'</p>
									</div>
								</td>
								<td>'.$tag.'</td>
								<td>
								<p class="m-0 mail-content">
									<span class="subject">'.$mini_desc.' ~ '.$department_name.'</span>
								</p>
								</td>
								<td><p class="m-0 mail-date">'.$last_modified.'</p></td>
							</tr>';
	}
}

?>



<div class="row">
	<div class="col-md-12" style="padding-right: 25px;">
<table class="table table-stripped table-condensed">
	<thead>
		<th style="width: 20%">Sent To</th>
		<th style="width: 10%">Tag</th>
		<th style="width: 50%">Detail</th>
		<th style="width: 20%">Date</th>
	</thead>
	<tbody>
		<?php echo $result_view;?>
	</tbody>
</table>
</div>
</div>

<div class="row">
	<div class="col-md-12" style="padding-right: 25px;">
		<div class="pull-right">
			<?php
				$link ='<ul style="list-style:none;">';
				// echo $page;
				$num_pages = 2;
				if($num_pages > $page)
				{
					// echo "now ".$num_pages." ".$page;
					$last_page = $num_pages -1;

					if($page > 0 AND $page < $last_page)
					{
						// echo $page;
						$page++;
						// echo "now".$page;
						$previous = $page -2;
						$link .='<li onclick="get_email_list('.$previous.')" class="pull-left" style="margin-right:20px;" > <i class="fa fa-angle-left"></i> Back</li>  <li onclick="get_email_list('.$page.')" class="pull-right"> Next <i class="fa fa-angle-right"></i> </li>';
					}else if($page == $last_page)
					{
						$page++;

						$previous = $page -2;
						// echo "equal".$num_pages." ".$page;
						$link .='<li onclick="get_email_list('.$previous.')" class="pull-left"> <i class="fa fa-angle-left"></i> Back</li>';
					}
					else
					{
						$page++;
						$link .='<li onclick="get_email_list('.$page.')" class="pull-right"> Next <i class="fa fa-angle-right"></i> </li>';
					}
					// var_dump($link); die();
				}
				$link .='</ul>';
				// echo $link;
				
			?>
		</div>
	</div>
</div>
