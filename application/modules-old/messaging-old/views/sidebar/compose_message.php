<section class="panel">
    <header class="panel-heading">
            <h5 class="pull-left"><i class="icon-reorder"></i><?php echo $title?></h5>
          <div class="widget-icons pull-right">
              
          </div>
          <div class="clearfix"></div>
    </header>
    <div class="panel-body">
        <div class="row">
            <?php echo form_open("finance/creditors/confirm_invoice_note", array("class" => "form-horizontal","id"=>"submit-message"));?>
            	<div class="col-md-12" id="top-div" style="display: block">

                    <div class="form-group">
                        <label class="col-lg-2 control-label">Message </label>
                        <div class="col-lg-3">
                            <div class="radio">
                                <label>
                                    <input id="optionsRadios2" type="radio" name="appointment_id" value="0"  onclick="schedule_appointment(0)">
                                    Patient List
                                </label>
                            </div>
                        </div>
                        
                        <div class="col-lg-3">
                            <div class="radio">
                                <label>
                                    <input id="optionsRadios2" type="radio" name="appointment_id" value="1" onclick="schedule_appointment(1)">
                                    Appointment Schedule
                                </label>
                            </div>
                        </div>
                        <div class="col-lg-3">
                            <div class="radio">
                                <label>
                                    <input id="optionsRadios2" type="radio" name="appointment_id" value="3" onclick="schedule_appointment(2)">
                                    Recall List
                                </label>
                            </div>
                        </div>
                    </div>
            		
            		
            	</div>
            	<div class="col-md-12" style="padding:10px;">
                    <div id="search-field" style="display: none;">
                        <div id="all-patients" style="display: none;">
                            <div class="col-md-5">
                               <input type="text" name="search_patient" id="search_patient" class="form-control" onkeyup="get_searched_patient()" placeholder="Search patient i.e name, phone, patient number" autocomplete="off">

                            </div>
                           <div class="col-md-5">
                                 <div class="form-group">
                                    <!-- <label class="col-md-2 control-label"> Date: </label>                                     -->
                                    <div class="col-md-6">
                                        <div class="input-group">
                                           <!--  <span class="input-group-addon">
                                                <i class="fa fa-calendar"></i>
                                            </span> -->
                                            <input data-format="yyyy-MM-dd" type="text"  class="form-control datepicker" name="reg_date_fomr" id="reg_date_from" placeholder="Registration Date" autocomplete="off" >
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="input-group">
                                            <!-- <span class="input-group-addon">
                                                <i class="fa fa-calendar"></i>
                                            </span> -->
                                            <input data-format="yyyy-MM-dd" type="text"  class="form-control datepicker" name="reg_date_to" id="reg_date_to"  placeholder="Registration Date to" autocomplete="off">
                                        </div>
                                    </div>
                                </div>
                            </div>
                               
                       
                        </div>
                        <br>
                        <input type="hidden" name="datepicker_one" id="datepicker_one" onkeydown="get_searched_patient()">
                        <input type="hidden" name="datepicker_two" id="datepicker_two" onkeydown="get_searched_patient()">

                        <div id="all-appointments" style="display: none;">
                            <div class="row">
                                <div class="col-md-5">
                                     <div class="form-group">
                                        <!-- <label class="col-md-2 control-label"> Date: </label>                                     -->
                                        <div class="col-md-6">
                                            <div class="input-group">
                                               <!--  <span class="input-group-addon">
                                                    <i class="fa fa-calendar"></i>
                                                </span> -->
                                                <input data-format="yyyy-MM-dd" type="text"  class="form-control datepicker" name="visit_date_from" id="date_from" placeholder="Date From" autocomplete="off" >
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="input-group">
                                                <!-- <span class="input-group-addon">
                                                    <i class="fa fa-calendar"></i>
                                                </span> -->
                                                <input data-format="yyyy-MM-dd" type="text"  class="form-control datepicker" name="visit_date_to" id="date_to"  placeholder="Date To" autocomplete="off">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                               
                                <div class="col-md-4">
                                    <select class="form-control" name="doctor_id" id="doctor_id" onchange="get_searched_patient()">
                                        <option value="">---- please select a dentist ---- </option>
                                       <?php
                                            $doctors = $this->reception_model->get_all_doctors();
                                            if($doctors->num_rows() > 0)
                                            {
                                                foreach ($doctors->result() as $key => $value) {
                                                    # code...
                                                    $personnel_id = $value->personnel_id;
                                                    $personnel_fname = $value->personnel_fname;
                                                    $personnel_onames = $value->personnel_onames;

                                                    echo '<option value="'.$personnel_id.'">'.$personnel_onames.' '.$personnel_fname.'</option>';
                                                }
                                            }
                                        ?>
                                    </select>
                                </div>
                                
                                <div class="col-md-3">
                                    <input type="text" name="search_patient_two" id="search_patient_two" class="form-control" onkeyup="get_searched_patient()" placeholder="Search patient i.e name, phone, patient number" autocomplete="off">
                                </div>
                            </div>
                           
                            <br>  
                        </div>

                        <div id="all-recall" style="display: none;">

                              <div class="row">
                                <div class="col-md-3">
                                     <div class="form-group">
                                        <!-- <label class="col-md-2 control-label"> Date: </label>                                     -->
                                        <div class="col-md-6">
                                            <div class="input-group">
                                               <!--  <span class="input-group-addon">
                                                    <i class="fa fa-calendar"></i>
                                                </span> -->
                                                <input data-format="yyyy-MM-dd" type="text"  class="form-control datepicker" name="visit_date_from" id="recall_date_from" placeholder="Date From" autocomplete="off" >
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="input-group">
                                                <!-- <span class="input-group-addon">
                                                    <i class="fa fa-calendar"></i>
                                                </span> -->
                                                <input data-format="yyyy-MM-dd" type="text"  class="form-control datepicker" name="visit_date_to" id="recall_date_to"  placeholder="Date To" autocomplete="off">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                               
                                <div class="col-md-3">
                                    <select class="form-control" name="schedule_list_id" id="schedule_list_id" onchange="get_searched_patient()">
                                        <option value="">---- please select a recall list ---- </option>
                                       <?php
                                            $schedule_rs = $this->reception_model->get_schedule_list();
                                            if($schedule_rs->num_rows() > 0)
                                            {
                                                foreach ($schedule_rs->result() as $key => $value) {
                                                    # code...
                                                    $list_id = $value->list_id;
                                                    $list_name = $value->list_name;

                                                    echo '<option value="'.$list_id.'">'.$list_name.'</option>';
                                                }
                                            }
                                        ?>
                                    </select>
                                </div>
                                <div class="col-md-3">
                                    <select class="form-control" name="recall_doctor_id" id="recall_doctor_id" onchange="get_searched_patient()">
                                        <option value="">---- please select a dentist ---- </option>
                                       <?php
                                            $doctors = $this->reception_model->get_all_doctors();
                                            if($doctors->num_rows() > 0)
                                            {
                                                foreach ($doctors->result() as $key => $value) {
                                                    # code...
                                                    $personnel_id = $value->personnel_id;
                                                    $personnel_fname = $value->personnel_fname;
                                                    $personnel_onames = $value->personnel_onames;

                                                    echo '<option value="'.$personnel_id.'">'.$personnel_onames.' '.$personnel_fname.'</option>';
                                                }
                                            }
                                        ?>
                                    </select>
                                </div>
                                
                                <div class="col-md-3">
                                    <input type="text" name="search_patient_two" id="search_patient_three" class="form-control" onkeyup="get_searched_patient()" placeholder="Search patient i.e name, phone, patient number" autocomplete="off">
                                </div>
                            </div>
                           
                            <br>  
                        </div>
                        <div class="col-md-12" style="margin-bottom: 10px;">
                            <div class="col-md-6">
                                <div style="height: 200px;overflow-y: scroll;">
                                    <h4 class="center-align">Category List</h4>
                                    <ul  id="searched-lab-test" style="list-style: none;"></ul>
                                </div>
                                  
                            </div>
                            <div class="col-md-6">
                                <div style="height: 200px;overflow-y: scroll;">
                                    <h4 class="center-align">Listed Items</h4>
                                     <input type="text" name="search_listed_patient" id="search_listed_patient" class="form-control" onkeyup="search_items_listed_patient()" placeholder="Search list i.e name, phone, patient number" autocomplete="off">
                                  <ul  id="listed-items" style="list-style: none;"></ul>
                                </div>
                            </div>
                          
                            <br>

                            
                        </div>


                        <div class="row">
                            <div class="col-md-3">

                                <div class="form-group">
                                    <!-- <label class="col-md-2 control-label"> Date: </label>                                     -->
                                    <div class="col-md-9">
                                        <div class="input-group">
                                            <input type="text" class="form-control" name="template_search" id="template_search" placeholder="Search Templates" autocomplete="off" onkeyup="get_searched_templates()" >
                                        </div>
                                        <br>
                                         
                                    </div>
                                    <div class="col-md-3">
                                        <div class="input-group">
                                            <button class="btn btn-sm btn-success"  data-toggle="modal" data-target="#add_template_note" ><i class="fa fa-plus"></i> Add Template </button>
                                        </div>
                                    </div>
                                </div>
                                <ul  id="searched-templates" style="list-style: none;"></ul>
                                
                            </div>
                            <div class="col-md-9">

                                 <div class="form-group" style="margin-bottom: 10px;">
                                    <label class="col-lg-2 control-label">Channel </label>
                                    <div class="col-lg-3">
                                        <div class="radio">
                                            <label>
                                                <input id="optionsRadios2" type="radio" name="channel_id" id="channel_id" value="0"  >
                                                SMS AND EMAIL
                                            </label>
                                        </div>
                                    </div>
                                    
                                    <div class="col-lg-3">
                                        <div class="radio">
                                            <label>
                                                <input id="optionsRadios2" type="radio" name="channel_id" id="channel_id" value="1" >
                                                SMS ONLY
                                            </label>
                                        </div>
                                    </div>
                                    <div class="col-lg-3">
                                        <div class="radio">
                                            <label>
                                                <input id="optionsRadios2" type="radio" name="channel_id" id="channel_id" value="3" >
                                                EMAIL ONLY
                                            </label>
                                        </div>
                                    </div>
                                </div>
                                <div  id="textarea-list" style="display: none;">
                                      <input type="hidden" name="subject" id="subject" value="" >
                                      <textarea class="form-control cleditor" name="about" id="message_item_description" placeholder="Message"></textarea>
                                </div>
                              
                            </div>
                        </div>
                       
                         <div class="col-md-12" style="padding:10px;">
                            <div class="input-group center-align">
                             <button type="submit" class="btn btn-sm btn-success" > Send Message </button>
                            </div>
                        </div>

                     
                        
                    </div>

            		
            	</div>
             <?php echo form_close();?>
        </div>
    </div>
</section>

<div class="row" style="margin-top: 5px;">
        <ul>
            <li style="margin-bottom: 5px;">
                <div class="row">
                    <div class="col-md-12 center-align">
                            <!-- <div id="old-patient-button" style="display:none">
                                                        
                                
                            </div> -->
                            <!-- <div> -->
                                <a  class="btn btn-sm btn-info" onclick="close_side_bar()"><i class="fa fa-folder-closed"></i> CLOSE SIDEBAR</a>
                            <!-- </div> -->
                                
                           
                    </div>
                </div>
                
            </li>
        </ul>
    </div>