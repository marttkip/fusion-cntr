<div class="row statistics">
   
    
    <div class="col-md-12 col-sm-12">
         <section class="panel panel-featured panel-featured-info">
            <header class="panel-heading">
                <h2 class="panel-title">Transaction breakdown <?php echo $title;?></h2>
            </header>             
        
              <!-- Widget content -->
              <div class="panel-body">
                
                <div class="row">
                    <!-- End Transaction Breakdown -->
                    
                    <div class="col-md-3">
                        <h5>Cash Breakdown</h5>
                        <table class="table table-striped table-hover table-condensed">
                            <tbody>
                                <?php
                                $total_cash_breakdown = 0;
                              
                                if($payment_methods->num_rows() > 0)
                                {
                                    foreach($payment_methods->result() as $res)
                                    {
                                        $method_name = $res->payment_method;
                                        $payment_method_id = $res->payment_method_id;
                                        $total = 0;
                                        
                                        if($normal_payments->num_rows() > 0)
                                        {
                                            foreach($normal_payments->result() as $res2)
                                            {
                                                $payment_method_id2 = $res2->payment_method_id;
                                               
                                            
                                                if($payment_method_id == $payment_method_id2)
                                                {
                                                   
                                                    $total += $res2->cr_amount;
                                                }
                                            }
                                        }
                                        
                                        $total_cash_breakdown += $total;
                                    
                                        echo 
                                        '
                                        <tr>
                                            <th>'.$method_name.'</th>
                                            <td>'.number_format($total, 2).'</td>
                                        </tr>
                                        ';
                                    }
                                    
                                    echo 
                                    '
                                    <tr>
                                        <th>Total</th>
                                        <td>'.number_format($total_cash_breakdown, 2).'</td>
                                    </tr>
                                    ';
                                }
                                ?>
                            </tbody>
                        </table>
                    </div>
                    <?php
                    $period_collection =  $this->reports_model->get_debt_payment_totals(0); 
                    $debt_collection = $this->reports_model->get_debt_payment_totals(1); 

                    ?>
                    <div class="col-md-3 center-align">
                        <h2>Period Collection</h2>   
                        <h3>Ksh <?php echo number_format($period_collection, 2);?></h3>
                    </div>
                    <div class="col-md-3 center-align">
                        <h2>Debt Payments</h2>   
                        <h3>Ksh <?php echo number_format($debt_collection, 2);?></h3>
                    </div>
                    <div class="col-md-3 center-align">
                        <h2>Total Collection</h2>   
                        <h3>Ksh <?php echo number_format($total_cash_breakdown, 2);?></h3>
                    </div>
                </div>
            </div>
        </section>
    </div>
</div>