<div>
<section class="panel">
    <header class="panel-heading">
            <h5 class="pull-left"><i class="icon-reorder"></i><?php echo $title?></h5>
          <div class="widget-icons pull-right">
              
          </div>
          <div class="clearfix"></div>
    </header>
    <div class="panel-body">
        <div class="padd">
        	<!-- <div class="col-md-6" id="top-div" style="display: block">
        		<select name="visit_id" id="visit_id" class="form-control" onchange="get_visit_payments(<?php echo $patient_id;?>)">
        			<option value="">------ select a visit date ------- </option>
        			<?php
        			if($open_visits->num_rows() > 0)
        			{
        				foreach ($open_visits->result() as $key => $value) {
        					# code...
        					$visit_id = $value->visit_id;
        					$visit_date = $value->visit_date;


        					echo "<option value='".$visit_id."'>".date('jS M Y',strtotime($visit_date))."</option>";
        					
        				}
        			}
        			?>
        		</select>
        		
        		
        	</div> -->
            <div class="col-md-12">                    
                <div class="col-md-6">
                    <div class="form-group">
                        <label class="col-lg-4 control-label">Invoice: </label>
                      
                        <div class="col-lg-8">
                            <!-- <input type="text" class="form-control" name="amount_paid" placeholder="" autocomplete="off"> -->
                            <select class="form-control" name="invoice_id" id="invoice_id">
                                <option value="0">--- select an invoice - ---</option>
                                <?php
                                 $invoices_rs = $this->accounts_model->get_patient_invoices($patient_id);


                                    if($invoices_rs->num_rows() > 0)
                                    {
                                      foreach ($invoices_rs->result() as $key => $value) {
                                        // code...
                                        $visit_invoice_id = $value->visit_invoice_id;
                                        $visit_invoice_number = $value->visit_invoice_number;
                                        // $visit_invoice_type = $value->visit_invoice_type;
                                        $balance = $value->balance;
                                        $dr_amount = $value->dr_amount;
                                        $cr_amount = $value->cr_amount;
                                        $invoice_date = $value->invoice_date;



                                        // if($visit_invoice_type == "Supplies Invoice")
                                        // {
                                        //   $invoice_type = 1;
                                        // }
                                        // else if($visit_invoice_type == "Opening Balance")
                                        // {
                                        //   $invoice_type = 2;
                                        // }
                                        // else
                                        // {
                                          $invoice_type = 1;
                                        // }

                                        if($cr_amount > 0)
                                        {
                                          $color_checked = 'orange';
                                        }
                                        else if($cr_amount == 0)
                                        {
                                          $color_checked = 'red';
                                        }
                                        else
                                        {
                                          $color_checked = 'white';
                                        }

                                        // var_dump($visit_invoice_id);die();
                                        if($balance > 0)
                                        {
                                           echo '<option value="'.$visit_invoice_id.'.'.$visit_invoice_number.'.'.$invoice_type.'" style="background:'.$color_checked.';color:white;"> '.$invoice_date.' # '.$visit_invoice_number.' Bill .'.number_format($dr_amount,2).' Payments.('.number_format($cr_amount,2).') Bal.'.number_format($balance,2).'</option>';
                                        }
                                       
                                      }
                                    }
                      
                                ?>
                                 <option value="0.0.3">--- on account - ---</option>
                            </select>
                        </div>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                        <label class="col-lg-4 control-label">Amount: </label>
                      
                        <div class="col-lg-8">
                            <input type="text" class="form-control" name="amount_paid" id="amount_paid" placeholder="" autocomplete="off">
                        </div>
                    </div>
                </div>
                <div class="col-md-2">
                    <div class="form-group">
                        <div class="center-align">
                            <button class="btn btn-info btn-sm" type="submit" onclick="add_payment_item(<?php echo $patient_id;?>)">Add Payment Item</button>
                        </div>
                    </div>
                </div>

            </div>
            <hr>
        	<div class="col-md-12" style="margin-top:10px;">
        		<div id="visit-payments-div" ></div>
        	</div>
        </div>
    </div>
</section>
<div class="col-md-12">					        	
	<div class="center-align">
		<a  class="btn btn-sm btn-warning" onclick="close_side_bar()"><i class="fa fa-folder-closed"></i> CLOSE SIDEBAR </a>
	</div>  
</div>
</div>