<?php   if ( ! defined('BASEPATH')) exit('No direct script access allowed');
// require_once "./application/modules/auth/controllers/auth.php";
class Cloud  extends MX_Controller
{	
	function __construct()
	{
		parent:: __construct();
		$this->load->model('cloud_model');
		$this->load->model('reception/reception_model');
	}
	
	public function save_cloud_data()
	{
		$json = file_get_contents('php://input');
	   	$response = $this->cloud_model->save_visit_data($json);

		echo json_encode($response);
	}
	public function sync_up_petty_cash()
	{
		$json = file_get_contents('php://input');
	   	$response = $this->cloud_model->save_petty_cash_data($json);

		echo json_encode($response);

	}
	
	public function cron_sync_up()
	{
		//get all unsynced visits
		$unsynced_visits = $this->cloud_model->get_unsynced_visits();
		
		$patients = array();
		
		if($unsynced_visits->num_rows() > 0)
		{
			//delete all unsynced visits
			$this->db->where('sync_status', 0);
			if($this->db->delete('sync'))
			{
				foreach($unsynced_visits->result() as $res)
				{
					$sync_data = $res->sync_data;
					$sync_table_name = $res->sync_table_name;
					$branch_code = $res->branch_code;
					array_push($patients[$sync_table_name], $value);
					
					/*$response = $this->sync_model->syn_up_on_closing_visit($visit_id);
		
					if($response)
					{
					}*/
				}
				$patients['branch_code'] = $branch_code;
			}
		}
		
		//sync data
		$response = $this->cloud_model->send_unsynced_visits($patients);
		var_dump($response);
		if($response)
		{
		}
		
		else
		{
		}
	}

	public function book_online_appointment()
	{
		$json = file_get_contents('php://input');
		$decoded = json_decode($json);

		$branch = $decoded->branch;
		$name = $decoded->name;
		$email = $decoded->email;
		$phone = $decoded->phone;
		$time = $decoded->time;
		$date = $decoded->date;
		$remarks = $decoded->remarks;

		// $name = 'Martin Tarus';
		// $email = 'marttkip@gmail.com';
		// $phone = '0704808007';
		// $time = '12:00:00';
		// $date = '2020-07-27';
		// $visit_status = 1;
		// $branch = 'RS#2';
		// $remarks = 'Removal of teeth';
		
		$visit_status = $decoded->visit_status;
		$appointment_type = 1;
		$category_id = 3;


		// $branch_cc = explode('#', $branch);

		$branch_code = 2;//$branch_cc[0];
		$branch_id = 'FC';


			
		$patient_surname = $name;
		$patient_phone1 = $phone;
		// $new_patient_number = $this->reception_model->create_new_patient_number();
		$new_patient_array['patient_surname'] = $name;
		$new_patient_array['patient_phone1'] = $phone;
		$new_patient_array['patient_number'] = '';
		$new_patient_array['category_id'] = 1;
		$new_patient_array['sync_status'] = 0;
		$new_patient_array['branch_id'] = $branch_id;
		$new_patient_array['branch_code'] = $branch_code;
		$new_patient_array['patient_year'] = date('Y');
		$new_patient_array['patient_date'] = date('Y-m-d H:i:s');
		

		$this->db->insert('patients',$new_patient_array);

		// push to cloud
		$patients_id = $this->db->insert_id();
		// $this->reception_model->send_patients_to_cloud($patients_id);


		$event_name = $patient_surname.' - '.$phone;
			
				
			
			
		$visit_date = date('Y-m-d',strtotime($date));



			// create visit 

		$time_start = strtotime($time);
		$branch_code = $branch;
		$branch_id = $branch_id;

		$minutes_to_add = 15;
		// $time = strtotime($time);
		$endTime = date("H:i", strtotime('+'.$minutes_to_add.' minutes', $time_start));


		$visit_data  = array(
						'visit_date' => $visit_date, 
						'appointment_id' => 1, 
						'time_start' => $time,
						'time_end' => $endTime, 
						'procedure_done' => $remarks,
						'personnel_id' => 0, 
						'patient_id' => $patients_id,
						'visit_type' => 1, 
						'close_card' => 2,
						'sync_status' => 0,
						'branch_code' => $branch_code,
						'branch_id' => $branch_id,
						'visit_account' => $visit_status
					);
	
		$this->db->insert('visit', $visit_data);
		$visit_id = $this->db->insert_id();


		
		// create appointment. 

		// $start_date = $this->input->post('start');
		$resource_id = $branch_code;
		

		$minutes_to_add = 15;
		// $time = strtotime($time);
		$endTime = date("H:i", strtotime('+'.$minutes_to_add.' minutes', $time_start));
		



		$appointment_array  = array(
										'appointment_date' => $visit_date, 
										'appointment_status' => 0, 
										'sync_status' => 0, 
										'appointment_start_time' => $time,
										'appointment_end_time' => $endTime, 
										'created_by' => 0,
										'created' => date('Y-m-d'),
										'resource_id' => $resource_id,
										'visit_id' => $visit_id
									);
		$appointment_array['appointment_date_time_end'] = $visit_date.'T'.$endTime;
		$appointment_array['appointment_date_time_start'] = $visit_date.'T'.$time;
		$appointment_status = 1;
		$appointment_array['event_description'] = $remarks;
		$appointment_array['event_name'] = $event_name;			
		$appointment_array['appointment_status'] = $appointment_status;
		$appointment_array['sync_status'] = 0;
		$appointment_array['appointment_type'] = $appointment_type;
		$appointment_array['duration'] = $minutes_to_add;


		$this->db->insert('appointments', $appointment_array);
		$appointment_id = $this->db->insert_id();
		$this->reception_model->send_appointments_to_cloud($appointment_id);
		$response['message'] ='success';
		$response['appointment_id'] = $appointment_id;

		echo json_encode($response);




	}
}
?>