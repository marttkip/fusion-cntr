<?php


$six_month = date("Y-m-01");
$today_month = date("Y-m-d");
$i = 0;


$year = $this->session->userdata('year');
$month = $this->session->userdata('month');
if(!empty($year) AND !empty($month))
{
	$six_month = date(''.$year.'-'.$month.'-01');
	$today_month = date("Y-m-t", strtotime($six_month));
	// var_dump($today_month);die();
	$current_year = $year;
	$current_month =$month;

	$start_year = 2018;
}

else
{
	$six_month = date("Y-m-01");
	$today_month = date("Y-m-d");

	$current_year = date('Y');
	$current_month = date('m');
	$start_year = 2018;
}

$dates = date('M Y',strtotime($six_month));


$add_years = '<option value="">---Select year---</option>';
for ($i=$start_year; $i <= $current_year ; $i++) { 
	# code...
	// var_dump($)
	if($i == $current_year)
	{
		$add_years .= '<option value="'.$i.'" selected>'.$i.'</option>';	
	}
	else
	{
		$add_years .= '<option value="'.$i.'" >'.$i.'</option>';
	}
	
}


$where = 'month_id > 0';
$table = 'month';
$select = '*';
$query_month = $this->dashboard_model->get_content($table, $where,$select);

$options = '<option value="" >---- select a month ------</option>';
if($query_month->num_rows() > 0)
{
	foreach ($query_month->result() as $key => $value) {
		# code...
		$month_id = $value->month_id;
		$month_name = $value->month_name;
		if($month_id <10)
		{
			$month_id = '0'.$month_id;
		}
		// var_dump($current_month);
		if($month_id === $current_month)
		{
			// var_dump($month_id);die();
			$options .= '<option value="'.$month_id.'" selected >'.$month_name.'</option>';
		}
		else
		{
			$options .= '<option value="'.$month_id.'" >'.$month_name.'</option>';
		}
	}
}
?>

<div class="row">
	<div class="col-md-4">
		<h4 class="mt-none">Patient Attendance Report</h4>
		<p class="mb-lg pull-left" >This is the population of patients attendance for <?php echo $dates;?> .</p>
	</div>
	<div class="col-md-8">

		<?php
		echo form_open("admin/search_dashboard", array("class" => "form-horizontal"));
		?>
		<div class="col-md-4">
			<div class="form-group">
                <!-- <label class="col-md-4 control-label">Year: </label> -->
                
                <div class="col-md-12">
                    <select class="form-control" name="year">
                    	
                        <?php
                            echo $add_years;
                        ?>
                    </select>
                </div>
            </div>
        </div>
        <div class="col-md-4">            
            <div class="form-group">
                <div class="col-md-12">
                    <select class="form-control" name="month">
                        <?php
                            echo $options;
                        ?>
                    </select>
                </div>
            </div>
        </div>
        <div class="col-md-1">            
           	<button type="submit" class="btn btn-sm btn-success">Search</button>
        </div>
        <?php echo form_close();?>
			
	</div>
</div>
	<div class="row">
		<div class="col-md-4 pull-right">

			<a href="<?php echo site_url().'print-summary/1/'.$six_month.'/'.$today_month.''?>" target="_blank" class="btn btn-sm btn-warning"><i class="fa fa-print"></i> Print  Summary</a>
			<a target="_blank" href="<?php echo site_url().'export-bookings/1/'.$six_month.'/'.$today_month.''?>" class="btn btn-sm btn-success"><i class="fa fa-print"></i> Export Bookings</a>
			
		</div>

		
	</div>

<?php

$where = 'patients.patient_delete = 0 AND visit.visit_id = appointments.visit_id AND patients.patient_id = visit.patient_id AND visit.visit_delete = 0 AND DATE(patients.patient_date) BETWEEN "'.$six_month.'" AND "'.$today_month.'" ';
$table = 'patients,visit,appointments';
$new_bookings = $this->reception_model->count_items($table, $where);




$where = 'patients.patient_delete = 0 AND visit.visit_id = appointments.visit_id AND patients.patient_id = visit.patient_id AND visit.visit_delete = 0 AND (appointments.appointment_status = 4 OR appointments.appointment_status = 7 OR appointments.appointment_status = 1) AND visit.visit_date BETWEEN "'.$six_month.'" AND "'.$today_month.'" ';
$table = 'patients,visit,appointments';
$total_appointments = $this->reception_model->count_items($table, $where);



$where = 'patients.patient_delete = 0 AND visit.visit_id = appointments.visit_id AND patients.patient_id = visit.patient_id AND visit.visit_delete = 0 AND appointments.appointment_rescheduled = 0 AND (appointments.appointment_status = 4 OR appointments.appointment_status = 7) AND visit.visit_date BETWEEN "'.$six_month.'" AND "'.$today_month.'" ';
$table = 'patients,visit,appointments';
$honoured_appointments = $this->reception_model->count_items($table, $where);


$where = 'patients.patient_delete = 0 AND visit.visit_id = appointments.visit_id AND patients.patient_id = visit.patient_id AND visit.visit_delete = 0 AND appointments.appointment_rescheduled = 1 AND visit.visit_date BETWEEN "'.$six_month.'" AND "'.$today_month.'" ';
$table = 'patients,visit,appointments';
$rescheduled_appointments = $this->reception_model->count_items($table, $where);




$where = 'patients.patient_delete = 0 AND visit.visit_id = appointments.visit_id AND patients.patient_id = visit.patient_id AND visit.visit_delete = 0 AND appointments.appointment_rescheduled = 0 AND (appointments.appointment_status = 4 OR appointments.appointment_status = 7) AND visit.visit_date BETWEEN "'.$six_month.'" AND "'.$today_month.'" ';
$table = 'patients,visit,appointments';
$month_patients = $this->reception_model->count_items($table, $where);
$showed_charts = '['.$i.', '.$month_patients.'],';


 $reschedule_where = 'patients.patient_delete = 0 AND visit.visit_id = appointments.visit_id AND patients.patient_id = visit.patient_id AND visit.visit_delete = 0 AND appointments.appointment_rescheduled = 1  AND visit.visit_date BETWEEN "'.$six_month.'" AND "'.$today_month.'" ';
$reschedule_table = 'patients,visit,appointments';
$reschedule_month_patients = $this->reception_model->count_items($reschedule_table, $reschedule_where);
$rescheduled_charts = '['.$i.', '.$reschedule_month_patients.'],';


$no_show_where = 'patients.patient_delete = 0 AND visit.visit_id = appointments.visit_id AND patients.patient_id = visit.patient_id AND visit.visit_delete = 0 AND (appointments.appointment_status = 1) AND appointments.appointment_rescheduled = 0 AND visit.visit_date BETWEEN "'.$six_month.'" AND "'.$today_month.'" ';
$no_show_table = 'patients,visit,appointments';
$no_show_month_patients = $this->reception_model->count_items($no_show_table, $no_show_where);
$no_show_charts = '['.$i.', '.$no_show_month_patients.'],';

$total_appointments = $no_show_month_patients+$reschedule_month_patients+$month_patients;




if(empty($honoured_appointments))
{
	$percentage_honored =0;
}
else
{
	$percentage_honored = round(($honoured_appointments *100) /$total_appointments) ;
}


// echo $month_patients.' '.$honoured_appointments;

?>

<div class="row">
	<div class="col-sm-3">
		<section class="card card-featured-left card-featured-primary mb-3">
			<div class="card-body">
				<div class="widget-summary">
					<div class="widget-summary-col widget-summary-col-icon">
						<div class="summary-icon summary-icon-sm bg-primary">
							<i class="fa fa-user"></i>
						</div>
					</div>
					<div class="widget-summary-col">
						<div class="summary">
							<h4 class="title">New Patients</h4>
							<div class="info">
								<strong class="amount"><?php echo $new_bookings;?></strong>
								<!-- <span class="text-primary">(14 unread)</span> -->
							</div>
						</div>
						<div class="summary-footer">
							<a class="text-muted text-uppercase" target="_blank" href="<?php echo site_url().'export-patients/1/'.$six_month.'/'.$today_month.''?>">(export)</a>
						</div>
					</div>
				</div>
			</div>
		</section>
	</div>
	<div class="col-sm-3">
		<section class="card card-featured-left card-featured-secondary">
			<div class="card-body">
				<div class="widget-summary">
					<div class="widget-summary-col widget-summary-col-icon">
						<div class="summary-icon bg-secondary">
							<i class="fa fa-calendar"></i>
						</div>
					</div>
					<div class="widget-summary-col">
						<div class="summary">
							<h4 class="title">Total Bookings</h4>
							<div class="info">
								<strong class="amount"><?php echo $total_appointments?></strong>
							</div>
						</div>
						<div class="summary-footer">
							<a class="text-muted text-uppercase" target="_blank" href="<?php echo site_url().'export-bookings/1/'.$six_month.'/'.$today_month.''?>">(export)</a>
						</div>
					</div>
				</div>
			</div>
		</section>
	</div>
	<div class="col-sm-3">
		<section class="card card-featured-left card-featured-tertiary mb-3">
			<div class="card-body">
				<div class="widget-summary">
					<div class="widget-summary-col widget-summary-col-icon">
						<div class="summary-icon bg-tertiary">
							<i class="fa fa-calendar"></i>
						</div>
					</div>
					<div class="widget-summary-col">
						<div class="summary">
							<h4 class="title">Honoured</h4>
							<div class="info">
								<strong class="amount"><?php echo $honoured_appointments;?></strong>
							</div>
						</div>
						<div class="summary-footer">
							<a class="text-muted text-uppercase" target="_blank" href="<?php echo site_url().'export-bookings/2/'.$six_month.'/'.$today_month.''?>">(export)</a>
						</div>
					</div>
				</div>
			</div>
		</section>
	</div>
	<div class="col-sm-3">
		<section class="card card-featured-left card-featured-quaternary">
			<div class="card-body">
				<div class="widget-summary">
					<div class="widget-summary-col widget-summary-col-icon">
						<div class="summary-icon bg-success">
							<i class="fa fa-calendar"></i>
						</div>
					</div>
					<div class="widget-summary-col">
						<div class="summary">
							<h4 class="title">Rescheduled</h4>
							<div class="info">
								<strong class="amount"><?php echo $rescheduled_appointments?></strong>
							</div>
						</div>
						<div class="summary-footer">
							<a class="text-muted text-uppercase" target="_blank" href="<?php echo site_url().'export-bookings/3/'.$six_month.'/'.$today_month.''?>" >(export)</a>
						</div>
					</div>
				</div>
			</div>
		</section>
	</div>
</div>

<div class="row">
	
	<div class="col-md-6">
		<section class="panel">
			<header class="panel-heading">
				<!-- <div class="panel-actions">
					<a href="ui-elements-charts.html#" class="panel-action panel-action-toggle" data-panel-toggle></a>
					<a href="ui-elements-charts.html#" class="panel-action panel-action-dismiss" data-panel-dismiss></a>
				</div> -->

				<h2 class="panel-title">Appointments Report</h2>
				<p class="panel-subtitle">Representation of patients appointments for <?php echo $dates;?></p>
			</header>
			<div class="panel-body">
				
				<!-- Flot: Pie -->
				<div class="chart chart-md" id="flotPie"></div>
				<script type="text/javascript">

					var flotPieData = [{
						label: "Honoured",
						data: [
							<?php echo $showed_charts?>
						],
						color: '#228B22'
					}, {
						label: "Rescheduled",
						data: [
							<?php echo $rescheduled_charts?>
						],
						color: '#B22222'
					}, {
						label: "No Show",
						data: [
							<?php echo $no_show_charts?>
						],
						color: '#FF4500'
					}];

					// See: assets/javascripts/ui-elements/examples.charts.js for more settings.

				</script>

			</div>
		</section>
	</div>
	<div class="col-md-6">
		<section class="panel">
			<header class="panel-heading">
				<div class="panel-actions">
					<a href="ui-elements-charts.html#" class="panel-action panel-action-toggle" data-panel-toggle></a>
					<a href="ui-elements-charts.html#" class="panel-action panel-action-dismiss" data-panel-dismiss></a>
				</div>

				<h2 class="panel-title">Honoured Appointments</h2>
				<p class="panel-subtitle">Percentage of honoured bookings out of the total bookings for <?php echo $dates;?></p>
			</header>
			<div class="panel-body">
				<div class="row">
					
					<div class="col-md-12">
						<meter min="0" max="100" value="<?php echo $percentage_honored;?>" id="meterDark"></meter>
					</div>
				</div>
			</div>
		</section>
		
	</div>
</div>

<div class="row">
	<div class="col-md-6">
		<section class="panel">
			<header class="panel-heading">
				<!-- <div class="panel-actions">
					<a href="ui-elements-charts.html#" class="panel-action panel-action-toggle" data-panel-toggle></a>
					<a href="ui-elements-charts.html#" class="panel-action panel-action-dismiss" data-panel-dismiss></a>
				</div> -->

				<h2 class="panel-title">Doctors Appointments Record</h2>
				<p class="panel-subtitle">Display of patients appointment based bookings done for <?php echo $dates;?> .</p>
			</header>
			<div class="panel-body">
<?php
				$doctors = $this->reception_model->get_all_doctors();
				$doctor_view = '';

				$total_honoured = 0;
				$total_old_showed_charts = '';
				if($doctors->num_rows() > 0)
				{
					foreach ($doctors->result() as $key => $value) {
						# code...
						$personnel_fname = $value->personnel_fname;
						$personnel_onames = $value->personnel_onames;
						$personnel_id = $value->personnel_id;


						$six_month = date("Y-m-01");
					    $today_month = date("Y-m-d");

					    $where = 'patients.patient_delete = 0 AND visit.visit_id = appointments.visit_id AND patients.patient_id = visit.patient_id AND visit.visit_delete = 0 AND appointments.appointment_rescheduled = 0 AND (appointments.appointment_status = 4 OR appointments.appointment_status = 7) AND visit.visit_date BETWEEN "'.$six_month.'" AND "'.$today_month.'" AND visit.personnel_id = '.$personnel_id;
						$table = 'patients,visit,appointments';
						$month_patients = $this->reception_model->count_items($table, $where);

						 $reschedule_where = 'patients.patient_delete = 0 AND visit.visit_id = appointments.visit_id AND patients.patient_id = visit.patient_id AND visit.visit_delete = 0 AND appointments.appointment_rescheduled = 1  AND visit.visit_date BETWEEN "'.$six_month.'" AND "'.$today_month.'" AND visit.personnel_id = '.$personnel_id;
						$reschedule_table = 'patients,visit,appointments';
						$reschedule_month_patients = $this->reception_model->count_items($reschedule_table, $reschedule_where);


						$no_show_where = 'patients.patient_delete = 0 AND visit.visit_id = appointments.visit_id AND patients.patient_id = visit.patient_id AND visit.visit_delete = 0 AND appointments.appointment_status = 1 AND appointments.appointment_rescheduled = 0  AND visit.visit_date BETWEEN "'.$six_month.'" AND "'.$today_month.'" AND visit.personnel_id = '.$personnel_id;
						$no_show_table = 'patients,visit,appointments';
						$no_show_month_patients = $this->reception_model->count_items($no_show_table, $no_show_where);

						$total_honoured += $month_patients;


						$total_old_showed_charts .= '{
													y: "'.$personnel_onames.'",
													a: '.$month_patients.',
													b: '.$reschedule_month_patients.',
													c: '.$no_show_month_patients.'
												},';


					}
				}



				?>
				<!-- Morris: Bar -->
				<div class="chart chart-md" id="morrisStackedPatients"></div>
				<script type="text/javascript">

					var morrisStackedData = [<?php echo $total_old_showed_charts;?>];

					// See: assets/javascripts/ui-elements/examples.charts.js for more settings.

				</script>
				<div class="col-md-12 text-center">
					 <span style="padding-left:15px;background-color: #228B22;margin-right: 2px;"></span> Honoured
					 <span style="padding-left:15px;background-color: #B22222;margin-right: 2px;"></span> Rescheduled
					 <span style="padding-left:15px;background-color: #FF4500;margin-right: 2px;"></span> No Show 
				</div>

			</div>
		</section>
	
	

	</div>
	<div class="col-md-6">
		<section class="panel">
			<header class="panel-heading">
				<!-- <div class="panel-actions">
					<a href="ui-elements-charts.html#" class="panel-action panel-action-toggle" data-panel-toggle></a>
					<a href="ui-elements-charts.html#" class="panel-action panel-action-dismiss" data-panel-dismiss></a>
				</div> -->

				<h2 class="panel-title">Doctors General Workload Report</h2>
				<p class="panel-subtitle">Workload report for doctors based on honoured appointments for <?php echo $dates;?>.</p>
			</header>
			<div class="panel-body">
				<?php
				$doctors_percentage = '';
				if($doctors->num_rows() > 0)
				{
					foreach ($doctors->result() as $key => $value) {
						# code...
						$personnel_fname = $value->personnel_fname;
						$personnel_onames = $value->personnel_onames;
						$personnel_id = $value->personnel_id;


						$six_month = date("Y-m-01");
					    $today_month = date("Y-m-d");

					    $where = 'patients.patient_delete = 0 AND visit.visit_id = appointments.visit_id AND patients.patient_id = visit.patient_id AND visit.visit_delete = 0 AND (appointments.appointment_status = 4 OR appointments.appointment_status = 7)  AND appointments.appointment_rescheduled = 0 AND visit.visit_date BETWEEN "'.$six_month.'" AND "'.$today_month.'" AND visit.personnel_id = '.$personnel_id;
						$table = 'patients,visit,appointments';
						$doctor_patients = $this->reception_model->count_items($table, $where);

						if(empty($doctor_patients))
						{
							$percentage = 0;
						}
						else
						{
							$percentage = ($doctor_patients *100)/$total_honoured;
						}
						

						$percentage = number_format($percentage,0);

						$doctors_percentage .= '{
													label: "'.$personnel_onames.'",
													value: '.$percentage.'
												},';

					}
				}
				?>
				<!-- Morris: Donut -->
				<div class="chart chart-md" id="morrisDonut"></div>
				<script type="text/javascript">

					var morrisDonutData = [<?php echo $doctors_percentage;?>];

					// See: assets/javascripts/ui-elements/examples.charts.js for more settings.

				</script>
			</div>
		</section>
	</div>
</div>



