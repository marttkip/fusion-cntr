<?php echo $this->load->view('search/search_charting_notation', '', TRUE);?>

<div class="row" style="margin-top: 10px;">
	<!-- <div class="col-md-12"> -->
    	<div class="pull-left">
        	<?php
			$search = $this->session->userdata('charting_notation_search');
			
			if(!empty($search))
			{
				echo '<a href="'.site_url().'hospital_administration/charting/close_charting_conotations_search/'.$dental_procedure_id.'" class="btn btn-warning btn-sm"><i class="fa fa-times"></i> Close search</a>';
			}
			?>
        </div>
        
		<div class="pull-right">
        	<a href="<?php echo site_url()?>administration/add-charting-notation/<?php echo $dental_procedure_id;?>" class="btn btn-sm btn-success"><i class="fa fa-plus"></i> Add charting notation </a>
			<a href="<?php echo site_url()?>administration/charting" class="btn btn-sm btn-primary"><i class="fa fa-angle-left"></i> Back to charting list </a>
			
			
		</div>
	<!-- </div> -->
</div>

	
<div class="row">
    <!-- <div class="col-md-12"> -->

 <section class="panel">
    <header class="panel-heading">
        <h2 class="panel-title"><?php echo $title;?></h2>
    </header>             

          <!-- Widget content -->
                <div class="panel-body">
          
<?php
		$error = $this->session->userdata('error_message');
		$success = $this->session->userdata('success_message');
		
		if(!empty($success))
		{
			echo '
				<div class="alert alert-success">'.$success.'</div>
			';
			$this->session->unset_userdata('success_message');
		}
		
		if(!empty($error))
		{
			echo '
				<div class="alert alert-danger">'.$error.'</div>
			';
			$this->session->unset_userdata('error_message');
		}
		$result = '';
		
		//if users exist display them
		//echo $query->num_rows();
		if ($query->num_rows() > 0)
		{
			$count = $page;
			
			$result .= 
				'
					<table class="table table-hover table-bordered ">
					  <thead>
						<tr>
						  <th>#</th>
						  <th>T.P</th>
						  <th>C.P</th>
						  <th>E.X</th>
						  <th>Teeth No</th>
						  <th>Surface ID</th>
						  <th colspan="3">Actions</th>
						</tr>
					  </thead>
					  <tbody>
				';
			
			//get all administrators
			$administrators = $this->personnel_model->retrieve_personnel();
			if ($administrators->num_rows() > 0)
			{
				$admins = $administrators->result();
			}
			
			else
			{
				$admins = NULL;
			}
			
			foreach ($query->result() as $row)
			{
				$notation_id = $row->notation_id;
				$tooth_id = $row->tooth_id;
				// $service_charge_status = $row->service_charge_status;
				$diagnosis_id = $row->diagnosis_id;
				$image_connotation_red = $row->image_connotation_red;
				$image_connotation_blue = $row->image_connotation_blue;
				$image_connotation_green = $row->image_connotation_green;
				$surface_id = $row->surface_id;
				

				//create deactivated status display
				// if($service_charge_status == 0)
				// {
				// 	$status = '<span class="label label-important">Deactivated</span>';
				// 	$button = '<a class="btn btn-info" href="'.site_url().'administration/activate-service-charge/'.$service_id.'/'.$notation_id.'" onclick="return confirm(\'Do you want to activate '.$tooth_id.'?\');" title="Activate '.$tooth_id.'"><i class="fa fa-thumbs-up"></i> Activate</a>';
				// }
				// //create activated status display
				// else if($service_charge_status == 1)
				// {
				// 	$status = '<span class="label label-success">Active</span>';
				// 	$button = '<a class="btn btn-default" href="'.site_url().'administration/deactivate-service-charge/'.$service_id.'/'.$notation_id.'" onclick="return confirm(\'Do you want to deactivate '.$tooth_id.'?\');" title="Deactivate '.$tooth_id.'"><i class="fa fa-thumbs-down"></i> Deactivate</a>';
				// }
				
				//creators & editors
				// <td><a href="'.site_url().'administration/delete-charting-notation/'.$notation_id.'/'.$dental_procedure_id.'" class="btn btn-sm btn-danger"><i class="fa fa-trash"></i> Edit</a></td>
				
				$count++;
				$result .= 
					'
						<tr>
							<td>'.$count.'</td>
							<td><img src="'.base_url()."assets/dentine/".$image_connotation_red.'" style=" width: 15px !important;height: 35px !important;"></td>
							<td><img src="'.base_url()."assets/dentine/".$image_connotation_blue.'" style=" width: 15px !important;height: 35px !important;"></td>
							<td><img src="'.base_url()."assets/dentine/".$image_connotation_green.'" style=" width: 15px !important;height: 35px !important;"></td>
							<td>'.$tooth_id.'</td>
							<td>'.$surface_id.'</td>
							<td><a href="'.site_url().'administration/edit-charting-notation/'.$dental_procedure_id.'/'.$notation_id.'" class="btn btn-sm btn-info"><i class="fa fa-pencil"></i> Edit</a></td>
							
							
						</tr> 
					';
			}
			
			$result .= 
			'
						  </tbody>
						</table>
			';
		}
		
		else
		{
			$result .= "There are no service charges";
		}
?>
            <?php echo $result; ?>
          </div>
          
          <div class="widget-foot">
                                
				<?php if(isset($links)){echo $links;}?>
            
                <div class="clearfix"></div> 
            
            </div>
        
		</section>
        <!-- </div> -->
        </div>
        