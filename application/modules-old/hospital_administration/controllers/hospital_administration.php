<?php   if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Hospital_administration extends MX_Controller 
{
	var $csv_path;
	function __construct()
	{
		parent:: __construct();
		
		$this->load->model('auth/auth_model');
		$this->load->model('site/site_model');
		$this->load->model('admin/users_model');
		$this->load->model('admin/sections_model');
		$this->load->model('admin/admin_model');
		$this->load->model('reception/database');
		$this->load->model('hr/personnel_model');
		$this->load->model('administration/administration_model');
		$this->load->model('administration/reports_model');
		$this->load->model('companies_model');
		$this->load->model('visit_types_model');
		$this->load->model('departments_model');
		$this->load->model('wards_model');
		$this->load->model('rooms_model');
		$this->load->model('beds_model');
		$this->load->model('services_model');
		$this->load->model('hospital_administration_model');
		$this->load->model('insurance_scheme_model');
		$this->load->model('accounts/accounts_model');
		
		$this->csv_path = realpath(APPPATH . '../assets/csv');
		if(!$this->auth_model->check_login())
		{
			redirect('login');
		}
	}
	
	public function index()
	{
		$this->session->unset_userdata('all_transactions_search');
		
		$data['content'] = $this->load->view('administration/dashboard', '', TRUE);
		
		$data['title'] = 'Dashboard';
		$this->load->view('admin/templates/general_page', $data);
	}

	public function update_service_charges($service_id)
	{
		// get
		// get
		$this->db->where('service_id ='.$service_id.' AND service_charge_status = 1 AND visit_type_id <> 1');
		$query = $this->db->get('service_charge');

		if($query->num_rows() > 0)
		{
			foreach ($query->result() as $key) {
				# code...
				$service_charge_id = $key->service_charge_id;
				$service_id = $key->service_id;
				$service_charge_name = $key->service_charge_name;
				$service_charge_amount = $key->service_charge_amount;

				$this->db->where('visit_type_id <> 1');
				$query_type = $this->db->get('visit_type');

				if($query_type->num_rows() > 0)
				{
					foreach ($query_type->result() as $key_query) {
						$visit_type_id = $key_query->visit_type_id;

						$this->db->where('service_charge_name = "'.$service_charge_name.'" AND service_id = '.$service_id.' AND service_charge_status = 1 AND visit_type_id = '.$visit_type_id);
						$query_update = $this->db->get('service_charge');

						if($query_update->num_rows() == 0)
						{

							$insert_query = array(
										'service_charge_amount'=>$service_charge_amount,
										'service_id'=>$service_id,
										'service_charge_name'=> $service_charge_name,
										'visit_type_id'=> $visit_type_id,
										'service_charge_status'=>1,
										'created'=>date('Y-m-d H:i:s'),
										'created_by'=>$this->session->userdata('personnel_id'),
										'modified_by'=>$this->session->userdata('personnel_id')
									);
							// var_dump($insert_query); die();
							$this->db->insert('service_charge',$insert_query);

						}


					}
				}
			}
		}
		$this->session->set_userdata("success_message", "Successfully updated charges ");
		redirect('administration/service-charges/'.$service_idd);
	}

	function import_invoices_template()
	{
		//export products template in excel 
		 $this->hospital_administration_model->import_invoice_template();
	}

	function import_invoices()
	{
		//open the add new product
		$v_data['title'] = 'Import invoices';
		$data['title'] = 'Import invoices';
		$data['content'] = $this->load->view('invoices', $v_data, true);
		$this->load->view('admin/templates/general_page', $data);
	}

	function do_invoice_import()
	{
		if(isset($_FILES['import_csv']))
		{
			if(is_uploaded_file($_FILES['import_csv']['tmp_name']))
			{
				//import products from excel 
				$response = $this->hospital_administration_model->import_csv_invoices($this->csv_path);
				
				if($response == FALSE)
				{
				}
				
				else
				{
					if($response['check'])
					{
						$v_data['import_response'] = $response['response'];
					}
					
					else
					{
						$v_data['import_response_error'] = $response['response'];
					}
				}
			}
			
			else
			{
				$v_data['import_response_error'] = 'Please select a file to import.';
			}
		}
		
		else
		{
			$v_data['import_response_error'] = 'Please select a file to import.';
		}
		
		//open the add new product
		$v_data['title'] = 'Import Charges';
		$data['title'] = 'Import Charges';
		$data['content'] = $this->load->view('invoices', $v_data, true);
		$this->load->view('admin/templates/general_page', $data);
	}
	function import_payments_template()
	{
		//export products template in excel 
		 $this->hospital_administration_model->import_payment_template();
	}
	function import_payments()
	{


		$where = 'account.account_id = batch_receipts.bank_id';
		$table = 'batch_receipts,account';
		//pagination
		$segment = 3;
		$this->load->library('pagination');
		$config['base_url'] = site_url().'accounts/batch-payments';
		$config['total_rows'] = $this->users_model->count_items($table, $where);
		$config['uri_segment'] = $segment;
		$config['per_page'] = 20;
		$config['num_links'] = 5;
		
		$config['full_tag_open'] = '<ul class="pagination pull-right">';
		$config['full_tag_close'] = '</ul>';
		
		$config['first_tag_open'] = '<li>';
		$config['first_tag_close'] = '</li>';
		
		$config['last_tag_open'] = '<li>';
		$config['last_tag_close'] = '</li>';
		
		$config['next_tag_open'] = '<li>';
		$config['next_link'] = 'Next';
		$config['next_tag_close'] = '</span>';
		
		$config['prev_tag_open'] = '<li>';
		$config['prev_link'] = 'Prev';
		$config['prev_tag_close'] = '</li>';
		
		$config['cur_tag_open'] = '<li class="active"><a href="#">';
		$config['cur_tag_close'] = '</a></li>';
		
		$config['num_tag_open'] = '<li>';
		$config['num_tag_close'] = '</li>';
		$this->pagination->initialize($config);
		
		$page = ($this->uri->segment($segment)) ? $this->uri->segment($segment) : 0;
        $v_data["links"] = $this->pagination->create_links();
		$query = $this->hospital_administration_model->get_current_payments($table, $where, $config["per_page"], $page);
		
		//change of order method 
		
		
		$data['title'] = 'Payments';		
		$v_data['query'] = $query;
		$v_data['page'] = $page;
		// var_dump($query);die();
		//open the add new product
		$v_data['title'] = 'Import payments';
		$data['title'] = 'Import payments';
		$data['content'] = $this->load->view('payments', $v_data, true);
		$this->load->view('admin/templates/general_page', $data);
	}

	

	function do_payment_import()
	{
		if(isset($_FILES['import_csv']))
		{
			if(is_uploaded_file($_FILES['import_csv']['tmp_name']))
			{
				//import products from excel 
				$response = $this->hospital_administration_model->import_csv_payments($this->csv_path);
				
				if($response == FALSE)
				{
				}
				
				else
				{
					if($response['check'])
					{
						$v_data['import_response'] = $response['response'];
					}
					
					else
					{
						$v_data['import_response_error'] = $response['response'];
					}
				}
			}
			
			else
			{
				$v_data['import_response_error'] = 'Please select a file to import.';
			}
		}
		
		else
		{
			$v_data['import_response_error'] = 'Please select a file to import.';
		}
		redirect('accounts/batch-payments');
		// //open the add new product
		// $v_data['title'] = 'Import Charges';
		// $data['title'] = 'Import Charges';
		// $data['content'] = $this->load->view('payments', $v_data, true);
		// $this->load->view('admin/templates/general_page', $data);
	}


	function import_patients_template()
	{
		//export products template in excel 
		 $this->hospital_administration_model->import_patients_data_template();
	}

	function import_patients_update()
	{
		//open the add new product
		$v_data['title'] = 'Import payments';
		$data['title'] = 'Import payments';
		$data['content'] = $this->load->view('patients', $v_data, true);
		$this->load->view('admin/templates/general_page', $data);
	}

	function do_patients_update_import()
	{
		if(isset($_FILES['import_csv']))
		{
			if(is_uploaded_file($_FILES['import_csv']['tmp_name']))
			{
				//import products from excel 
				$response = $this->hospital_administration_model->import_csv_patients_update($this->csv_path);
				
				if($response == FALSE)
				{
				}
				
				else
				{
					if($response['check'])
					{
						$v_data['import_response'] = $response['response'];
					}
					
					else
					{
						$v_data['import_response_error'] = $response['response'];
					}
				}
			}
			
			else
			{
				$v_data['import_response_error'] = 'Please select a file to import.';
			}
		}
		
		else
		{
			$v_data['import_response_error'] = 'Please select a file to import.';
		}
		
		//open the add new product
		$v_data['title'] = 'Import Patients';
		$data['title'] = 'Import Patients';
		$data['content'] = $this->load->view('patients', $v_data, true);
		$this->load->view('admin/templates/general_page', $data);
	}


	public function update_patient_records()
	{
		$this->db->where('PatNum > 0 AND sync_status = 0');
		$query_patient = $this->db->get('patient');
		// var_dump($query_patient);die();
		if($query_patient->num_rows() > 0)
		{
			foreach ($query_patient->result() as $key => $value) {
				# code...
				$PatNum = $value->PatNum;
				$LName = $value->LName;
				$FName = $value->FName;
				$MiddleI = $value->MiddleI;
				$Gender = $value->Gender;
				$Birthdate = $value->Birthdate;
				$WirelessPhone = $value->WirelessPhone;
				$DateFirstVisit = $value->DateFirstVisit;
				$BillingType = $value->BillingType;

				if($Birthdate == "0001-01-01")
				{
					$birth_day = NULL;
				}
				else
				{
					$birth_day = date('Y-m-d',strtotime($value->Birthdate));
				}


				if($DateFirstVisit == "0001-01-01")
				{
					$patient_date = NULL;
				}
				else
				{
					$patient_date = date('Y-m-d',strtotime($value->DateFirstVisit));
				}


				if($DateFirstVisit == "0001-01-01")
				{
					$last_visit = NULL;
				}
				else
				{
					$last_visit = date('Y-m-d',strtotime($value->DateFirstVisit));
				}


				$Email = $value->Email;


				

				$name = $FName.' '.$MiddleI.' '.$LName;

				$name = str_replace('  ', ' ', $name);

				$name = str_replace('0.00', '', $name);

				$WirelessPhone = str_replace(' ', '', $WirelessPhone);
				$WirelessPhone = str_replace('  ', '', $WirelessPhone);

				$reg_year = date('y',strtotime($value->DateFirstVisit));
				// check if patient number exist

				if($PatNum < 100)
				{
					if($PatNum < 10)
					{
						$number = '00'.$PatNum;
					}
					else
					{
						$number = '0'.$PatNum;
					}
				}
				else
				{
					$number = $PatNum;
				}
				$cardnumber = $number;
				$this->db->where('patient_number',$cardnumber);
				$query_patient = $this->db->get('patients');

				$array['prefix'] = $PatNum;
				$array['suffix'] = $reg_year;
				if($Gender == 1)
				{
					$array['gender_id'] = 2;
				}
				else
				{
					$array['gender_id'] = 1;
				}

				if($query_patient->num_rows() == 0)
				{

					
					// do an insert 
					$array['patient_id'] = $PatNum;
					$array['patient_number'] = $cardnumber;
					$array['patient_phone1'] = $WirelessPhone;
					$array['patient_email'] = $Email;
					$array['patient_surname'] = $name;
					$array['patient_town'] = $value->Country;
					$array['patient_date_of_birth'] = $birth_day;
					$array['patient_date'] = $patient_date;
					$array['insurance_company_id'] = $BillingType;
					$array['last_visit'] = $last_visit;
					// $array['insurance_company_id'] = $insurance_company_id;
					// var_dump($array);die();

					$this->db->insert('patients',$array);

					
					$array_list['sync_status'] = 1;
					$this->db->where('PatNum',$PatNum);
					$this->db->update('patient',$array_list);

				}
				else
				{
					// $array_list['is_new'] = 2;
					// $this->db->where('patient_number',$cardnumber);
					// $this->db->update('patients',$array_list);
				}


			}
			echo "completed";
		}
	}


	public function update_invoices_payments()
	{

		// select * bills  with this card number

		$this->db->where('procedurelog.sync_status = 0 AND service_charge.codenum = procedurelog.CodeNum AND procedurelog.CodeNum = procedurecode.CodeNum');
		 // $this->db->limit(50);
		 $this->db->order_by('procedurelog.ProcNum','ASC');
		$query_items = $this->db->get('procedurelog,service_charge,procedurecode');
		// var_dump($query_items);die();
		if($query_items->num_rows() > 0)
		{
			foreach ($query_items->result() as $key => $value) 
			{
				# code...
				$patient_id = $value->PatNum;
				$ProcNum = $value->ProcNum;
				$visit_date = $value->ProcDate;
				$visit_charge_amount = $value->ProcFee;
				$surface = $value->Surf;
				$ToothNum = $value->ToothNum;
				$ProcStatus = $value->ProcStatus;
				$Dx = $value->Dx;
				$UnitQty = $value->UnitQty;
				$service_charge_id = $value->service_charge_id;
				// $Note = $value->Note;
				// $UserNum = $value->UserNum;
				$ProcCat = $value->ProcCat;

				$cavity_status = NULL;
				$tooth_id = $ToothNum;
				$surface_id = $surface;
				$service_charge_id = $service_charge_id;
				$plan_status = $ProcStatus;


				$visit_id = NULL;

				// select * bills

				$this->db->where('visit.visit_date = "'.$visit_date.'"  AND visit.patient_id = '.$patient_id);
				$this->db->limit(1);
				$query_item = $this->db->get('visit');

				if($query_item->num_rows() > 0)
				{

					foreach ($query_item->result() as $keyviews => $value_charged) {
						# code...
						$visit_id = $value_charged->visit_id;
					}


					if(!empty($tooth_id) OR $tooth_id != "")
					{
						$this->db->where('category_id = '.$ProcCat);
						$query_cat = $this->db->get('dental_procedure');

						if($query_cat->num_rows() > 0)
						{
							foreach ($query_cat->result() as $key => $items) {
								# code...
								$cavity_status = $items->dental_procedure_id;
							}
						}
						else
						{
							$cavity_status =  20;
						}



						if(empty($surface_id) OR $surface_id == 'null')
						{
							// $teeth_section = NULL;
							$data['teeth_section'] = NULL;
						}
						else
						{
							$data['teeth_section'] = $surface_id;
						}

						$data['cavity_status'] = $cavity_status;
						$data['teeth_id'] = $tooth_id;
						$data['patient_id'] = $patient_id;
						$data['plan_status'] = $plan_status;
					

						$data['created'] = $visit_date;
						$data['created_by'] = $this->session->userdata('personnel_id');

						$this->db->where('teeth_id = "'.$tooth_id.'" AND patient_id = '.$patient_id.'');
						$query = $this->db->get('dentine');

						if($query->num_rows() > 0)
						{
							foreach ($query->result() as $key => $value) {
								# code...
								$dentine_id = $value->dentine_id;
							}

							$this->db->where('dentine_id',$dentine_id);
							$this->db->update('dentine',$data);	

						}
						else
						{
							$this->db->insert('dentine',$data);
							$dentine_id = $this->db->insert_id();
							
						}
					}
					else
					{
						$dentine_id = NULL;
					}
					

					

					$provider_id = $this->session->userdata("personnel_id");
					// $visit_date = date('Y-m-d');
					// $amount = $this->accounts_model->get_service_charge_detail($service_charge_id);

					$this->db->where('ProcNum ='.$ProcNum);
					$query_notes = $this->db->get('procnote');

					if($query_notes->num_rows() > 0)
					{
						foreach ($query_notes->result() as $key => $notes_value) {
							# code...
							$visit_charge_notes = $notes_value->Note;
							$UserNum = $notes_value->UserNum;
						}
					}
					else
					{
						$visit_charge_notes = NULL;
						$UserNum = 0;
					}

					if($visit_id > 0)
					{
						$visit_data = array('visit_charge_units'=>$UnitQty,
										'visit_id'=>$visit_id,
										'visit_charge_amount'=>$visit_charge_amount,
										'service_charge_id'=>$service_charge_id, 
										'created_by'=>$UserNum,
										'provider_id'=>$UserNum,
										'date'=>$visit_date,
										'time'=>date('H:i:s'),
										'personnel_id'=>$UserNum,
										'charged'=>1,
										'charge_to'=>1,
										'dentine_id'=>$dentine_id,
										'surface_id'=>$surface_id,
										'teeth_id'=>$tooth_id,
										'plan_status'=>$plan_status,
										'visit_charge_notes'=>$visit_charge_notes,
										'ProcNum'=>$ProcNum
										);

						if($this->db->insert('visit_charge', $visit_data))
						{
							
						}
					
						$this->db->where('ProcNum = '.$ProcNum);
						$array_update['sync_status'] = 1;
						$this->db->update('procedurelog',$array_update);
					}

								
							
				}
				else
				{
					$UserNum = 0;
					// var_dump($ToothNum);die();

					$visit_data = array(
										"branch_code" => $this->session->userdata('branch_code'),
										"branch_id" =>2,
										"visit_date" => $visit_date,
										"patient_id" => $patient_id,
										"personnel_id" => $UserNum,
										"insurance_limit" => '',
										"patient_insurance_number" => '',
										"visit_type" => 1,
										"time_start"=>date('H:i:s'),
										"time_end"=>date('H:i:s'),
										"appointment_id"=>0,
										"close_card"=>0,
										"procedure_done"=>'',
										"insurance_description"=>'',
										"invoice_number"=>NULL
									);
					// var_dump($visit_data);die();
					if($this->db->insert('visit', $visit_data))
					{
						// insert into the visit charge table

						$visit_id = $this->db->insert_id();


						
						
						// var_dump($tooth_id);die();

						if(!empty($tooth_id) OR $tooth_id != "")
						{
							$this->db->where('category_id = '.$ProcCat);
							$query_cat = $this->db->get('dental_procedure');

							if($query_cat->num_rows() > 0)
							{
								foreach ($query_cat->result() as $key => $items) {
									# code...
									$cavity_status = $items->dental_procedure_id;
								}
							}
							else
							{
								$cavity_status =  20;
							}



							if(empty($surface_id) OR $surface_id == 'null')
							{
								// $teeth_section = NULL;
								$data['teeth_section'] = NULL;
							}
							else
							{
								$data['teeth_section'] = $surface_id;
							}

							$data['cavity_status'] = $cavity_status;
							$data['teeth_id'] = $tooth_id;
							$data['patient_id'] = $patient_id;
							$data['plan_status'] = $plan_status;
						

							$data['created'] = $visit_date;
							$data['created_by'] = $this->session->userdata('personnel_id');

							$this->db->where('teeth_id = "'.$tooth_id.'" AND patient_id = '.$patient_id.'');
							$query = $this->db->get('dentine');

							if($query->num_rows() > 0)
							{
								foreach ($query->result() as $key => $value) {
									# code...
									$dentine_id = $value->dentine_id;
								}

								$this->db->where('dentine_id',$dentine_id);
								$this->db->update('dentine',$data);	

							}
							else
							{
								$this->db->insert('dentine',$data);
								$dentine_id = $this->db->insert_id();
								
							}
						}
						else
						{
							$dentine_id = NULL;
						}
						
						// var_dump($dentine_id);die();

						

						$provider_id = $this->session->userdata("personnel_id");
						

						$this->db->where('ProcNum ='.$ProcNum);
						$query_notes = $this->db->get('procnote');

						if($query_notes->num_rows() > 0)
						{
							foreach ($query_notes->result() as $key => $notes_value) {
								# code...
								$visit_charge_notes = $notes_value->Note;
								$UserNum = $notes_value->UserNum;
							}
						}
						else
						{
							$visit_charge_notes = NULL;
							$UserNum = 0;
						}


						if($visit_id > 0)
						{
							$visit_data = array('visit_charge_units'=>$UnitQty,
											'visit_id'=>$visit_id,
											'visit_charge_amount'=>$visit_charge_amount,
											'service_charge_id'=>$service_charge_id, 
											'created_by'=>$this->session->userdata("personnel_id"),
											'provider_id'=>$this->session->userdata("personnel_id"),
											'date'=>$visit_date,
											'time'=>date('H:i:s'),
											'personnel_id'=>$this->session->userdata("personnel_id"),
											'charged'=>1,
											'charge_to'=>1,
											'dentine_id'=>$dentine_id,
											'surface_id'=>$surface_id,
											'teeth_id'=>$tooth_id,
											'plan_status'=>$plan_status,
											'visit_charge_notes'=>$visit_charge_notes,
											'ProcNum'=>$ProcNum
											);
							// var_dump($visit_data);die();


							if($this->db->insert('visit_charge', $visit_data))
							{
								
							}

							$this->db->where('ProcNum = '.$ProcNum);
							$array_update['sync_status'] = 1;
							$this->db->update('procedurelog',$array_update);
						}
						
						
					}


					


				}

						
					
			}
				
			
		}

		echo "completed";
		
	}

	public function update_patients_payments()
	{		

		// select * bills
		$this->db->where('sync_status = 0');
		$this->db->select('*');
		$query_item = $this->db->get('payment_open_dental');
		// var_dump($query_item);die();
		if($query_item->num_rows() > 0)
		{
			foreach ($query_item->result() as $key => $value) {
				# code...

				$PayNum = $value->PayNum;
				$PatNum = $value->PatNum;
				$visit_date = $value->PayDate;
				$receipt_number = $value->Receipt;
				$payment_date = date('Y-m-d',strtotime($value->DateEntry));
				$payed_by = 0;
				$pay_type = $value->PayType;
				$amount_paid = $value->PayAmt;

				
				$payment_method = 1;
				

				$type_payment = 1;
				$transaction_code = $receipt_number;
				// $transaction_code = $confirm_;
				$payment_service_id = $PayNum;
				$change = 0;

				// check if the receipt is already in
				$this->db->where('visit_date = '.$visit_date.' AND patient_id = "'.$PatNum.'" ');
				$query_amount = $this->db->get('visit');
				// if($query_amount->num_rows() > 0)
				// {
				// 	foreach ($query_amount->result() as $key => $value) {
				// 		# code...
				// 		$visit_id = $value->visit_id;
				// 	}


					$data = array(
						'visit_id' => NULL,
						'payment_method_id'=>$payment_method,
						'amount_paid'=>$amount_paid,
						'personnel_id'=>$this->session->userdata("personnel_id"),
						'payment_type'=>$type_payment,
						'transaction_code'=>$transaction_code,
						'payment_service_id'=>$payment_service_id,
						'change'=>$change,
						'payment_created'=>$payment_date,
						'payed_by'=>$payed_by,
						'patient_id'=>$PatNum,
						'payment_created_by'=>$this->session->userdata("personnel_id"),
						'approved_by'=>$this->session->userdata("personnel_id"),
						'date_approved'=>$payment_date
					);

					// var_dump($data);die();

					$this->db->insert('payments', $data);

					$array_gift['sync_status'] = 1;
					$this->db->where('PayNum = '.$PayNum.'');
					$this->db->update('payment_open_dental',$array_gift);
				// }

			}
		}
		echo "success";
	}



	public function delete_department_account($department_account_id,$department_id)
	{
		$array['deleted'] = 1;
		$array['deleted_by'] = $this->session->userdata('personnel_id');

		$this->db->where('department_account_id',$department_account_id);

		if ($this->db->update('department_account',$array))
		{
			$this->session->set_userdata("success_message", "You have successfully deleted the account from department");
		}
		else
		{
			$this->session->set_userdata("error_message", validation_errors());
		}

		redirect('administration/department-accounts/'.$department_id);
	}

	public function update_payments($batch_receipt_id)
	{
		$this->db->where('current_payment_status = 0 AND batch_receipt_id ='.$batch_receipt_id);
		$query = $this->db->get('batch_payments');
		
		if($query->num_rows() > 0)
		{
			foreach ($query->result() as $key => $value) {

				$invoice_number = $value->invoice_number;
				$amount_paid = $value->amount;
				$payment_date = $value->payment_date;
				$receipt_number = $value->receipt_number;
				$payment_method_id = $value->payment_method_id;
				$paid_by = $value->paid_by;
				$batch_payment_id = $value->batch_payment_id;
				$bank_id = $value->bank_id;

				$type_payment = 1;
				$transaction_code = $receipt_number;
				$payment_service_id = 16;
				$change = 0;

				$this->db->where('visit_invoice_number = "'.$invoice_number.'" AND visit.visit_id = visit_invoice.visit_id');
				$query_amount = $this->db->get('visit_invoice,visit');

				if($query_amount->num_rows() == 1)
				{
					foreach ($query_amount->result() as $key => $value) {
						# code...
						$visit_id = $value->visit_id;
						$visit_invoice_id = $value->visit_invoice_id;
						$patient_id = $value->patient_id;
						$branch_id = $value->branch_id;
						$branch_code = $value->branch_code;
					}

					
					$prefix = $suffix = $this->accounts_model->create_receipt_number();
				

					$branch_code = str_replace("RS", "CT", $branch_code);

					if($branch_code == "CT")
					{
						$branch_code = "CTR";
					}

					
					if($prefix < 10)
					{
						$number = '00000'.$prefix;
					}
					else if($prefix < 100 AND $prefix >= 10)
					{
						$number = '0000'.$prefix;
					}
					else if($prefix < 1000 AND $prefix >= 100)
					{
						$number = '000'.$prefix;
					}
					else if($prefix < 10000 AND $prefix >= 1000)
					{
						$number = '00'.$prefix;
					}
					else if($prefix < 100000 AND $prefix >= 10000)
					{
						$number = '0'.$prefix;
					}

					$data = array(
									'patient_id' => $patient_id,
									'visit_id' => $visit_id,
									'payment_method_id'=>$payment_method_id,
									'amount_paid'=>$amount_paid,
									'personnel_id'=>$this->session->userdata("personnel_id"),
									'payment_type'=>$type_payment,
									'transaction_code'=>$transaction_code,
									'reason'=>'Service Payment',
									'payment_service_id'=>$payment_service_id,
									'change'=>0,
									'payment_date'=>$payment_date,
									'payment_created_by'=>$this->session->userdata("personnel_id"),
									'approved_by'=>$this->session->userdata("personnel_id"),'date_approved'=>$payment_date
								);


					$invoice_number = $branch_code.$number;
					$data['confirm_number'] = $invoice_number;
					$data['suffix'] = $suffix;
					$data['branch_id'] = $branch_id;
					$data['bank_id'] = $bank_id;
					$data['payment_created'] = date("Y-m-d");


					// var_dump($visit_invoice_id); die();
					if($this->db->insert('payments', $data))
					{
						$payment_id = $this->db->insert_id();


						$service = array(
							              'visit_invoice_id'=>$visit_invoice_id,
							              'invoice_type'=>1,
							              'patient_id' => $patient_id,
							              'created_by' => $this->session->userdata('personnel_id'),
							              'created' => date('Y-m-d'),
							              'payment_item_amount'=>$amount_paid,
							              // 'payment_id'=>NULL
							            );
						$service['payment_id'] = $payment_id;
					    

					    $this->db->insert('payment_item',$service);


						$update_array['payment_id'] = $payment_id;
						$update_array['current_payment_status'] = 2;
						$this->db->where('batch_payment_id',$batch_payment_id);
						$this->db->update('batch_payments',$update_array);

					}
				}
				
			}
		}

		redirect('accounts/batch-payments');
	}

	public function view_batch_items($batch_receipt_id)
	{

		$where = 'account.account_id = batch_receipts.bank_id AND batch_receipts.batch_receipt_id = batch_payments.batch_receipt_id AND batch_payments.batch_receipt_id ='.$batch_receipt_id;
		$table = 'batch_receipts,account,batch_payments';
		//pagination
		$segment = 4;
		$this->load->library('pagination');
		$config['base_url'] = site_url().'hospital_administration/view_batch_items/'.$batch_receipt_id;
		$config['total_rows'] = $this->users_model->count_items($table, $where);
		$config['uri_segment'] = $segment;
		$config['per_page'] = 20;
		$config['num_links'] = 5;
		
		$config['full_tag_open'] = '<ul class="pagination pull-right">';
		$config['full_tag_close'] = '</ul>';
		
		$config['first_tag_open'] = '<li>';
		$config['first_tag_close'] = '</li>';
		
		$config['last_tag_open'] = '<li>';
		$config['last_tag_close'] = '</li>';
		
		$config['next_tag_open'] = '<li>';
		$config['next_link'] = 'Next';
		$config['next_tag_close'] = '</span>';
		
		$config['prev_tag_open'] = '<li>';
		$config['prev_link'] = 'Prev';
		$config['prev_tag_close'] = '</li>';
		
		$config['cur_tag_open'] = '<li class="active"><a href="#">';
		$config['cur_tag_close'] = '</a></li>';
		
		$config['num_tag_open'] = '<li>';
		$config['num_tag_close'] = '</li>';
		$this->pagination->initialize($config);
		
		$page = ($this->uri->segment($segment)) ? $this->uri->segment($segment) : 0;
        $v_data["links"] = $this->pagination->create_links();
		$query = $this->hospital_administration_model->get_batch_receipts_payments($table, $where, $config["per_page"], $page);
		
		//change of order method 
		
		
		$data['title'] = 'Payments';		
		$v_data['query'] = $query;
		$v_data['page'] = $page;
		// var_dump($query);die();
		//open the add new product
		$v_data['title'] = 'Batch Items';
		$data['title'] = 'Batch Items';
		$data['content'] = $this->load->view('view_batch_items', $v_data, true);
		$this->load->view('admin/templates/general_page', $data);

	}
}
?>