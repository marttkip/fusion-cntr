 <section class="panel panel-primary">
    <header class="panel-heading">
        <h2 class="panel-title">Search General queue Patients for <?php echo date('jS M Y',strtotime(date('Y-m-d'))); ?></h2>
    </header>
    <!-- Widget content -->
         <div class="panel-body">
    	<div class="padd">
			<?php
			
			
			echo form_open("reception/search_general_queue/".$page_name, array("class" => "form-horizontal"));
			
            
            ?>
            <div class="row">
                 <div class="col-md-3">
                    <div class="form-group">
                        <label class="col-lg-4 control-label">Branch: </label>
                        
                        <div class="col-lg-8">
                            <select class="form-control" name="branch_id">
                                <option value="">---Select branch---</option>
                                <?php
                                    if($branches->num_rows() > 0){
                                        foreach($branches->result() as $row):
                                            $branch_name = $row->branch_name;
                                            $branch_code = $row->branch_code;
                                            $branch_id = $row->branch_id;
                                            echo "<option value='".$branch_id."'>".$branch_name."</option>";
                                        endforeach;
                                    }
                                ?>
                            </select>
                        </div>
                    </div>
                    
                </div>
                
                <div class="col-md-3">
                   
                    
                    <div class="form-group">
                        <label class="col-md-4 control-label">Patient number: </label>
                        
                        <div class="col-md-8">
                            <input type="text" class="form-control" name="patient_number" placeholder="Patient number">
                        </div>
                    </div>
                    
                </div>
                
                <div class="col-md-3">
                    
                    <div class="form-group">
                        <label class="col-md-4 control-label">Patient Name: </label>
                        
                        <div class="col-md-8">
                            <input type="text" class="form-control" name="surname" placeholder="Name name">
                        </div>
                    </div>
                </div>
                <div class="col-md-2">
                    <div class="form-group">
                            <div class="center-align">
                                <button type="submit" class="btn btn-info">Search</button>
                            </div>
                    </div>
                </div>
            </div>
            <?php
            echo form_close();
            ?>
    	</div>
</section>