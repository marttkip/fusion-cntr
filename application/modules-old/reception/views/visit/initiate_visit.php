<?php

 $insurance_company = $this->reception_model->get_patient_insurance_company($patient_id);

?>
		<style type="text/css">
			#insured_company{display:none;}
		</style>
		<section class="panel">
            <header class="panel-heading">
                <h2 class="panel-title"><strong>First name:</strong> <?php echo $patient_surname.' '.$patient_othernames;?> 
                 <strong>Balance:</strong>   Kes <?php echo number_format($account_balance, 2);?>

                </h2>
                <div class="pull-right" style="margin-top:-25px;">
                    <a href="<?php echo site_url();?>patients" class="btn btn-primary btn-sm ">  <i class="fa fa-angle-left"></i> Patients list</a>
                     <a href="<?php echo site_url();?>administration/individual_statement/<?php echo $patient_id;?>/2" class="btn btn-sm btn-success" target="_blank">Statement</a>
                </div>
            </header>
		        <!-- Widget content -->
		        <div class="panel-body">                	
                	
					<?php 
                        $validation_error = validation_errors();
                        
                        if(!empty($validation_error))
                        {
                            echo '<div class="alert alert-danger center-align">'.$validation_error.'</div>';
                        }
						
						$error = $this->session->userdata('error_message');
						$success = $this->session->userdata('success_message');
						
						if(!empty($error))
						{
							echo '<div class="alert alert-danger">'.$error.'</div>';
							$this->session->unset_userdata('error_message');
						}
						
						if(!empty($success))
						{
							echo '<div class="alert alert-success">'.$success.'</div>';
							$this->session->unset_userdata('success_message');
						}
                      
                    ?>
                    
					<?php $this->load->view('visit/initiate_outpatient');?>
                    
                </div>
        	</section>

