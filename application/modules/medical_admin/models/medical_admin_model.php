<?php

class Medical_admin_model extends CI_Model 
{
	function objective_findings()
	{
		$table = "objective_findings";
		$where = "objective_findings_status = 1";
		$items = "*";
		$order = "objective_findings.objective_findings_name";

		$result = $this->database->select_entries_where($table, $where, $items, $order);
		return $result;		
	}

	function subjective_ros_findings()
	{
		$table = "subjective_ros_findings";
		$where = "subjective_ros_findings_status = 1";
		$items = "*";
		$order = "subjective_ros_findings.subjective_ros_findings_name";

		$result = $this->database->select_entries_where($table, $where, $items, $order);
		return $result;		
	}

	function objective_ros_findings()
	{
		$table = "objective_ros_findings";
		$where = "objective_ros_findings_status = 1";
		$items = "*";
		$order = "objective_ros_findings.objective_ros_findings_name";

		$result = $this->database->select_entries_where($table, $where, $items, $order);
		return $result;		
	}

	public function get_objective_findings_classes()
	{
		$table = "objective_findings_class";
		$order = "objective_findings_class_name";
		
		$this->db->order_by($order, 'ASC');
		$result = $this->db->get($table);
		return $result;
	}


	public function get_subjective_ros_classes($visit_id,$is_report=NULL)
	{

		if($is_report == 1)
		{
			$where = 'subjective_ros_class_id IN (SELECT class_id FROM visit_subjective_ros_findings WHERE visit_id = '.$visit_id.')';
		}
		else
		{
			$where = 'subjective_ros_class_id > 0';
		}
		$table = "subjective_ros_class";
		$order = "subjective_ros_class_id";
		$this->db->where($where);
		$this->db->order_by($order, 'ASC');
		$result = $this->db->get($table);
		return $result;
	}

	public function get_objective_ros_classes($visit_id,$is_report=NULL)
	{
		if($is_report == 1)
		{
			$where = 'objective_ros_class_id IN (SELECT class_id FROM visit_objective_ros_findings WHERE visit_id = '.$visit_id.')';
		}
		else
		{
			$where = 'objective_ros_class_id > 0';
		}


		$table = "objective_ros_class";
		$order = "objective_ros_class_id";
		$this->db->where($where);
		$this->db->order_by($order, 'ASC');
		$result = $this->db->get($table);
		return $result;
	}

	function visit_objective($visit_id){

		$table = "visit";
		$where = "visit_id = ". $visit_id;
		$items = "visit_objective_findings";
		$order = "visit_id";

		$result = $this->database->select_entries_where($table, $where, $items, $order);
		
		return $result;
								
	}
}
?>