<?php

$rs2 = $this->nurse_model->get_visit_symptoms($visit_id);
$num_rows2 = count($rs2);

$v_data['signature_location'] = base_url().'assets/signatures/';
$v_data['query'] =  $query_data = $this->nurse_model->get_notes(13, $visit_id);

if($query_data->num_rows() > 0)
{
	foreach ($query_data->result() as $key => $value_two) {
		# code...
		$summary = $value_two->notes_name;
	}
	
}
else
{
	$summary = '';
}
if(!isset($mobile_personnel_id))
{
	$mobile_personnel_id = NULL;
}
// var_dump($summary); die();
$v_data['mobile_personnel_id'] = $mobile_personnel_id;
$v_data['module'] = $module;

$notes = $this->load->view('nurse/patients/notes', $v_data, TRUE);
if(empty($module))
{


?>
<div class='col-md-6'>
	<div class="row">
    	<div class='col-md-12'>
        	<input type="hidden" name="date" value="<?php echo date('Y-m-d');?>" />
        	<input type="hidden" name="time" value="<?php echo date('H:i');?>" />
            <textarea class='form-control' id='action_plan<?php echo $visit_id;?>' placeholder="define your action plan" ><?php echo $summary?></textarea>
        </div>
    </div>
    <br>
    <div class="row">
    	<div class='col-md-12 center-align'>
    		<a class='btn btn-info btn-sm' type='submit' onclick='save_action_plan(<?php echo $visit_id;?>,13)'> Save Action Plan</a>
    	</div>
    </div>
</div>

<?php

}
else
{
    ?>
    <div class='col-md-6'>

        <?php echo $notes?>
    </div>
    <?php

}

?>
