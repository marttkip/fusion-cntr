<?php
	
$dental = 0;

 $data['visit_id'] = $visit_id;
 $data['lab_test'] = 100;
if(empty($module))
{

 ?>


<div class="row">
	<div class="col-md-12">
	  <!-- Widget -->
	  <section class="panel panel-featured panel-featured-info">
	 
	        <div class="panel-body">
	          <div class="padd">
	             <!-- vitals from java script -->

	             <?php

					$rs2 = $this->nurse_model->get_visit_symptoms($visit_id);
					$num_rows2 = count($rs2);

					$v_data['signature_location'] = base_url().'assets/signatures/';
					$v_data['query'] =  $query_data = $this->nurse_model->get_referral_notes(12, $visit_id);

					if($query_data->num_rows() > 0)
					{
						foreach ($query_data->result() as $key => $value_two) {
							# code...
							$referral_reason = $value_two->referral_reason;
							$doctor = $value_two->doctor;
							$evaluate = $value_two->evaluate;
							$share = $value_two->share;
							$specialist_id = $value_two->specialist_id;
							$demographics = $value_two->demographics;

							if($evaluate == 1)
							{
								$evaluate_check = 'checked';
							}
							else
							{
								$evaluate_check = '';
							}

							if($share == 1)
							{
								$share_check = 'checked';
							}
							else
							{
								$share_check = '';
							}
						}
						
					}
					else
					{
						$summary = '';
						$referral_reason = '';//$value_two->referral_reason;
						$doctor =  '' ;//$value_two->doctor;
						$evaluate = '';//$value_two->evaluate;
						$share = '';//$value_two->share;
						$specialist_id = '';//$value_two->specialist_id;
						$demographics = '';//$value_two->demographics;
						$evaluate_check = '';
						$share_check = '';
					}
					if(!isset($mobile_personnel_id))
					{
						$mobile_personnel_id = NULL;
					}
					// var_dump($summary); die();
					$v_data['mobile_personnel_id'] = $mobile_personnel_id;


					?>
					<div class='col-md-6'>

						<div class="row">
	                    	<div class='col-md-12'>
	                        	<select class="form-control" name="specialist_id" id="specialist_id<?php echo $visit_id;?>">
	                        		<option value="">--- select a specialist ----- </option>
	                        		<option value="1"> Cardiologist </option>
	                        	</select>
	                        </div>
	                    </div>
	                    <br>

						<div class="row">
	                    	<div class='col-md-12'>
	                            <textarea class='form-control' id='referal_notes<?php echo $visit_id;?>' placeholder="Reason for referal" ><?php echo $referral_reason;?></textarea>
	                        </div>
	                    </div>
	                    <br>
	                    <div class="row">
	                    	<div class='col-md-12'>
	                        	<input type="checkbox" name="evaluate" id="evaluate<?php echo $visit_id;?>" <?php echo $evaluate_check;?>> Evaluate and Treat
	                        </div>
	                        <div class='col-md-12'>
	                        	<input type="checkbox" name="share" id="share<?php echo $visit_id;?>" <?php echo $share_check;?>> Share your findings
	                        </div>
	                    </div>
	                    
	                </div>
	                <div class='col-md-6'>
	                    <div class="row" style="margin-bottom: 5px;">
	                    	<div class='col-md-12'>
	                        	<input type="text" name="doctor" id="doctor<?php echo $visit_id;?>" class="form-control" value="<?php echo $doctor;?>" placeholder="Specialist" > 
	                        </div>
	                        <br>
	                    </div>
	                    <div class="row">
	                        <div class='col-md-12'>
	                        	<textarea class='form-control' id='demographics<?php echo $visit_id;?>' placeholder="Demographics and contacts" ><?php echo $demographics;?></textarea>
	                        </div>
	                    </div>
	                    <br>
	                    <div class="row">
	                    	<div class='col-md-12 center-align'>
	                    		<a class='btn btn-info btn-sm' type='submit' onclick='save_referal_notes(<?php echo $visit_id;?>)'> Save Referal Note</a>
	                    		<a class='btn btn-warning btn-sm' target="_blank" href="<?php echo site_url()?>print-referral-note/<?php echo $visit_id;?>"> Print Refaral Note</a>
	                    	</div>
	                    </div>

	                	
	                </div>
	                



	          </div>
	        </div>
	    </section>
  	</div>
</div>
<?php
}
else
{
	?>
	<div class="row">
	<div class="col-md-12">
	  <!-- Widget -->
	  <section class="panel panel-featured panel-featured-info">
	 
	        <div class="panel-body">
	          <div class="padd">
	             <!-- vitals from java script -->

	             <?php

					$rs2 = $this->nurse_model->get_visit_symptoms($visit_id);
					$num_rows2 = count($rs2);

					$v_data['signature_location'] = base_url().'assets/signatures/';
					$v_data['query'] =  $query_data = $this->nurse_model->get_referral_notes(12, $visit_id);

					if($query_data->num_rows() > 0)
					{
						foreach ($query_data->result() as $key => $value_two) {
							# code...
							$referral_reason = $value_two->referral_reason;
							$doctor = $value_two->doctor;
							$evaluate = $value_two->evaluate;
							$share = $value_two->share;
							$specialist_id = $value_two->specialist_id;
							$demographics = $value_two->demographics;

							if($evaluate == 1)
							{
								$evaluate_check = 'checked';
							}
							else
							{
								$evaluate_check = '';
							}

							if($share == 1)
							{
								$share_check = 'checked';
							}
							else
							{
								$share_check = '';
							}
						}
						
					}
					else
					{
						$summary = '';
						$referral_reason = '';//$value_two->referral_reason;
						$doctor =  '' ;//$value_two->doctor;
						$evaluate = '';//$value_two->evaluate;
						$share = '';//$value_two->share;
						$specialist_id = '';//$value_two->specialist_id;
						$demographics = '';//$value_two->demographics;
						$evaluate_check = '';
						$share_check = '';
					}
					if(!isset($mobile_personnel_id))
					{
						$mobile_personnel_id = NULL;
					}
					// var_dump($summary); die();
					$v_data['mobile_personnel_id'] = $mobile_personnel_id;


					?>
					<div class='col-md-6'>

						<div class="row">
	                    	<div class='col-md-12'>
	                        	<select class="form-control" name="specialist_id" id="specialist_id<?php echo $visit_id;?>">
	                        		<option value="1"> Cardiologist </option>
	                        	</select>
	                        </div>
	                    </div>
	                    <br>

						<div class="row">
	                    	<div class='col-md-12'>
	                            <textarea class='form-control' id='referal_notes<?php echo $visit_id;?>' placeholder="Reason for referal" ><?php echo $referral_reason;?></textarea>
	                        </div>
	                    </div>
	                    <br>
	                    <div class="row">
	                    	<div class='col-md-12'>
	                        	<input type="checkbox" name="evaluate" id="evaluate<?php echo $visit_id;?>" <?php echo $evaluate_check;?>> Evaluate and Treat
	                        </div>
	                        <div class='col-md-12'>
	                        	<input type="checkbox" name="share" id="share<?php echo $visit_id;?>" <?php echo $share_check;?>> Share your findings
	                        </div>
	                    </div>
	                    
	                </div>
	                <div class='col-md-6'>
	                    <div class="row" style="margin-bottom: 5px;">
	                    	<div class='col-md-12'>
	                        	<input type="text" name="doctor" id="doctor<?php echo $visit_id;?>" class="form-control" value="<?php echo $doctor;?>" placeholder="Specialist" > 
	                        </div>
	                        <br>
	                    </div>
	                    <div class="row">
	                        <div class='col-md-12'>
	                        	<textarea class='form-control' id='demographics<?php echo $visit_id;?>' placeholder="Demographics and contacts" ><?php echo $demographics;?></textarea>
	                        </div>
	                    </div>
	                   

	                	
	                </div>
	                



	          </div>
	        </div>
	    </section>
  	</div>
</div>

	<?php
}
?>