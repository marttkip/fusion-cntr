<?php

//patient details
$visit_type = $patient['visit_type'];
$patient_type = $patient['patient_type'];
$patient_othernames = $patient['patient_othernames'];
$patient_surname = $patient['patient_surname'];
$patient_surname = $patient['patient_surname'];
$patient_number = $patient['patient_number'];
$gender = $patient['gender'];

$today = date('jS F Y H:i a',strtotime(date("Y:m:d h:i:s")));
$visit_date = date('jS F Y',strtotime($this->accounts_model->get_visit_date($visit_id)));

//doctor
$doctor = $this->accounts_model->get_att_doctor($visit_id);

//served by
$served_by = $this->accounts_model->get_personnel($this->session->userdata('personnel_id'));



$title = 'Patient Visit Chart';
$heading = 'Checkup';
$number = 'MI/MED/00'.$visit_id;
?>

<!DOCTYPE html>
<html lang="en">
	<style type="text/css">
		.receipt_spacing{letter-spacing:0px; font-size: 12px;}
		.center-align{margin:0 auto; text-align:center;}
		
		.receipt_bottom_border{border-bottom: #888888 medium solid;}
		.row .col-md-12 table {
			border:solid #000 !important;
			border-width:1px 0 0 1px !important;
			font-size:10px;
			padding: 0px;
		}
		.row .col-md-12 th, .row .col-md-12 td {
			border:solid #000 !important;
			border-width:0 1px 1px 0 !important;
			padding-top: 0;
			padding-bottom: 0;
		}
		
		.row .col-md-12 .title-item{float:left;width: 130px; font-weight:bold; text-align:right; padding-right: 20px;}
		.title-img{float:left; padding-left:30px;}
	</style>
    <head>
        <title><?php echo $contacts['company_name'];?> | <?php echo $title;?></title>
        <!-- For mobile content -->
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <!-- IE Support -->
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <!-- Bootstrap -->
       <!--  <link rel="stylesheet" href="<?php echo base_url()."assets/themes/admin/"?>bower_components/bootstrap/dist/css/bootstrap.min.css"> -->
        <link rel="stylesheet" href="<?php echo base_url()."assets/themes/porto-admin/1.4.1/";?>assets/vendor/bootstrap/css/bootstrap.css" />
         <!-- <script src="<?php echo base_url()."assets/themes/admin/";?>bower_components/jquery/dist/jquery.min.js"></script> -->
         <script src="<?php echo base_url()."assets/themes/bluish/"?>js/jquery.js"></script> <!-- jQuery -->
    </head>
    <body class="receipt_spacing">
    	<div class="row" >
        	<div class="row">
	        	<div class="col-xs-12">
	            	<img src="<?php echo base_url().'assets/logo/'.$contacts['logo'];?>" alt="<?php echo $contacts['company_name'];?>" class="img-responsive logo center-align" style="height:100px"/>
	            </div>
	        </div>
        	<div class="row">
        	<div class="col-md-12 center-align receipt_bottom_border">
            	<strong>
                	<?php echo $contacts['company_name'];?><br/>
                    P.O. Box <?php echo $contacts['address'];?> <?php echo $contacts['post_code'];?>, <?php echo $contacts['city'];?><br/>
                    E-mail: <?php echo $contacts['email'];?>. Tel : <?php echo $contacts['phone'];?><br/>
                    <?php echo $contacts['location'];?>, <?php echo $contacts['building'];?>, <?php echo $contacts['floor'];?><br/>
                </strong>
            </div>
        </div>
        </div>
        
      <div class="row receipt_bottom_border" >
        	<div class="col-md-12 center-align">
            	<strong><?php echo $title;?></strong>
            </div>
        </div>
        
        <!-- Patient Details -->
    	<div class="row receipt_bottom_border" style="margin: 10px;">
        	<div class="col-md-6 pull-left">
            	<div class="row">
                	<div class="col-md-12">
                    	
                    	<div class="title-item">Patient Name:</div>
                        
                    	<?php echo $patient_surname.' '.$patient_othernames; ?>
                    </div>
                </div>
            	
            	<div class="row">
                	<div class="col-md-12">
                    	<div class="title-item">Patient Number:</div> 
                        
                    	<?php echo $patient_number; ?>
                    </div>
                </div>
            
            </div>
            
        	<div class="col-md-6 pull-right">
            	<div class="row">
                	<div class="col-md-12">
                    	<div class="title-item">Doc No.:</div>
                        
                    	<?php echo $number; ?>
                    </div>
                </div>
            	
            	<div class="row">
                	<div class="col-md-12">
                    	<div class="title-item">Att. Doctor: </div> 
                        
                    	<?php echo $doctor; ?>
                    </div>
                </div>
                <div class="row">
                	<div class="col-md-12">
                    	<div class="title-item">Visit Date: </div> 
                        
                    	<?php echo $visit_date; ?>
                    </div>
                </div>
            </div>
        </div>
        
    
	    <div style="padding-left:10px;padding-right:10px">
	        <div class="col-md-12">
	        	<h4>Nurse Intake Assesment</h4>
	        	
	        	<div class="row">
	                <div class="col-md-12">
	                    <!-- Widget -->
	                    <div class="widget">
	                        <!-- Widget head -->
	                        <div class="widget-head">
	                          <h5 class="pull-left" style="font-weight:bold">Vitals</h5>
	                          <div class="widget-icons pull-right">
	                          </div>
	                          <div class="clearfix"></div>
	                        </div>              
	                        
	                        <!-- Widget content -->
	                        <div class="widget-content">
	                            <div class="padd">	                                
	                                <div id="previous_vitals"></div>	                                
	                            </div>
	                        </div>
	                    </div>
	                </div>
	            </div>
	            <div class="row">
	                <div class="col-md-12">
	                    <!-- Widget -->
	                    <div class="widget">
	                        <!-- Widget head -->
	                        <div class="widget-head">
	                          <h5 class="pull-left" style="font-weight:bold">Patient History</h5>
	                          <div class="widget-icons pull-right">
	                          </div>
	                          <div class="clearfix"></div>
	                        </div>              
	                        
	                        <!-- Widget content -->
	                        <div class="widget-content">
	                            <div class="padd">
	                            	<div class="col-md-6 pull-left">
	                            		<h5>PT DX</h5>
	                            		<div id="pt-dx"></div> 
	                            	</div>
	                            	<div class="col-md-6 pull-right">
	                            		<h5>PT RX</h5>
	                            		<div id="pt-rx"></div> 
	                            	</div>	                                
	                                	                                
	                            </div>
	                        </div>
	                    </div>
	                </div>
	            </div>

	            <div class="row">
	                <div class="col-md-12">
	                    <!-- Widget -->
	                    <div class="widget">
	                        <!-- Widget head -->
	                        <div class="widget-head">
	                          <h5 class="pull-left" style="font-weight:bold">Allegies</h5>
	                          <div class="widget-icons pull-right">
	                          </div>
	                          <div class="clearfix"></div>
	                        </div>              
	                        
	                        <!-- Widget content -->
	                        <div class="widget-content">
	                            <div class="padd">
	                            	<div class="col-md-6 pull-left">
	                            		<h5>Medicine Allegies</h5>
	                            		<div id="medicine-allergy"></div>
	                            	</div>
	                            	<div class="col-md-6 pull-right">
	                            		<h5>Food Allergies</h5>
	                            		<div id="food-allergy"></div>
	                            	</div>	                                
	                            </div>
	                        </div>
	                    </div>
	                </div>
	            </div>

	            <div class="row">
	                <div class="col-md-12">
	                    <!-- Widget -->
	                    <div class="widget">
	                        <!-- Widget head -->
	                        <div class="widget-head">
	                          <h5 class="pull-left" style="font-weight:bold">Vaccines</h5>
	                          <div class="widget-icons pull-right">
	                          </div>
	                          <div class="clearfix"></div>
	                        </div>              
	                        
	                        <!-- Widget content -->
	                        <div class="widget-content">
	                            <div class="padd">
	                            	<div class="col-md-12">
	                            		<div id="patient_vaccine"></div>
	                            	</div>
	                            	                             
	                            </div>
	                        </div>
	                    </div>
	                </div>
	            </div>

	            <div class="row">
	                <div class="col-md-12">
	                    <!-- Widget -->
	                    <div class="widget">
	                        <!-- Widget head -->
	                        <div class="widget-head">
	                          <h5 class="pull-left" style="font-weight:bold">PT Family History</h5>
	                          <div class="widget-icons pull-right">
	                          </div>
	                          <div class="clearfix"></div>
	                        </div>              
	                        
	                        <!-- Widget content -->
	                        <div class="widget-content">
	                            <div class="padd">
	                            	<div class="col-md-12">
	                            		<div id="family-diseases"></div>
	                            	</div>
	                            	                             
	                            </div>
	                        </div>
	                    </div>
	                </div>
	            </div>

	            <div class="row">
	                <div class="col-md-6 pull-left">
	                    <!-- Widget -->
	                    <div class="widget">
	                        <!-- Widget head -->
	                        <div class="widget-head">
	                          <h5 class="pull-left" style="font-weight:bold">Chief Complain</h5>
	                          <div class="widget-icons pull-right">
	                          </div>
	                          <div class="clearfix"></div>
	                        </div>              
	                        
	                        <!-- Widget content -->
	                        <div class="widget-content">
	                            <div class="padd">
	                            	<div class="col-md-12">
	                            		<div id="chief-complain"></div>
	                            	</div>
	                            	                             
	                            </div>
	                        </div>
	                    </div>
	                </div>
	                <div class="col-md-6 pull-right">
	                    <!-- Widget -->
	                    <div class="widget">
	                        <!-- Widget head -->
	                        <div class="widget-head">
	                          <h5 class="pull-left" style="font-weight:bold">Hx of P.I</h5>
	                          <div class="widget-icons pull-right">
	                          </div>
	                          <div class="clearfix"></div>
	                        </div>              
	                        
	                        <!-- Widget content -->
	                        <div class="widget-content">
	                            <div class="padd">
	                            	<div class="col-md-12">
	                            		<div id="h-p-i"></div>
	                            	</div>
	                            	                             
	                            </div>
	                        </div>
	                    </div>
	                </div>
	            </div>
	            <div class="row">
	                <div class="col-md-12">
	                    <!-- Widget -->
	                    <div class="widget">
	                        <!-- Widget head -->
	                        <div class="widget-head">
	                          <h5 class="pull-left" style="font-weight:bold">Review of systems</h5>
	                          <div class="widget-icons pull-right">
	                          </div>
	                          <div class="clearfix"></div>
	                        </div>              
	                        
	                        <!-- Widget content -->
	                        <div class="widget-content">
	                            <div class="padd">
	                            	
	                            	<div class="col-md-6">
	                            		<h5>SYSTEM REVIEW</h5>
	                            		<div id="subjective-ros"></div>    
	                            	</div>
	                            	
	                            	<div class="col-md-6">
	                            		<h5>PHYSICAL EXAM</h5>
	                            		<div id="objective-ros"></div>    
	                            	</div>
	                            	                             
	                            </div>
	                        </div>
	                    </div>
	                </div>
	            </div>
	            <div class="row">
	                <div class="col-md-12">
	                    <!-- Widget -->
	                    <div class="widget">
	                        <!-- Widget head -->
	                        <div class="widget-head">
	                          <h5 class="pull-left" style="font-weight:bold">Assessment</h5>
	                          <div class="widget-icons pull-right">
	                          </div>
	                          <div class="clearfix"></div>
	                        </div>              
	                        
	                        <!-- Widget content -->
	                        <div class="widget-content">
	                            <div class="padd">
	                            	<div class="col-md-6">
	                            		<h5>Diagnosis</h5>
	                            		<div id="visit_diagnosis_original"></div>    
	                            	</div>
	                            	<div class="col-md-6">
	                            		<h5>Other Notes</h5>
	                            		<div id="assessment"></div>    
	                            	</div>
	                            	                             
	                            </div>
	                        </div>
	                    </div>
	                </div>
	            </div>

	            <div class="row">
	                <div class="col-md-12">
	                    <!-- Widget -->
	                    <div class="widget">
	                        <!-- Widget head -->
	                        <div class="widget-head">
	                          <h5 class="pull-left" style="font-weight:bold">Plan</h5>
	                          <div class="widget-icons pull-right">
	                          </div>
	                          <div class="clearfix"></div>
	                        </div>              
	                        
	                        <!-- Widget content -->
	                        <div class="widget-content">
	                            <div class="padd">
	                            	
	                            	<div class="col-md-12">
	                            		<h5>Orders</h5>
	                            		<div id="orders"></div>    
	                            	</div>
	                            	
	                            	<div class="col-md-12">
	                            		<h5>Referral</h5>
	                            		<div id="other-plans"></div>      
	                            	</div>
	                            	                             
	                            </div>
	                        </div>
	                    </div>
	                </div>
	            </div>

	        </div>	
		</div>
	</div>
        
        
    	
       
 <div class="row" style="font-style:italic; font-size:11px;">
        	
		<div class="col-md-8 pull-left">
            <div class="col-md-4 pull-left">
            	Served by: <?php echo $served_by;?> 
            </div>
            <div class="col-md-4 pull-left">
              Signature by: ..................
            </div>
            <div class="col-md-4 pull-left">
              Patient Signature : ..............
            </div>
          </div>
        	<div class="col-md-4 pull-right">
            	<?php echo $today; ?>
            </div>
        </div>
    </body>
    
</html>

  <script type="text/javascript">
  	
	var config_url = '<?php echo site_url();?>';

	// $(document).ready(function(){
	//   	alert(config_url);
	  	// vitals_interface(<?php echo $visit_id;?>);
	// });

	$(function() {
	   vitals_interface(<?php echo $visit_id;?>);
	});


 function vitals_interface(visit_id){

    previous_vitals(visit_id);
	// display_procedure(visit_id);
	// get_medication(visit_id);
	// get_surgeries(visit_id);
	get_vaccines(visit_id);
	// display_vaccines(visit_id);
	// display_visit_vaccines(visit_id);
	// display_visit_consumables(visit_id);
	get_family_diseases(visit_id);
	get_food_types_view(visit_id);
	get_patient_medicine_allergy(visit_id);
    display_nurse_notes(visit_id,17);
    display_nurse_notes(visit_id,18);
    display_nurse_notes(visit_id,19);
    subjective_ros(visit_id);
    objective_ros(visit_id);
    get_disease(visit_id);
    display_action_plan(visit_id);
    display_notes(visit_id,14);
    display_notes(visit_id,15);
    display_notes(visit_id,16);
    display_referal_note(visit_id);
    assessment(visit_id);
    
}


function previous_vitals(visit_id){
    
    var XMLHttpRequestObject = false;
        
    if (window.XMLHttpRequest) {
    
        XMLHttpRequestObject = new XMLHttpRequest();
    } 
        
    else if (window.ActiveXObject) {
        XMLHttpRequestObject = new ActiveXObject("Microsoft.XMLHTTP");
    }
   	var config_url = '<?php echo site_url();?>';
    var url = config_url+"nurse/previous_vitals/"+visit_id;//window.alert(url);
    
    if(XMLHttpRequestObject) {
        
        var obj = document.getElementById("previous_vitals");
                
        XMLHttpRequestObject.open("GET", url);
                
        XMLHttpRequestObject.onreadystatechange = function(){
            
            if (XMLHttpRequestObject.readyState == 4 && XMLHttpRequestObject.status == 200) {

                obj.innerHTML = XMLHttpRequestObject.responseText;
            }
        }
                
        XMLHttpRequestObject.send(null);
    }
}


function display_nurse_notes(visit_id,type)
{

 var XMLHttpRequestObject = false;
     
 if (window.XMLHttpRequest) {
 
     XMLHttpRequestObject = new XMLHttpRequest();
 } 
     
 else if (window.ActiveXObject) {
     XMLHttpRequestObject = new ActiveXObject("Microsoft.XMLHTTP");
 }
 
 var config_url = '<?php echo site_url();?>';
 var url = config_url+"nurse/display_nurses_notes/"+visit_id+"/"+type+"/1";
// alert(url);
 if(XMLHttpRequestObject) {
             
     XMLHttpRequestObject.open("GET", url);
             
     XMLHttpRequestObject.onreadystatechange = function(){
         
         if (XMLHttpRequestObject.readyState == 4 && XMLHttpRequestObject.status == 200) {
            if(type == 17)
            {

             document.getElementById("pt-dx").innerHTML=XMLHttpRequestObject.responseText;
            }
            else if(type == 18)
            {
              document.getElementById("pt-rx").innerHTML=XMLHttpRequestObject.responseText;
            }
            else if(type == 19)
            {
              document.getElementById("chief-complain").innerHTML=XMLHttpRequestObject.responseText;
            }
            
         }
     }
             
     XMLHttpRequestObject.send(null);
 }
}

function get_patient_medicine_allergy(visit_id)
{

    var XMLHttpRequestObject = false;
        
    if (window.XMLHttpRequest) {
    
        XMLHttpRequestObject = new XMLHttpRequest();
    } 
        
    else if (window.ActiveXObject) {
        XMLHttpRequestObject = new ActiveXObject("Microsoft.XMLHTTP");
    }
    var url = config_url+"nurse/patient_medicine_allergy/"+visit_id+"/1";

    // alert(url);
    
    if(XMLHttpRequestObject) {
        
        var obj = document.getElementById("medicine-allergy");
                
        XMLHttpRequestObject.open("GET", url);

                
        XMLHttpRequestObject.onreadystatechange = function(){
            
            if (XMLHttpRequestObject.readyState == 4 && XMLHttpRequestObject.status == 200) {

                obj.innerHTML = XMLHttpRequestObject.responseText;
            }
        }
                
        XMLHttpRequestObject.send(null);
    }
}


function get_food_types_view(visit_id)
{

    var XMLHttpRequestObject = false;
        
    if (window.XMLHttpRequest) {
    
        XMLHttpRequestObject = new XMLHttpRequest();
    } 
        
    else if (window.ActiveXObject) {
        XMLHttpRequestObject = new ActiveXObject("Microsoft.XMLHTTP");
    }
    // var config_url = document.getElementById("config_url").value;
    var url = config_url+"nurse/patient_food_types/"+visit_id+"/1";
    
    if(XMLHttpRequestObject) {
        
        var obj = document.getElementById("food-allergy");
                
        XMLHttpRequestObject.open("GET", url);

                
        XMLHttpRequestObject.onreadystatechange = function(){
            
            if (XMLHttpRequestObject.readyState == 4 && XMLHttpRequestObject.status == 200) {

                obj.innerHTML = XMLHttpRequestObject.responseText;
            }
        }
                
        XMLHttpRequestObject.send(null);
    }
}


function get_medication(visit_id){
    
    var XMLHttpRequestObject = false;
        
    if (window.XMLHttpRequest) {
    
        XMLHttpRequestObject = new XMLHttpRequest();
    } 
        
    else if (window.ActiveXObject) {
        XMLHttpRequestObject = new ActiveXObject("Microsoft.XMLHTTP");
    }

    // var config_url = document.getElementById("config_url").value;
    var url = config_url+"nurse/load_medication/"+visit_id;

    if(XMLHttpRequestObject) {
        
        var obj = document.getElementById("medication");
                
        XMLHttpRequestObject.open("GET", url);
                
        XMLHttpRequestObject.onreadystatechange = function(){
            
            if (XMLHttpRequestObject.readyState == 4 && XMLHttpRequestObject.status == 200) {

                obj.innerHTML = XMLHttpRequestObject.responseText;
            }
        }
                
        XMLHttpRequestObject.send(null);
    }
}

function get_vaccines(visit_id){
    
    var XMLHttpRequestObject = false;
        
    if (window.XMLHttpRequest) {
    
        XMLHttpRequestObject = new XMLHttpRequest();
    } 
        
    else if (window.ActiveXObject) {
        XMLHttpRequestObject = new ActiveXObject("Microsoft.XMLHTTP");
    }
    // var config_url = document.getElementById("config_url").value;
    var url = config_url+"nurse/patient_vaccine/"+visit_id;
    
    if(XMLHttpRequestObject) {
        
        var obj = document.getElementById("patient_vaccine");
                
        XMLHttpRequestObject.open("GET", url);

                
        XMLHttpRequestObject.onreadystatechange = function(){
            
            if (XMLHttpRequestObject.readyState == 4 && XMLHttpRequestObject.status == 200) {

                obj.innerHTML = XMLHttpRequestObject.responseText;
            }
        }
                
        XMLHttpRequestObject.send(null);
    }
}
function get_family_diseases(visit_id){
    
    var XMLHttpRequestObject = false;
        
    if (window.XMLHttpRequest) {
    
        XMLHttpRequestObject = new XMLHttpRequest();
    } 
        
    else if (window.ActiveXObject) {
        XMLHttpRequestObject = new ActiveXObject("Microsoft.XMLHTTP");
    }
    // var config_url = document.getElementById("config_url").value;
    var url = config_url+"nurse/patient_family_disease/"+visit_id;
    
    if(XMLHttpRequestObject) {
        
        var obj = document.getElementById("family-diseases");
                
        XMLHttpRequestObject.open("GET", url);

                
        XMLHttpRequestObject.onreadystatechange = function(){
            
            if (XMLHttpRequestObject.readyState == 4 && XMLHttpRequestObject.status == 200) {

                obj.innerHTML = XMLHttpRequestObject.responseText;
            }
        }
                
        XMLHttpRequestObject.send(null);
    }
}

function subjective_ros(visit_id){

  var XMLHttpRequestObject = false;
    
  if (window.XMLHttpRequest) {
  
    XMLHttpRequestObject = new XMLHttpRequest();
  } 
    
  else if (window.ActiveXObject) {
    XMLHttpRequestObject = new ActiveXObject("Microsoft.XMLHTTP");
  }
  
   // var config_url = $('#config_url').val();

  var url = config_url+"nurse/subjective_ros/"+visit_id+"/1";
  // alert(url);

  if(XMLHttpRequestObject) {
    
    var obj = document.getElementById("subjective-ros");
      
    XMLHttpRequestObject.open("GET", url);
        
    XMLHttpRequestObject.onreadystatechange = function(){
      
      if (XMLHttpRequestObject.readyState == 4 && XMLHttpRequestObject.status == 200) {
        obj.innerHTML = XMLHttpRequestObject.responseText;
        
        // patient_details(visit_id);
        // objective_findings(visit_id);
        // assessment(visit_id);
        // plan(visit_id);
        // doctor_notes(visit_id);
        // nurse_notes(visit_id);
      }
    }
        
    XMLHttpRequestObject.send(null);
  }
}

function objective_ros(visit_id){

  var XMLHttpRequestObject = false;
    
  if (window.XMLHttpRequest) {
  
    XMLHttpRequestObject = new XMLHttpRequest();
  } 
    
  else if (window.ActiveXObject) {
    XMLHttpRequestObject = new ActiveXObject("Microsoft.XMLHTTP");
  }
  
   // var config_url = $('#config_url').val();

  var url = config_url+"nurse/objective_ros/"+visit_id+"/1";
  // alert(url);

  if(XMLHttpRequestObject) {
    
    var obj = document.getElementById("objective-ros");
      
    XMLHttpRequestObject.open("GET", url);
        
    XMLHttpRequestObject.onreadystatechange = function(){
      
      if (XMLHttpRequestObject.readyState == 4 && XMLHttpRequestObject.status == 200) {
        obj.innerHTML = XMLHttpRequestObject.responseText;
        
        // patient_details(visit_id);
        // objective_findings(visit_id);
        // assessment(visit_id);
        // plan(visit_id);
        // doctor_notes(visit_id);
        // nurse_notes(visit_id);
      }
    }
        
    XMLHttpRequestObject.send(null);
  }
}


function get_disease(visit_id){
  
  var XMLHttpRequestObject = false;
    
  if (window.XMLHttpRequest) {
  
    XMLHttpRequestObject = new XMLHttpRequest();
  } 
    
  else if (window.ActiveXObject) {
    XMLHttpRequestObject = new ActiveXObject("Microsoft.XMLHTTP");
  }
  // var config_url = $('#config_url').val();
  var url = config_url+"nurse/get_diagnosis/"+visit_id;

  // alert(url);
  if(XMLHttpRequestObject) {
    var obj = document.getElementById("visit_diagnosis_original");
    var obj2 = document.getElementById("visit_diagnosis");
        
    XMLHttpRequestObject.open("GET", url);
        
    XMLHttpRequestObject.onreadystatechange = function(){
      
      if (XMLHttpRequestObject.readyState == 4 && XMLHttpRequestObject.status == 200) {
        obj.innerHTML = XMLHttpRequestObject.responseText;
        obj2.innerHTML = XMLHttpRequestObject.responseText;
      }
    }
    
    XMLHttpRequestObject.send(null);
  }
}

function assessment(visit_id){
  var XMLHttpRequestObject = false;
    
  if (window.XMLHttpRequest) {
  
    XMLHttpRequestObject = new XMLHttpRequest();
  } 
    
  else if (window.ActiveXObject) {
    XMLHttpRequestObject = new ActiveXObject("Microsoft.XMLHTTP");
  }
  // var config_url = $('#config_url').val();
  var url = config_url+"nurse/view_assessment/"+visit_id;
  
  if(XMLHttpRequestObject) {
    
    var obj = document.getElementById("assessment");
        
    XMLHttpRequestObject.open("GET", url);
        
    XMLHttpRequestObject.onreadystatechange = function(){
      
      if (XMLHttpRequestObject.readyState == 4 && XMLHttpRequestObject.status == 200) {
        obj.innerHTML = XMLHttpRequestObject.responseText;
      }
    }
        
    XMLHttpRequestObject.send(null);
  }
}

 function display_notes(visit_id,type)
   {
   
     var XMLHttpRequestObject = false;
         
     if (window.XMLHttpRequest) {
     
         XMLHttpRequestObject = new XMLHttpRequest();
     } 
         
     else if (window.ActiveXObject) {
         XMLHttpRequestObject = new ActiveXObject("Microsoft.XMLHTTP");
     }
     
     // var config_url = document.getElementById("config_url").value;
     var url = config_url+"doctor/display_notes/"+visit_id+"/"+type+"/1";
     // alert(url);
     if(XMLHttpRequestObject) {
                 
         XMLHttpRequestObject.open("GET", url);
                 
         XMLHttpRequestObject.onreadystatechange = function(){
             
             if (XMLHttpRequestObject.readyState == 4 && XMLHttpRequestObject.status == 200) {
                if(type == 14)
                {

                 document.getElementById("chief-complain").innerHTML=XMLHttpRequestObject.responseText;
                }
                else if(type == 15)
                {
                  document.getElementById("h-p-i").innerHTML=XMLHttpRequestObject.responseText;
                }
                else if(type == 16)
                {
                  document.getElementById("other-plans").innerHTML=XMLHttpRequestObject.responseText;
                }
             }
         }
                 
         XMLHttpRequestObject.send(null);
     }
   }

   function display_action_plan(visit_id)
   {
   
     var XMLHttpRequestObject = false;
         
     if (window.XMLHttpRequest) {
     
         XMLHttpRequestObject = new XMLHttpRequest();
     } 
         
     else if (window.ActiveXObject) {
         XMLHttpRequestObject = new ActiveXObject("Microsoft.XMLHTTP");
     }
     
     // var config_url = document.getElementById("config_url").value;
     var url = config_url+"doctor/display_action_plan/"+visit_id+"/1";
     // alert(url);
     if(XMLHttpRequestObject) {
                 
         XMLHttpRequestObject.open("GET", url);
                 
         XMLHttpRequestObject.onreadystatechange = function(){
             
             if (XMLHttpRequestObject.readyState == 4 && XMLHttpRequestObject.status == 200) {
   
                 document.getElementById("action-plan").innerHTML=XMLHttpRequestObject.responseText;
             }
         }
                 
         XMLHttpRequestObject.send(null);
     }
   }

    function display_referal_note(visit_id)
   {
   
     var XMLHttpRequestObject = false;
         
     if (window.XMLHttpRequest) {
     
         XMLHttpRequestObject = new XMLHttpRequest();
     } 
         
     else if (window.ActiveXObject) {
         XMLHttpRequestObject = new ActiveXObject("Microsoft.XMLHTTP");
     }
     
     // var config_url = document.getElementById("config_url").value;
     var url = config_url+"doctor/display_referal_note/"+visit_id+"/1";
     
     if(XMLHttpRequestObject) {
                 
         XMLHttpRequestObject.open("GET", url);
                 
         XMLHttpRequestObject.onreadystatechange = function(){
             
             if (XMLHttpRequestObject.readyState == 4 && XMLHttpRequestObject.status == 200) {
   
                 document.getElementById("note-referal").innerHTML=XMLHttpRequestObject.responseText;
             }
         }
                 
         XMLHttpRequestObject.send(null);
     }
   }

  </script>