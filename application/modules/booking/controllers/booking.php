<?php   if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Booking extends MX_Controller 
{
	function __construct()
	{
		parent:: __construct();
		
		$this->load->model('site/site_model');
		$this->load->model('administration/reports_model');
		$this->load->model('admin/users_model');
		$this->load->model('admin/sections_model');
		$this->load->model('admin/file_model');
		$this->load->model('admin/admin_model');
		$this->load->model('admin/email_model');
		// $this->load->model('accounting/payroll_model');
		$this->load->model('hr/personnel_model');
		$this->load->model('admin/branches_model');
		$this->load->model('accounting/petty_cash_model');
		$this->load->model('booking/booking_model');
		$this->load->model('pos/pos_model');
		$this->load->model('booking/database');
		// $this->load->model('accounting/salary_advance_model');
		// $this->load->model('accounts/reception_model');
		// $this->load->model('accounting/hospital_accounts_model');
		$this->load->model('auth/auth_model');
	}
	public function active_bookings()
	{
		$branch_code = $this->session->userdata('search_branch_code');
		
		if(empty($branch_code))
		{
			$branch_code = $this->session->userdata('branch_code');
		}
		
		$this->db->where('branch_code', $branch_code);
		$query = $this->db->get('branch');
		
		if($query->num_rows() > 0)
		{
			$row = $query->row();
			$branch_name = $row->branch_name;
		}
		
		else
		{
			$branch_name = '';
		}
		$v_data['branch_name'] = $branch_name;
		$v_data['branches'] = $this->reports_model->get_all_active_branches();
		$where = 'visit.visit_delete = 0 AND visit.customer_id > 0 AND visit.customer_id = customer.customer_id AND visit.close_card = 0 AND visit.room_id > 0';
		
		$table = 'visit, customer';
		
		$visit_search = $this->session->userdata('visit_accounts_search');
		
		if(!empty($visit_search))
		{
			$where .= $visit_search;
		}
		//pagination
		$this->load->library('pagination');
		$config['base_url'] = site_url().'bookings/active-bookings';
		$config['total_rows'] = $this->reception_model->count_items($table, $where);
		$config['uri_segment'] = 3;
		$config['per_page'] = 20;
		$config['num_links'] = 5;
		
		$config['full_tag_open'] = '<ul class="pagination pull-right">';
		$config['full_tag_close'] = '</ul>';
		
		$config['first_tag_open'] = '<li>';
		$config['first_tag_close'] = '</li>';
		
		$config['last_tag_open'] = '<li>';
		$config['last_tag_close'] = '</li>';
		
		$config['next_tag_open'] = '<li>';
		$config['next_link'] = 'Next';
		$config['next_tag_close'] = '</span>';
		
		$config['prev_tag_open'] = '<li>';
		$config['prev_link'] = 'Prev';
		$config['prev_tag_close'] = '</li>';
		
		$config['cur_tag_open'] = '<li class="active"><a href="#">';
		$config['cur_tag_close'] = '</a></li>';
		
		$config['num_tag_open'] = '<li>';
		$config['num_tag_close'] = '</li>';
		$this->pagination->initialize($config);
		
		$page = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
		$v_data['type_links'] =1;
        $v_data["links"] = $this->pagination->create_links();
		$query = $this->booking_model->get_all_ongoing_visits($table, $where, $config["per_page"], $page);
		
		$v_data['query'] = $query;
		$v_data['page'] = $page;
		
		$data['title'] = 'Bookings';
		$v_data['title'] = 'Bookings';
		$v_data['module'] = 0;
		$v_data['close_page'] = 1;
		
		
		$data['content'] = $this->load->view('booking/booking/all_bookings', $v_data, true);
		
		$this->load->view('admin/templates/general_page', $data);
		// end of it
		
	}
	public function search_visits($pager)
	{
		$visit_type_id = $this->input->post('visit_type_id');
		$surnames = $this->input->post('surname');
		$personnel_id = $this->input->post('personnel_id');
		$visit_date = $this->input->post('visit_date');
		$othernames = $this->input->post('othernames');
		$branch_code = $this->input->post('branch_code');
		$this->session->set_userdata('search_branch_code', $branch_code);
		
		if(!empty($visit_type_id))
		{
			$visit_type_id = ' AND visit.visit_type = '.$visit_type_id.' ';
		}
		
		if(!empty($personnel_id))
		{
			$personnel_id = ' AND visit.personnel_id = '.$personnel_id.' ';
		}
		
		if(!empty($visit_date))
		{
			$visit_date = ' AND visit.visit_date = \''.$visit_date.'\' ';
		}
		
		//search surname
		$surnames = explode(" ",$surnames);
		$total = count($surnames);
		
		$count = 1;
		$surname = ' AND (';
		for($r = 0; $r < $total; $r++)
		{
			if($count == $total)
			{
				$surname .= ' patients.patient_surname LIKE \'%'.mysql_real_escape_string($surnames[$r]).'%\'';
			}
			
			else
			{
				$surname .= ' patients.patient_surname LIKE \'%'.mysql_real_escape_string($surnames[$r]).'%\' AND ';
			}
			$count++;
		}
		$surname .= ') ';
		
		//search other_names
		$other_names = explode(" ",$othernames);
		$total = count($other_names);
		
		$count = 1;
		$other_name = ' AND (';
		for($r = 0; $r < $total; $r++)
		{
			if($count == $total)
			{
				$other_name .= ' patients.patient_othernames LIKE \'%'.mysql_real_escape_string($other_names[$r]).'%\'';
			}
			
			else
			{
				$other_name .= ' patients.patient_othernames LIKE \'%'.mysql_real_escape_string($other_names[$r]).'%\' AND ';
			}
			$count++;
		}
		$other_name .= ') ';
		
		$search = $visit_type_id.$surname.$other_name.$visit_date.$personnel_id;
		$this->session->unset_userdata('visit_accounts_search');
		$this->session->set_userdata('visit_accounts_search', $search);
		redirect('bookings/active-bookings');
		
		
	}
	public function close_queue_search($pager)
	{
		$this->session->unset_userdata('visit_accounts_search');
		redirect('bookings/active-bookings');
	}
	public function accounts_unclosed_queue()
	{
		//$where = 'visit.visit_delete = 0 AND visit_department.visit_id = visit.visit_id AND visit_department.department_id = 6 AND visit_department.visit_department_status = 1 AND visit.patient_id = patients.patient_id AND (visit.close_card = 0 OR visit.close_card = 7)';
		$branch_code = $this->session->userdata('search_branch_code');
		
		if(empty($branch_code))
		{
			$branch_code = $this->session->userdata('branch_code');
		}
		
		$this->db->where('branch_code', $branch_code);
		$query = $this->db->get('branch');
		
		if($query->num_rows() > 0)
		{
			$row = $query->row();
			$branch_name = $row->branch_name;
		}
		
		else
		{
			$branch_name = '';
		}
		$v_data['branch_name'] = $branch_name;
		$v_data['branches'] = $this->reports_model->get_all_active_branches();
		$where = 'visit.inpatient = 0 AND visit.visit_delete = 0 AND visit_department.visit_id = visit.visit_id AND visit_department.visit_department_status = 1 AND visit.patient_id = patients.patient_id AND (visit.close_card = 0 OR visit.close_card = 7) AND visit_type.visit_type_id = visit.visit_type AND visit.branch_code = \''.$branch_code.'\'';
		
		$table = 'visit_department, visit, patients, visit_type';
		
		$visit_search = $this->session->userdata('visit_accounts_search');
		$segment = 3;
		
		if(!empty($visit_search))
		{
			$where .= $visit_search;
			$segment = 4;
		}
		//pagination
		$this->load->library('pagination');
		$config['base_url'] = site_url().'accounts/accounts_unclosed_queue';
		$config['total_rows'] = $this->reception_model->count_items($table, $where);
		$config['uri_segment'] = $segment;
		$config['per_page'] = 40;
		$config['num_links'] = 5;
		
		$config['full_tag_open'] = '<ul class="pagination pull-right">';
		$config['full_tag_close'] = '</ul>';
		
		$config['first_tag_open'] = '<li>';
		$config['first_tag_close'] = '</li>';
		
		$config['last_tag_open'] = '<li>';
		$config['last_tag_close'] = '</li>';
		
		$config['next_tag_open'] = '<li>';
		$config['next_link'] = 'Next';
		$config['next_tag_close'] = '</span>';
		
		$config['prev_tag_open'] = '<li>';
		$config['prev_link'] = 'Prev';
		$config['prev_tag_close'] = '</li>';
		
		$config['cur_tag_open'] = '<li class="active"><a href="#">';
		$config['cur_tag_close'] = '</a></li>';
		
		$config['num_tag_open'] = '<li>';
		$config['num_tag_close'] = '</li>';
		$this->pagination->initialize($config);
		
		$page = ($this->uri->segment($segment)) ? $this->uri->segment($segment) : 0;
		$v_data['type_links'] =2;
        $v_data["links"] = $this->pagination->create_links();
		$query = $this->reception_model->get_all_ongoing_visits2($table, $where, $config["per_page"], $page);
		
		$v_data['query'] = $query;
		$v_data['page'] = $page;
		$v_data['close_page'] = 2;
		
		$data['title'] = 'Accounts Unclosed Visits';
		$v_data['title'] = 'Accounts Unclosed Visits';
		$v_data['module'] = 0;
		
		$v_data['type'] = $this->reception_model->get_types();
		$v_data['doctors'] = $this->reception_model->get_doctor();
		
		$data['content'] = $this->load->view('accounts_queue', $v_data, true);
		$data['sidebar'] = 'accounts_sidebar';
		
		$this->load->view('admin/templates/general_page', $data);
		// end of it
		
	}
	public function accounts_closed_visits()
	{
		$branch_code = $this->session->userdata('search_branch_code');
		
		if(empty($branch_code))
		{
			$branch_code = $this->session->userdata('branch_code');
		}
		
		$this->db->where('branch_code', $branch_code);
		$query = $this->db->get('branch');
		
		if($query->num_rows() > 0)
		{
			$row = $query->row();
			$branch_name = $row->branch_name;
		}
		
		else
		{
			$branch_name = '';
		}
		$v_data['branch_name'] = $branch_name;
		$where = 'visit.visit_delete = 0  AND visit.patient_id = patients.patient_id AND visit.close_card = 1 ';
		$where = 'visit.visit_delete = 0 AND visit_department.visit_id = visit.visit_id AND visit_department.visit_department_status = 1 AND visit.patient_id = patients.patient_id AND visit.close_card = 1 AND visit_type.visit_type_id = visit.visit_type AND visit.branch_code = \''.$branch_code.'\'';
		
		$table = 'visit_department, visit, patients, visit_type';
		
		$visit_search = $this->session->userdata('visit_accounts_search');
		$segment = 3;
		
		if(!empty($visit_search))
		{
			$where .= $visit_search;
			$segment = 3;
		}
		//pagination
		$this->load->library('pagination');
		$config['base_url'] = site_url().'accounts/accounts_closed_visits';
		$config['total_rows'] = $this->reception_model->count_items($table, $where);
		$config['uri_segment'] = $segment;
		$config['per_page'] = 20;
		$config['num_links'] = 5;
		
		$config['full_tag_open'] = '<ul class="pagination pull-right">';
		$config['full_tag_close'] = '</ul>';
		
		$config['first_tag_open'] = '<li>';
		$config['first_tag_close'] = '</li>';
		
		$config['last_tag_open'] = '<li>';
		$config['last_tag_close'] = '</li>';
		
		$config['next_tag_open'] = '<li>';
		$config['next_link'] = 'Next';
		$config['next_tag_close'] = '</span>';
		
		$config['prev_tag_open'] = '<li>';
		$config['prev_link'] = 'Prev';
		$config['prev_tag_close'] = '</li>';
		
		$config['cur_tag_open'] = '<li class="active"><a href="#">';
		$config['cur_tag_close'] = '</a></li>';
		
		$config['num_tag_open'] = '<li>';
		$config['num_tag_close'] = '</li>';
		$this->pagination->initialize($config);
		
		$page = ($this->uri->segment($segment)) ? $this->uri->segment($segment) : 0;
        $v_data["links"] = $this->pagination->create_links();
        $v_data['type_links'] =3;
		$query = $this->reception_model->get_all_ongoing_visits2($table, $where, $config["per_page"], $page);
		
		$v_data['query'] = $query;
		$v_data['page'] = $page;
		
		$data['title'] = 'Accounts closed Visits';
		$v_data['title'] = 'Accounts closed Visits';
		$v_data['module'] = 7;
		$v_data['close_page'] = 3;
		$v_data['branches'] = $this->reports_model->get_all_active_branches();
		
		$v_data['type'] = $this->reception_model->get_types();
		$v_data['doctors'] = $this->reception_model->get_doctor();
		
		$data['content'] = $this->load->view('accounts_queue', $v_data, true);
		$data['sidebar'] = 'accounts_sidebar';
		
		$this->load->view('admin/templates/general_page', $data);
		// end of it
		
	}
	public function invoice($visit_id)
	{
		?>
        	<script type="text/javascript">
        		var config_url = $('#config_url').val();
				window.open(config_url+"/accounts/print_invoice/<?php echo $visit_id;?>","Popup","height=900,width=1200,,scrollbars=yes,"+"directories=yes,location=yes,menubar=yes,"+"resizable=no status=no,history=no top = 50 left = 100");
				window.location.href="<?php echo base_url("index.php/accounts/accounts_queue")?>";
			</script>
        <?php
		
		$this->accounts_queue();
	}
	
	public function billing($visit_id, $close_page = NULL)
	{
		$v_data = array('visit_id'=>$visit_id);
		
		$v_data['cancel_actions'] = $this->booking_model->get_cancel_actions();
		
		$patient = $this->booking_model->customer_names2(NULL, $visit_id);
		$v_data['customer_first_name'] = $patient['customer_first_name'];
		$v_data['customer_surname'] = $patient['customer_surname'];
		$v_data['account_balance'] = $patient['account_balance'];
		$v_data['customer_id'] = $patient['customer_id'];
		$primary_key = $patient['patient_id'];
		$v_data['close_page'] = $close_page;
		$data['content'] = $this->load->view('billing', $v_data, true);
		
		$data['title'] = 'Payments';
		$this->load->view('admin/templates/general_page', $data);
	}
	
	public function make_payments($visit_id, $close_page = NULL)
	{
		$this->form_validation->set_rules('payment_method', 'Payment Method', 'trim|required|xss_clean');
		$this->form_validation->set_rules('amount_paid', 'Amount', 'trim|required|xss_clean');
		$this->form_validation->set_rules('type_payment', 'Type of payment', 'trim|required|xss_clean');
		$payment_method = $this->input->post('payment_method');
		// normal or credit note or debit note
		$type_payment = $this->input->post('type_payment');
		
		// Normal
		if($type_payment == 1)
		{
			$this->form_validation->set_rules('service_id', 'Service', 'xss_clean');
			if(!empty($payment_method))
			{
				if($payment_method == 1)
				{
					// check for cheque number if inserted
					$this->form_validation->set_rules('cheque_number', 'Cheque Number', 'trim|required|xss_clean');
				}
				else if($payment_method == 3)
				{
					// check for insuarance number if inserted
					$this->form_validation->set_rules('insuarance_number', 'Insurance Number', 'trim|required|xss_clean');
				}
				else if($payment_method == 5)
				{
					//  check for mpesa code if inserted
					$this->form_validation->set_rules('mpesa_code', 'Amount', 'trim|required|xss_clean');
				}
			}
		}
		else if($type_payment == 2)
		{
			// debit note
			$this->form_validation->set_rules('username', 'Username', 'trim|required|xss_clean');
			$this->form_validation->set_rules('password', 'Password', 'trim|required|xss_clean');
			$this->form_validation->set_rules('payment_service_id', 'Service', 'required|xss_clean');
		}
		else if($type_payment == 3)
		{
			$this->form_validation->set_rules('username', 'Username', 'trim|required|xss_clean');
			$this->form_validation->set_rules('password', 'Password', 'trim|required|xss_clean');
			$this->form_validation->set_rules('payment_service_id', 'Service', 'required|xss_clean');
		}
		//if form conatins invalid data
		if ($this->form_validation->run())
		{
			
			if($type_payment == 3 || $type_payment == 2)
			{
				$username=$this->input->post('username');
				$password=$this->input->post('password');
				// check if the username and password is for an administrator
				// $checker_response = $this->booking_model->check_admin_person($username, $password);
				// end of checker function
				// if(($checker_response > 0))
				// {
					$this->booking_model->receipt_payment($visit_id, $checker_response);
				// }
				// else
				// {
				// 	$this->session->set_userdata("error_message","Seems like you dont have the priviledges to effect this event. Please contact your administrator.");
				// }
			}
			else
			{
				$this->booking_model->receipt_payment($visit_id);

				//$this->sync_model->syn_up_on_closing_visit($visit_id);
			}
			
			//sync data
			//$response = $this->sync_model->syn_up_on_closing_visit($visit_id);
			
			redirect('pos/billing/'.$visit_id);
		}
		else
		{
			$this->session->set_userdata("error_message", validation_errors());
			redirect('pos/billing/'.$visit_id);
		}
	}
	
	public function add_billing($visit_id, $close_page = NULL)
	{
		$this->form_validation->set_rules('billing_method_id', 'Billing Method', 'required|numeric');
		
		//if form conatins invalid data
		if ($this->form_validation->run())
		{
			if($this->booking_model->add_billing($visit_id))
			{
				$this->session->set_userdata('success_message', 'Billing method successfully added');
			}
			else
			{
				$this->session->set_userdata("error_message","Unable to add billing method. Please try again");
			}
		}
		else
		{
			$this->session->set_userdata("error_message","Fill in the fields");
		}
		
		redirect('booking/billing/'.$visit_id.'/'.$close_page);
	}
	
	public function print_invoice($visit_id)
	{
		$this->booking_model->receipt($visit_id);
	}
	
	public function print_invoice_old($visit_id)
	{
		$this->booking_model->receipt($visit_id, TRUE);
	}
	
	public function print_invoice_new($visit_id,$department_name = NULL)
	{
		$data = array('visit_id'=>$visit_id);
		$data['contacts'] = $this->site_model->get_contacts();
		
		$patient = $this->pos_model->customer_names($visit_id);
		$data['patient'] = $patient;
		$this->load->view('invoice', $data);
		
	}
	public function print_receipt_new($visit_id)
	{
		$data = array('visit_id'=>$visit_id);
		$data['contacts'] = $this->site_model->get_contacts();
		
		$patient = $this->booking_model->customer_names2(NULL, $visit_id);

		// var_dump($patient); die();
		$data['patient'] = $patient;
		$this->load->view('receipt', $data);
	}
	public function print_single_receipt($payment_id)
	{
		$data = array('payment_id' => $payment_id);
		$data['contacts'] = $this->site_model->get_contacts();
		$data['receipt_payment_id'] = $payment_id;
		
		$patient = $this->booking_model->customer_names2(NULL, $visit_id);
		$data['patient'] = $patient;
		$this->load->view('single_receipt', $data);
	}
	public function bulk_close_visits($page)
	{
		$total_visits = sizeof($_POST['visit']);
		
		//check if any checkboxes have been ticked
		if($total_visits > 0)
		{	
			for($r = 0; $r < $total_visits; $r++)
			{	
				$visit = $_POST['visit'];
				$visit_id = $visit[$r]; 
				//check if card is held
				if($this->reception_model->is_card_held($visit_id))
				{
				}
				
				else
				{
					if($this->booking_model->end_visit($visit_id))
					{
						$this->session->set_userdata('success_message', 'Visits ended successfully');
					}
					
					else
					{
						$this->session->set_userdata('error_message', 'Unable to end visits');
					}
				}
			}
		}
		
		else
		{
			$this->session->set_userdata('error_message', 'Please select visits to terminate first');
		}
		
		redirect('accounts/accounts_unclosed_queue/'.$page);
	}
	
	public function send_to_department($visit_id, $department_id)
	{
		$data['accounts'] = 1;
		$this->db->where('visit_department.visit_department_status = 1 AND visit_department.visit_id = '.$visit_id);
		if($this->db->update('visit_department', $data))
		{
			$this->db->where('visit_id', $visit_id);
			$query = $this->db->get('visit');
			$row = $query->row();
			$visit_type = $row->visit_type;
			
			if($this->reception_model->set_visit_department($visit_id, $department_id, $visit_type))
			{
				$this->session->set_userdata('success_message', 'Patient has been sent');
				redirect('accounts/accounts-queue');
			}
			else
			{
				$this->session->set_userdata('error_message', 'Unable to send patient');
				redirect('booking/billing/'.$visit_id.'/1');
			}
		}
		
		else
		{
			$this->session->set_userdata('error_message', 'Patient could not be sent');
			redirect('booking/billing/'.$visit_id.'/1');
		}
	}
	
	public function cancel_payment($payment_id, $visit_id)
	{
		$this->form_validation->set_rules('cancel_description', 'Description', 'trim|required|xss_clean');
		$this->form_validation->set_rules('cancel_action_id', 'Action', 'trim|required|xss_clean');
		
		//if form conatins invalid data
		if ($this->form_validation->run())
		{
			// end of checker function
			if($this->booking_model->cancel_payment($payment_id))
			{
				$this->session->set_userdata("success_message", "Payment action saved successfully");
			}
			else
			{
				$this->session->set_userdata("error_message", "Oops something went wrong. Please try again");
			}
			redirect('booking/billing/'.$visit_id);
		}
		else
		{
			$this->session->set_userdata("error_message", validation_errors());
			redirect('booking/billing/'.$visit_id);
		}
	}

	public function end_visit($visit_id, $page = NULL)
	{
		//check if card is held

			$payments_value = $this->booking_model->total_payments($visit_id);

			$invoice_total = $this->booking_model->total_invoice($visit_id);


		
			$data = array(
				"close_card" => 1,
				"visit_time_out" => date('Y-m-d H:i:s')
			);
			$table = "visit";
			$key = $visit_id;
			if($this->database->update_entry($table, $data, $key))
			{
				$this->session->set_userdata("success_message", "Booking Closed successfully");
			}
			else
			{
				$this->session->set_userdata("error_message", "Sorry Please try again to close the visit");
			}
			//sync data
		
			redirect('bookings/active-bookings');
		
	}
	public function update_rejected_reasons($visit_id)
	{
		$this->form_validation->set_rules('rejected_amount', 'Amount', 'trim|required|xss_clean');
        $this->form_validation->set_rules('rejected_reason', 'Reason', 'trim|required|xss_clean');
        $redirect_url = $this->input->post('redirect_url');
        //if form conatins invalid data
        if ($this->form_validation->run())
        {
            // end of checker function
            if($this->booking_model->update_rejected_reasons($visit_id))
            {
                $this->session->set_userdata("success_message", "Reject  saved successfully");
            }
            else
            {
                $this->session->set_userdata("error_message", "Oops something went wrong. Please try again");
            }
        }
        else
        {
            $this->session->set_userdata("error_message", validation_errors());
           
        }
        redirect('booking/billing/'.$visit_id);
	}
	public function dislay_invoices($visit_id)
	{
		$data = array('visit_id'=>$visit_id);
		$this->load->view('invoice_view',$data);
	}
	function procedure($procedure_id,$visit_id,$suck,$store_id)
	{
		// $this->booking_model->submitvisitprocedure($procedure_id,$visit_id,$suck);
		$this->booking_model->visit_charge_insert($visit_id,$procedure_id,$suck,$store_id);
		
		$this->view_procedure($visit_id);	
	}
	function view_procedure($visit_id){
		$data = array('visit_id'=>$visit_id);
		$this->load->view('view_procedure',$data);
	}
	public function procedure_total($procedure_id,$units,$amount){
		

		$visit_data = array('visit_charge_units'=>$units,'modified_by'=>$this->session->userdata("personnel_id"),'date_modified'=>date("Y-m-d"));
		$this->db->where(array("visit_charge_id"=>$procedure_id));
		$this->db->update('visit_charge_pos', $visit_data);
	}
	function delete_procedure($procedure_id)
	{
		$visit_data = array('visit_charge_delete'=>1,'deleted_by'=>$this->session->userdata("personnel_id"),'deleted_on'=>date("Y-m-d"),'modified_by'=>$this->session->userdata("personnel_id"),'date_modified'=>date("Y-m-d"));

		$this->db->where(array("visit_charge_id"=>$procedure_id));
		$this->db->update('visit_charge_pos', $visit_data);
	}
	
}
?>