
<div id="dental-formula"></div>
<div class="row">
	<div class="col-md-12" >
		<?php
			$one_child = $this->dental_model->get_dentine_value($patient_id,55);
			$two_child = $this->dental_model->get_dentine_value($patient_id,54);
			$three_child = $this->dental_model->get_dentine_value($patient_id,53);
			$four_child = $this->dental_model->get_dentine_value($patient_id,52);
			$five_child = $this->dental_model->get_dentine_value($patient_id,51);
			$six_child = $this->dental_model->get_dentine_value($patient_id,61);
			$seven_child = $this->dental_model->get_dentine_value($patient_id,62);
			$eight_child = $this->dental_model->get_dentine_value($patient_id,63);
			$nine_child = $this->dental_model->get_dentine_value($patient_id,64);
			$ten_child = $this->dental_model->get_dentine_value($patient_id,65);
			$eleven_child = $this->dental_model->get_dentine_value($patient_id,85);
			$twelve_child = $this->dental_model->get_dentine_value($patient_id,84);
			$thirteen_child = $this->dental_model->get_dentine_value($patient_id,83);
			$fourteen_child = $this->dental_model->get_dentine_value($patient_id,82);
			$fifteen_child = $this->dental_model->get_dentine_value($patient_id,81);
			$sixteen_child = $this->dental_model->get_dentine_value($patient_id,71);
			$seventeen_child = $this->dental_model->get_dentine_value($patient_id,72);
			$eighteen_child = $this->dental_model->get_dentine_value($patient_id,73);
			$nineteen_child = $this->dental_model->get_dentine_value($patient_id,74);
			$twenty_child = $this->dental_model->get_dentine_value($patient_id,75);


		?>

		<div >
			<div class="col-lg-6 col-md-6 col-sm-6" style="border-left: 2px solid #000;border-bottom: 2px solid #000;">
			 	<div class="col-md-12">
			 		<h3 class="center-align">RIGHT</h3>
			 		<br>
			 		<table align='center' class='table table-striped table-condensed table-bordered'>
			 			<tr>
			 				<td class="center-align" style="height: 35px;width: 55px;"><?php echo $one_child;?></td>
			 				<td class="center-align" style="height: 35px;width: 55px;" ><?php echo $two_child;?></td>
			 				<td class="center-align" style="height: 35px;width: 55px;" ><?php echo $three_child;?></td>
			 				<td class="center-align" style="height: 35px;width: 55px;" ><?php echo $four_child;?></td>
			 				<td class="center-align" style="height: 35px;width: 55px;" ><?php echo $five_child;?></td>
			 			</tr>
			 			<tr >
			 				<td class="center-align" style="height: 35px;width: 55px;" onclick="check_department_type(55)"> 55 </td>
			 				<td class="center-align" style="height: 35px;width: 55px;" onclick="check_department_type(54)"> 54 </td>
			 				<td class="center-align" style="height: 35px;width: 55px;" onclick="check_department_type(53)"> 53 </td>
			 				<td class="center-align" style="height: 35px;width: 55px;" onclick="check_department_type(52)"> 52 </td>
			 				<td class="center-align" style="height: 35px;width: 55px;" onclick="check_department_type(51)"> 51 </td>
			 				
			 			</tr>
			 		</table>
			 	</div>
			</div>
			<div class="col-lg-6 col-md-6 col-sm-6"  style="border-right: 2px solid #000; border-left: 2px solid #000;border-bottom: 2px solid #000; ">			
			 	<div class="col-md-12">
			 		<h3 class="center-align">LEFT</h3>
			 		<br>
			 		<table align='center' class='table table-striped table-condensed table-bordered'>
			 			<tr>
			 				<td class="center-align" style="height: 35px;width: 55px;"><?php echo $six_child;?> </td>
			 				<td class="center-align" style="height: 35px;width: 55px;"><?php echo $seven_child;?></td>
			 				<td class="center-align" style="height: 35px;width: 55px;"><?php echo $eight_child;?></td>
			 				<td class="center-align" style="height: 35px;width: 55px;"><?php echo $nine_child;?></td>
			 				<td class="center-align" style="height: 35px;width: 55px;"><?php echo $ten_child;?></td>
			 			</tr>
			 			<tr >
			 				<td class="center-align" style="height: 35px;width: 55px;"  onclick="check_department_type(61)"> 61 </td>
			 				<td class="center-align" style="height: 35px;width: 55px;"  onclick="check_department_type(62)"> 62 </td>
			 				<td class="center-align" style="height: 35px;width: 55px;"  onclick="check_department_type(63)"> 63 </td>
			 				<td class="center-align" style="height: 35px;width: 55px;"  onclick="check_department_type(64)"> 64 </td>
			 				<td class="center-align" style="height: 35px;width: 55px;"  onclick="check_department_type(65)"> 65 </td>
			 				
			 			</tr>
			 		</table>
			 	</div>
			</div>
			<div class="col-lg-6 col-md-6 col-sm-6" style="border-left: 2px solid #000;">
			 	<div class="col-md-12">
			 		<table align='center' class='table table-striped table-condensed table-bordered'>
			 			<tr >
			 				<td class="center-align" style="height: 35px;width: 55px;"  onclick="check_department_type(85)"> 85 </td>
			 				<td class="center-align" style="height: 35px;width: 55px;"  onclick="check_department_type(84)"> 84 </td>
			 				<td class="center-align" style="height: 35px;width: 55px;"  onclick="check_department_type(83)"> 83 </td>
			 				<td class="center-align" style="height: 35px;width: 55px;"  onclick="check_department_type(82)"> 82 </td>
			 				<td class="center-align" style="height: 35px;width: 55px;"  onclick="check_department_type(81)"> 81 </td>
			 				
			 			</tr>
			 			<tr>
			 				<td class="center-align" style="height: 35px;width: 55px;"><?php echo $eleven_child;?> </td>
			 				<td class="center-align" style="height: 35px;width: 55px;"><?php echo $twelve_child;?></td>
			 				<td class="center-align" style="height: 35px;width: 55px;"><?php echo $thirteen_child;?></td>
			 				<td class="center-align" style="height: 35px;width: 55px;"><?php echo $fourteen_child;?></td>
			 				<td class="center-align" style="height: 35px;width: 55px;"><?php echo $fifteen_child;?></td>
			 			</tr>
			 			
			 		</table>
			 	</div>
			</div>
			<div class="col-lg-6 col-md-6 col-sm-6"  style="border-right: 2px solid #000; border-left: 2px solid #000; ">			
			 	<div class="col-md-12">
			 		<table align='center' class='table table-striped table-condensed table-bordered'>
			 			<tr >
			 				<td class="center-align" style="height: 35px;width: 55px;"  onclick="check_department_type(71)"> 71 </td>
			 				<td class="center-align" style="height: 35px;width: 55px;"  onclick="check_department_type(72)"> 72 </td>
			 				<td class="center-align" style="height: 35px;width: 55px;"  onclick="check_department_type(73)"> 73 </td>
			 				<td class="center-align" style="height: 35px;width: 55px;"  onclick="check_department_type(74)"> 74 </td>
			 				<td class="center-align" style="height: 35px;width: 55px;"  onclick="check_department_type(75)"> 75 </td>
			 				
			 			</tr>
			 			<tr>
			 				<td class="center-align" style="height: 35px;width: 55px;"><?php echo $sixteen_child;?> </td>
			 				<td class="center-align" style="height: 35px;width: 55px;"><?php echo $seventeen_child;?></td>
			 				<td class="center-align" style="height: 35px;width: 55px;"><?php echo $eighteen_child;?></td>
			 				<td class="center-align" style="height: 35px;width: 55px;"><?php echo $nineteen_child;?></td>
			 				<td class="center-align" style="height: 35px;width: 55px;"><?php echo $twenty_child;?></td>
			 			</tr>
			 			
			 		</table>
			 	</div>
			</div>
		</div>
	</div>
</div>
<hr>
<div class="row">
	<div class="col-md-12" >
		<?php
			$one = $this->dental_model->get_dentine_value($patient_id,1);
			$two = $this->dental_model->get_dentine_value($patient_id,2);
			$three = $this->dental_model->get_dentine_value($patient_id,3);
			$four = $this->dental_model->get_dentine_value($patient_id,4);
			$five = $this->dental_model->get_dentine_value($patient_id,5);
			$six = $this->dental_model->get_dentine_value($patient_id,6);
			$seven = $this->dental_model->get_dentine_value($patient_id,7);
			$eight = $this->dental_model->get_dentine_value($patient_id,8);
			$nine = $this->dental_model->get_dentine_value($patient_id,9);
			$ten = $this->dental_model->get_dentine_value($patient_id,10);
			$eleven = $this->dental_model->get_dentine_value($patient_id,11);
			$twelve = $this->dental_model->get_dentine_value($patient_id,12);
			$thirteen = $this->dental_model->get_dentine_value($patient_id,13);
			$fourteen = $this->dental_model->get_dentine_value($patient_id,14);
			$fifteen = $this->dental_model->get_dentine_value($patient_id,15);
			$sixteen = $this->dental_model->get_dentine_value($patient_id,16);
			$seventeen = $this->dental_model->get_dentine_value($patient_id,17);
			$eighteen = $this->dental_model->get_dentine_value($patient_id,18);
			$nineteen = $this->dental_model->get_dentine_value($patient_id,19);
			$twenty = $this->dental_model->get_dentine_value($patient_id,20);
			$twenty_one = $this->dental_model->get_dentine_value($patient_id,21);
			$twenty_two = $this->dental_model->get_dentine_value($patient_id,22);
			$twenty_three = $this->dental_model->get_dentine_value($patient_id,23);
			$twenty_four = $this->dental_model->get_dentine_value($patient_id,24);
			$twenty_five = $this->dental_model->get_dentine_value($patient_id,25);
			$twenty_six = $this->dental_model->get_dentine_value($patient_id,26);
			$twenty_seven = $this->dental_model->get_dentine_value($patient_id,27);
			$twenty_eight = $this->dental_model->get_dentine_value($patient_id,28);
			$twenty_nine = $this->dental_model->get_dentine_value($patient_id,29);
			$thirty = $this->dental_model->get_dentine_value($patient_id,30);
			$thirty_one = $this->dental_model->get_dentine_value($patient_id,31);
			$thirty_two = $this->dental_model->get_dentine_value($patient_id,32);


		?>

		<div >
			<div class="col-lg-6 col-md-6 col-sm-6" style="border-left: 2px solid #000;border-bottom: 2px solid #000;">
			 	<div class="col-md-12">
			 		<h3 class="center-align">RIGHT</h3>
			 		<br>
			 		<table align='center' class='table table-striped table-condensed table-bordered'>
			 			<tr>
			 				<td class="center-align" style="height: 35px;width: 55px;"><?php echo $one;?></td>
			 				<td class="center-align" style="height: 35px;width: 55px;" ><?php echo $two;?></td>
			 				<td class="center-align" style="height: 35px;width: 55px;" ><?php echo $three;?></td>
			 				<td class="center-align" style="height: 35px;width: 55px;" ><?php echo $four;?></td>
			 				<td class="center-align" style="height: 35px;width: 55px;" ><?php echo $five;?></td>
			 				<td class="center-align" style="height: 35px;width: 55px;" ><?php echo $six;?></td>
			 				<td class="center-align" style="height: 35px;width: 55px;" ><?php echo $seven;?></td>
			 				<td class="center-align" style="height: 35px;width: 55px;" ><?php echo $eight;?></td>
			 			</tr>
			 			<tr >
			 				<td class="center-align" style="height: 35px;width: 55px;" onclick="check_department_type(1)"> 18 </td>
			 				<td class="center-align" style="height: 35px;width: 55px;" onclick="check_department_type(2)"> 17 </td>
			 				<td class="center-align" style="height: 35px;width: 55px;" onclick="check_department_type(3)"> 16 </td>
			 				<td class="center-align" style="height: 35px;width: 55px;" onclick="check_department_type(4)"> 15 </td>
			 				<td class="center-align" style="height: 35px;width: 55px;" onclick="check_department_type(5)"> 14 </td>
			 				<td class="center-align" style="height: 35px;width: 55px;" onclick="check_department_type(6)"> 13 </td>
			 				<td class="center-align" style="height: 35px;width: 55px;" onclick="check_department_type(7)"> 12 </td>
			 				<td class="center-align" style="height: 35px;width: 55px;" onclick="check_department_type(8)"> 11 </td>
			 				
			 			</tr>
			 		</table>
			 	</div>
			</div>
			<div class="col-lg-6 col-md-6 col-sm-6"  style="border-right: 2px solid #000; border-left: 2px solid #000;border-bottom: 2px solid #000; ">			
			 	<div class="col-md-12">
			 		<h3 class="center-align">LEFT</h3>
			 		<br>
			 		<table align='center' class='table table-striped table-condensed table-bordered'>
			 			<tr>
			 				<td class="center-align" style="height: 35px;width: 55px;"><?php echo $nine;?> </td>
			 				<td class="center-align" style="height: 35px;width: 55px;"><?php echo $ten;?></td>
			 				<td class="center-align" style="height: 35px;width: 55px;"><?php echo $eleven;?></td>
			 				<td class="center-align" style="height: 35px;width: 55px;"><?php echo $twelve;?></td>
			 				<td class="center-align" style="height: 35px;width: 55px;"><?php echo $thirteen;?></td>
			 				<td class="center-align" style="height: 35px;width: 55px;"><?php echo $fourteen;?></td>
			 				<td class="center-align" style="height: 35px;width: 55px;"><?php echo $fifteen;?></td>
			 				<td class="center-align" style="height: 35px;width: 55px;"><?php echo $sixteen;?></td>
			 			</tr>
			 			<tr >
			 				<td class="center-align" style="height: 35px;width: 55px;"  onclick="check_department_type(9)"> 21 </td>
			 				<td class="center-align" style="height: 35px;width: 55px;"  onclick="check_department_type(10)"> 22 </td>
			 				<td class="center-align" style="height: 35px;width: 55px;"  onclick="check_department_type(11)"> 23 </td>
			 				<td class="center-align" style="height: 35px;width: 55px;"  onclick="check_department_type(12)"> 24 </td>
			 				<td class="center-align" style="height: 35px;width: 55px;"  onclick="check_department_type(13)"> 25 </td>
			 				<td class="center-align" style="height: 35px;width: 55px;"  onclick="check_department_type(14)"> 26 </td>
			 				<td class="center-align" style="height: 35px;width: 55px;"  onclick="check_department_type(15)"> 27 </td>
			 				<td class="center-align" style="height: 35px;width: 55px;"  onclick="check_department_type(16)"> 28 </td>
			 				
			 			</tr>
			 		</table>
			 	</div>
			</div>
			<div class="col-lg-6 col-md-6 col-sm-6" style="border-left: 2px solid #000;">
			 	<div class="col-md-12">
			 		<table align='center' class='table table-striped table-condensed table-bordered'>
			 			<tr >
			 				<td class="center-align" style="height: 35px;width: 55px;"  onclick="check_department_type(17)"> 48 </td>
			 				<td class="center-align" style="height: 35px;width: 55px;"  onclick="check_department_type(18)"> 47 </td>
			 				<td class="center-align" style="height: 35px;width: 55px;"  onclick="check_department_type(19)"> 46 </td>
			 				<td class="center-align" style="height: 35px;width: 55px;"  onclick="check_department_type(20)"> 45 </td>
			 				<td class="center-align" style="height: 35px;width: 55px;"  onclick="check_department_type(21)"> 44 </td>
			 				<td class="center-align" style="height: 35px;width: 55px;"  onclick="check_department_type(22)"> 43 </td>
			 				<td class="center-align" style="height: 35px;width: 55px;"  onclick="check_department_type(23)"> 42 </td>
			 				<td class="center-align" style="height: 35px;width: 55px;"  onclick="check_department_type(24)"> 41 </td>
			 				
			 			</tr>
			 			<tr>
			 				<td class="center-align" style="height: 35px;width: 55px;"><?php echo $seventeen;?> </td>
			 				<td class="center-align" style="height: 35px;width: 55px;"><?php echo $eighteen;?></td>
			 				<td class="center-align" style="height: 35px;width: 55px;"><?php echo $nineteen;?></td>
			 				<td class="center-align" style="height: 35px;width: 55px;"><?php echo $twenty;?></td>
			 				<td class="center-align" style="height: 35px;width: 55px;"><?php echo $twenty_one;?></td>
			 				<td class="center-align" style="height: 35px;width: 55px;"><?php echo $twenty_two;?></td>
			 				<td class="center-align" style="height: 35px;width: 55px;"><?php echo $twenty_three;?></td>
			 				<td class="center-align" style="height: 35px;width: 55px;"><?php echo $twenty_four;?></td>
			 			</tr>
			 			
			 		</table>
			 	</div>
			</div>
			<div class="col-lg-6 col-md-6 col-sm-6"  style="border-right: 2px solid #000; border-left: 2px solid #000; ">			
			 	<div class="col-md-12">
			 		<table align='center' class='table table-striped table-condensed table-bordered'>
			 			<tr >
			 				<td class="center-align" style="height: 35px;width: 55px;"  onclick="check_department_type(25)"> 31 </td>
			 				<td class="center-align" style="height: 35px;width: 55px;"  onclick="check_department_type(26)"> 32 </td>
			 				<td class="center-align" style="height: 35px;width: 55px;"  onclick="check_department_type(27)"> 33 </td>
			 				<td class="center-align" style="height: 35px;width: 55px;"  onclick="check_department_type(28)"> 34 </td>
			 				<td class="center-align" style="height: 35px;width: 55px;"  onclick="check_department_type(29)"> 35 </td>
			 				<td class="center-align" style="height: 35px;width: 55px;"  onclick="check_department_type(30)"> 36 </td>
			 				<td class="center-align" style="height: 35px;width: 55px;"  onclick="check_department_type(31)"> 37 </td>
			 				<td class="center-align" style="height: 35px;width: 55px;"  onclick="check_department_type(32)"> 38 </td>
			 				
			 			</tr>
			 			<tr>
			 				<td class="center-align" style="height: 35px;width: 55px;"><?php echo $twenty_five;?> </td>
			 				<td class="center-align" style="height: 35px;width: 55px;"><?php echo $twenty_six;?></td>
			 				<td class="center-align" style="height: 35px;width: 55px;"><?php echo $twenty_seven;?></td>
			 				<td class="center-align" style="height: 35px;width: 55px;"><?php echo $twenty_eight;?></td>
			 				<td class="center-align" style="height: 35px;width: 55px;"><?php echo $twenty_nine;?></td>
			 				<td class="center-align" style="height: 35px;width: 55px;"><?php echo $thirty;?></td>
			 				<td class="center-align" style="height: 35px;width: 55px;"><?php echo $thirty_one;?></td>
			 				<td class="center-align" style="height: 35px;width: 55px;"><?php echo $thirty_two;?></td>
			 			</tr>
			 			
			 		</table>
			 	</div>
			</div>
		</div>
	</div>
</div>

<script type="text/javascript">
	
</script>
