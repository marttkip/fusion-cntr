<div id="loader" style="display: none;"></div>
<div style="margin-top:50px !important;"></div>
<?php if($mike == 1){

	}else{?>

		<?php
			$personnel_id = $this->session->userdata('personnel_id');
			$is_admin = $this->reception_model->check_personnel_department_id($personnel_id,1);
			$is_admin = $this->reception_model->check_personnel_department_id($personnel_id,1);
			$is_doctor = $this->reception_model->check_personnel_department_id($personnel_id,2);
			$is_nurse = $this->reception_model->check_personnel_department_id($personnel_id,8);
			$is_obs = $this->reception_model->check_personnel_department_id($personnel_id,10);
			$is_psychiatrist = $this->reception_model->check_personnel_department_id($personnel_id,13);
			$is_urologist = $this->reception_model->check_personnel_department_id($personnel_id,22);
			$is_peaditrician = $this->reception_model->check_personnel_department_id($personnel_id,23);
			$is_physician = $this->reception_model->check_personnel_department_id($personnel_id,24);


			// var_dump($is_physician);die();

			if(($is_doctor OR $is_peaditrician OR $is_urologist OR $is_psychiatrist OR $is_physician OR $is_obs) AND $doctor_id == 0 )
			{
				

			}
			else
			{
				?>

				<div class="well well-sm info">
					<h5 style="margin:0;">
						<div class="row">
							<div class="col-md-4">
								<div class="row">
									<div class="col-lg-4">
										<strong>Name:</strong>
									</div>
									<div class="col-lg-8">
										<?php echo $patient_surname.' '.$patient_othernames;?>
									</div>
								</div>
							</div>
							<div class="col-md-2">
								<div class="row">
									<div class="col-lg-6">
										<strong>Gender:</strong>
									</div>
									<div class="col-lg-6">
										<?php echo $gender;?>
									</div>
								</div>
							</div>
							<div class="col-md-2">
								<div class="row">
									<div class="col-lg-6">
										<strong>Age:</strong>
									</div>
									<div class="col-lg-6">
										<?php echo $patient_age;?>
									</div>
								</div>
							</div>
							<div class="col-md-2">
								<div class="row">
									<div class="col-lg-6">
										<strong>Balance:</strong>
									</div>
									<div class="col-lg-6">
										Kes <?php echo number_format($account_balance, 2);?>
									</div>
								</div>
							</div>
							<div class="col-md-2">
								<a href="<?php echo site_url();?>queue" class="btn btn-info btn-sm pull-right " ><i class="fa fa-arrow-left"></i> Back to Outpatient Queue</a>
							</div>
						</div>
					</h5>
					 
				</div>
				
				<?php
				

			}
		 } ?>


<div class="center-align">
	<?php
		$error = $this->session->userdata('error_message');
		$validation_error = validation_errors();
		$success = $this->session->userdata('success_message');
		
		if(!empty($error))
		{
		echo '<div class="alert alert-danger">'.$error.'</div>';
		$this->session->unset_userdata('error_message');
		}
		
		if(!empty($validation_error))
		{
		echo '<div class="alert alert-danger">'.$validation_error.'</div>';
		}
		
		if(!empty($success))
		{
		echo '<div class="alert alert-success">'.$success.'</div>';
		$this->session->unset_userdata('success_message');
		}
		?>
	<div class="clearfix"></div>
</div>
<div class="clearfix"></div>
<?php
if((($is_doctor OR $is_peaditrician OR $is_urologist OR $is_psychiatrist OR $is_physician OR $is_obs OR $is_admin OR $personnel_id == 0) AND $doctor_id > 0) OR $is_nurse)
{
echo $this->load->view("allergies_brief", '', TRUE);
?>

		<div class="tabbable" style="margin-bottom: 18px;">
			<ul class="nav nav-tabs nav-justified">
				<li class="active"><a href="#patient-history" data-toggle="tab">Visit's Timeline</a></li>
				<?php if($mike == 1){

				}else{
						?>
						<li ><a href="#vitals-pane" data-toggle="tab">Nursing Intake Assesment</a></li>
				<?php
				}
				?>
				<li><a href="#soap" data-toggle="tab">Doctor's Notes</a></li>
			</ul>
			<div class="tab-content" style="padding-bottom: 9px; border-bottom: 1px solid #ddd;">
				<div class="tab-pane active" id="patient-history">
					<div class="panel">
						<div class="panel-body">
							<div id="patient-history-view"></div>

						</div>
					</div>
				</div>	
			
				<div class="tab-pane " id="vitals-pane">
					<div class="panel">
						<div class="panel-body">
							<?php echo $this->load->view("patients/vitals", '', TRUE);?>
						</div>
					</div>
				</div>

				<div class="tab-pane" id="soap">
					<div class="panel">
						<div class="panel-body">
							<?php echo $this->load->view("patients/soap", '', TRUE);?>
						</div>
					</div>
				</div>
				<div class="tab-pane" id="medical-checkup"> 
				 	<div class="panel">
						<div class="panel-body">
							<?php //echo $this->load->view("patients/medical_checkup", '', TRUE);?>
						</div>
					</div>
				</div>
				<div class="tab-pane" id="investigations">
					<div class="panel">
						<div class="panel-body">
							<div id="patient-investigation-view"></div>
						</div>
					</div>
				</div>
				<div class="tab-pane" id="plan-visit">
					<div id="patient-plan-view"></div>
				</div>
				<div class="tab-pane" id="visit_trail">
					<?php //echo $this->load->view("patients/visit_trail", '', TRUE);?>
				</div>
			</div>
		</div>

<?php
}
?>
<?php if($mike == 1){
	}else{?>


<section class="panel">
	<div class="row">
		<?php if ($module == 0){?>
		<div class="col-md-2">
			<div class="center-align">
				<?php echo form_open("nurse/send_to_doctor/".$visit_id, array("class" => "form-horizontal"));?>
				<input type="submit" class="btn btn-sm btn-primary" value="Send To Doctor" onclick="return confirm('Send to Doctor?');"/>
				<?php echo form_close();?>
			</div>
		</div>
		<?php }
			if(($is_doctor OR $is_peaditrician OR $is_urologist OR $is_psychiatrist OR $is_physician OR $is_obs) AND $doctor_id == 0 AND $module == 1)
			{
				?>
				<div class="col-md-6">
					<div class="center-align">
						<?php echo form_open("nurse/attend_to_patient/".$visit_id."/".$module, array("class" => "form-horizontal"));?>
						<input type="submit" class="btn btn-sm btn-success center-align" value="Attend to patient" onclick="return confirm('Are you sure you want to attend to patient ?');"/>
						<?php echo form_close();?>
					</div>
				</div>
				<?php

			}
			else
			{
				?>
				<div class="col-md-2">
					<div class="center-align">
						<?php echo form_open("nurse/send_to_labs/".$visit_id."/".$module, array("class" => "form-horizontal"));?>
						<input type="submit" class="btn btn-sm btn-success center-align" value="Send To Laboratory" onclick="return confirm('Send to Laboratory?');"/>
						<?php echo form_close();?>
					</div>
				</div>
				<div class="col-md-2">
					<div class="center-align">
						<?php echo form_open("nurse/send_to_dental/".$visit_id."/".$module, array("class" => "form-horizontal"));?>
						<input type="submit" class="btn btn-sm btn-warning center-align" value="Send To Dental" onclick="return confirm('Send to Dental?');"/>
						<?php echo form_close();?>
					</div>
				</div>
				<div class="col-md-2">
					<div class="center-align">
						<?php echo form_open("nurse/send_to_xray/".$visit_id."/".$module, array("class" => "form-horizontal"));?>
						<input type="submit" class="btn btn-sm btn-danger center-align" value="Send To XRAY" onclick="return confirm('Send to XRAY?');"/>
						<?php echo form_close();?>
					</div>
				</div>
				<div class="col-md-2">
					<div class="center-align">
						<?php echo form_open("nurse/send_to_pharmacy/".$visit_id."/".$module, array("class" => "form-horizontal"));?>
						<input type="submit" class="btn btn-sm btn-warning center-align" value="Send To Pharmacy" onclick="return confirm('Send to Pharmacy?');"/>
						<?php echo form_close();?>
					</div>
				</div>
				<?php
				

			}
		?>
			

		
		<div class="col-md-2">
			<div class="center-align">
				<a href="<?php echo site_url();?>queues/outpatient-queue" class="btn btn-info btn-sm pull-right " ><i class="fa fa-arrow-left"></i> Back to Outpatient Queue</a>
			</div>
		</div>
	</div>
</section>
<?php } ?>
<script type="text/javascript">
	var config_url = document.getElementById("config_url").value;

	
	$(document).ready(function(){
		var visit_id = <?php echo $visit_id?>;
		var config_url = document.getElementById("config_url").value;
		get_patient_history(visit_id);

		
		// $.get(config_url+"nurse/get_family_history/<?php echo $visit_id;?>", function( data ) {
		// 	$("#new-nav").html(data);
		// 	$("#checkup_history").html(data);
		// });
	});

	function get_patient_history(visit_id)
	{
		 var XMLHttpRequestObject = false;       
		 if (window.XMLHttpRequest) {
		 
		   XMLHttpRequestObject = new XMLHttpRequest();
		 } 
		   
		 else if (window.ActiveXObject) {
		   XMLHttpRequestObject = new ActiveXObject("Microsoft.XMLHTTP");
		 }
		 var config_url = $('#config_url').val();
		 var url = config_url+"nurse/patient_history_view/"+visit_id;
		 // alert(url);
		 if(XMLHttpRequestObject) {
		   var obj = document.getElementById("patient-history-view");
		   XMLHttpRequestObject.open("GET", url);
		       
		   XMLHttpRequestObject.onreadystatechange = function(){
		     
		     if (XMLHttpRequestObject.readyState == 4 && XMLHttpRequestObject.status == 200) {
		       obj.innerHTML = XMLHttpRequestObject.responseText;
		       //obj2.innerHTML = XMLHttpRequestObject.responseText;
		       // alert("sadhkasjdhakj");
       			
		     }
		   }
		   
		   XMLHttpRequestObject.send(null);
		 }

	}
	function get_patient_investigation(visit_id)
	{
		 // document.getElementById("loader-circle").style.display = "block";
		 document.getElementById("loader").style.display = "block";
		 var XMLHttpRequestObject = false;       
		 if (window.XMLHttpRequest) {
		 
		   XMLHttpRequestObject = new XMLHttpRequest();
		 } 
		   
		 else if (window.ActiveXObject) {
		   XMLHttpRequestObject = new ActiveXObject("Microsoft.XMLHTTP");
		 }
		 var config_url = $('#config_url').val();
		 var url = config_url+"nurse/patient_investigation_view/"+visit_id;
		 if(XMLHttpRequestObject) {
		   var obj = document.getElementById("patient-investigation-view");
		   XMLHttpRequestObject.open("GET", url);
		       
		   XMLHttpRequestObject.onreadystatechange = function(){
		     
		     if (XMLHttpRequestObject.readyState == 4 && XMLHttpRequestObject.status == 200) {

		       obj.innerHTML = XMLHttpRequestObject.responseText;
		       //obj2.innerHTML = XMLHttpRequestObject.responseText;
		       $("#lab_test_id").customselect();
		       $("#xray_id").customselect();
		       get_lab_table(visit_id);
		        get_xray_table(visit_id);
		       document.getElementById("loader").style.display = "none";
		     }
		   }
		   
		   XMLHttpRequestObject.send(null);
		 }

	}

	function parse_xray(visit_id)
	{
	  var xray_id = document.getElementById("xray_id").value;
	  xray(xray_id, visit_id);

	}

	function xray(id, visit_id){
	    
	    var XMLHttpRequestObject = false;
	        
	    if (window.XMLHttpRequest) {
	    
	        XMLHttpRequestObject = new XMLHttpRequest();
	    } 
	        
	    else if (window.ActiveXObject) {
	        XMLHttpRequestObject = new ActiveXObject("Microsoft.XMLHTTP");
	    }
	    var url = "<?php echo site_url();?>radiology/xray/test_xray/"+visit_id+"/"+id;
	    // window.alert(url);
	    if(XMLHttpRequestObject) {
	                
	        XMLHttpRequestObject.open("GET", url);
	                
	        XMLHttpRequestObject.onreadystatechange = function(){
	            
	            if (XMLHttpRequestObject.readyState == 4 && XMLHttpRequestObject.status == 200) {
	                
	               document.getElementById("xray_table").innerHTML = XMLHttpRequestObject.responseText;
	               //get_xray_table(visit_id);
	            }
	        }
	        
	        XMLHttpRequestObject.send(null);
	    }
	}

	function get_xray_table(visit_id){
	    var XMLHttpRequestObject = false;
	        
	    if (window.XMLHttpRequest) {
	    
	        XMLHttpRequestObject = new XMLHttpRequest();
	    } 
	        
	    else if (window.ActiveXObject) {
	        XMLHttpRequestObject = new ActiveXObject("Microsoft.XMLHTTP");
	    }
	    var url = "<?php echo site_url();?>radiology/xray/test_xray/"+visit_id;
	    // alert(url);
	    if(XMLHttpRequestObject) {
	                
	        XMLHttpRequestObject.open("GET", url);
	                
	        XMLHttpRequestObject.onreadystatechange = function(){
	            
	            if (XMLHttpRequestObject.readyState == 4 && XMLHttpRequestObject.status == 200) {
	                
	                document.getElementById("xray_table").innerHTML = XMLHttpRequestObject.responseText;
	            }
	        }
	        
	        XMLHttpRequestObject.send(null);
	    }
	}

	function get_xray_scans(visit_id){
	    var XMLHttpRequestObject = false;
	        
	    if (window.XMLHttpRequest) {
	    
	        XMLHttpRequestObject = new XMLHttpRequest();
	    } 
	        
	    else if (window.ActiveXObject) {
	        XMLHttpRequestObject = new ActiveXObject("Microsoft.XMLHTTP");
	    }
	    var url = "<?php echo site_url();?>radiology/xray/test_scans/"+visit_id;
	    // alert(url);
	    if(XMLHttpRequestObject) {
	                
	        XMLHttpRequestObject.open("GET", url);
	                
	        XMLHttpRequestObject.onreadystatechange = function(){
	            
	            if (XMLHttpRequestObject.readyState == 4 && XMLHttpRequestObject.status == 200) {
	                
	                document.getElementById("xray_scans").innerHTML = XMLHttpRequestObject.responseText;
	            }
	        }
	        
	        XMLHttpRequestObject.send(null);
	    }
	}

	function get_patient_plan(visit_id)
	{
		

  		document.getElementById("loader").style.display = "block";
		 var XMLHttpRequestObject = false;       
		 if (window.XMLHttpRequest) {
		 
		   XMLHttpRequestObject = new XMLHttpRequest();
		 } 
		   
		 else if (window.ActiveXObject) {
		   XMLHttpRequestObject = new ActiveXObject("Microsoft.XMLHTTP");
		 }
		 var config_url = $('#config_url').val();
		 var url = config_url+"nurse/patient_plan_view/"+visit_id;
		 // alert(url);
		 if(XMLHttpRequestObject) {
		   var obj = document.getElementById("patient-plan-view");
		   XMLHttpRequestObject.open("GET", url);
		       
		   XMLHttpRequestObject.onreadystatechange = function(){
		     
		     if (XMLHttpRequestObject.readyState == 4 && XMLHttpRequestObject.status == 200) {

		       obj.innerHTML = XMLHttpRequestObject.responseText;
		       //obj2.innerHTML = XMLHttpRequestObject.responseText;
		       $("#drug_id").customselect();
		       $("#diseases_id").customselect();
		       get_disease(visit_id);
		       display_prescription(visit_id,0);
		       display_inpatient_prescription(visit_id,0);
		       	tinymce.init({
   					selector: ".cleditor",
   					height : "100"
   				});

		       document.getElementById("loader").style.display = "none";
		       // get_lab_table(visit_id);

		     }
		   }
		   
		   XMLHttpRequestObject.send(null);
		 }

	}

	// start of lab details
	function parse_lab_test(visit_id)
   {
     var lab_test_id = document.getElementById("lab_test_id").value;
      lab(lab_test_id, visit_id);
     
   }
    function get_lab_table(visit_id){
         var XMLHttpRequestObject = false;
             
         if (window.XMLHttpRequest) {
         
             XMLHttpRequestObject = new XMLHttpRequest();
         } 
             
         else if (window.ActiveXObject) {
             XMLHttpRequestObject = new ActiveXObject("Microsoft.XMLHTTP");
         }
         var config_url = $('#config_url').val();
         var url = config_url+"laboratory/test_lab/"+visit_id;

     
         if(XMLHttpRequestObject) {
                     
             XMLHttpRequestObject.open("GET", url);
                     
             XMLHttpRequestObject.onreadystatechange = function(){
                 
                 if (XMLHttpRequestObject.readyState == 4 && XMLHttpRequestObject.status == 200) {
                     
                     document.getElementById("lab_table").innerHTML = XMLHttpRequestObject.responseText;
                 }
             }
             
             XMLHttpRequestObject.send(null);
         }
     }
   
    function lab(id, visit_id){
     
     var XMLHttpRequestObject = false;
         
     if (window.XMLHttpRequest) {
     
         XMLHttpRequestObject = new XMLHttpRequest();
     } 
         
     else if (window.ActiveXObject) {
         XMLHttpRequestObject = new ActiveXObject("Microsoft.XMLHTTP");
     }
     var config_url = $('#config_url').val();
     var url = config_url+"laboratory/test_lab/"+visit_id+"/"+id;
     // window.alert(url);
     if(XMLHttpRequestObject) {
                 
         XMLHttpRequestObject.open("GET", url);
                 
         XMLHttpRequestObject.onreadystatechange = function(){
             
             if (XMLHttpRequestObject.readyState == 4 && XMLHttpRequestObject.status == 200) {
                 
                document.getElementById("lab_table").innerHTML = XMLHttpRequestObject.responseText;
                //get_lab_table(visit_id);
             }
         }
         
         XMLHttpRequestObject.send(null);
     }
   }
   function view_previous_card(visit_id)
   {
   		open_sidebar();

   		var config_url = $('#config_url').val();
	      var data_url = config_url+"nurse/previous_card/"+visit_id;
	      // window.alert(data_url);
	      $.ajax({
	      type:'POST',
	      url: data_url,
	      data:{visit_id: visit_id},
	      dataType: 'text',
	      success:function(data){
	      //window.alert("You have successfully updated the symptoms");
	      //obj.innerHTML = XMLHttpRequestObject.responseText;
	       $("#sidebar-div").html(data);

	        $('.datepicker').datepicker({
	              format: 'yyyy-mm-dd'
	          });

	        // $('.datepicker').datepicker();
	        $('.timepicker').timepicker();
	        // vitals_interface_old(visit_id);
	        // alert(data);
	      },
	      error: function(xhr, status, error) {
	      //alert("XMLHttpRequest=" + xhr.responseText + "\ntextStatus=" + status + "\nerrorThrown=" + error);
	      alert(error);
	      }

	      });
   }
   
	// end of lab details
</script>




