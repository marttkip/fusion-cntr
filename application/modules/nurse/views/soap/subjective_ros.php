<?php
	$get_objective_rs = $this->medical_admin_model->subjective_ros_findings();
	$num_rows = count($get_objective_rs);
	
	$rs2 = $this->nurse_model->get_visit_subjective_findings($visit_id);
	$num_rows2 = count($rs2);

	$subjective_roses = $this->medical_admin_model->get_subjective_ros_classes($visit_id,$is_report);

	$patient_id = $this->nurse_model->get_patient_id($visit_id);
?>

<?php echo form_open("reception/register-other-patient", array("class" => "form-horizontal"));?>

<div class="row">
	<div class="col-md-12">
		
		 <section class="panel panel-featured panel-featured-info">
			<header class="panel-heading">
		        <h2 class="panel-title">Systems Review</h2>
		        <div class="pull-right">
			        
			    </div>
		    </header>

			<div class="panel-body">
            	<div class="col-md-12">
					<?php
	                $count = 0;
	                $prev_name = '';
					
					if($subjective_roses->num_rows() > 0)
					{
						foreach($subjective_roses->result() as $res)
						{
							$class_name = $res->subjective_ros_class_name;
							$class_id = $res->subjective_ros_class_id;  


							$system_visit_notes_rs = $this->nurse_model->get_visit_system_notes($class_id,$visit_id);
							$description = '';
							if($system_visit_notes_rs->num_rows() > 0)
							{
								foreach ($system_visit_notes_rs->result() as $key => $value) {
									# code...
									$description = $value->description;
								}

								$checked = 'checked';
								$display = 'block';
								$count =1;
							}
							else
							{
								$display = 'none';
								$checked = '';
								$count =0;
							}

							
							?>
							<div class="row">
								
								<div class="col-md-6">
									<input type="checkbox" name="link1" value="link"  <?php echo $checked;?> onclick="add_subjective_ros_findings(<?php echo $class_id;?>,<?php echo $visit_id;?>,<?php echo $count;?>)">	<strong ><?php echo strtoupper($class_name);?>  </strong>
								</div>
								<div class="col-md-6">
									<?php
									if($is_report == 1)
									{

										if($class_id < 8)
										{


										?>
											<div id="" style="min-height: 100px;border: 1px solid #000;padding: 10px" > <?php echo $description;?></div>
								
										<?php
										}
									}
									else
									{

										if($class_id < 8)
										{
											?>
											<div  style="display:<?php echo $display?>"> <textarea class="form-control" id="note-text-area<?php echo $class_id;?>" rows="5" onkeyup="update_subjective_system_notes(<?php echo $class_id;?>,<?php echo $visit_id;?>)"><?php echo $description?></textarea> </div>
											<?php
										}
									}
									?>
									
								</div>
							
								
						
							</div>
							
							<?php 
						}
					}
	                ?>
	            </div>
					    
            </div>
        </section>
		
    </div>
</div>
<?php echo form_close();?>

<script type="text/javascript">
	
	function close_objective_findings(visit_id)
	{
		window.close(this);
	}

function add_objective_findings(objective_findings_id, visit_id, status)
{
	
  var XMLHttpRequestObject = false;
    
  if (window.XMLHttpRequest) {
  
    XMLHttpRequestObject = new XMLHttpRequest();
  } 
    
  else if (window.ActiveXObject) {
    XMLHttpRequestObject = new ActiveXObject("Microsoft.XMLHTTP");
  }
  var config_url = $('#config_url').val();
  var url = config_url+"nurse/add_objective_findings/"+objective_findings_id+"/"+visit_id+"/"+status;
 
  if(XMLHttpRequestObject) {
    var obj3 = window.opener.document.getElementById("visit_objective_findings1");
    XMLHttpRequestObject.open("GET", url);
        
    XMLHttpRequestObject.onreadystatechange = function(){
      
      if (XMLHttpRequestObject.readyState == 4 && XMLHttpRequestObject.status == 200) 
	  {
		  	var myTarget = document.getElementById('objective_check');
			
			if(status == '1')
			{
				myTarget.value = '0';
			}
			
			else
			{
				myTarget.value = '1';
			}
        	obj3.innerHTML = XMLHttpRequestObject.responseText;
      }
    }
        
    XMLHttpRequestObject.send(null);
  }
}

function toggleField(objective_findings) 
{
	var myTarget = document.getElementById(objective_findings);

	if(myTarget.style.display == 'none'){
  		myTarget.style.display = 'block';
    } 
	
	else {
	  myTarget.style.display = 'none';
	  myTarget.value = '';
	}
}


function update_visit_obj(objective_findings_id,visit_id,update_id){
	
	var XMLHttpRequestObject = false;
		
	if (window.XMLHttpRequest) {
	
		XMLHttpRequestObject = new XMLHttpRequest();
	}
	else if (window.ActiveXObject) {
		XMLHttpRequestObject = new ActiveXObject("Microsoft.XMLHTTP");
	}
	
	var id= "myTF".concat(objective_findings_id);
	var description = document.getElementById(id).value;
	var config_url = $('#config_url').val();
  	var url = config_url+"nurse/update_objective_findings/"+objective_findings_id+"/"+visit_id+"/"+update_id+"/"+description;
		if(XMLHttpRequestObject) {
				
		XMLHttpRequestObject.open("GET", url);
				
		XMLHttpRequestObject.onreadystatechange = function(){
			
			if (XMLHttpRequestObject.readyState == 4 && XMLHttpRequestObject.status == 200) {
				var obj3 = window.opener.document.getElementById("visit_objective_findings1");
				obj3.innerHTML = XMLHttpRequestObject.responseText;
			}
		}
				
		XMLHttpRequestObject.send(null);
	}
}
</script>