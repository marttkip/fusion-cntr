<?php
	$get_objective_rs = $this->medical_admin_model->objective_ros_findings();
	$num_rows = count($get_objective_rs);
	
	$rs2 = $this->nurse_model->get_visit_objective_ros_findings($visit_id);
	$num_rows2 = count($rs2);

	$objective_roses = $this->medical_admin_model->get_objective_ros_classes();
?>

<?php echo form_open("reception/register-other-patient", array("class" => "form-horizontal"));?>

<div class="row">
	<div class="col-md-12">
		
		<section class="panel">

			<div class="panel-body">
            	
				<?php
                $count = 0;
                $prev_name = '';
				
				if($objective_roses->num_rows() > 0)
				{
					foreach($objective_roses->result() as $res)
					{
						$class_name = $res->objective_ros_class_name;
						$class_id = $res->objective_ros_class_id;   
						?>
						<div class="row">
						
						<h2 class="panel-title"><strong><?php echo $class_name;?> : </strong></h2>
					
						<?php 
						
						foreach ($get_objective_rs as $key2):
							$class_id2 = $key2->objective_ros_findings_class_id;
							
							if($class_id == $class_id2)
							{
								$s = $count;
								

								$objective_id = $key2->objective_ros_findings_id;
								$status = 0;
								$description= '';
							
								if($num_rows2 > 0)
								{
									foreach ($rs2 as $key)
									{
										$objective_ros_findings_id = $key->objective_ros_findings_id;
										
										if($objective_ros_findings_id == $objective_id)
										{
											$status = 1;
											$objective_ros_findings_name = $key->objective_ros_findings_name;
											$visit_objective_ros_findings_id = $key->visit_objective_ros_findings_id;
											// $objective_ros_name = $key->objective_ros_name;
											$description= $key->description;
											break;
										}
									}
								}
								?>
							
								<!-- <div class="col-md-2">
									<div class="checkbox"> -->
										<label>
											<?php 
												if($status == 1)
												{
													?>
													<!-- <input name="<?php echo $objective_name?>" type="checkbox" onClick="add_objective_ros_findings(<?php echo $objective_id;?>, <?php echo $visit_id?>, <?php echo $status;?>);" checked="checked" id="objective_check"> -->


											<?php
											$objective_name = $key2->objective_ros_findings_name_opp;
											$item = '';
											$style = 'style="border: #F00 solid 2px;padding: 2px;"';
												}
												
												else
												{
													?>
													<!-- <input name="<?php echo $objective_name?>" type="checkbox" onClick="add_objective_ros_findings(<?php echo $objective_id;?>, <?php echo $visit_id?>, <?php echo $status;?>);" id="objective_check"> -->
											<?php
											$item = 'no ';
											$style = '';
											$objective_name = $key2->objective_ros_findings_name;
												}
											?>
											
											<h4 <?php echo $style;?>><a  onClick="add_objective_ros_findings(<?php echo $objective_id;?>, <?php echo $visit_id?>, <?php echo $status;?>);"> <?php echo $objective_name.' '?>;  </a>
											</h4>
										</label>
									<!-- </div>	
								</div>  -->
								<?php 
							}
							$count++;
						endforeach;     
						?>
						</div>
						<?php 
					}
				}
                ?>
					    
            </div>
        </section>
		
    </div>
</div>
<?php echo form_close();?>

<script type="text/javascript">
	
	function close_objective_findings(visit_id)
	{
		window.close(this);
	}

function add_objective_findings(objective_findings_id, visit_id, status)
{
	
  var XMLHttpRequestObject = false;
    
  if (window.XMLHttpRequest) {
  
    XMLHttpRequestObject = new XMLHttpRequest();
  } 
    
  else if (window.ActiveXObject) {
    XMLHttpRequestObject = new ActiveXObject("Microsoft.XMLHTTP");
  }
  var config_url = $('#config_url').val();
  var url = config_url+"nurse/add_objective_findings/"+objective_findings_id+"/"+visit_id+"/"+status;
 
  if(XMLHttpRequestObject) {
    var obj3 = window.opener.document.getElementById("visit_objective_findings1");
    XMLHttpRequestObject.open("GET", url);
        
    XMLHttpRequestObject.onreadystatechange = function(){
      
      if (XMLHttpRequestObject.readyState == 4 && XMLHttpRequestObject.status == 200) 
	  {
		  	var myTarget = document.getElementById('objective_check');
			
			if(status == '1')
			{
				myTarget.value = '0';
			}
			
			else
			{
				myTarget.value = '1';
			}
        	obj3.innerHTML = XMLHttpRequestObject.responseText;
      }
    }
        
    XMLHttpRequestObject.send(null);
  }
}

function toggleField(objective_findings) 
{
	var myTarget = document.getElementById(objective_findings);

	if(myTarget.style.display == 'none'){
  		myTarget.style.display = 'block';
    } 
	
	else {
	  myTarget.style.display = 'none';
	  myTarget.value = '';
	}
}


function update_visit_obj(objective_findings_id,visit_id,update_id){
	
	var XMLHttpRequestObject = false;
		
	if (window.XMLHttpRequest) {
	
		XMLHttpRequestObject = new XMLHttpRequest();
	}
	else if (window.ActiveXObject) {
		XMLHttpRequestObject = new ActiveXObject("Microsoft.XMLHTTP");
	}
	
	var id= "myTF".concat(objective_findings_id);
	var description = document.getElementById(id).value;
	var config_url = $('#config_url').val();
  	var url = config_url+"nurse/update_objective_findings/"+objective_findings_id+"/"+visit_id+"/"+update_id+"/"+description;
		if(XMLHttpRequestObject) {
				
		XMLHttpRequestObject.open("GET", url);
				
		XMLHttpRequestObject.onreadystatechange = function(){
			
			if (XMLHttpRequestObject.readyState == 4 && XMLHttpRequestObject.status == 200) {
				var obj3 = window.opener.document.getElementById("visit_objective_findings1");
				obj3.innerHTML = XMLHttpRequestObject.responseText;
			}
		}
				
		XMLHttpRequestObject.send(null);
	}
}
</script>