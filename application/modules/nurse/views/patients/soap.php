<?php echo form_open("reception/register-other-patient", array("class" => "form-horizontal"));?>
<?php
if($dental == 1)
{

}
else{
// echo "damsdkajhsk";
?>
<div class='row'>
    <div class='center-align '>
        <a class='btn btn-info btn-md' type='submit' onclick='save_soap("<?php echo $visit_id;?>")'>Save SOAP</a>
    </div>
</div>
  <br/>

  <div class="row">
    <div class="col-md-12">
        <section class="panel panel-featured panel-featured-info">
            
            <div class="panel-heading with-border">
              <h4 class="panel-title"><i class="icon-reorder"></i>HISTORIES</h4>
              <div class="panel-tools pull-right">
                <a href="#" class="wminimize"><i class="icon-chevron-up"></i></a> 
                <a href="#" class="wclose"><i class="icon-remove"></i></a>
              </div>
              <div class="clearfix"></div>
            </div>             
                <!-- Widget content -->
                <div class="panel-body">
                  <div class="padd">
                    <div class="col-md-6">                        
                        <h3 class="center-align">CHIEF COMPLAINT</h3>
                        <br>
                        <div id="chief-complain"></div>                        
                    </div>
                    <div class="col-md-6">
                        <h3 class="center-align">Hx of P.I</h3>
                        <br>
                        <div id="h-p-i"></div>                      
                    </div>
                    
                   
                </div>
            </div>

        </section>
    </div>    
</div>

<div class="row">
  <div class="col-md-12">
        <div class="row">
            <div class="col-md-12">

              <!-- Widget -->
               <section class="panel panel-featured panel-featured-info">
                    <!-- Widget head -->
                    <div class="panel-heading with-border">
                      <h3 class="panel-title"><i class="icon-reorder"></i> Review of Systems </h3>
                      
                      <div class="clearfix"></div>
                    </div> 
                    <div class="panel-body">
                      <div class="padd"> 
                        <div class="col-md-6">
                           <div id="subjective-ros"></div>  
                        </div> 
                        <div class="col-md-6">
                          <div id="objective-ros"></div>  
                        </div>
                       
                      </div>
                    </div>
                
              </section>
            </div>
        </div>
    </div>
   
</div>

<div class="row">
 <div class="col-md-12">
        <div class="row">
            <div class="col-md-12">

              <!-- Widget -->
              <section class="panel panel-featured panel-featured-info">
                    <!-- Widget head -->
                    <div class="panel-heading with-border">
                      <h3 class="panel-title"><i class="icon-reorder"></i>Assessment & Diagnosis</h3>
                      <div class="widget-tools pul-right">
                       
                        <!-- <input type='button' class='btn btn-sm btn-primary'  data-toggle='control-sidebar'  value='ICD 10 Diagnosis' onclick='icd10codes_sidebar(<?php echo $visit_id;?>)'/> -->
                      </div>
                      <div class="clearfix"></div>
                    </div>             

                <!-- Widget content -->
                    <div class="panel-body">
                        <div class="padd">

                          

                          <div class="col-md-6">
                             <h4 class="center-align"> ASSESMENT</h4>
                             <br>
                              <!-- vitals from java script -->
                                <div id="assessment"></div>
                                <!-- end of vitals data -->
                            
                          </div>
                          <div class="col-md-6">
                             <a class="btn btn-sm btn-warning pull-right" onclick="icd10codes_sidebar(<?php echo $visit_id;?>)"><i class="fa fa-arrow-left"></i> ICD 10 Diagnosis</a>
                            <h4 class="center-align"> DIAGNOSIS</h4>
                            <br>
                            <div id='visit_diagnosis_original'></div>
                          </div>
                             
                               
                        </div>

                     </div>
                
                </section>
            </div>
        </div>
    </div>
</div>
<?php
}
?>


<div class="row">
 
    <div class="col-md-12">
       <div class="row">
        <div class="col-md-12">

          <!-- Widget -->
          <div class="panel">
                <!-- Widget head -->
                <div class="panel-heading with-border">
                  <h3 class="panel-title"><i class="icon-reorder"></i>Plan</h3>
                  <div class="panel-tools pull-right">
                    <a href="#" class="wminimize"><i class="icon-chevron-up"></i></a> 
                    <a href="#" class="wclose"><i class="icon-remove"></i></a>
                  </div>
                  <div class="clearfix"></div>
                </div>             

            <!-- Widget content -->
                <div class="panel-body">
                     
                      <div class='col-md-12'>
                        <div class="center-align">
                           <a class="btn btn-sm btn-warning "   onclick='laboratory_sidebar(<?php echo $visit_id;?>)'><i class="fa fa-arrow-left"></i> Laboratory Test 
                           </a>
             
                        </div>
                      </div>
                      <div class='col-md-12'>
                         <h4>LAB ORDER </h4>
                        <div class="tabbable" style="margin-top: 18px;">
                          <ul class="nav nav-tabs nav-justified">
                            <li class="active"><a href="#patient-lab-test" data-toggle="tab">Lab Test</a></li>
                            <li><a href="#lab-results-view" data-toggle="tab">Lab Test Results</a></li>
                          </ul>
                          <div class="tab-content" style="padding-bottom: 9px; border-bottom: 1px solid #ddd;">
                            <div class="tab-pane active" id="patient-lab-test">
                              <div class="panel">
                                <div class="panel-body">
                                  <div id="lab_table"></div>
                                </div>
                              </div>
                            </div> 

                            <div class="tab-pane" id="lab-results-view">
                              <div class="panel">
                                <div class="panel-body">
                                 <?php
                                  $data['visit_id'] = $visit_id;
                                  $data['lab_test'] = 100; 

                                  echo $this->load->view("laboratory/tests/test2", $data, TRUE); ?>
                                </div>
                              </div>
                            </div> 

                            
                          </div> 
                        </div>
                         <h4>DIAGNOSTIC PROCEDURES</h4>
                         <div class='col-md-12'>
                          <div class="center-align">
                             <a class="btn btn-sm btn-warning"   onclick='radiology_sidebar(<?php echo $visit_id;?>)'> Radiology</a>               
                             <a class='btn btn-primary' value='Theatre' onclick='theatre_sidebar(<?php echo $visit_id;?>)'> Theatre</a>
               
                          </div>
                          <div class="col-md-6">
                            <h4>XRAYs's Tests</h4>
                            <div id="radiology_table"></div>
                            <div id="xray_scans"></div>
                            <div class="row">
                              <div class='center-align'>
                              <a href='<?php echo site_url()."ultra_sound/print_test_request/".$visit_id;?>' class='btn btn-sm btn-warning' target='_blank'>Print Request</a>
                            </div>
                            </div>
                          </div>

                          <div class="col-md-6">
                               <h4>Theatre's Procedures</h4>
                            <div id="theatre_table"></div>
                          </div>
                        </div>
                        <hr>
                        <h4>Prescriptions</h4>
                        <div class="center-align">
                          <a class="btn btn-sm btn-warning" onclick='prescription_sidebar(<?php echo $visit_id;?>)' > Prescription </a>                  
                        </div>
                        <div class="col-md-12">
                          <div id="visit_prescription"></div>
                        </div>

                      </div>

                      
                      <div class='col-md-12'>
                        <div class='col-md-6'>
                           <div class="col-md-12">
                              <h4> - NEXT APPOINTMENT </h4>
                              <br>
                              <div class="center-align">
                                <a class="btn btn-sm btn-warning "   onclick='appointment_sidebar(<?php echo $patient_id;?>,<?php echo $visit_id;?>,<?php echo $department_id;?>,<?php echo $personnel_id;?>)' > Add appointment Detail </a>                  
                              </div>
                              <div id="next-appointment-view"></div>
                              

                           </div>
                        </div>
                        <div class='col-md-6'>
                          <h4> - REFERAL NOTE </h4>
                          <br>
                          <div class='col-md-12'>
                            <div id="other-plans"></div>
                          </div>

                        </div>
                      </div>

                      
                  
                  
                </div>
            </div>
        </div>
        </div>
    </div>
</div>



<div class='row'>
    <div class='center-align '>
        <a class='btn btn-info btn-md' type='submit' onclick='save_soap("<?php echo $visit_id;?>")'>Save SOAP</a>

        <a class='btn btn-warning btn-md' target="_blank" href="<?php echo base_url().'print-patient-chart/'.$visit_id?>" >Print Patient Chart</a>
    </div>
</div>
<?php echo form_close();?>




<script type="text/javascript">

  $(document).ready(function(){
      symptoms(<?php echo $visit_id;?>);
      objective_findings(<?php echo $visit_id;?>);
      // assessment(<?php echo $visit_id;?>);
      plan(<?php echo $visit_id;?>);
      doctor_notes(<?php echo $visit_id;?>);
      nurse_notes(<?php echo $visit_id?>);
      get_disease(<?php echo $visit_id?>);
      display_inpatient_prescription(<?php echo $visit_id?>,0);
      get_lab_table_view(<?php echo $visit_id?>);
      get_xray_scans(<?php echo $visit_id?>);
      get_radiology_table_view(<?php echo $visit_id?>);
      get_theatre_table_view(<?php echo $visit_id?>);
      // display_inpatient_prescription(visit_id,module)
      subjective_ros(<?php echo $visit_id?>);
      objective_ros(<?php echo $visit_id?>);
      display_referal_note(<?php echo $visit_id?>);
      display_action_plan(<?php echo $visit_id?>);
      display_notes(<?php echo $visit_id?>,14);
      display_notes(<?php echo $visit_id?>,15);
      display_notes(<?php echo $visit_id?>,16);
      display_notes(<?php echo $visit_id?>,5);

      display_next_appointment(<?php echo $patient_id?>,<?php echo $visit_id?>);


      
  });
  
  function symptoms(visit_id){

  var XMLHttpRequestObject = false;
    
  if (window.XMLHttpRequest) {
  
    XMLHttpRequestObject = new XMLHttpRequest();
  } 
    
  else if (window.ActiveXObject) {
    XMLHttpRequestObject = new ActiveXObject("Microsoft.XMLHTTP");
  }
  
   var config_url = $('#config_url').val();

  var url = config_url+"nurse/view_symptoms/"+visit_id;
  // alert(url);
  if(XMLHttpRequestObject) {
    
    var obj = document.getElementById("symptoms");
      
    XMLHttpRequestObject.open("GET", url);
        
    XMLHttpRequestObject.onreadystatechange = function(){
      
      if (XMLHttpRequestObject.readyState == 4 && XMLHttpRequestObject.status == 200) {
        obj.innerHTML = XMLHttpRequestObject.responseText;
        
        // patient_details(visit_id);
        // objective_findings(visit_id);
        // assessment(visit_id);
        // plan(visit_id);
        // doctor_notes(visit_id);
        // nurse_notes(visit_id);
      }
    }
        
    XMLHttpRequestObject.send(null);
  }
}


function objective_findings(visit_id){
  var XMLHttpRequestObject = false;
    
  if (window.XMLHttpRequest) {
  
    XMLHttpRequestObject = new XMLHttpRequest();
  } 
    
  else if (window.ActiveXObject) {
    XMLHttpRequestObject = new ActiveXObject("Microsoft.XMLHTTP");
  }
   var config_url = $('#config_url').val();
  var url = config_url+"nurse/view_objective_findings/"+visit_id;
  // alert(url);
  if(XMLHttpRequestObject) {
    
    var obj = document.getElementById("objective_findings");
        
    XMLHttpRequestObject.open("GET", url);
        
    XMLHttpRequestObject.onreadystatechange = function(){
      
      if (XMLHttpRequestObject.readyState == 4 && XMLHttpRequestObject.status == 200) {
        obj.innerHTML = XMLHttpRequestObject.responseText;
      }
    }
        
    XMLHttpRequestObject.send(null);
  }
}
function save_doctor_notes(visit_id){
      var config_url = $('#config_url').val();
        var data_url = config_url+"nurse/save_doctor_notes/"+visit_id;
        //window.alert(data_url);
         var doctor_notes = $('#doctor_notes_item').val();//document.getElementById("vital"+vital_id).value;
        $.ajax({
        type:'POST',
        url: data_url,
        data:{notes: doctor_notes},
        dataType: 'text',
        success:function(data){
        //obj.innerHTML = XMLHttpRequestObject.responseText;
        },
        error: function(xhr, status, error) {
        //alert("XMLHttpRequest=" + xhr.responseText + "\ntextStatus=" + status + "\nerrorThrown=" + error);
        alert(error);
        }

        });

      
}

function assessment(visit_id){
  var XMLHttpRequestObject = false;
    
  if (window.XMLHttpRequest) {
  
    XMLHttpRequestObject = new XMLHttpRequest();
  } 
    
  else if (window.ActiveXObject) {
    XMLHttpRequestObject = new ActiveXObject("Microsoft.XMLHTTP");
  }
  var config_url = $('#config_url').val();
  var url = config_url+"nurse/view_assessment/"+visit_id;
  
  if(XMLHttpRequestObject) {
    
    var obj = document.getElementById("assessment");
        
    XMLHttpRequestObject.open("GET", url);
        
    XMLHttpRequestObject.onreadystatechange = function(){
      
      if (XMLHttpRequestObject.readyState == 4 && XMLHttpRequestObject.status == 200) {
        obj.innerHTML = XMLHttpRequestObject.responseText;
      }
    }
        
    XMLHttpRequestObject.send(null);
  }
}

function plan(visit_id){
  var XMLHttpRequestObject = false;
    
  if (window.XMLHttpRequest) {
  
    XMLHttpRequestObject = new XMLHttpRequest();
  } 
    
  else if (window.ActiveXObject) {
    XMLHttpRequestObject = new ActiveXObject("Microsoft.XMLHTTP");
  }
  var config_url = $('#config_url').val();
  var url = config_url+"nurse/view_plan/"+visit_id;
  
  if(XMLHttpRequestObject) {
    
    var obj = document.getElementById("plan");
        
    XMLHttpRequestObject.open("GET", url);
        
    XMLHttpRequestObject.onreadystatechange = function(){
      
      if (XMLHttpRequestObject.readyState == 4 && XMLHttpRequestObject.status == 200) {
        obj.innerHTML = XMLHttpRequestObject.responseText;
         get_test_results(100, visit_id);
         closeit(79, visit_id);
         display_prescription(visit_id, 2);
      }
    }
        
    XMLHttpRequestObject.send(null);
  }
}

function get_test_results(page, visit_id){

  var XMLHttpRequestObject = false;
    
  if (window.XMLHttpRequest) {
  
    XMLHttpRequestObject = new XMLHttpRequest();
  } 
    
  else if (window.ActiveXObject) {
    XMLHttpRequestObject = new ActiveXObject("Microsoft.XMLHTTP");
  }
  var config_url = $('#config_url').val();
  if((page == 1) || (page == 65) || (page == 85)){
  // request from the lab
    url = config_url+"/laboratory/test/"+visit_id;
  }
  
  else if ((page == 75) || (page == 100)){
    // this is for the doctor and the nurse
    url = config_url+"/laboratory/test2/"+visit_id;
  }
  else
  {
    url = config_url+"/laboratory/test2/"+visit_id;
  }
  // alert(url);
  if(XMLHttpRequestObject) {
    if((page == 75) || (page == 85)){
      var obj = window.opener.document.getElementById("test_results");
    }
    else{
      var obj = document.getElementById("test_results");
    }
    XMLHttpRequestObject.open("GET", url);
    
    XMLHttpRequestObject.onreadystatechange = function(){
    
      if (XMLHttpRequestObject.readyState == 4 && XMLHttpRequestObject.status == 200) {
  //window.alert(XMLHttpRequestObject.responseText);
        obj.innerHTML = XMLHttpRequestObject.responseText;
        if((page == 75) || (page == 85)){
          window.close(this);
        }
        
      }
    }
    XMLHttpRequestObject.send(null);
  }
}



function display_prescription(visit_id, page){
  
  var XMLHttpRequestObject = false;
    
  if (window.XMLHttpRequest) {
  
    XMLHttpRequestObject = new XMLHttpRequest();
  } 
    
  else if (window.ActiveXObject) {
    XMLHttpRequestObject = new ActiveXObject("Microsoft.XMLHTTP");
  }
   var config_url = $('#config_url').val();
  var url = config_url+"/pharmacy/display_prescription/"+visit_id;
  
  if(page == 1){
    var obj = window.opener.document.getElementById("prescription");
  }
  
  else{
    var obj = document.getElementById("prescription");
  }
  if(XMLHttpRequestObject) {
        
    XMLHttpRequestObject.open("GET", url);
        
    XMLHttpRequestObject.onreadystatechange = function(){
      
      if (XMLHttpRequestObject.readyState == 4 && XMLHttpRequestObject.status == 200) {
        obj.innerHTML = XMLHttpRequestObject.responseText;
        
        if(page == 1){
          window.close(this);
        
        }
        plan(visit_id);
      }
    }
    
    XMLHttpRequestObject.send(null);
  }
}

function open_window_lab(test, visit_id){
  var config_url = $('#config_url').val();
  window.open(config_url+"/laboratory/laboratory_list/"+test+"/"+visit_id,"Popup","height=1200, width=800, , scrollbars=yes, "+ "directories=yes,location=yes,menubar=yes," + "resizable=no status=no,history=no top = 50 left = 100");
}

function open_symptoms(visit_id){
  var config_url = $('#config_url').val();
  window.open(config_url+"nurse/symptoms_list/"+visit_id,"Popup","height=1000,width=600,,scrollbars=yes,"+ 
                        "directories=yes,location=yes,menubar=yes," + 
                         "resizable=no status=no,history=no top = 50 left = 100");
    
  
}

function open_objective_findings(visit_id){
  var config_url = $('#config_url').val();
  window.open(config_url+"nurse/objective_finding/"+visit_id,"Popup","height=600,width=1000,,scrollbars=yes,"+ 
                        "directories=yes,location=yes,menubar=yes," + 
                         "resizable=no status=no,history=no top = 50 left = 100");
    
  
}


function save_assessment(visit_id){
 
  var config_url = $('#config_url').val();
  var data_url = config_url+"nurse/save_assessment/"+visit_id;
  //window.alert(data_url);
   var assessment = $('#visit_assessment').val();//document.getElementById("vital"+vital_id).value;
  $.ajax({
  type:'POST',
  url: data_url,
  data:{notes: assessment},
  dataType: 'text',
  success:function(data){
    //window.alert("You have successfully updated the assessment");
  //obj.innerHTML = XMLHttpRequestObject.responseText;
  },
  error: function(xhr, status, error) {
  //alert("XMLHttpRequest=" + xhr.responseText + "\ntextStatus=" + status + "\nerrorThrown=" + error);
  alert(error);
  }

  });
}

function open_window(plan, visit_id){
    var config_url = $('#config_url').val();
  if(plan == 6){
  
    window.open(config_url+"nurse/disease/"+visit_id,"Popup","height=1000,width=600,,scrollbars=yes,"+ 
                        "directories=yes,location=yes,menubar=yes," + 
                         "resizable=no status=no,history=no top = 50 left = 100");
  }
  else if (plan == 1){
    
    window.open(config_url+"/pharmacy/prescription/"+visit_id,"Popup","height=1200,width=1300,,scrollbars=yes,"+ 
                        "directories=yes,location=yes,menubar=yes," + 
                         "resizable=yes status=yes,history=yes top = 50 left = 100");
  }
}



function doctor_notes(visit_id){
    var XMLHttpRequestObject = false;
    
  if (window.XMLHttpRequest) {
  
    XMLHttpRequestObject = new XMLHttpRequest();
  } 
    
  else if (window.ActiveXObject) {
    XMLHttpRequestObject = new ActiveXObject("Microsoft.XMLHTTP");
  }
  
  var config_url = $('#config_url').val();
  var url = config_url+"nurse/doctor_notes/"+visit_id;
  
  if(XMLHttpRequestObject) {
    
    var obj = document.getElementById("doctor_notes");
        
    XMLHttpRequestObject.open("GET", url);
        
    XMLHttpRequestObject.onreadystatechange = function(){
      
      if (XMLHttpRequestObject.readyState == 4 && XMLHttpRequestObject.status == 200) {
        obj.innerHTML = XMLHttpRequestObject.responseText;
        symptoms3(visit_id);
        
      }
    }
        
    XMLHttpRequestObject.send(null);
  }
}


function nurse_notes(visit_id){
    var XMLHttpRequestObject = false;
    
  if (window.XMLHttpRequest) {
  
    XMLHttpRequestObject = new XMLHttpRequest();
  } 
    
  else if (window.ActiveXObject) {
    XMLHttpRequestObject = new ActiveXObject("Microsoft.XMLHTTP");
  }
  var config_url = $('#config_url').val();
  var url = config_url+"nursenurse_notes/"+visit_id;
  
  if(XMLHttpRequestObject) {
    
    var obj = document.getElementById("nurse_notes");
        
    XMLHttpRequestObject.open("GET", url);
        
    XMLHttpRequestObject.onreadystatechange = function(){
      
      if (XMLHttpRequestObject.readyState == 4 && XMLHttpRequestObject.status == 200) {
        obj.innerHTML = XMLHttpRequestObject.responseText;
      }
    }
        
    XMLHttpRequestObject.send(null);
  }
}

function save_symptoms(visit_id){
  

  var config_url = $('#config_url').val();
  var data_url = config_url+"nurse/save_symptoms/"+visit_id;
  //window.alert(data_url);
   var symptoms = $('#visit_symptoms').val();//document.getElementById("vital"+vital_id).value;
  $.ajax({
  type:'POST',
  url: data_url,
  data:{notes: symptoms},
  dataType: 'text',
  success:function(data){
  //window.alert("You have successfully updated the symptoms");
  //obj.innerHTML = XMLHttpRequestObject.responseText;
  },
  error: function(xhr, status, error) {
  //alert("XMLHttpRequest=" + xhr.responseText + "\ntextStatus=" + status + "\nerrorThrown=" + error);
  alert(error);
  }

  });
}

function save_objective_findings(visit_id){
 
  var config_url = $('#config_url').val();
  var data_url = config_url+"nurse/save_objective_findings/"+visit_id;
  //window.alert(data_url);
   var objective_findings = $('#visit_objective_findings').val();//document.getElementById("vital"+vital_id).value;
  $.ajax({
  type:'POST',
  url: data_url,
  data:{notes: objective_findings},
  dataType: 'text',
  success:function(data){
    //window.alert("You have successfully updated the objective findings");
  //obj.innerHTML = XMLHttpRequestObject.responseText;
  },
  error: function(xhr, status, error) {
  //alert("XMLHttpRequest=" + xhr.responseText + "\ntextStatus=" + status + "\nerrorThrown=" + error);
  alert(error);
  }

  });
}
function save_plan(visit_id){
 
  var config_url = $('#config_url').val();
  var data_url = config_url+"nurse/save_plan/"+visit_id;
  //window.alert(data_url);
   var plan = $('#visit_plan').val();//document.getElementById("vital"+vital_id).value;
  $.ajax({
  type:'POST',
  url: data_url,
  data:{notes: plan},
  dataType: 'text',
  success:function(data){
    //window.alert("You have successfully updated the visit plan");
  //obj.innerHTML = XMLHttpRequestObject.responseText;
  },
  error: function(xhr, status, error) {
  //alert("XMLHttpRequest=" + xhr.responseText + "\ntextStatus=" + status + "\nerrorThrown=" + error);
  alert(error);
  }

  });
}

function get_disease(visit_id){
  
  var XMLHttpRequestObject = false;
    
  if (window.XMLHttpRequest) {
  
    XMLHttpRequestObject = new XMLHttpRequest();
  } 
    
  else if (window.ActiveXObject) {
    XMLHttpRequestObject = new ActiveXObject("Microsoft.XMLHTTP");
  }
  var config_url = $('#config_url').val();
  var url = config_url+"nurse/get_diagnosis/"+visit_id;

  
  if(XMLHttpRequestObject) {
    var obj = document.getElementById("visit_diagnosis_original");
    var obj2 = document.getElementById("visit_diagnosis");
        
    XMLHttpRequestObject.open("GET", url);
        
    XMLHttpRequestObject.onreadystatechange = function(){
      
      if (XMLHttpRequestObject.readyState == 4 && XMLHttpRequestObject.status == 200) {
        obj.innerHTML = XMLHttpRequestObject.responseText;
        obj2.innerHTML = XMLHttpRequestObject.responseText;
      }
    }
    
    XMLHttpRequestObject.send(null);
  }
}

$(document).on("click","a.delete_diagnosis",function() 
{
    var diagnosis_id = $(this).attr('href');
    var visit_id = $(this).attr('id');
  var config_url = $('#config_url').val();
  var url = config_url+"nurse/delete_diagnosis/"+diagnosis_id;
  
  $.get(url, function( data ) {
    get_disease(visit_id);
  });
  
  return false;
});

function save_soap(visit_id)
{
  save_symptoms(visit_id);
  save_objective_findings(visit_id);
  save_assessment(visit_id);
  save_plan(visit_id);
  window.alert("You have successfully updated the SOAP");
}








function symptoms_sidebar(visit_id){
  

  var config_url = $('#config_url').val();
  var data_url = config_url+"nurse/symptoms_sidebar/"+visit_id;
  //window.alert(data_url);
  $.ajax({
  type:'POST',
  url: data_url,
  data:{visit_id: visit_id},
  dataType: 'text',
  success:function(data){
  //window.alert("You have successfully updated the symptoms");
  //obj.innerHTML = XMLHttpRequestObject.responseText;
   $("#sidebar-div").html(data);
   symptoms2(visit_id);
    // alert(data);
  },
  error: function(xhr, status, error) {
  //alert("XMLHttpRequest=" + xhr.responseText + "\ntextStatus=" + status + "\nerrorThrown=" + error);
  alert(error);
  }

  });
}

function search_symptoms(visit_id)
{
  var config_url = $('#config_url').val();
  var data_url = config_url+"nurse/search_symptoms/"+visit_id;
  //window.alert(data_url);
  var symptoms = $('#q').val();
  $.ajax({
  type:'POST',
  url: data_url,
  data:{visit_id: visit_id, symptoms : symptoms},
  dataType: 'text',
  success:function(data){
  //window.alert("You have successfully updated the symptoms");
  //obj.innerHTML = XMLHttpRequestObject.responseText;
   $("#searched-items").html(data);
    // alert(data);
  },
  error: function(xhr, status, error) {
  //alert("XMLHttpRequest=" + xhr.responseText + "\ntextStatus=" + status + "\nerrorThrown=" + error);
  alert(error);
  }

  });
}

function add_symptoms(symptoms_id, status, visit_id){

  var XMLHttpRequestObject = false;
    
  if (window.XMLHttpRequest) {
  
    XMLHttpRequestObject = new XMLHttpRequest();
  } 
    
  else if (window.ActiveXObject) {
    XMLHttpRequestObject = new ActiveXObject("Microsoft.XMLHTTP");
  }
  var config_url = $('#config_url').val();
  var url = config_url+"nurse/symptoms/"+symptoms_id+"/"+status+"/"+visit_id;


  if(XMLHttpRequestObject) {
    // var obj3 = window.opener.document.getElementById("visit_symptoms1");
     var obj3 = document.getElementById("view-list");
        
    XMLHttpRequestObject.open("GET", url);
        
    XMLHttpRequestObject.onreadystatechange = function(){
      
      if (XMLHttpRequestObject.readyState == 4 && XMLHttpRequestObject.status == 200) {
        
        obj3.innerHTML = XMLHttpRequestObject.responseText;




      }
    }
        
    XMLHttpRequestObject.send(null);
  }

   symptoms(visit_id);
}



function symptoms2(visit_id){
    var XMLHttpRequestObject = false;
        
    if (window.XMLHttpRequest) {
    
        XMLHttpRequestObject = new XMLHttpRequest();
    } 
        
    else if (window.ActiveXObject) {
        XMLHttpRequestObject = new ActiveXObject("Microsoft.XMLHTTP");
    }
    var config_url = $('#config_url').val();
    var url = config_url+"nurse/view_selected_symptoms/"+visit_id;
    // alert(url);
    if(XMLHttpRequestObject) {
        
        var obj = document.getElementById("view-list");
            
        XMLHttpRequestObject.open("GET", url);
                
        XMLHttpRequestObject.onreadystatechange = function(){
            
            if (XMLHttpRequestObject.readyState == 4 && XMLHttpRequestObject.status == 200) {
                obj.innerHTML = XMLHttpRequestObject.responseText;
            }
        }
                
        XMLHttpRequestObject.send(null);
    }
}






function icd10codes_sidebar(visit_id){
  
  open_sidebar();
  var config_url = $('#config_url').val();
  var data_url = config_url+"nurse/icd10_sidebar/"+visit_id;
  //window.alert(data_url);
  $.ajax({
  type:'POST',
  url: data_url,
  data:{visit_id: visit_id},
  dataType: 'text',
  success:function(data){
  //window.alert("You have successfully updated the symptoms");
  //obj.innerHTML = XMLHttpRequestObject.responseText;
   $("#sidebar-div").html(data);
   get_disease2(visit_id)
    // alert(data);
  },
  error: function(xhr, status, error) {
  //alert("XMLHttpRequest=" + xhr.responseText + "\ntextStatus=" + status + "\nerrorThrown=" + error);
  alert(error);
  }

  });
}



function search_diseases_code(visit_id)
{
  var config_url = $('#config_url').val();
  var data_url = config_url+"nurse/search_diseases_code/"+visit_id;
  //window.alert(data_url);
  var disease_code = $('#q').val();
  $.ajax({
  type:'POST',
  url: data_url,
  data:{visit_id: visit_id, disease_code : disease_code},
  dataType: 'text',
  success:function(data){
  //window.alert("You have successfully updated the symptoms");
  //obj.innerHTML = XMLHttpRequestObject.responseText;
   $("#searched-items-diseases").html(data);
    // alert(data);
  },
  error: function(xhr, status, error) {
  //alert("XMLHttpRequest=" + xhr.responseText + "\ntextStatus=" + status + "\nerrorThrown=" + error);
  alert(error);
  }

  });
}



function add_diseases(disease_id, status, visit_id){

  var XMLHttpRequestObject = false;
    
  if (window.XMLHttpRequest) {
  
    XMLHttpRequestObject = new XMLHttpRequest();
  } 
    
  else if (window.ActiveXObject) {
    XMLHttpRequestObject = new ActiveXObject("Microsoft.XMLHTTP");
  }
  var config_url = $('#config_url').val();
  var url = config_url+"nurse/diseases/"+disease_id+"/"+status+"/"+visit_id;


  if(XMLHttpRequestObject) {
    // var obj3 = window.opener.document.getElementById("visit_symptoms1");
     var obj3 = document.getElementById("view-list-diseases");
        
    XMLHttpRequestObject.open("GET", url);
        
    XMLHttpRequestObject.onreadystatechange = function(){
      
      if (XMLHttpRequestObject.readyState == 4 && XMLHttpRequestObject.status == 200) {

        obj3.innerHTML = XMLHttpRequestObject.responseText;
         get_disease(visit_id);
         close_side_bar();
      }
    }
        
    XMLHttpRequestObject.send(null);
  }

   
}


function get_disease2(visit_id){
    var XMLHttpRequestObject = false;
        
    if (window.XMLHttpRequest) {
    
        XMLHttpRequestObject = new XMLHttpRequest();
    } 
        
    else if (window.ActiveXObject) {
        XMLHttpRequestObject = new ActiveXObject("Microsoft.XMLHTTP");
    }
    var config_url = $('#config_url').val();
    var url = config_url+"nurse/view_selected_diseases/"+visit_id;
    // alert(url);
    if(XMLHttpRequestObject) {
        
       var obj3 = document.getElementById("view-list-diseases");
        
        XMLHttpRequestObject.open("GET", url);
            
        XMLHttpRequestObject.onreadystatechange = function(){
          
          if (XMLHttpRequestObject.readyState == 4 && XMLHttpRequestObject.status == 200) {
            
            obj3.innerHTML = XMLHttpRequestObject.responseText;


          }
        }
            
        XMLHttpRequestObject.send(null);
    }
}


// laboratory



function laboratory_sidebar(visit_id){
  
  open_sidebar();
  var config_url = $('#config_url').val();
  var data_url = config_url+"nurse/laboratory_sidebar/"+visit_id;
  //window.alert(data_url);
  $.ajax({
  type:'POST',
  url: data_url,
  data:{visit_id: visit_id},
  dataType: 'text',
  success:function(data){
  //window.alert("You have successfully updated the symptoms");
  //obj.innerHTML = XMLHttpRequestObject.responseText;
   $("#sidebar-div").html(data);
   get_visit_labtest2(visit_id);
    // alert(data);
  },
  error: function(xhr, status, error) {
  //alert("XMLHttpRequest=" + xhr.responseText + "\ntextStatus=" + status + "\nerrorThrown=" + error);
  alert(error);
  }

  });
}

function search_laboratory_tests(visit_id)
{
  var config_url = $('#config_url').val();
  var data_url = config_url+"nurse/search_laboratory_tests/"+visit_id;
  //window.alert(data_url);
  var lab_test = $('#q').val();
  $.ajax({
  type:'POST',
  url: data_url,
  data:{visit_id: visit_id, lab_test : lab_test},
  dataType: 'text',
  success:function(data){
  //window.alert("You have successfully updated the symptoms");
  //obj.innerHTML = XMLHttpRequestObject.responseText;
   $("#searched-lab-test").html(data);
    // alert(data);
  },
  error: function(xhr, status, error) {
  //alert("XMLHttpRequest=" + xhr.responseText + "\ntextStatus=" + status + "\nerrorThrown=" + error);
  alert(error);
  }

  });
}

function add_lab_test(service_charge_id, status, visit_id){

  var XMLHttpRequestObject = false;
    
  if (window.XMLHttpRequest) {
  
    XMLHttpRequestObject = new XMLHttpRequest();
  } 
    
  else if (window.ActiveXObject) {
    XMLHttpRequestObject = new ActiveXObject("Microsoft.XMLHTTP");
  }
  var config_url = $('#config_url').val();
  var url = config_url+"nurse/lab_test/"+service_charge_id+"/"+status+"/"+visit_id;


  if(XMLHttpRequestObject) {
    // var obj3 = window.opener.document.getElementById("visit_symptoms1");
     var obj3 = document.getElementById("view-list-lab-test");
        
    XMLHttpRequestObject.open("GET", url);
        
    XMLHttpRequestObject.onreadystatechange = function(){
      
      if (XMLHttpRequestObject.readyState == 4 && XMLHttpRequestObject.status == 200) {

        obj3.innerHTML = XMLHttpRequestObject.responseText;
         get_lab_table_view(visit_id);
      }
    }
        
    XMLHttpRequestObject.send(null);
  }

   
}


function get_visit_labtest2(visit_id){
    var XMLHttpRequestObject = false;
        
    if (window.XMLHttpRequest) {
    
        XMLHttpRequestObject = new XMLHttpRequest();
    } 
        
    else if (window.ActiveXObject) {
        XMLHttpRequestObject = new ActiveXObject("Microsoft.XMLHTTP");
    }
    var config_url = $('#config_url').val();
    var url = config_url+"nurse/view_selected_lab_tests/"+visit_id;
    // alert(url);
    if(XMLHttpRequestObject) {
        
       var obj3 = document.getElementById("view-list-lab-test");
        
        XMLHttpRequestObject.open("GET", url);
            
        XMLHttpRequestObject.onreadystatechange = function(){
          
          if (XMLHttpRequestObject.readyState == 4 && XMLHttpRequestObject.status == 200) {
            
            obj3.innerHTML = XMLHttpRequestObject.responseText;

          }
        }
            
        XMLHttpRequestObject.send(null);
    }
}

 function get_lab_table_view(visit_id){
     var XMLHttpRequestObject = false;
         
     if (window.XMLHttpRequest) {
     
         XMLHttpRequestObject = new XMLHttpRequest();
     } 
         
     else if (window.ActiveXObject) {
         XMLHttpRequestObject = new ActiveXObject("Microsoft.XMLHTTP");
     }
     var config_url = $('#config_url').val();
     var url = config_url+"laboratory/test_lab/"+visit_id;

 
     if(XMLHttpRequestObject) {
                 
         XMLHttpRequestObject.open("GET", url);
                 
         XMLHttpRequestObject.onreadystatechange = function(){
             
             if (XMLHttpRequestObject.readyState == 4 && XMLHttpRequestObject.status == 200) {
                 
                 document.getElementById("lab_table").innerHTML = XMLHttpRequestObject.responseText;
             }
         }
         
         XMLHttpRequestObject.send(null);
     }
 }

 function delete_lab_test(service_charge_id,status,visit_id)
 {

    var res = confirm('Are you sure you want to remove this test ? ');

    if(res)
    {
      var XMLHttpRequestObject = false;
        
      if (window.XMLHttpRequest) {
      
        XMLHttpRequestObject = new XMLHttpRequest();
      } 
        
      else if (window.ActiveXObject) {
        XMLHttpRequestObject = new ActiveXObject("Microsoft.XMLHTTP");
      }
      var config_url = $('#config_url').val();
      var url = config_url+"nurse/delete_lab_test/"+service_charge_id+"/"+status+"/"+visit_id;


      if(XMLHttpRequestObject) {
        // var obj3 = window.opener.document.getElementById("visit_symptoms1");
         var obj3 = document.getElementById("view-list-lab-test");
            
        XMLHttpRequestObject.open("GET", url);
            
        XMLHttpRequestObject.onreadystatechange = function(){
          
          if (XMLHttpRequestObject.readyState == 4 && XMLHttpRequestObject.status == 200) {

            obj3.innerHTML = XMLHttpRequestObject.responseText;
             get_lab_table_view(visit_id);
          }
        }
            
        XMLHttpRequestObject.send(null);
      }
    }
 }

 function save_referal_notes(visit_id){
  

    var config_url = $('#config_url').val();
    var data_url = "<?php echo site_url();?>nurse/save_referal_notes/"+visit_id;
    // window.alert(data_url);
     var symptoms = document.getElementById("referal_notes"+visit_id).value;
     var specialist_id = document.getElementById("specialist_id"+visit_id).value;
    
     var doctor = document.getElementById("doctor"+visit_id).value;
     var demographics = document.getElementById("demographics"+visit_id).value;

     

     var evaluate = document.getElementById("evaluate"+visit_id).checked;
     var share = document.getElementById("share"+visit_id).checked;

     // alert(evaluate);
     if(evaluate == true)
     {
       evaluate = 1;
     }
     else
     {
      evaluate = 0;
     }

     if(share == true)
     {
       share = 1;
     }
     else
     {
      share = 0;
     }

    $.ajax({
    type:'POST',
    url: data_url,
    data:{notes: symptoms,specialist_id: specialist_id,evaluate: evaluate, share: share,doctor:doctor,demographics: demographics},
    dataType: 'json',
    success:function(data){
      if(data.result == 'success')
      {
        // $('#history_notes').html(data.message);
        // tinymce.get('visit_presenting_complaint').setContent('');
       
        alert("You have successfully saved the referal summary");
      }
      else
      {
        alert("Unable to add the discharge summary");
      }
    //obj.innerHTML = XMLHttpRequestObject.responseText;
    },
    error: function(xhr, status, error) {
    //alert("XMLHttpRequest=" + xhr.responseText + "\ntextStatus=" + status + "\nerrorThrown=" + error);
    alert(error);
    }

    });
  }

  function radiology_sidebar(visit_id)
  {
    open_sidebar();
    var config_url = $('#config_url').val();
    var data_url = config_url+"nurse/radiology_sidebar/"+visit_id;
    //window.alert(data_url);
    $.ajax({
    type:'POST',
    url: data_url,
    data:{visit_id: visit_id},
    dataType: 'text',
    success:function(data){
    //window.alert("You have successfully updated the symptoms");
    //obj.innerHTML = XMLHttpRequestObject.responseText;
     $("#sidebar-div").html(data);
     get_radiology_sidebar(visit_id);
      // alert(data);
    },
    error: function(xhr, status, error) {
    //alert("XMLHttpRequest=" + xhr.responseText + "\ntextStatus=" + status + "\nerrorThrown=" + error);
    alert(error);
    }

    });

  }


function get_radiology_sidebar(visit_id){
    var XMLHttpRequestObject = false;
        
    if (window.XMLHttpRequest) {
    
        XMLHttpRequestObject = new XMLHttpRequest();
    } 
        
    else if (window.ActiveXObject) {
        XMLHttpRequestObject = new ActiveXObject("Microsoft.XMLHTTP");
    }
    var config_url = $('#config_url').val();
    var url = config_url+"nurse/view_selected_radiology_tests/"+visit_id;
    // alert(url);
    if(XMLHttpRequestObject) {
        
       var obj3 = document.getElementById("view-list-radiology-tests");
        
        XMLHttpRequestObject.open("GET", url);
            
        XMLHttpRequestObject.onreadystatechange = function(){
          
          if (XMLHttpRequestObject.readyState == 4 && XMLHttpRequestObject.status == 200) {
            
            obj3.innerHTML = XMLHttpRequestObject.responseText;

          }
        }
            
        XMLHttpRequestObject.send(null);
    }
}


function search_radiology_tests(visit_id)
{
  var config_url = $('#config_url').val();
  var data_url = config_url+"nurse/search_radiology_tests/"+visit_id;
  //window.alert(data_url);
  var radiology = $('#q').val();
  $.ajax({
  type:'POST',
  url: data_url,
  data:{visit_id: visit_id, radiology : radiology},
  dataType: 'text',
  success:function(data){
  //window.alert("You have successfully updated the symptoms");
  //obj.innerHTML = XMLHttpRequestObject.responseText;
   $("#searched-radiology-test").html(data);
    // alert(data);
  },
  error: function(xhr, status, error) {
  //alert("XMLHttpRequest=" + xhr.responseText + "\ntextStatus=" + status + "\nerrorThrown=" + error);
  alert(error);
  }

  });
}

function add_radiology_test(service_charge_id, status, visit_id){

  var XMLHttpRequestObject = false;
    
  if (window.XMLHttpRequest) {
  
    XMLHttpRequestObject = new XMLHttpRequest();
  } 
    
  else if (window.ActiveXObject) {
    XMLHttpRequestObject = new ActiveXObject("Microsoft.XMLHTTP");
  }
  var config_url = $('#config_url').val();
  var url = config_url+"nurse/radiology/"+service_charge_id+"/"+status+"/"+visit_id;


  if(XMLHttpRequestObject) {
    // var obj3 = window.opener.document.getElementById("visit_symptoms1");
     var obj3 = document.getElementById("view-list-radiology-tests");
        
    XMLHttpRequestObject.open("GET", url);
        
    XMLHttpRequestObject.onreadystatechange = function(){
      
      if (XMLHttpRequestObject.readyState == 4 && XMLHttpRequestObject.status == 200) {

        obj3.innerHTML = XMLHttpRequestObject.responseText;
         get_radiology_table_view(visit_id);
      }
    }
        
    XMLHttpRequestObject.send(null);
  }

   
}


 function get_radiology_table_view(visit_id)
 {
     var XMLHttpRequestObject = false;
         
     if (window.XMLHttpRequest) {
     
         XMLHttpRequestObject = new XMLHttpRequest();
     } 
         
     else if (window.ActiveXObject) {
         XMLHttpRequestObject = new ActiveXObject("Microsoft.XMLHTTP");
     }
     var config_url = $('#config_url').val();
     var url = config_url+"nurse/ultrasound_table/"+visit_id;

 
     if(XMLHttpRequestObject) {
                 
         XMLHttpRequestObject.open("GET", url);
                 
         XMLHttpRequestObject.onreadystatechange = function(){
             
             if (XMLHttpRequestObject.readyState == 4 && XMLHttpRequestObject.status == 200) {
                 
                 document.getElementById("radiology_table").innerHTML = XMLHttpRequestObject.responseText;
             }
         }
         
         XMLHttpRequestObject.send(null);
     }
 }


 function delete_radiology_test(service_charge_id,status,visit_id)
 {

    var res = confirm('Are you sure you want to remove this test ? ');

    if(res)
    {
      var XMLHttpRequestObject = false;
        
      if (window.XMLHttpRequest) {
      
        XMLHttpRequestObject = new XMLHttpRequest();
      } 
        
      else if (window.ActiveXObject) {
        XMLHttpRequestObject = new ActiveXObject("Microsoft.XMLHTTP");
      }
      var config_url = $('#config_url').val();
      var url = config_url+"nurse/delete_radiology_test/"+service_charge_id+"/"+status+"/"+visit_id;


      if(XMLHttpRequestObject) {
        // var obj3 = window.opener.document.getElementById("visit_symptoms1");
         var obj3 = document.getElementById("view-list-radiology-tests");
            
        XMLHttpRequestObject.open("GET", url);
            
        XMLHttpRequestObject.onreadystatechange = function(){
          
          if (XMLHttpRequestObject.readyState == 4 && XMLHttpRequestObject.status == 200) {

            obj3.innerHTML = XMLHttpRequestObject.responseText;
             get_radiology_table_view(visit_id);
          }
        }
            
        XMLHttpRequestObject.send(null);
      }
    }
 }

 function get_xray_scans(visit_id){
      var XMLHttpRequestObject = false;
          
      if (window.XMLHttpRequest) {
      
          XMLHttpRequestObject = new XMLHttpRequest();
      } 
          
      else if (window.ActiveXObject) {
          XMLHttpRequestObject = new ActiveXObject("Microsoft.XMLHTTP");
      }
      var url = "<?php echo site_url();?>radiology/xray/test_scans/"+visit_id;
      // alert(url);
      if(XMLHttpRequestObject) {
                  
          XMLHttpRequestObject.open("GET", url);
                  
          XMLHttpRequestObject.onreadystatechange = function(){
              
              if (XMLHttpRequestObject.readyState == 4 && XMLHttpRequestObject.status == 200) {
                  
                  document.getElementById("xray_scans").innerHTML = XMLHttpRequestObject.responseText;
              }
          }
          
          XMLHttpRequestObject.send(null);
      }
  }




  // theathre procedures



  function theatre_sidebar(visit_id)
  {
    open_sidebar();
    var config_url = $('#config_url').val();
    var data_url = config_url+"nurse/theatre_sidebar/"+visit_id;
    //window.alert(data_url);
    $.ajax({
    type:'POST',
    url: data_url,
    data:{visit_id: visit_id},
    dataType: 'text',
    success:function(data){
    //window.alert("You have successfully updated the symptoms");
    //obj.innerHTML = XMLHttpRequestObject.responseText;
     $("#sidebar-div").html(data);
     get_theatre_sidebar(visit_id);
      // alert(data);
    },
    error: function(xhr, status, error) {
    //alert("XMLHttpRequest=" + xhr.responseText + "\ntextStatus=" + status + "\nerrorThrown=" + error);
    alert(error);
    }

    });

  }


function get_theatre_sidebar(visit_id){
    var XMLHttpRequestObject = false;
        
    if (window.XMLHttpRequest) {
    
        XMLHttpRequestObject = new XMLHttpRequest();
    } 
        
    else if (window.ActiveXObject) {
        XMLHttpRequestObject = new ActiveXObject("Microsoft.XMLHTTP");
    }
    var config_url = $('#config_url').val();
    var url = config_url+"nurse/view_selected_theatre_tests/"+visit_id;
    // alert(url);
    if(XMLHttpRequestObject) {
        
       var obj3 = document.getElementById("view-list-theatre-tests");
        
        XMLHttpRequestObject.open("GET", url);
            
        XMLHttpRequestObject.onreadystatechange = function(){
          
          if (XMLHttpRequestObject.readyState == 4 && XMLHttpRequestObject.status == 200) {
            
            obj3.innerHTML = XMLHttpRequestObject.responseText;

          }
        }
            
        XMLHttpRequestObject.send(null);
    }
}


function search_theatre_tests(visit_id)
{
  var config_url = $('#config_url').val();
  var data_url = config_url+"nurse/search_theatre_tests/"+visit_id;
  //window.alert(data_url);
  var theatre = $('#q').val();
  $.ajax({
  type:'POST',
  url: data_url,
  data:{visit_id: visit_id, theatre : theatre},
  dataType: 'text',
  success:function(data){
  //window.alert("You have successfully updated the symptoms");
  //obj.innerHTML = XMLHttpRequestObject.responseText;
   $("#searched-theatre-test").html(data);
    // alert(data);
  },
  error: function(xhr, status, error) {
  //alert("XMLHttpRequest=" + xhr.responseText + "\ntextStatus=" + status + "\nerrorThrown=" + error);
  alert(error);
  }

  });
}

function add_theatre_test(service_charge_id, status, visit_id){

  var XMLHttpRequestObject = false;
    
  if (window.XMLHttpRequest) {
  
    XMLHttpRequestObject = new XMLHttpRequest();
  } 
    
  else if (window.ActiveXObject) {
    XMLHttpRequestObject = new ActiveXObject("Microsoft.XMLHTTP");
  }
  var config_url = $('#config_url').val();
  var url = config_url+"nurse/theatre/"+service_charge_id+"/"+status+"/"+visit_id;


  if(XMLHttpRequestObject) {
    // var obj3 = window.opener.document.getElementById("visit_symptoms1");
     var obj3 = document.getElementById("view-list-theatre-tests");
        
    XMLHttpRequestObject.open("GET", url);
        
    XMLHttpRequestObject.onreadystatechange = function(){
      
      if (XMLHttpRequestObject.readyState == 4 && XMLHttpRequestObject.status == 200) {

        obj3.innerHTML = XMLHttpRequestObject.responseText;
         get_theatre_table_view(visit_id);
      }
    }
        
    XMLHttpRequestObject.send(null);
  }

   
}


 function get_theatre_table_view(visit_id)
 {
     var XMLHttpRequestObject = false;
         
     if (window.XMLHttpRequest) {
     
         XMLHttpRequestObject = new XMLHttpRequest();
     } 
         
     else if (window.ActiveXObject) {
         XMLHttpRequestObject = new ActiveXObject("Microsoft.XMLHTTP");
     }
     var config_url = $('#config_url').val();
     var url = config_url+"nurse/theatre_table/"+visit_id;

 
     if(XMLHttpRequestObject) {
                 
         XMLHttpRequestObject.open("GET", url);
                 
         XMLHttpRequestObject.onreadystatechange = function(){
             
             if (XMLHttpRequestObject.readyState == 4 && XMLHttpRequestObject.status == 200) {
                 
                 document.getElementById("theatre_table").innerHTML = XMLHttpRequestObject.responseText;
             }
         }
         
         XMLHttpRequestObject.send(null);
     }
 }


 function delete_theatre_test(service_charge_id,status,visit_id)
 {

    var res = confirm('Are you sure you want to remove this test ? ');

    if(res)
    {
      var XMLHttpRequestObject = false;
        
      if (window.XMLHttpRequest) {
      
        XMLHttpRequestObject = new XMLHttpRequest();
      } 
        
      else if (window.ActiveXObject) {
        XMLHttpRequestObject = new ActiveXObject("Microsoft.XMLHTTP");
      }
      var config_url = $('#config_url').val();
      var url = config_url+"nurse/delete_theatre_test/"+service_charge_id+"/"+status+"/"+visit_id;


      if(XMLHttpRequestObject) {
        // var obj3 = window.opener.document.getElementById("visit_symptoms1");
         var obj3 = document.getElementById("view-list-theatre-tests");
            
        XMLHttpRequestObject.open("GET", url);
            
        XMLHttpRequestObject.onreadystatechange = function(){
          
          if (XMLHttpRequestObject.readyState == 4 && XMLHttpRequestObject.status == 200) {

            obj3.innerHTML = XMLHttpRequestObject.responseText;
             get_theatre_table_view(visit_id);
          }
        }
            
        XMLHttpRequestObject.send(null);
      }
    }
 }



  function subjective_ros(visit_id){

  var XMLHttpRequestObject = false;
    
  if (window.XMLHttpRequest) {
  
    XMLHttpRequestObject = new XMLHttpRequest();
  } 
    
  else if (window.ActiveXObject) {
    XMLHttpRequestObject = new ActiveXObject("Microsoft.XMLHTTP");
  }
  
   var config_url = $('#config_url').val();

  var url = config_url+"nurse/subjective_ros/"+visit_id;
  // alert(url);

  if(XMLHttpRequestObject) {
    
    var obj = document.getElementById("subjective-ros");
      
    XMLHttpRequestObject.open("GET", url);
        
    XMLHttpRequestObject.onreadystatechange = function(){
      
      if (XMLHttpRequestObject.readyState == 4 && XMLHttpRequestObject.status == 200) {
        obj.innerHTML = XMLHttpRequestObject.responseText;
        
        // patient_details(visit_id);
        // objective_findings(visit_id);
        // assessment(visit_id);
        // plan(visit_id);
        // doctor_notes(visit_id);
        // nurse_notes(visit_id);
      }
    }
        
    XMLHttpRequestObject.send(null);
  }
}


function add_subjective_ros_findings(subjective_ros_findings_id, visit_id, status)
{
  var config_url = $('#config_url').val();
  var data_url = config_url+"nurse/add_subjective_ros_findings/"+subjective_ros_findings_id+"/"+visit_id+"/"+status;
     // window.alert(data_url);
    $.ajax({
    type:'POST',
    url: data_url,
    data:{visit_id: visit_id},
    dataType: 'text',
    success:function(data){
      subjective_ros(visit_id);
    // alert(data);
    },
    error: function(xhr, status, error) {
    //alert("XMLHttpRequest=" + xhr.responseText + "\ntextStatus=" + status + "\nerrorThrown=" + error);
    alert(error);
    }

    });
}




function objective_ros(visit_id){

  var XMLHttpRequestObject = false;
    
  if (window.XMLHttpRequest) {
  
    XMLHttpRequestObject = new XMLHttpRequest();
  } 
    
  else if (window.ActiveXObject) {
    XMLHttpRequestObject = new ActiveXObject("Microsoft.XMLHTTP");
  }
  
   var config_url = $('#config_url').val();

  var url = config_url+"nurse/objective_ros/"+visit_id;
  // alert(url);

  if(XMLHttpRequestObject) {
    
    var obj = document.getElementById("objective-ros");
      
    XMLHttpRequestObject.open("GET", url);
        
    XMLHttpRequestObject.onreadystatechange = function(){
      
      if (XMLHttpRequestObject.readyState == 4 && XMLHttpRequestObject.status == 200) {
        obj.innerHTML = XMLHttpRequestObject.responseText;
        
        // patient_details(visit_id);
        // objective_findings(visit_id);
        // assessment(visit_id);
        // plan(visit_id);
        // doctor_notes(visit_id);
        // nurse_notes(visit_id);
      }
    }
        
    XMLHttpRequestObject.send(null);
  }
}


function add_objective_ros_findings(objective_ros_findings_id, visit_id, status)
{
  var config_url = $('#config_url').val();
  var data_url = config_url+"nurse/add_objective_ros_findings/"+objective_ros_findings_id+"/"+visit_id+"/"+status;
     // window.alert(data_url);
    $.ajax({
    type:'POST',
    url: data_url,
    data:{visit_id: visit_id},
    dataType: 'text',
    success:function(data){
      objective_ros(visit_id);
    // alert(data);
    },
    error: function(xhr, status, error) {
    //alert("XMLHttpRequest=" + xhr.responseText + "\ntextStatus=" + status + "\nerrorThrown=" + error);
    alert(error);
    }

    });
}


function prescription_sidebar(visit_id)
{
  open_sidebar();
   var config_url = $('#config_url').val();
    var data_url = config_url+"nurse/prescription_sidebar/"+visit_id;
    // window.alert(data_url);
    $.ajax({
    type:'POST',
    url: data_url,
    data:{visit_id: visit_id},
    dataType: 'text',
    success:function(data){
    //window.alert("You have successfully updated the symptoms");
    //obj.innerHTML = XMLHttpRequestObject.responseText;
     $("#sidebar-div").html(data);
      // alert(data);
    },
    error: function(xhr, status, error) {
    //alert("XMLHttpRequest=" + xhr.responseText + "\ntextStatus=" + status + "\nerrorThrown=" + error);
    alert(error);
    }

    });

}


function search_drug_directory(visit_id)
{

    var config_url = $('#config_url').val();
    var data_url = config_url+"nurse/search_drug_directory/"+visit_id;
    //window.alert(data_url);
    var drug = $('#q').val();
    $.ajax({
    type:'POST',
    url: data_url,
    data:{visit_id: visit_id, drug : drug},
    dataType: 'text',
    success:function(data){
    //window.alert("You have successfully updated the symptoms");
    //obj.innerHTML = XMLHttpRequestObject.responseText;
    $("#searched-drugs").html(data);
    // alert(data);
    },
    error: function(xhr, status, error) {
    //alert("XMLHttpRequest=" + xhr.responseText + "\ntextStatus=" + status + "\nerrorThrown=" + error);
    alert(error);
    }

    });
}


function add_prescribed_drug(product_id, status, visit_id){

  

    var config_url = $('#config_url').val();
    var data_url = config_url+"nurse/add_prescribed_drug/"+product_id+"/"+status+"/"+visit_id;
    //window.alert(data_url);
    // var drug = $('#q').val();
    $.ajax({
    type:'POST',
    url: data_url,
    data:{visit_id: visit_id},
    dataType: 'text',
    success:function(data){
         var data = jQuery.parseJSON(data);
        // alert(data.message);
    //window.alert("You have successfully updated the symptoms");
    //obj.innerHTML = XMLHttpRequestObject.responseText;
    $('#adding-list-product').css('display', 'block');
    document.getElementById("medicine_id").value = data.product_id;
    document.getElementById("drug_name").value = data.product_name;
    get_drug_to_prescribe(visit_id);
    // alert(data);
    },
    error: function(xhr, status, error) {
    //alert("XMLHttpRequest=" + xhr.responseText + "\ntextStatus=" + status + "\nerrorThrown=" + error);
    alert(error);
    }

    });

   
}



function save_drug_prescription(visit_id)
{

    var config_url = $('#config_url').val();
    var data_url = config_url+"nurse/save_drug_allergy/"+visit_id;
   
    var product_id = $('#product_id').val();
    var reaction = $('#reaction_medicine').val();
    var serviority = $('#serviority_medicine').val();
     // window.alert(data_url);
    $.ajax({
    type:'POST',
    url: data_url,
    data:{visit_id: visit_id, product_id : product_id, reaction: reaction, serviority: serviority},
    dataType: 'text',
    success:function(data){
    //window.alert("You have successfully updated the symptoms");
    //obj.innerHTML = XMLHttpRequestObject.responseText;

    var data = jQuery.parseJSON(data);
    if(data.status = 1)
    {
        $('#adding-list').css('display', 'none');
        document.getElementById("product_id").value = '';
        document.getElementById("product_name").value = '';

        alert('You have successfully added the allergy detail');
    }
    else
    {
        alert('could not add the allergy details please try again');
    }
    // alert(data);
    },
    error: function(xhr, status, error) {
    //alert("XMLHttpRequest=" + xhr.responseText + "\ntextStatus=" + status + "\nerrorThrown=" + error);
    alert(error);
    }

    });
}
 function get_drug_to_prescribe(visit_id)
 {
 var XMLHttpRequestObject = false;
       
   if (window.XMLHttpRequest) {
   
       XMLHttpRequestObject = new XMLHttpRequest();
   } 
       
   else if (window.ActiveXObject) {
       XMLHttpRequestObject = new ActiveXObject("Microsoft.XMLHTTP");
   }
   var drug_id = document.getElementById("medicine_id").value;
 
   var url = "<?php echo site_url();?>pharmacy/inpatient_prescription/"+visit_id+"/"+drug_id+"/2";
     // alert(url);
    if(XMLHttpRequestObject) {
               
       XMLHttpRequestObject.open("GET", url);
               
       XMLHttpRequestObject.onreadystatechange = function(){
           
           if (XMLHttpRequestObject.readyState == 4 && XMLHttpRequestObject.status == 200) {
             var prescription_view = document.getElementById("prescription_view");
            
             document.getElementById("prescription_view").innerHTML=XMLHttpRequestObject.responseText;
              prescription_view.style.display = 'block';
           }
       }
               
       XMLHttpRequestObject.send(null);
   }
 
 
 }


function check_frequency_type()
{

 var x = document.getElementById("x_value").value;
 var type_of_drug = document.getElementById("type_of_drug").value;
   
 var number_of_days = document.getElementById("number_of_days_value").value;
 var quantity = document.getElementById("quantity_value").value;

 // alert(quantity);
 var dose_value = document.getElementById("dose_value").value;

var service_charge_id = document.getElementById("drug_id").value;
var visit_id = document.getElementById("visit_id").value;
var consumption = document.getElementById("consumption_value").value;

 if(x == "" || x == 0)
 {

   // alert("Please select the frequency of the medicine");
   x = 1;
 }
 
 if(number_of_days == "" || number_of_days == 0)
 {
    number_of_days = 1;
 }
 
     
  var url = "<?php echo base_url();?>pharmacy/get_values";
   $.ajax({
   type:'POST',
   url: url,
   data:{quantity: quantity, x: x, service_charge_id : service_charge_id, visit_id : visit_id, number_of_days: number_of_days,dose_value:dose_value,consumption: consumption,type_of_drug: type_of_drug},
   dataType: 'text',
   success:function(data){
      var data = jQuery.parseJSON(data);

      var amount = data.amount;
      var frequency = data.frequency;
       var item = data.item;

       var total_units = data.total_units;
       var total_amount = data.total_amount;

      // {"message":"success","amount":"35","frequency":"5"}

      // if(type_of_drug == 3)
      // {
       
      //   var total_units = number_of_days * frequency * quantity;
      //   var total_amount =  number_of_days * frequency * amount * quantity;
      // }
      // else
      // {

      //    var total_units =   quantity;
      //    var total_amount =   amount * quantity;
      // }
    

    // document.getElementById("total_units").innerHTML = "<h2>"+ amount +" units</h2>";
     $( "#total_units" ).html("<h2>"+ total_units +" units</h2>");
     $( "#total_amount" ).html("<h3>Ksh. "+ total_amount +"</h3>");
     $( "#item_description" ).html("<p> "+ item +"</p>");

     document.getElementById("input-total-value").value = total_units;
     // document.getElementById("total_amount").innerHTML = "<h3>Ksh. "+ frequency +" units</h3>";


   
   },
   error: function(xhr, status, error) {
   alert("XMLHttpRequest=" + xhr.responseText + "\ntextStatus=" + status + "\nerrorThrown=" + error);
   
   }
   });

    
 
}


function pass_prescription()
   {

   var quantity = document.getElementById("quantity_value").value;
   var x = document.getElementById("x_value").value;
   var dose_value = document.getElementById("dose_value").value;
   var duration = 1;//document.getElementById("duration_value").value;
   var consumption = document.getElementById("consumption_value").value;
   var number_of_days = document.getElementById("number_of_days_value").value;
   var service_charge_id = document.getElementById("drug_id").value;
   var visit_id = document.getElementById("visit_id").value;
   var input_total_units = document.getElementById("input-total-value").value;
   var module = document.getElementById("module").value;
   var passed_value = document.getElementById("passed_value").value;
   var type_of_drug = document.getElementById("type_of_drug").value;
   
   var url = "<?php echo base_url();?>pharmacy/prescribe_prescription";
   
  
   $.ajax({
   type:'POST',
   url: url,
   data:{quantity: quantity, x: x, duration: duration,consumption: consumption, service_charge_id : service_charge_id, visit_id : visit_id, number_of_days: number_of_days,module: module,passed_value:passed_value,input_total_units:input_total_units,dose_value: dose_value,type_of_drug: type_of_drug},
   dataType: 'text',
   success:function(data){
   
   var prescription_view = document.getElementById("prescription_view");
   prescription_view.style.display = 'none';
   display_inpatient_prescription(visit_id,0);
   close_side_bar();
   
   },
   error: function(xhr, status, error) {
   alert("XMLHttpRequest=" + xhr.responseText + "\ntextStatus=" + status + "\nerrorThrown=" + error);
   
   }
   });
   
   return false;
   }

   function display_inpatient_prescription(visit_id,module)
   {
   
     var XMLHttpRequestObject = false;
         
     if (window.XMLHttpRequest) {
     
         XMLHttpRequestObject = new XMLHttpRequest();
     } 
         
     else if (window.ActiveXObject) {
         XMLHttpRequestObject = new ActiveXObject("Microsoft.XMLHTTP");
     }
     
     var config_url = document.getElementById("config_url").value;
     var url = config_url+"pharmacy/display_inpatient_prescription/"+visit_id+"/"+module;
     
     if(XMLHttpRequestObject) {
                 
         XMLHttpRequestObject.open("GET", url);
                 
         XMLHttpRequestObject.onreadystatechange = function(){
             
             if (XMLHttpRequestObject.readyState == 4 && XMLHttpRequestObject.status == 200) {
   
                 document.getElementById("visit_prescription").innerHTML=XMLHttpRequestObject.responseText;
             }
         }
                 
         XMLHttpRequestObject.send(null);
     }
   }

   function delete_prescription(prescription_id, visit_id,visit_charge_id,module)
   {
   var res = confirm('Are you sure you want to delete this from the  prescription ?');
   
   if(res)
   {
     var XMLHttpRequestObject = false;
     
     if (window.XMLHttpRequest) {
       XMLHttpRequestObject = new XMLHttpRequest();
     } 
     
     else if (window.ActiveXObject) {
       XMLHttpRequestObject = new ActiveXObject("Microsoft.XMLHTTP");
     }
      var config_url = document.getElementById("config_url").value;
     var url = config_url+"pharmacy/delete_inpatient_prescription/"+prescription_id+"/"+visit_id+"/"+visit_charge_id+"/"+module;
     
     if(XMLHttpRequestObject) {
       
       XMLHttpRequestObject.open("GET", url);
       
       XMLHttpRequestObject.onreadystatechange = function(){
         
         if (XMLHttpRequestObject.readyState == 4 && XMLHttpRequestObject.status == 200) {
           
            display_inpatient_prescription(visit_id,0);
          
         }
       }
       XMLHttpRequestObject.send(null);
     }
   }
   }

   function display_referal_note(visit_id)
   {
   
     var XMLHttpRequestObject = false;
         
     if (window.XMLHttpRequest) {
     
         XMLHttpRequestObject = new XMLHttpRequest();
     } 
         
     else if (window.ActiveXObject) {
         XMLHttpRequestObject = new ActiveXObject("Microsoft.XMLHTTP");
     }
     
     var config_url = document.getElementById("config_url").value;
     var url = config_url+"doctor/display_referal_note/"+visit_id;
     
     if(XMLHttpRequestObject) {
                 
         XMLHttpRequestObject.open("GET", url);
                 
         XMLHttpRequestObject.onreadystatechange = function(){
             
             if (XMLHttpRequestObject.readyState == 4 && XMLHttpRequestObject.status == 200) {
   
                 document.getElementById("note-referal").innerHTML=XMLHttpRequestObject.responseText;
             }
         }
                 
         XMLHttpRequestObject.send(null);
     }
   }

   function save_action_plan(visit_id)
   {

      var config_url = $('#config_url').val();
      var data_url = "<?php echo site_url();?>nurse/save_action_plan/"+visit_id;
      // window.alert(data_url);
      var symptoms = document.getElementById("action_plan"+visit_id).value; 

      $.ajax({
        type:'POST',
        url: data_url,
        data:{notes: symptoms},
        dataType: 'json',
        success:function(data){
          if(data.result == 'success')
          {
            // $('#history_notes').html(data.message);
            // tinymce.get('visit_presenting_complaint').setContent('');
           
            alert("You have successfully saved the action plan");
            display_action_plan(visit_id);
          }
          else
          {
            alert("Unable to add the action plan");
          }
        //obj.innerHTML = XMLHttpRequestObject.responseText;
        },
        error: function(xhr, status, error) {
        //alert("XMLHttpRequest=" + xhr.responseText + "\ntextStatus=" + status + "\nerrorThrown=" + error);
        alert(error);
        }
      });

   }

   function display_action_plan(visit_id)
   {
   
     var XMLHttpRequestObject = false;
         
     if (window.XMLHttpRequest) {
     
         XMLHttpRequestObject = new XMLHttpRequest();
     } 
         
     else if (window.ActiveXObject) {
         XMLHttpRequestObject = new ActiveXObject("Microsoft.XMLHTTP");
     }
     
     var config_url = document.getElementById("config_url").value;
     var url = config_url+"doctor/display_action_plan/"+visit_id;
     
     if(XMLHttpRequestObject) {
                 
         XMLHttpRequestObject.open("GET", url);
                 
         XMLHttpRequestObject.onreadystatechange = function(){
             
             if (XMLHttpRequestObject.readyState == 4 && XMLHttpRequestObject.status == 200) {
   
                 document.getElementById("action-plan").innerHTML=XMLHttpRequestObject.responseText;
             }
         }
                 
         XMLHttpRequestObject.send(null);
     }
   }


   function save_doctors_notes(visit_id,type)
   {

      var config_url = $('#config_url').val();
      var data_url = "<?php echo site_url();?>nurse/save_soap_notes/"+visit_id+"/"+type;
      

      if(type == 14)
      {
         var symptoms = document.getElementById("chief_complain"+visit_id).value; 
      }
      else if(type == 15)
      {
         var symptoms = document.getElementById("h_p_i"+visit_id).value; 
      }
      else if(type == 16)
      {
        var symptoms = document.getElementById("other_plan"+visit_id).value;
      }
      else if(type == 5)
      {
        var symptoms = document.getElementById("assessment_notes"+visit_id).value;
      }
      

      // window.alert(symptoms);

      $.ajax({
        type:'POST',
        url: data_url,
        data:{notes: symptoms},
        dataType: 'json',
        success:function(data){
          if(data.result == 'success')
          {
            // $('#history_notes').html(data.message);
            // tinymce.get('visit_presenting_complaint').setContent('');
           
            alert("You have successfully saved the note");
            
            display_notes(visit_id,type);
          }
          else
          {
            alert("Unable to add the action plan");
          }
        //obj.innerHTML = XMLHttpRequestObject.responseText;
        },
        error: function(xhr, status, error) {
        //alert("XMLHttpRequest=" + xhr.responseText + "\ntextStatus=" + status + "\nerrorThrown=" + error);
        alert(error);
        }
      });

   }

   function display_notes(visit_id,type)
   {
   
     var XMLHttpRequestObject = false;
         
     if (window.XMLHttpRequest) {
     
         XMLHttpRequestObject = new XMLHttpRequest();
     } 
         
     else if (window.ActiveXObject) {
         XMLHttpRequestObject = new ActiveXObject("Microsoft.XMLHTTP");
     }
     
     var config_url = document.getElementById("config_url").value;
     var url = config_url+"doctor/display_notes/"+visit_id+"/"+type;
     
     if(XMLHttpRequestObject) {
                 
         XMLHttpRequestObject.open("GET", url);
                 
         XMLHttpRequestObject.onreadystatechange = function(){
             
             if (XMLHttpRequestObject.readyState == 4 && XMLHttpRequestObject.status == 200) {
                if(type == 14)
                {

                 document.getElementById("chief-complain").innerHTML=XMLHttpRequestObject.responseText;
                }
                else if(type == 15)
                {
                  document.getElementById("h-p-i").innerHTML=XMLHttpRequestObject.responseText;
                }
                else if(type == 16)
                {
                  document.getElementById("other-plans").innerHTML=XMLHttpRequestObject.responseText;
                }
                 else if(type == 5)
                {
                  document.getElementById("assessment").innerHTML=XMLHttpRequestObject.responseText;
                }
             }
         }
                 
         XMLHttpRequestObject.send(null);
     }
   }

   function update_subjective_system_notes(class_id,visit_id)
   {

     var config_url = $('#config_url').val();
      var data_url = "<?php echo site_url();?>nurse/update_subjective_class_finding/"+class_id+"/"+visit_id;
      var notes = document.getElementById("note-text-area"+class_id).value;
      // alert(notes);
      
      $.ajax({
        type:'POST',
        url: data_url,
        data:{notes: notes},
        dataType: 'text',
        success:function(data){
           var data = jQuery.parseJSON(data);
          if(data.result == 'success')
          {

          }
          else
          {
            alert("Unable to update class plan");
          }
        //obj.innerHTML = XMLHttpRequestObject.responseText;
        },
        error: function(xhr, status, error) {
        //alert("XMLHttpRequest=" + xhr.responseText + "\ntextStatus=" + status + "\nerrorThrown=" + error);
        alert(error);
        }
      });

   }
   function update_objective_system_notes(class_id,visit_id)
   {

     var config_url = $('#config_url').val();
      var data_url = "<?php echo site_url();?>nurse/update_objective_class_finding/"+class_id+"/"+visit_id;
      var notes = document.getElementById("physical-exam-note"+class_id).value;
      // alert(notes);
      
      $.ajax({
        type:'POST',
        url: data_url,
        data:{notes: notes},
        dataType: 'text',
        success:function(data){
           var data = jQuery.parseJSON(data);
          if(data.result == 'success')
          {

          }
          else
          {
            alert("Unable to update class plan");
          }
        //obj.innerHTML = XMLHttpRequestObject.responseText;
        },
        error: function(xhr, status, error) {
        //alert("XMLHttpRequest=" + xhr.responseText + "\ntextStatus=" + status + "\nerrorThrown=" + error);
        alert(error);
        }
      });

   }


  function appointment_sidebar(patient_id,visit_id,department_id,personnel_id)
  {
      open_sidebar();
     var config_url = $('#config_url').val();
      var data_url = config_url+"nurse/appointment_sidebar/"+patient_id+"/"+visit_id+"/"+department_id+"/"+personnel_id;
      // window.alert(data_url);
      $.ajax({
      type:'POST',
      url: data_url,
      data:{visit_id: visit_id},
      dataType: 'text',
      success:function(data){
      //window.alert("You have successfully updated the symptoms");
      //obj.innerHTML = XMLHttpRequestObject.responseText;
       $("#sidebar-div").html(data);

        $('.datepicker').datepicker({
              format: 'yyyy-mm-dd'
          });

        // $('.datepicker').datepicker();
        $('.timepicker').timepicker();
        // alert(data);
      },
      error: function(xhr, status, error) {
      //alert("XMLHttpRequest=" + xhr.responseText + "\ntextStatus=" + status + "\nerrorThrown=" + error);
      alert(error);
      }

      });

  }

function save_appointment()
{

// $(document).on("submit","form#next_appointment_detail",function(e)
// {
  // alert("sdasdjakgdaskjdag");
  // e.preventDefault();
  
  // var form_data = new FormData(this);

  // alert(form_data);


  var visit_id = $('#visit_id').val();
  var patient_id = $('#patient_id').val();
  var appointment_note = $('#appointment_note').val();
  var next_appointment_date = $('#next_appointment_date').val();
  var doctor_id = $('#doctor_id').val();
  var department_id = $('#department_id').val();
  
  var config_url = $('#config_url').val();  

   var url = config_url+"reception/save_appointment_accounts/"+patient_id+"/"+visit_id;
       $.ajax({
       type:'POST',
       url: url,
       data:{visit_id: visit_id,patient_id: patient_id,appointment_note: appointment_note,visit_date: next_appointment_date,doctor_id: doctor_id,department_id: department_id},
       dataType: 'text',
       // processData: false,
       // contentType: false,
       success:function(data)
       {
          var data = jQuery.parseJSON(data);
          
          if(data.status == "success")
          {
            
            alert('You have successfully added an appointment');

            display_next_appointment(patient_id,visit_id);
            close_side_bar();
          }
          else
          {

            alert(data.message);
          }
       
       },
       error: function(xhr, status, error) {
       alert("XMLHttpRequest=" + xhr.responseText + "\ntextStatus=" + status + "\nerrorThrown=" + error);
       
       }
       });
}

function display_next_appointment(patient_id,visit_id)
{
  // alert(patient_id);

  var XMLHttpRequestObject = false;
         
     if (window.XMLHttpRequest) {
     
         XMLHttpRequestObject = new XMLHttpRequest();
     } 
         
     else if (window.ActiveXObject) {
         XMLHttpRequestObject = new ActiveXObject("Microsoft.XMLHTTP");
     }
     
     var config_url = document.getElementById("config_url").value;
     var url = config_url+"nurse/display_appointment_view/"+patient_id+"/"+visit_id;
     
     if(XMLHttpRequestObject) {
                 
         XMLHttpRequestObject.open("GET", url);
                 
         XMLHttpRequestObject.onreadystatechange = function(){
             
             if (XMLHttpRequestObject.readyState == 4 && XMLHttpRequestObject.status == 200) {
   
                 document.getElementById("next-appointment-view").innerHTML=XMLHttpRequestObject.responseText;
             }
         }
                 
         XMLHttpRequestObject.send(null);
     }
}



</script>