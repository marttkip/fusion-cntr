<?php

//patient details
$visit_type = $patient['visit_type'];
$patient_type = $patient['patient_type'];
$patient_othernames = $patient['patient_othernames'];
$patient_surname = $patient['patient_surname'];
$patient_surname = $patient['patient_surname'];
$patient_number = $patient['patient_number'];
$gender = $patient['gender'];

$today = date('jS F Y H:i a',strtotime(date("Y:m:d h:i:s")));
$visit_date = date('jS F Y',strtotime($this->accounts_model->get_visit_date($visit_id)));

//doctor
$doctor = $this->accounts_model->get_att_doctor($visit_id);

//served by
$served_by = $this->accounts_model->get_personnel($this->session->userdata('personnel_id'));



$title = 'Patient Visit Chart';
$heading = 'Checkup';
$number = 'MI/MED/00'.$visit_id;
?>

<section class="panel" >
	<div class="panel-body" style="height:80vh">
		<div style="padding-left:10px;padding-right:10px">
	        <div class="col-md-12">
	        	<h4>Nurse Intake Assesment</h4>
	        	
	        	<div class="row">
	                <div class="col-md-12">
	                    <!-- Widget -->
	                    <div class="widget">
	                        <!-- Widget head -->
	                        <div class="widget-head">
	                          <h5 class="pull-left" style="font-weight:bold">Vitals</h5>
	                          <div class="widget-icons pull-right">
	                          </div>
	                          <div class="clearfix"></div>
	                        </div>              
	                        
	                        <!-- Widget content -->
	                        <div class="widget-content">
	                            <div class="padd">	                                
	                                <div id="previous_vitals"></div>	                                
	                            </div>
	                        </div>
	                    </div>
	                </div>
	            </div>
	            <div class="row">
	                <div class="col-md-12">
	                    <!-- Widget -->
	                    <div class="widget">
	                        <!-- Widget head -->
	                        <div class="widget-head">
	                          <h5 class="pull-left" style="font-weight:bold">Patient History</h5>
	                          <div class="widget-icons pull-right">
	                          </div>
	                          <div class="clearfix"></div>
	                        </div>              
	                        
	                        <!-- Widget content -->
	                        <div class="widget-content">
	                            <div class="padd">
	                            	<div class="col-md-6 pull-left">
	                            		<h5>PT DX</h5>
	                            		<div id="pt-dx"></div> 
	                            	</div>
	                            	<div class="col-md-6 pull-right">
	                            		<h5>PT RX</h5>
	                            		<div id="pt-rx"></div> 
	                            	</div>	                                
	                                	                                
	                            </div>
	                        </div>
	                    </div>
	                </div>
	            </div>

	            <div class="row">
	                <div class="col-md-12">
	                    <!-- Widget -->
	                    <div class="widget">
	                        <!-- Widget head -->
	                        <div class="widget-head">
	                          <h5 class="pull-left" style="font-weight:bold">Allegies</h5>
	                          <div class="widget-icons pull-right">
	                          </div>
	                          <div class="clearfix"></div>
	                        </div>              
	                        
	                        <!-- Widget content -->
	                        <div class="widget-content">
	                            <div class="padd">
	                            	<div class="col-md-6 pull-left">
	                            		<h5>Medicine Allegies</h5>
	                            		<div id="medicine-allergy"></div>
	                            	</div>
	                            	<div class="col-md-6 pull-right">
	                            		<h5>Food Allergies</h5>
	                            		<div id="food-allergy"></div>
	                            	</div>	                                
	                            </div>
	                        </div>
	                    </div>
	                </div>
	            </div>

	            <div class="row">
	                <div class="col-md-12">
	                    <!-- Widget -->
	                    <div class="widget">
	                        <!-- Widget head -->
	                        <div class="widget-head">
	                          <h5 class="pull-left" style="font-weight:bold">Vaccines</h5>
	                          <div class="widget-icons pull-right">
	                          </div>
	                          <div class="clearfix"></div>
	                        </div>              
	                        
	                        <!-- Widget content -->
	                        <div class="widget-content">
	                            <div class="padd">
	                            	<div class="col-md-12">
	                            		<div id="patient_vaccine"></div>
	                            	</div>
	                            	                             
	                            </div>
	                        </div>
	                    </div>
	                </div>
	            </div>

	            <div class="row">
	                <div class="col-md-12">
	                    <!-- Widget -->
	                    <div class="widget">
	                        <!-- Widget head -->
	                        <div class="widget-head">
	                          <h5 class="pull-left" style="font-weight:bold">PT Family History</h5>
	                          <div class="widget-icons pull-right">
	                          </div>
	                          <div class="clearfix"></div>
	                        </div>              
	                        
	                        <!-- Widget content -->
	                        <div class="widget-content">
	                            <div class="padd">
	                            	<div class="col-md-12">
	                            		<div id="family-diseases"></div>
	                            	</div>
	                            	                             
	                            </div>
	                        </div>
	                    </div>
	                </div>
	            </div>

	            <div class="row">
	                <div class="col-md-6 pull-left">
	                    <!-- Widget -->
	                    <div class="widget">
	                        <!-- Widget head -->
	                        <div class="widget-head">
	                          <h5 class="pull-left" style="font-weight:bold">Chief Complain</h5>
	                          <div class="widget-icons pull-right">
	                          </div>
	                          <div class="clearfix"></div>
	                        </div>              
	                        
	                        <!-- Widget content -->
	                        <div class="widget-content">
	                            <div class="padd">
	                            	<div class="col-md-12">
	                            		<div id="chief-complain"></div>
	                            	</div>
	                            	                             
	                            </div>
	                        </div>
	                    </div>
	                </div>
	                <div class="col-md-6 pull-right">
	                    <!-- Widget -->
	                    <div class="widget">
	                        <!-- Widget head -->
	                        <div class="widget-head">
	                          <h5 class="pull-left" style="font-weight:bold">Hx of P.I</h5>
	                          <div class="widget-icons pull-right">
	                          </div>
	                          <div class="clearfix"></div>
	                        </div>              
	                        
	                        <!-- Widget content -->
	                        <div class="widget-content">
	                            <div class="padd">
	                            	<div class="col-md-12">
	                            		<div id="h-p-i"></div>
	                            	</div>
	                            	                             
	                            </div>
	                        </div>
	                    </div>
	                </div>
	            </div>
	            <div class="row">
	                <div class="col-md-12">
	                    <!-- Widget -->
	                    <div class="widget">
	                        <!-- Widget head -->
	                        <div class="widget-head">
	                          <h5 class="pull-left" style="font-weight:bold">Review of systems</h5>
	                          <div class="widget-icons pull-right">
	                          </div>
	                          <div class="clearfix"></div>
	                        </div>              
	                        
	                        <!-- Widget content -->
	                        <div class="widget-content">
	                            <div class="padd">
	                            	
	                            	<div class="col-md-6">
	                            		<h5>SYSTEM REVIEW</h5>
	                            		<div id="subjective-ros"></div>    
	                            	</div>
	                            	
	                            	<div class="col-md-6">
	                            		<h5>PHYSICAL EXAM</h5>
	                            		<div id="objective-ros"></div>    
	                            	</div>
	                            	                             
	                            </div>
	                        </div>
	                    </div>
	                </div>
	            </div>
	            <div class="row">
	                <div class="col-md-12">
	                    <!-- Widget -->
	                    <div class="widget">
	                        <!-- Widget head -->
	                        <div class="widget-head">
	                          <h5 class="pull-left" style="font-weight:bold">Assessment</h5>
	                          <div class="widget-icons pull-right">
	                          </div>
	                          <div class="clearfix"></div>
	                        </div>              
	                        
	                        <!-- Widget content -->
	                        <div class="widget-content">
	                            <div class="padd">
	                            	<div class="col-md-6">
	                            		<h5>Diagnosis</h5>
	                            		<div id="visit_diagnosis_original"></div>    
	                            	</div>
	                            	<div class="col-md-6">
	                            		<h5>Other Notes</h5>
	                            		<div id="assessment"></div>    
	                            	</div>
	                            	                             
	                            </div>
	                        </div>
	                    </div>
	                </div>
	            </div>

	            <div class="row">
	                <div class="col-md-12">
	                    <!-- Widget -->
	                    <div class="widget">
	                        <!-- Widget head -->
	                        <div class="widget-head">
	                          <h5 class="pull-left" style="font-weight:bold">Plan</h5>
	                          <div class="widget-icons pull-right">
	                          </div>
	                          <div class="clearfix"></div>
	                        </div>              
	                        
	                        <!-- Widget content -->
	                        <div class="widget-content">
	                            <div class="padd">
	                            	
	                            	<div class="col-md-12">
	                            		<h5>Orders</h5>
	                            		<div id="orders"></div>    
	                            	</div>
	                            	
	                            	<div class="col-md-12">
	                            		<h5>Referral</h5>
	                            		<div id="other-plans"></div>      
	                            	</div>
	                            	                             
	                            </div>
	                        </div>
	                    </div>
	                </div>
	            </div>

	        </div>	
		</div>
	</div>
</section>

<div class="row" style="margin-top: 5px;">
	<ul>
		<li style="margin-bottom: 5px;">
			<div class="row">
		        <div class="col-md-12 center-align">
			        	<!-- <div id="old-patient-button" style="display:none">
			        				        		
			        		
			        	</div> -->
			        	<!-- <div> -->
			        		<a  class="btn btn-sm btn-info" onclick="close_side_bar()"><i class="fa fa-folder-closed"></i> CLOSE SIDEBAR</a>
			        	<!-- </div> -->
			        		
		               
		        </div>
		    </div>
			
		</li>
	</ul>
</div>