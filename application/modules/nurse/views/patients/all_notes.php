<?php

$rs2 = $this->nurse_model->get_visit_symptoms($visit_id);
$num_rows2 = count($rs2);

$v_data['signature_location'] = base_url().'assets/signatures/';
$v_data['query'] =  $query_data = $this->nurse_model->get_notes($type, $visit_id);

// var_dump($type);

if($query_data->num_rows() > 0)
{
	foreach ($query_data->result() as $key => $value_two) {
		# code...
		$summary = $value_two->notes_name;
	}
	
}
else
{
	$summary = '';
}
if(!isset($mobile_personnel_id))
{
	$mobile_personnel_id = NULL;
}

if($type == 17)
{
	$checked = 'past_diagnosis_dx';

}else if($type == 18)
{
	$checked = 'past_treatment_rx';
	// var_dump($summary); die();
}

else if($type == 18)
{
	$checked = 'chief_complain';
	// var_dump($summary); die();
}


$v_data['mobile_personnel_id'] = $mobile_personnel_id;
$v_data['module'] = $module;
$notes = $this->load->view('nurse/patients/notes', $v_data, TRUE);
if($module <> 1)
{

?>
<div class='col-md-12'>
	<div class="row">
    	<div class='col-md-8'>
        	<input type="hidden" name="date" value="<?php echo date('Y-m-d');?>" />
        	<input type="hidden" name="time" value="<?php echo date('H:i');?>" />
            <textarea class='form-control' rows="5" id='<?php echo $checked.$visit_id;?>' placeholder="Describe" ><?php echo $summary?></textarea>
        </div>
    
    	<div class='col-md-4 center-align'>
    		<a class='btn btn-info btn-sm' type='submit' onclick='save_nurses_notes(<?php echo $visit_id;?>,<?php echo $type;?>)'> Save Note</a>
    	</div>
    </div>
</div>
<br>
<div class='col-md-12' style="margin-top: 20px !important;">

	<?php echo $notes?>
</div>
<?php
}
else
{
	?>
	<div class='col-md-12'>
		<?php echo $notes?>
	</div>
	<?php

}

?>
