<?php
// get visit details


// $visit_rs = $this->reception_model->get_visit_details($visit_id);

// foreach ($visit_rs as $key) {
// 	# code...

// 	$time_end=$key->time_end;
// 	$time_start=$key->time_start;
// 	$visit_date=$key->visit_date;
// 	$personnel_id=$key->personnel_id;
// 	$room_id=$key->room_id;
// 	$patient_id=$key->patient_id;
// 	// $scheme_name=$key->scheme_name;
// 	$insurance_description=$key->insurance_description;
// 	$patient_insurance_number=$key->patient_insurance_number;
// 	$insurance_limit=$key->insurance_limit;
// 	$principal_member=$key->principal_member;
// }


$patient = $this->reception_model->patient_names2($patient_id);
$visit_type = $patient['visit_type'];
$patient_type = $patient['patient_type'];
$patient_othernames = $patient['patient_othernames'];
$patient_surname = $patient['patient_surname'];
$patient_date_of_birth = $patient['patient_date_of_birth'];
$age = $this->reception_model->calculate_age($patient_date_of_birth);
$v_data['age'] = $age;
$gender = $patient['gender'];
$account_balance = $patient['account_balance'];
$phone_number = $patient['patient_phone_number'];
$patient_id = $patient['patient_id'];
$visit_type_name = $patient['visit_type_name'];
$chart_type = $patient['chart_type'];

?>
<div class="row">
	<div class="row">
	

	<div class="center-align">
		<?php
			$error = $this->session->userdata('error_message');
			$validation_error = validation_errors();
			$success = $this->session->userdata('success_message');
			
			if(!empty($error))
			{
				echo '<div class="alert alert-danger">'.$error.'</div>';
				$this->session->unset_userdata('error_message');
			}
			
			if(!empty($validation_error))
			{
				echo '<div class="alert alert-danger">'.$validation_error.'</div>';
			}
			
			if(!empty($success))
			{
				echo '<div class="alert alert-success">'.$success.'</div>';
				$this->session->unset_userdata('success_message');
			}


		?>
	</div>
	
	<?php //echo $this->load->view("nurse/allergies_brief", '', TRUE);?>
	<?php //echo $this->load->view("patient_history", '', TRUE);?>
	<div class="panel-body">
		<input type="hidden" name="patient_id" id="patient_id" value="<?php echo $patient_id;?>">
		<input type="hidden" name="chart_type" id="chart_type" value="<?php echo $chart_type;?>">
		<div class="col-print-6" >
			
			<div id="page_item"></div>
			<div class="row">
				<div class="col-md-12" style="margin-top:10px;">
					<table class="table table-condensed table-stripped table-bordered">
						<tr>
							<td>Patient Name</td>
							<td><?php echo $patient['patient_surname']?></td>
						</tr>
						<tr>
							<td>Gender</td>
							<td><?php echo $patient['gender']?></td>
						</tr>
						<tr>
							<td>Age</td>
							<td><?php echo $age?></td>
						</tr>

						<tr>
							<td>Account</td>
							<td><?php echo $patient['visit_type_name']?></td>
						</tr>
						
						<tr>
							<td>Account Balance</td>
							<td><?php echo $patient['account_balance']?></td>
						</tr>
					</table>
					
				</div>
				
			</div>
			
			
		</div>
		<div class="col-print-6" >
			<a href="<?php echo site_url().'patients'?>" class="btn btn-sm btn-warning pull-right" style="margin-bottom: 10px"> <i class="fa fa-arrow-left"></i> Back to patients </a>
			<div class="col-md-12" style="height: 500px; overflow-y:  scroll;" >

				<ul class="nav nav-tabs nav-justified">
					<li class="active" ><a href="#notes-billing" data-toggle="tab" >Notes & Billing <i class="fa fa-recycle"></i></a></li>
					<li><a href="#prescription-old" data-toggle="tab" onclick="prescription_view()">Prescription</a></li>
					<li><a href="#sicksheet" data-toggle="tab" onclick="sicksheet_view()">Sick Leave</a></li>
					<li><a href="#uploads" data-toggle="tab" onclick="uploads_view()">Uploads</a></li>				
					<!-- <li><a href="#quotation-form" data-toggle="tab">Quotation</a></li>
					<li><a href="#patient_details" data-toggle="tab">Patient Details</a></li>
					<li><a href="#visit_trail" data-toggle="tab">Visit Trail</a></li> -->
				</ul>
				<div class="tab-content" style="padding-bottom: 9px; border-bottom: 1px solid #ddd;">
					
					<div class="tab-pane active" id="notes-billing" >
						<div id="visit-billing-items"></div>
					</div>
					<div class="tab-pane" id="prescription-old">
						
						<div id="visit-prescription"></div>
					</div>
					<div class="tab-pane" id="sicksheet">
						<
						<div id="sick-sheet"></div>
					</div>
					<div class="tab-pane " id="uploads">
						
						<div id="patients-uploads"></div>
						
					</div>
					<div class="tab-pane " id="diary">
					</div>
					<div class="tab-pane" id="patient_details">

					
					</div>
					<div class="tab-pane" id="billing-form">

						<?php //echo $this->load->view("billing", '', TRUE);?>
					</div>
					<div class="tab-pane" id="quotation-form">

					
					</div>
					<div class="tab-pane" id="visit_trail">
						
					</div>
				</div>

			</div>
		</div>
			
	</div>
	</div>
</div>	  


		
	
  
  <script type="text/javascript">
  	
	var config_url = $("#config_url").val();
		
	$(document).ready(function(){
		

		var patient_id = $("#patient_id").val();
		var chart_type = $("#chart_type").val();
		// alert(patient_id);

		get_page_item(1,patient_id,chart_type);

		// prescription_view();
		// sicksheet_view();
		// uploads_view();


	});
	function get_page_item(page_id,patient_id,chart_type = null)
    {
        // alert(page_id);
        // get_page_links(page_id,patient_id);
        // if(page_id > 1)
        // {
            var visit_id = null;//window.localStorage.getItem('visit_id');
             // alert(visit_id);
            if(visit_id > 0)
            {

              var visit_id = visit_id;
              // get_page_header(visit_id); 
              var url = "<?php echo base_url();?>dental/get_page_item/"+page_id+"/"+patient_id+"/"+visit_id+"/"+chart_type;  
              // alert(url);
              $.ajax({
              type:'POST',
              url: url,
              data:{page_id: page_id,patient_id: patient_id},
              dataType: 'text',
              success:function(data){
                  var data = jQuery.parseJSON(data);
                  var page_item = data.page_item;

                  // alert(page_item);
                  $('#page_item').html(data.page_item);
              },
              error: function(xhr, status, error) {
              // alert("XMLHttpRequest=" + xhr.responseText + "\ntextStatus=" + status + "\nerrorThrown=" + error);
                  // display_patient_bill(visit_id);
              }
              });                
            }
            else
            {
              var visit_id = null;
             
            
                var url = "<?php echo base_url();?>dental/get_page_item/"+page_id+"/"+patient_id+"/"+visit_id+"/"+chart_type;  
            	// alert(url);
                $.ajax({
                type:'POST',
                url: url,
                data:{page_id: page_id,patient_id: patient_id},
                dataType: 'text',
                success:function(data){
                    var data = jQuery.parseJSON(data);
                    var page_item = data.page_item;
                    $('#page_item').html(data.page_item);
                },
                error: function(xhr, status, error) {
                alert("XMLHttpRequest=" + xhr.responseText + "\ntextStatus=" + status + "\nerrorThrown=" + error);
                    // display_patient_bill(visit_id);
                }
                });   
              
              
            }
       
       	visit_display_billing(patient_id);
       // return false;

    }
	function visit_display_billing(patient_id)
	{



		var config_url = $('#config_url').val();
        var data_url = config_url+"dental/view_patient_notes/"+patient_id;

        // alert(data_url);
        $.ajax({
        type:'POST',
        url: data_url,
        data:{patient_id: patient_id},
        dataType: 'text',
		success:function(data)
		{
			// alert(data);
			$("#visit-billing-items").html(data);		
		},
        error: function(xhr, status, error) {
	        //alert("XMLHttpRequest=" + xhr.responseText + "\ntextStatus=" + status + "\nerrorThrown=" + error);
	        alert(error);
        }

        });
	    
	}


  function prescription_view()
  {

    // var myTarget = document.getElementById("add_item");
    // myTarget.style.display = 'block';

    var visit_id = null;
    var patient_id = document.getElementById("patient_id").value;
    // alert(patient_id);
     var XMLHttpRequestObject = false;
          
      if (window.XMLHttpRequest) {
      
          XMLHttpRequestObject = new XMLHttpRequest();
      } 
          
      else if (window.ActiveXObject) {
          XMLHttpRequestObject = new ActiveXObject("Microsoft.XMLHTTP");
      }
      
      var config_url = document.getElementById("config_url").value;
      var url = config_url+"dental/display_patient_prescription/"+visit_id+"/"+patient_id;
      // alert(url);
      if(XMLHttpRequestObject) {
                  
          XMLHttpRequestObject.open("GET", url);
                  
          XMLHttpRequestObject.onreadystatechange = function(){
              
              if (XMLHttpRequestObject.readyState == 4 && XMLHttpRequestObject.status == 200) {

                  document.getElementById("visit-prescription").innerHTML=XMLHttpRequestObject.responseText;
              }
          }
                  
          XMLHttpRequestObject.send(null);
      }
  }
  function sicksheet_view()
	{
		 // var myTarget = document.getElementById("add_item");
    // myTarget.style.display = 'block';

    var visit_id = null;
    var patient_id = document.getElementById("patient_id").value;
    // alert(patient_id);
     var XMLHttpRequestObject = false;
          
      if (window.XMLHttpRequest) {
      
          XMLHttpRequestObject = new XMLHttpRequest();
      } 
          
      else if (window.ActiveXObject) {
          XMLHttpRequestObject = new ActiveXObject("Microsoft.XMLHTTP");
      }
      
      var config_url = document.getElementById("config_url").value;
      var url = config_url+"dental/display_patient_sick_leave/"+visit_id+"/"+patient_id;
      // alert(url);
      if(XMLHttpRequestObject) {
                  
          XMLHttpRequestObject.open("GET", url);
                  
          XMLHttpRequestObject.onreadystatechange = function(){
              
              if (XMLHttpRequestObject.readyState == 4 && XMLHttpRequestObject.status == 200) {

                  document.getElementById("sick-sheet").innerHTML=XMLHttpRequestObject.responseText;
              }
          }
                  
          XMLHttpRequestObject.send(null);
      }
	}

	function uploads_view()
	{
		 // var myTarget = document.getElementById("add_item");
    // myTarget.style.display = 'block';

    var visit_id = null;
    var patient_id = document.getElementById("patient_id").value;
    // alert(patient_id);
     var XMLHttpRequestObject = false;
          
      if (window.XMLHttpRequest) {
      
          XMLHttpRequestObject = new XMLHttpRequest();
      } 
          
      else if (window.ActiveXObject) {
          XMLHttpRequestObject = new ActiveXObject("Microsoft.XMLHTTP");
      }
      
      var config_url = document.getElementById("config_url").value;
      var url = config_url+"dental/display_patient_uploads/"+visit_id+"/"+patient_id;
      // alert(url);
      if(XMLHttpRequestObject) {
                  
          XMLHttpRequestObject.open("GET", url);
                  
          XMLHttpRequestObject.onreadystatechange = function(){
              
              if (XMLHttpRequestObject.readyState == 4 && XMLHttpRequestObject.status == 200) {

                  document.getElementById("patients-uploads").innerHTML=XMLHttpRequestObject.responseText;
              }
          }
                  
          XMLHttpRequestObject.send(null);
      }
	}

</script>