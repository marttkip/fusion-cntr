<section class="panel" >
	<div class="panel-body" style="height:80vh">
<div class="col-md-12" id="sidebar-container" style="margin-bottom: 20px;" >
<?php

$next_app_rs = $this->nurse_model->get_next_appointment($visit_id,$patient_id);


$visit_date = '';
$appointment_note = '';
if($next_app_rs->num_rows() == 1)
{
	foreach ($next_app_rs->result() as $key => $value) {
		# code...
		$visit_date = $value->visit_date;
		$appointment_note = $value->appointment_note;
	}
	
}

?>
	
	<!-- <form id="next_appointment_detail" method="post" > -->
		<div class="row">
			<div class="form-group">
		        <label class="col-md-2 control-label">Date: </label>
		        
		          <div class="col-md-10">
		              <div class="input-group">
		                  <span class="input-group-addon">
		                      <i class="fa fa-calendar"></i>
		                  </span>
		                  <input data-format="yyyy-MM-dd" type="text" data-plugin-datepicker class="datepicker form-control" name="next_appointment_date" id="next_appointment_date" placeholder="Date" value="<?php echo $visit_date?>" style="background-color: #ffffff;">
		              </div>
		          </div>
		      </div>
		      <div class="form-group">
		        <label class="col-md-2 control-label">Note: </label>
		        <div class="col-md-10">
		            <textarea class="form-control" name="appointment_note" id="appointment_note"><?php echo $appointment_note?></textarea>
		        </div>
		      </div>
		      </br>
		      <input type="hidden" name="visit_id" id="visit_id" value="<?php echo $visit_id?>">
		      <input type="hidden" name="patient_id" id="patient_id" value="<?php echo $patient_id?>">
		      <input type="hidden" name="department_id" id="department_id" value="<?php echo $department_id?>">
		      <input type="hidden" name="doctor_id" id="doctor_id" value="<?php echo $personnel_id?>">
		      <div class="row">
		        <div class="center-align ">
		          <button class="btn btn-info btn-sm" type="submit" onclick="save_appointment()">Save Appointment Detail</button>
		        </div>
		      </div>
		</div>
       
    <!-- </form> -->

</div>
</div>
</section>
<div class="row" style="margin-top: 5px;">
	<ul>
		<li style="margin-bottom: 5px;">
			<div class="row">
		        <div class="col-md-12 center-align">
			        	<!-- <div id="old-patient-button" style="display:none">
			        				        		
			        		
			        	</div> -->
			        	<!-- <div> -->
			        		<a  class="btn btn-sm btn-info" onclick="close_side_bar()"><i class="fa fa-folder-closed"></i> CLOSE SIDEBAR</a>
			        	<!-- </div> -->
			        		
		               
		        </div>
		    </div>
			
		</li>
	</ul>
</div>