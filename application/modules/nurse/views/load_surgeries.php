<?php
$patient_id = $this->nurse_model->get_patient_id($visit_id);

$surgery_rs = $this->nurse_model->get_surgeries($patient_id);

$num_surgeries = count($surgery_rs);



$surgery_list_rs = $this->nurse_model->get_surgery_list();
$num_surgeries_list = count($surgery_list_rs);

// var_dump($surgery_list_rs);die();
$list = '<option value="">---- select from list ---</option>';
if($num_surgeries_list > 0){
	foreach ($surgery_list_rs as $key_surgeries):
		$surgery_list_id = $key_surgeries->surgery_list_id;
		$surgery_name = $key_surgeries->surgery_name;


		$list .= '<option value="'.$surgery_list_id.'">'.$surgery_name.'</option>';
	endforeach;
}



echo
"
<div class='col-md-6'>
<table align='center' class='table table-striped table-hover table-condensed'>
	<tr>
		<th>Year</th>
		<th>Surgery</th>
		<th></th>
	</tr>
	<tr>
		<td valign='top'><input type='text' id='datepicker' autocomplete='off' size='5' class='form-control'/>
  
		</td>
		<td valign='top'>
			<select id='surgery_id' class='form-control'>
				".$list."
			</select>
		</td>
      
        <td><input type='button' class='btn btn-sm btn-success' value='Save' onclick='save_surgery(".$visit_id.")' /></td>
    </tr>
 </table>
</div>
<div class='col-md-6'>
		<table align='center' class='table table-striped table-hover table-condensed'>
		<tr>
			<th>Year</th>
			<th>Surgery</th>
			<th></th>
		</tr>
		";
		if($num_surgeries > 0){
			foreach ($surgery_rs as $key):
			$date = $key->surgery_year;
			$month = $key->surgery_name;
			$description = $key->surgery_description;
			$id = $key->surgery_id;
			echo
			"
					<tr>
						<td>".$date."</td>
						<td>".$month."</td>
						<td>
						<a class='btn btn-sm btn-danger' onclick='delete_surgery(".$id.", ".$visit_id.")'><i class='fa fa-trash'></i></a>
						</td>
					</tr>
			";
			endforeach;
		}
echo
"
 </table>
</div>

";
  // <td><textarea id='surgery_description' class='form-control'></textarea></td>
?>