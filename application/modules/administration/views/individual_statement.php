<!-- search -->
<?php //echo $this->load->view('search/search_patient', '', TRUE);

$res = $patient->row();
$patient_id = $res->patient_id;
$patient_surname = $res->patient_surname;
$patient_othernames = $res->patient_othernames;
$title_id = $res->title_id;
$patient_date_of_birth = $res->patient_date_of_birth;
$gender_id = $res->gender_id;
$religion_id = $res->religion_id;
$civil_status_id = $res->civil_status_id;
$patient_email = $res->patient_email;
$patient_address = $res->patient_address;
$patient_postalcode = $res->patient_postalcode;
$patient_town = $res->patient_town;
$patient_phone1 = $res->patient_phone1;
$patient_phone2 = $res->patient_phone2;
$patient_kin_sname = $res->patient_kin_sname;
$patient_kin_othernames = $res->patient_kin_othernames;
$relationship_id = $res->relationship_id;
$patient_national_id = $res->patient_national_id;
$insurance_company_id = $res->insurance_company_id;
$next_of_kin_contact = $res->patient_kin_phonenumber1;


?>
<!-- end search -->

<div class="row">
    <div class="col-md-12">

        <section class="panel">
            <header class="panel-heading">
                
                <h2 class="panel-title"><?php echo $title;?></h2>
                <a href="<?php echo site_url().'print-patient-statement/'.$patient_id;?>" class="btn btn-sm btn-warning pull-right" style="margin-top: -25px;" target="_blank"> Print Statement</a>
            </header>
            
            <div class="panel-body">
                <div class="well well-sm info">
                    <h5 style="margin:0;">
                        <div class="row">
                            <div class="col-md-3">
                                <div class="row">
                                    <div class="col-md-6">
                                        <strong>First name:</strong>
                                    </div>
                                    <div class="col-md-6">
                                        <?php echo $patient_surname;?>
                                    </div>
                                </div>
                            </div>
                            
                            <div class="col-md-3">
                                <div class="row">
                                    <div class="col-md-6">
                                        <strong>Other names:</strong>
                                    </div>
                                    <div class="col-md-6">
                                        <?php echo $patient_othernames;?>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </h5>
                </div>
			<?php
		$error = $this->session->userdata('error_message');
		$success = $this->session->userdata('success_message');
		
		if(!empty($error))
		{
			echo '<div class="alert alert-danger">'.$error.'</div>';
			$this->session->unset_userdata('error_message');
		}
		
		if(!empty($success))
		{
			echo '<div class="alert alert-success">'.$success.'</div>';
			$this->session->unset_userdata('success_message');
		}
				
		$result =  '';
		if($module > 0)
		{

		}
		else
		{
			$result = '<a href="'.site_url().'administration/patient_statement" class="btn btn-success">Back to Statements</a>';
	
		}
		
		
		
		$result .= '';
		// var_dump($query->num_rows());die();
		//if users exist display them
		if ($query->num_rows() > 0)
		{
			$count = 0;
			
			
			$result .= 
			'
				<table class="table table-hover table-bordered ">
				  <thead>
					<tr>
					  <th style="text-align:center" rowspan=2>Date</th>
					  <th rowspan=2>Document Number</th>
					  <th rowspan=2>RX</th>
					  <th colspan=2 style="text-align:center;">Amount</th>
					
					</tr>
					<tr>
					  
					  <th style="text-align:center">Invoice</th>
					  <th style="text-align:center">Payment</th>
					  <th style="text-align:center">Balance</th>
					</tr>
				  </thead>
				  <tbody>
			';
			
			
			$personnel_query = $this->personnel_model->get_all_personnel();
			
			
			
			$total_waiver = 0;
			$total_payments = 0;
			$total_invoice = 0;
			$total_balance = 0;
			$total_rejected_amount = 0;
			$total_cash_balance = 0;
			$total_insurance_payments =0;
			$total_insurance_invoice =0;
			$total_payable_by_patient = 0;
			$total_payable_by_insurance = 0;
			$total_debit_notes = 0;
			$total_credit_notes= 0;
			foreach ($query->result() as $row)
			{
				$total_invoiced = 0;
				$visit_date = date('jS M Y',strtotime($row->transaction_date));
				
				
				$visit_id = $row->visit_id;
				$patient_id = $row->patient_id;
				$personnel_id = $row->personnel_id;
				$dependant_id = $row->dependant_id;
				$strath_no = $row->strath_no;
				// $visit_type_id = $row->visit_type;
				$patient_number = $row->patient_number;
				// $visit_type = $row->visit_type;
				// $visit_table_visit_type = $visit_type;
				// $patient_table_visit_type = $visit_type_id;
				// $rejected_amount = $row->amount_rejected;
				$visit_invoice_number = $row->visit_invoice_number;
				$visit_invoice_id = $row->visit_invoice_id;
				// $parent_visit = $row->parent_visit;
				$branch_code = $row->branch_code;

				// var_dump($visit_invoice_id);die();
				// $coming_from = $this->reception_model->coming_from($visit_id);
				// $sent_to = $this->reception_model->going_to($visit_id);
				$visit_type_name = $row->payment_type_name;
				$patient_othernames = $row->patient_othernames;
				$patient_surname = $row->patient_surname;
				$patient_date_of_birth = $row->patient_date_of_birth;

				$doctor = $row->personnel_fname;
				

				$invoice_total = $row->dr_amount;
				$payments_value = $this->accounts_model->get_visit_invoice_payments($visit_invoice_id);
				$balance  = $this->accounts_model->balance($payments_value,$invoice_total);

				$total_payable_by_patient += $invoice_total;
				$total_payments += $payments_value;
				$total_balance += $balance;

				$payments_rs = $this->accounts_model->get_visit_invoice_payments_item($visit_invoice_id);
               
                $payments_made = '';
                if($payments_rs->num_rows() > 0)
                {
                    foreach ($payments_rs->result() as $key_items):
             
                        // $payment_type = $key_items->payment_type;
                        // $payment_status = $key_items->payment_status;
                       
                        $payment_method = $key_items->payment_method;
                        $amount_paid = $key_items->total_amount;
                        $payment_date = $key_items->payment_date;
                        

                        $payments_made .='<tr>
											<td>'.$payment_date.'</td>
											<td>'.$payment_method.'</td>
											<td>'.number_format($amount_paid).'</td>
										</tr>';
                        


                    endforeach;
                    
                }
                else
                {
                	 $payments_made .='<tr>
											<td colspan=2>No Payments Done</td>
										</tr>';
                }

				$item_invoiced_rs = $this->accounts_model->get_visit_invoice_items($visit_invoice_id);

				// var_dump($item_invoiced_rs);die();
				$charged_services = '<table class="table">
									  <thead>
										<tr>
										  <th >Name</th>
										  <th >Units</th>
										  <th >Charge</th>
										  <th >Total</th>										
										</tr>
									  <tbody>';

				if($item_invoiced_rs->num_rows() > 0){
					$s=0;
					$total_nhif_days = 0;
					$total = 0;
					
					foreach ($item_invoiced_rs->result() as $key => $key_items):
						$service_charge_id = $key_items->service_charge_id;
						$service_charge_name = $key_items->service_charge_name;
						$visit_charge_amount = $key_items->visit_charge_amount;
						$service_name = $key_items->service_name;
						$units = $key_items->visit_charge_units;
						$service_id = $key_items->service_id;
						$personnel_id = $key_items->personnel_id;
						$total += $units*$visit_charge_amount;

						$charged_services .=  '<tr>
													<td>'.$service_charge_name.'</td>
													<td>'.$units.'</td>
													<td>'.$visit_charge_amount.'</td>
													<td> '.number_format($units*$visit_charge_amount,2).'</td>
												</tr>';
						
					endforeach;
					$charged_services .=  '<tr>
													<td colspan=3>TOTAL</td>
													<td> '.number_format($total,2).'</td>
												</tr>';
				}
				$charged_services .= '</tbody>
									</table>
									<p><strong>PAYMENTS</strong><p>';

				$charged_services .= '<table class="table">
									  <thead>
										<tr>
										  <th >Date</th>
										  <th >Amount</th>										
										</tr>
										</thead>
									  <tbody>
									  	'.$payments_made.'
										</tbody>
									</table>';


				$count++;
				
				

				$result .= 
				'
					<tr>
						<td style="text-align:center">'.$visit_date.'</td>
						<td>'.$visit_invoice_number.'</td>
						<td>'.$charged_services.'</td>
						<td style="text-align:center">'.number_format($invoice_total,2).'</td>
						<td style="text-align:center">'.number_format($payments_value,2).'</td>
						<td style="text-align:center">'.number_format($balance,2).'</td>
					</tr> 
				';
				

				
				
			}
				$result .= 
					'
						<tr>
							<td></td>
							<td></td>
							<td style="text-align:center">Totals</td>
							<td style="text-align:center; font-weight:bold;"> '.number_format($total_payable_by_patient,2).'</td>
							<td style="text-align:center; font-weight:bold;">'.number_format($total_payments,2).'</td>
							<td style="text-align:center; font-weight:bold;">'.number_format($total_balance,2).'</td>
						</tr> 
					';
				$Balance =  $total_payable_by_patient -$total_payments;
					$result .= 
					'
						<tr>
							<td></td>
							<td></td>
							<td style="text-align:center; font-weight:bold;">Balance</td>
							<td colspan="3" style="text-align:center; font-weight:bold;">'.number_format($Balance,2).'</td>
						</tr> 
					';
			$result .= 
			'
						  </tbody>
						</table>
			';
		}
		
		else
		{
			$result .= "There are no items";
		}
		
		echo $result;
?>
          	</div>
          
          	<div class="widget-foot">
                                
				<?php if(isset($links)){echo $links;}?>
            
            </div>
		</section>
    </div>
  </div>