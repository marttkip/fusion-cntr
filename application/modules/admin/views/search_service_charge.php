<?php

$result = '<table class="table table-condensed table-bordered">
				<thead>
					<th>#</th>
					<th>Department</th>
					<th>Service</th>
					<th>Charge Name</th>
					<th>Unit Price</th>
				</thead>
				<tbody>
			';
if($query != null)
{
	if($query->num_rows() > 0)
	{
		foreach ($query->result() as $key => $value) {
			# code...
			$service_charge_id = $value->service_charge_id;
			$service_charge_name = $value->service_charge_name;
			$service_charge_amount = $value->service_charge_amount;
			// $service_charge_code = $value->service_charge_code;
			$service_name = $value->service_name;
			$department_name = $value->department_name;
			// $brand_name = $value->brand_name;
			// $service_charge_code = $value->service_charge_code;

			if(empty($service_charge_amount))
			{
				$service_charge_amount = 0;
			}

			$result .='

						<tr >
							<td><input type="checkbox" name="" class="menu-icon " ></td>
							<td>'.$department_name.'</td>
							<td>'.$service_name.'</td>
							<td>'.$service_charge_name.'</td>
							<td>'.number_format($service_charge_amount,2).'</td>
						</tr>';
		}



		
	}
	else
	{
		$result .='
					<tr>
						<td >No charges found with that name</td>
					
				  </li>';
	}

	

}
else
{
	$result .='
					<tr>
						<td >No charges found with that name</td>
					
				  </li>';
}

$result .='</tbody>
				</table>';

echo $result;

?>