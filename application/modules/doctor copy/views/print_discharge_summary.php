<?php

//patient details
$visit_type = $patient['visit_type'];
$patient_type = $patient['patient_type'];
$patient_othernames = $patient['patient_othernames'];
$patient_surname = $patient['patient_surname'];
$patient_surname = $patient['patient_surname'];
$patient_number = $patient['patient_number'];
$gender = $patient['gender'];

$today = date('jS F Y H:i a',strtotime(date("Y:m:d h:i:s")));
$visit_date = date('jS F Y',strtotime($this->accounts_model->get_visit_date($visit_id)));

//doctor
$doctor = $this->accounts_model->get_att_doctor($visit_id);

$visit_rs = $this->accounts_model->get_visit_details($visit_id);

if($visit_rs->num_rows() > 0)
{
    foreach ($visit_rs->result() as $key => $value) {
        # code...
        $visit_date = $value->visit_date;
        $admission_date = $value->admission_date;
        $discharge_date = $value->discharge_date;
    }
}


$discharge_date = date('jS F Y',strtotime($discharge_date));
$admission_date = date('jS F Y',strtotime($admission_date));

?>

<!DOCTYPE html>
<html lang="en">
    <head>
        <title><?php echo $contacts['company_name'];?> | Invoice</title>
        <!-- For mobile content -->
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <!-- IE Support -->
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <!-- Bootstrap -->
        <link rel="stylesheet" href="<?php echo base_url()."assets/themes/porto-admin/1.4.1/";?>assets/vendor/bootstrap/css/bootstrap.css" media="all"/>
        <link rel="stylesheet" href="<?php echo base_url()."assets/themes/porto-admin/1.4.1/";?>assets/stylesheets/theme-custom.css" media="all"/>
        <style type="text/css">
			.receipt_spacing{letter-spacing:0px; font-size: 12px;}
			.center-align{margin:0 auto; text-align:center;}
			
			.receipt_bottom_border{border-bottom: #888888 medium solid;}
			.row .col-md-12 table {
				border:solid #000 !important;
				border-width:1px 0 0 1px !important;
				font-size:10px;
			}
			.row .col-md-12 th, .row .col-md-12 td {
				border:solid #000 !important;
				border-width:0 1px 1px 0 !important;
			}
			.table thead > tr > th, .table tbody > tr > th, .table tfoot > tr > th, .table thead > tr > td, .table tbody > tr > td, .table tfoot > tr > td
			{
				 padding: 2px;
			}
			
			.row .col-md-12 .title-item{float:left;width: 130px; font-weight:bold; text-align:right; padding-right: 20px;}
			.title-img{float:left; padding-left:30px;}
			img.logo{max-height:70px; margin:0 auto;}
		</style>
    </head>
    <body class="receipt_spacing">
    	<div class="row receipt_bottom_border">
        	<div class="col-xs-6 pull-left" >
            	<img src="<?php echo base_url().'assets/logo/'.$contacts['logo'];?>" alt="<?php echo $contacts['company_name'];?>" class="img-responsive logo"/>
            </div>
    	   <div class="col-xs-6 pull-right">
            	<strong>
                	<?php echo $contacts['company_name'];?><br/>
                    P.O. Box <?php echo $contacts['address'];?> <?php echo $contacts['post_code'];?>, <?php echo $contacts['city'];?><br/>
                    E-mail: <?php echo $contacts['email'];?>. Tel : <?php echo $contacts['phone'];?><br/>
                    <?php echo $contacts['location'];?>, <?php echo $contacts['building'];?>, <?php echo $contacts['floor'];?><br/>
                </strong>
            </div>
        </div>
        
        
        <!-- Patient Details -->
    	<div class="row " style="margin:10px;border:2px solid grey;padding: 5px">
        	<div class="col-md-4 pull-left" style="font-size:15px;margin: 5px;">
                <strong>IP/No:</strong> <?php echo $patient_number; ?><br>
                <strong>Name:</strong><?php echo $patient_surname.' '.$patient_othernames; ?><br>
                <strong>DOA:</strong> <?php echo $admission_date; ?>
            </div>
        	<div class="col-md-4 pull-right" style="font-size:15px;">
            	<strong>AGE:</strong> ......................................<br>
                <strong>Gender:</strong> ...................................... <br>
                <strong>DOD:</strong> <?php echo $discharge_date; ?>
                    
            </div>
        </div>
        
    	<div class="row receipt_bottom_border">
        	<div class="col-md-12 center-align" style="font-size: 15px;">
            	<strong>DISCHARGE SUMMARY</strong>
            </div>
        </div>
        <div class="padd">

            <div class="row">
                <div class="" style="margin:20px;border:2px solid grey;padding: 5px">
                    <?php
                    $v_data['query'] =  $query_data = $this->nurse_model->get_notes(10, $visit_id);
                    $summary = '';
                    if($query_data->num_rows() > 0)
                    {
                        foreach ($query_data->result() as $key => $value_two) {
                            # code...
                            $summary = $value_two->notes_name;
                        }
                        
                    }
                    echo $summary;
                    ?>
                </div>
            </div>
            
        </div>
    	
        
    	<div class="padd" style="font-style:italic; font-size:14px; margin-top: 10px; padding: 10px">
        	
            <p>Discharging Officer: ..................................................</p>
          
            <p>Signature: ............................................................</p>
           
            <?php echo date('jS M Y H:i a'); ?> Thank you
            
        </div>
    </body>
    
</html>