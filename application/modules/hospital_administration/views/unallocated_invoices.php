
 <div class="row">
	<div class="col-md-12">
			<?php echo $this->load->view('search_batches', '', TRUE);?>

	</div>
</div>
<div class="row">
    <div class="col-md-12">

      
 <section class="panel">
    <header class="panel-heading">
        <h2 class="panel-title"><?php echo $title;?></h2>
      </header>             

          <!-- Widget content -->
                <div class="panel-body">
          
<?php
		$error = $this->session->userdata('error_message');
		$success = $this->session->userdata('success_message');
		
		if(!empty($success))
		{
			echo '
				<div class="alert alert-success">'.$success.'</div>
			';
			$this->session->unset_userdata('success_message');
		}
		
		if(!empty($error))
		{
			echo '
				<div class="alert alert-danger">'.$error.'</div>
			';
			$this->session->unset_userdata('error_message');
		}


		$search = $this->session->userdata('unallocated_invoice_search');
		
		if(!empty($search))
		{
			echo '<a href="'.site_url().'hospital_administration/close_search" class="btn btn-warning btn-sm"><i class="fa fa-times"></i> Close search</a>';
		}
			
		$result = '';
		
		//if users exist display them
		if ($query->num_rows() > 0)
		{
			$count = $page;
			
			$result .= 
				'
					<table class="table table-hover table-bordered ">
					  <thead>
						<tr>
						  <th>#</th>
						  <th><a >Invoice Number</a></th>
						  <th><a >Customer Name</a></th>
						  <th><a >Amount</a></th>
						  <th colspan="1">Actions</th>
						</tr>
					  </thead>
					  <tbody>
				';
			
			//get all administrators
			$administrators = $this->personnel_model->retrieve_personnel();
			if ($administrators->num_rows() > 0)
			{
				$admins = $administrators->result();
			}
			
			else
			{
				$admins = NULL;
			}
			
			foreach ($query->result() as $row)
			{
				
				$invoice_number = $row->invoice_number;
				$customer_name = $row->customer_name;
				$amount  = $this->hospital_administration_model->get_invoice_amount($invoice_number);
			
				
				$count++;
				$result .= 
					'
						<tr>
							<td>'.$count.'</td>
							<td>'.$invoice_number.'</td>
							<td>'.$customer_name.'</td>
							<td>'.number_format($amount,2).'</td>
							<td><a href="'.site_url().'hospital-administration/invoice-detail/'.$invoice_number.'" class="btn btn-sm btn-warning"><i class="fa fa-money"></i> Invoice Detail</a></td>
							
						</tr> 
					';
			}
			
			$result .= 
			'
						  </tbody>
						</table>
			';
		}
		
		else
		{
			$result .= "There are no services";
		}
		?>
            <?php echo $result; ?>
          </div>
          
          <div class="widget-foot">
                                
				<?php if(isset($links)){echo $links;}?>
            
                <div class="clearfix"></div> 
            
            </div>
        
		</section>
        </div>
        </div>
        