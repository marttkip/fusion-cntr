   
 <section class="panel">
    <header class="panel-heading">
        <h2 class="panel-title">Search List</h2>
    </header>             

          <!-- Widget content -->
                <div class="panel-body">
          
			<?php echo form_open("hospital_administration/search_invoices_customers", array("class" => "form-horizontal"));?>
            <div class="row">

                <div class="col-md-4">
                    <div class="form-group">
                        <label class="col-lg-4 control-label">Invoice Number: </label>
                        
                        <div class="col-lg-8">
                          	 <input type="text" class="form-control" name="invoice_number" placeholder="Invoice Number">
                        </div>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                        <label class="col-lg-4 control-label">Name: </label>
                        
                        <div class="col-lg-8">
                          	 <input type="text" class="form-control" name="customer_name" placeholder="Invoice Number">
                        </div>
                    </div>
                </div>
                
                <div class="col-md-3">
                	<div class="center-align">
                        <button type="submit" class="btn btn-info"><i class="fa fa-search"></i> Search</button>
                    </div>
                </div>
            </div>
            
            
            <?php
            echo form_close();
            ?>
            </div>
        
		</section>