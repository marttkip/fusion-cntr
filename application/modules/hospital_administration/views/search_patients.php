   
 <section class="panel">
    <header class="panel-heading">
        <h2 class="panel-title">Search <?php echo $title;?></h2>
    </header>             

          <!-- Widget content -->
                <div class="panel-body">
          
			<?php echo form_open("hospital_administration/search_patient_record", array("class" => "form-horizontal"));?>
            <div class="row center-align">
                <div class="col-md-5">
                    <div class="form-group">
                        <label class="col-lg-4 control-label">Number: </label>
                        
                        <div class="col-lg-8">
                          	 <input type="text" class="form-control" name="patient_number" placeholder="Patient  Number">
                        </div>
                    </div>
                </div>
                <div class="col-md-5">
                    <div class="form-group">
                        <label class="col-lg-4 control-label">Name: </label>
                        
                        <div class="col-lg-8">
                             <input type="text" class="form-control" name="patient_name" placeholder="Name">
                        </div>
                    </div>
                </div>
                
                <div class="col-md-2">
                	<div class="center-align">
                        <button type="submit" class="btn btn-info"><i class="fa fa-search"></i> </button>
                    </div>
                </div>
            </div>
            
            
            <?php
            echo form_close();
            ?>
            </div>
        
		</section>