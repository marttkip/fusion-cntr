<div class="row" style="margin-top: 20px;">
<section class="panel">
  
    <div class="panel-body">
    	<?php echo form_open_multipart('dental/upload_documents/'.$patient_id.'/'.$visit_id, array("class" => "form-horizontal", "role" => "form", "id" => "uploads-form"));?>
    	
    	<input type="hidden" name="patient_id" value="<?php echo $patient_id?>">
    	<input type="hidden" name="visit_id" value="<?php echo $visit_id?>">
	    <div class="row" style="margin-top:10px;">
	        <div class="col-md-12">
	        	<div class="col-md-4">
                	<div class="form-group">
                        <label class="col-lg-5 control-label">Document Type: </label>
                        
                        <div class="col-lg-7">
                            <select class="form-control" name="document_type_id">
                                <?php
                                    if($document_types->num_rows() > 0)
                                    {
                                        foreach($document_types->result() as $res)
                                        {
                                            $document_type_id2 = $res->document_type_id;
                                            $document_type_name = $res->document_type_name;
                                            
                                            echo '<option value="'.$document_type_id2.'">'.$document_type_name.'</option>';
                                        }
                                    }
                                ?>
                            </select>
                        </div>
        			</div>
                </div>
                
	        	<div class="col-md-4">
		            <div class="form-group">
		                <label class="col-lg-5 control-label ">Document Name *: </label>
		                <div class="col-lg-7">
		                    <input type="text" class="form-control " name="document_item_name"  placeholder="Document Title Name">
		                </div>
		            </div>
		        </div>
	        	<div class="col-md-4">
		            <div class="form-group">
		                <label class="col-lg-5 control-label ">Document Scan *: </label>
		                
		                <div class="col-lg-7">
		                    <input type="file" class="form-control " name="document_scan"  value="">
		                </div>
		            </div>
		        </div>
	        </div>
	    </div>
	    <div class="row" style="margin-top:10px;">
	        <div class="col-md-12">
	            <div class="form-actions center-align">
	                <button class="submit btn btn-primary" type="submit">
	                    Upload document scan
	                </button>
	            </div>
	        </div>
	    </div>
	<?php echo form_close();?>
</div>
</section>
</div>

<div class="row" style="margin-top: 5px;">
	<ul>
		<li style="margin-bottom: 5px;">
			<div class="row">
		        <div class="col-md-12 center-align">
			        <a  class="btn btn-sm btn-info" onclick="close_side_bar()"><i class="fa fa-folder-closed"></i> CLOSE SIDEBAR</a>			        
		        </div>
		    </div>
			
		</li>
	</ul>
</div>