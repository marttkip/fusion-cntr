<?php

$document_types = $this->xray_model->all_template();
?>
<?php echo form_open_multipart('radiology/xray/upload_documents/'.$visit_id, array("class" => "form-horizontal", "role" => "form", "id" => "add_result"));?>
			    
    <div class="row"  style="margin-top: 20px !important;">
        <div class="col-md-12">
            	<div class="form-group">
                    <label class="col-lg-2 control-label">Template: </label>
                    
                    <div class="col-lg-10">
                        <select class="form-control" name="document_type_id" required="required" onchange="get_template_details(this.value)">
                        	<option value="">---- SELECT A TEMPLATE -------- </option>
                            <?php
                                if($document_types->num_rows() > 0)
                                {
                                    foreach($document_types->result() as $res)
                                    {
                                        $upload_template_id = $res->upload_template_id;
                                        $upload_template_description = $res->upload_template_description;
                                        $upload_template_title = $res->upload_template_title;
                                        
                                        echo '<option value="'.$upload_template_id.'">'.$upload_template_title.'</option>';
                                    }
                                }
                            ?>
                        </select>
                    </div>
    			</div>

    			<input type="hidden" name="patient_id" id="patient_id" value="<?php echo $patient_id?>">
    			<input type="hidden" name="visit_id" id="visit_id" value="<?php echo $visit_id?>">
	            <div class="form-group">
	                <label class="col-lg-2 control-label ">Results Summary *: </label>
	                <div class="col-lg-10">
	                	<textarea class="form-control cleditor" name="document_upload_description" id="document_upload_description"></textarea>
	                </div>
	            </div>
	            <div class="form-group">
	                <label class="col-lg-2 control-label ">Document Scan *: </label>
	                
	                <div class="col-lg-10">
	                    <input type="file" class="form-control " name="document_scan"  value="">
	                </div>
	            </div>
	        </div>
        	
     </div>
    
    <div class="row" style="margin-top:10px;">
        <div class="col-md-12">
            <div class="form-actions center-align">
                <button class="submit btn btn-primary" type="submit">
                    Upload document scan
                </button>
            </div>
        </div>
    </div>
<?php echo form_close();?>