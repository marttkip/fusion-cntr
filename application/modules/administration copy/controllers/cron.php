<?php   if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Cron  extends MX_Controller
{	
	function __construct()
	{
		parent:: __construct();
		$this->load->model('administration/sync_model');
	}
	
	public function sync_visits()
	{
		
		$date = date('Y-m-d');
		//Sync OSE
		$this->session->set_userdata('branch_code', 'OSE');
		$this->db->where('branch_code = "'.$this->session->userdata('branch_code').'" AND visit_date = "'.$date.'"');
		$query = $this->db->get('visit');
		
		if($query->num_rows() > 0)
		{
			foreach($query->result() as $res)
			{
				$visit_id = $res->visit_id;
				
				if($this->sync_model->syn_up_on_closing_visit($visit_id))
				{
				}
			}
		}
	}
	public function backup()
    {
        // Load the DB utility class
        $this->load->dbutil();
        $date = date('YmdHis');
        $prefs = array(
            'ignore'        => array('table','diseases','icd10'),                     // List of tables to omit from the backup
            'format'        => 'txt',                       // gzip, zip, txt
            'filename'      => $date.'_backup.sql',              // File name - NEEDED ONLY WITH ZIP FILES
            'newline'       => "\n"                         // Newline character used in backup file
        );

        // Backup your entire database and assign it to a variable
        $backup = $this->dbutil->backup($prefs);

        // Load the file helper and write the file to your server
        $this->load->helper('file');
        write_file('G://backups/'.$date.'_backup.sql', $backup);

        // Load the download helper and send the file to your desktop
        // $this->load->helper('download');
        // force_download('mybackup.gz', $backup);



    }

    public function update_all_products_table()
    {
    	$this->db->where('service_charge_delete = 0 AND product_id > 0 AND drug_id = 0');
    	$query = $this->db->get('service_charge');

    	if($query->num_rows() > 0)
    	{
    		$updated_charge = 0;
    		foreach ($query->result() as $key => $value) {
    			# code...
    			$product_id = $value->product_id;
    			$service_charge_id = $value->service_charge_id;

    			$array_update['product_id'] = $product_id;
    			$this->db->where('service_charge_id',$service_charge_id);
    			$this->db->update('visit_charge',$array_update);

    			if($updated_charge != $service_charge_id)
    			{
    				$updated_charge = $service_charge_id;
	    			$charge_update['drug_id'] = $product_id;
	    			$this->db->where('product_id',$product_id);
	    			$this->db->update('service_charge',$charge_update);
    			}
    			// var_dump($query); die();

    		}
    	}
    }


		public function update_drugs_charges()
    {
    	$this->db->where('service_charge.service_charge_id = visit_charge.service_charge_id AND visit_charge.product_id > 0  AND visit_charge.buying_price IS NULL');
			// $this->db->limit(10000);
    	$query = $this->db->get('service_charge,visit_charge');

    	if($query->num_rows() > 0)
    	{
    		$updated_charge = 0;
    		foreach ($query->result() as $key => $value) {
    			# code...
    			$product_id = $value->product_id;
					$visit_charge_id = $value->visit_charge_id;
    			$service_charge_id = $value->service_charge_id;
					$vatable = $value->vatable;
					$service_charge_amount = $value->service_charge_amount;
					if($vatable > 0)
					{
						$service_charge_amount = ($service_charge_amount *100) /116;
					}

					$service_charge_amount = ($service_charge_amount*100/133);
					// var_dump($service_charge_amount);die();
    			$array_update['buying_price'] = $service_charge_amount;
    			$this->db->where('visit_charge_id',$visit_charge_id);
    			$this->db->update('visit_charge',$array_update);
    		}
    	}
    }






    // update the tables neeeded to migrate the data

    public function update_service_charge_table()
    {
        $this->db->where('service_id = 1 AND synced IS NULL AND parent_charge_id IS NULL');

        $query_services = $this->db->get('service_charge');

        if($query_services->num_rows() > 0)
        {
            foreach ($query_services->result() as $key => $value) {
                # code...
                
                $service_charge_id = $value->service_charge_id;

                for ($i=12; $i < 14 ; $i++) { 
                    # code...
                    $insert_array['service_id'] = $i;
                    $insert_array['visit_type_id'] = $value->visit_type_id;
                    $insert_array['service_charge_name'] = $value->service_charge_name;
                    $insert_array['service_charge_amount'] = $value->service_charge_amount;
                    $insert_array['service_charge_status'] = $value->service_charge_status;
                    $insert_array['lab_test_id'] = $value->lab_test_id;
                    $insert_array['drug_id'] = $value->drug_id;
                    $insert_array['service_charge_delete'] = $value->service_charge_delete;
                    $insert_array['vaccine_id'] = $value->vaccine_id;
                    $insert_array['modified_by'] = $value->modified_by;
                    $insert_array['created'] = $value->created;
                    $insert_array['created_by'] = $value->created_by;
                    $insert_array['product_id'] = $value->product_id;
                    $insert_array['patient_type'] = $value->patient_type;
                    $insert_array['vatable'] = $value->vatable;
                    $insert_array['parent_charge_id'] = $service_charge_id;
                    $insert_array['synced'] = 1;
                    // var_dump($insert_array);die();
                    $this->db->insert('service_charge',$insert_array);

                }

                // update the service charge table with the sync status
                $update_array['synced'] = 1;
                $this->db->where('service_charge_id',$service_charge_id);
                $this->db->update('service_charge',$update_array);

            }
        }
    }


    public function visit_department_reconcilliation()
    {


        $select_statement ="
                            SELECT 
                            *
                            from
                            (
                            select visit.visit_id,visit.visit_type,visit_department.department_id AS department, visit.department_id AS department_id from visit,visit_department WHERE visit.visit_id = visit_department.visit_id GROUP BY visit_department.visit_id) AS data WHERE (data.department = 3 OR data.department = 4) AND data.department_id IS NULL";


        $query_services = $this->db->query($select_statement);

        if($query_services->num_rows() > 0)
        {
            foreach ($query_services->result() as $key => $value) {
                # code...
                
                $visit_id = $value->visit_id;
                $department_id = $value->department;
                $visit_type = $value->visit_type;
                // get the 

                $this->db->where('service_charge.service_id = 1 AND service_charge.service_charge_id = visit_charge.service_charge_id AND visit_charge.visit_id = '.$visit_id);
                $query_charge = $this->db->get('service_charge,visit_charge');
                 
                if($query_charge->num_rows() > 0)
                {
                    foreach ($query_charge->result() as $key => $value_two) {
                        # code...

                        // get the now charge 
                        $visit_charge_id = $value_two->visit_charge_id;
                        $service_charge_id = $value_two->service_charge_id;
                        $service_id = $value_two->service_id;

                        $this->db->where('service.department_id = '.$department_id.' AND service.service_id = service_charge.service_id  AND service_charge.visit_type_id = '.$visit_type.' AND parent_charge_id ='.$service_charge_id);
                        // $this->db->limit(1);
                        $query_select = $this->db->get('service_charge,service');

                        $rows = $query_select->row();
                        // var_dump($rows);die();
                        $charge_id = $rows->service_charge_id;
                        

                        $update_array['service_charge_id'] = $charge_id;
                        $this->db->where('visit_charge_id',$visit_charge_id);
                        $this->db->update('visit_charge',$update_array);
                    }
                }
                // 

                // var_dump($visit_type);die();
                $update_array_change['department_id'] = $department_id;
                $this->db->where('visit_id',$visit_id);
                $this->db->update('visit',$update_array_change);

            }
        }

    }
}
?>