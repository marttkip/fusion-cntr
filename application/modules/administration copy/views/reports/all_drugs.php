<?php
$drug_result = '';
if($query->num_rows() > 0)
{
	$count = $page;
	$drug_result .='
			<table class="table table-hover table-bordered table-striped table-responsive col-md-12" id="customers">
				<thead>
					<tr>
						<th>#</th>
						<th>Type</th>
						<th>Patient Name</th>
						<th><a href="'.site_url().'medical-reports/drugs/visit.visit_date/'.$order_method.'">Visit Date</a></th>
						<th>Date Dispensed</th>
						<th>Drug</th>
						<th>B.Price P.U</th>
						<th>S.Price P.U</th>
						<th>Units</th>
						<th>Total Amount</th>
						<th>Profit</th>
						<th>Profit margin (%)</th>
					</tr>
				</thead>
				<tbody>
			';
	foreach($query->result() as $visit_drug_result)
	{
		$visit_id = $visit_drug_result->visit_id;
		$patient_id = $visit_drug_result->patient_id;
		$patient_surname = $visit_drug_result->patient_surname;
		$patient_other_names = $visit_drug_result->patient_other_names;
		$service_charge = $visit_drug_result->service_charge_name;
		$service_amount = $visit_drug_result->service_charge_amount;
		$buying_price = $visit_drug_result->buying_price;
		$visit_charge_amount = $visit_drug_result->visit_charge_amount;
		$date = $visit_drug_result->date;
		$visit_date = date('jS M Y',strtotime($visit_drug_result->visit_date));
		$qty_given = $visit_drug_result->visit_charge_units;
		$patient_type = $visit_drug_result->type;
		$count++;
		
		//branch Code
		if($patient_type == 0)
		{
			$branch_code = 'Outpatient';
		}
		else
		{
			$branch_code = 'Walkin';
		}

		$total_selling_price = $visit_charge_amount * $qty_given;
		$total_buying_price = $buying_price * $qty_given;
		$total_profit = $total_selling_price - $total_buying_price;
		$total_profit_margin = ($total_profit/$total_buying_price) * 100;

		$drug_result .='
					<tr>
						<td>'.$count.'</td>
						<td>'.$branch_code.'</td>
						<td>'.$patient_surname.' '.$patient_other_names.'</td>
						<td>'.$visit_date.'</td>
						<td>'.date('jS M Y',strtotime($date)).'</td>
						<td>'.$service_charge.'</td>
						<td>'.number_format($buying_price,2).'</td>
						<td>'.number_format($visit_charge_amount,2).'</td>
						<td>'.$qty_given.'</td>
						<td>'.number_format($qty_given*$visit_charge_amount,2).'</td>
						<td>'.number_format($total_profit,2).'</td>
						<td>'.$total_profit_margin.'</td>
					</tr>';
	}
	$drug_result.='
				</tbody>
			</table>';
}
else
{
	$drug_result.= 'No drugs have been dispensed';
}
//echo $this->load->view('administration/reports/graphs/drug_sales', '', TRUE);
?>
<div class="row">
        <section class="panel panel-featured panel-featured-info">
            <header class="panel-heading">
            	 <h2 class="panel-title"><?php echo $title;?></h2>
            </header>
            <!-- Widget content -->
            <div class="panel-body">
            	<?php echo $this->load->view("administration/reports/search/drugs", '', TRUE);?>
            </div>
        </section>
</div>
<div class="row">
        <section class="panel panel-featured panel-featured-info">
            <header class="panel-heading">
            	 <h2 class="panel-title">Sales Summaries</h2>
            </header>
            <!-- Widget content -->
            <div class="panel-body">
            	<?php echo $this->load->view("administration/reports/drug_sales_summaries", '', TRUE);?>
            </div>
        </section>
</div>
<div class="row">
        <section class="panel panel-featured panel-featured-info">
            <header class="panel-heading">
            	 <h2 class="panel-title"><?php echo $title;?></h2>
            </header>
            <!-- Widget content -->
            <div class="panel-body">
                <div class="row">
                	<div class="col-md-4 col-md-offset-8">
                    	<!-- <a class="btn btn-sm btn-warning" id="open_search" onclick="open_search_box()" pull-right><i class="fa fa-search"></i> Open Search</a>
                    	<a class="btn btn-sm btn-info" id="close_search" style="display:none;" onclick="close_search()"><i class="fa fa-search-minus"></i> Close Search</a> -->
                        <?php
                        $search = $this->session->userdata('all_drugs_search');
						if(!empty($search))
						{
						?>
                    	<a class="btn btn-sm btn-danger" href="<?php echo site_url().'administration/reports/clear_drugs_search';?>"><i class="fa fa-search"></i> Clear Search</a>
                        <?php }?>
                    	<a class="btn btn-sm btn-success" target="_blank" href="<?php echo site_url().'administration/reports/export_drugs';?>">Download</a>
                    </div>
                </div>
                <div class="row">
                	<div class="col-md-12">
                        <div id="search_section" style="display:none;">
        
                            
                        </div>
                    </div>
                </div>
                <div class="row">
                	<div class="col-md-12">
            			<?php echo $drug_result;?>
                    </div>
                </div>
            </div>
            <!--<a href="#" onClick ="$('#customers').tableExport({type:'excel',escape:'false'});">EXCEL DOWNLOADS</a>-->
            <div class="widget-foot">
                                
				<?php if(isset($links)){echo $links;}?>
            
                <div class="clearfix"></div> 
            
            </div>
        </section>
</div>

<script type="text/javascript">

	function open_search_box()
	{
		var myTarget2 = document.getElementById("search_section");
		var button = document.getElementById("open_search");
		var button2 = document.getElementById("close_search");

		myTarget2.style.display = '';
		button.style.display = 'none';
		button2.style.display = '';
	}
	
	function close_search()
	{
		var myTarget2 = document.getElementById("search_section");
		var button = document.getElementById("open_search");
		var button2 = document.getElementById("close_search");

		myTarget2.style.display = 'none';
		button.style.display = '';
		button2.style.display = 'none';
	}
</script>