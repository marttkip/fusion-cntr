<?php

$revenue_query = $this->reports_model->get_outpatient_revenue();

$patients_type_result = '';
$grand_outpatient = 0;
$grand_units =0;

$grand_units = 0;
$grand_cost_price = 0;
$grand_revenue = 0;
$grand_profit = 0;

// var_dump($revenue_query->num_rows());die();
if($revenue_query->num_rows() > 0)
{
	foreach ($revenue_query->result() as $key => $value) {
		# code...
		$patient_type = $value->patient_type;
		$total_units = $value->total_units;
		$total_revenue = $value->total_revenue;
		$total_cost_price = $value->total_cost_price;

		if($patient_type == 0)
		{
			$type = 'OUTPATIENT';
		}
		else if($patient_type == 1)
		{
			$type = 'WALKIN';
		}

		if($total_revenue > 0)
		{
			$total_revenue = $total_revenue;
		}
		else
		{
			$total_revenue = 0;
		}


		if($total_cost_price > 0)
		{
			$total_cost_price = $total_cost_price;
		}
		else
		{
			$total_cost_price = 0;
		}

		


		$total_profit = $total_revenue - $total_cost_price;


        $grand_units += $total_units;
        $grand_cost_price += $total_cost_price;
        $grand_revenue += $total_revenue;
        $grand_profit += $total_profit;


		$patients_type_result .= '<tr>
		                            <th>'.$type.'</th>
		                            <td>'.$total_units.'</td>
		                            <td>'.number_format($total_cost_price,2).'</td>
		                            <td>'.number_format($total_revenue,2).'</td>
		                            <td>'.number_format($total_profit,2).'</td>
		                        </tr>';
		if($patient_type == 0)
		{
			$grand_outpatient = $total_revenue;
		}
		
	}

    $patients_type_result .= '<tr>
                                    <th>TOTAL</th>
                                    <th>'.$grand_units.'</th>
                                    <th>'.number_format($grand_cost_price,2).'</th>
                                    <th>'.number_format($grand_revenue,2).'</th>
                                    <th>'.number_format($grand_profit,2).'</th>
                                </tr>';

}

// var_dump($grand_outpatient);die();
if($grand_outpatient > 0)
{
	$commision = 0.05 * $grand_outpatient;
}
else
{
	$commision = 0;
}

?>

<div class="row statistics">
    <div class="col-md-5 col-sm-12">
         <section class="panel panel-featured panel-featured-info">
            <header class="panel-heading">
                <h2 class="panel-title">Visits Breakdown</h2>
              </header>             
        
              <!-- Widget content -->
              <div class="panel-body">
                <!-- <h5>Visit Breakdown</h5> -->
                <table class="table table-striped table-hover table-condensed">
                    <thead>
                        <tr>
                            <th>Type</th>
                            <th>Drugs Sold</th>
                            <th>Total Cost Prices</th>
                            <th>Total Selling Prices</th>
                            <th>Profit</th>
                        </tr>
                    </thead>
                    <tbody>
                        
                       <?php echo $patients_type_result?>


                    </tbody>
                </table>
                <!-- Text -->
               <!--  <h5>Total Amount</h5>
                <h4><?php echo 200;?></h4> -->
                
                <div class="clearfix"></div>
            </div>
        </section>
    </div>
    <div class="col-md-7 col-sm-12">
         <section class="panel panel-featured panel-featured-info">
            <header class="panel-heading">
                <h2 class="panel-title">Transaction breakdown <?php echo $title;?></h2>
            </header>             
        
              <!-- Widget content -->
              <div class="panel-body">
                
                <div class="row">
                    <!-- End Transaction Breakdown -->
                    
                    <div class="col-md-6">
                        <h5>OUTPATIENT REVENUE</h5>
                        <p>(collection of all drug dispensed to outpatients)</p>
                        <h3>Ksh <?php echo number_format($grand_outpatient, 2);?></h3>
                       
                    </div>
                    <div class="col-md-6">
                        <h4>PERIOD COMMISION (5%)</h4> 
                        <p>(5% commision payable to the clinic on outpatient revenue)</p>
                        <h3>Ksh <?php echo number_format($commision, 2);?></h3>
                    </div>
                    
                </div>
            </div>
        </section>
    </div>
</div>