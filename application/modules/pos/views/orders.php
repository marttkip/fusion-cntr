<!-- search -->
<?php
$visit_types = $this->pos_model->get_visit_types();
?>
<?php // echo $this->load->view('search/patient_search', '', TRUE);?>
<!-- end search -->
 <section class="panel">
    <header class="panel-heading">
			<h2 class="panel-title"><i class="icon-reorder"></i><?php echo $title;?> for <?php echo date('jS M Y',strtotime(date('Y-m-d')));?></h2>

        		<button type="button" class="btn btn-sm btn-warning pull-right" data-toggle="modal" data-target="#edit_account" ><i class="fa fa-pencil"></i> NEW ORDER</button>
        		 <div class="modal fade" id="edit_account" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
					<div class="modal-dialog" role="document">
						<div class="modal-content ">
							<div class="modal-header">
								<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
								<h4 class="modal-title" id="myModalLabel" class="center-align">NEW ORDER</h4>
							</div>
							<div class="modal-body">
								<?php echo form_open("pos/create_new_customer", array("class" => "form-horizontal"));?>
								 <div class="row">
								 	<!-- personnel detail -->

								 	 <div class="form-group">
										<label class="col-lg-4 control-label">Staff Or Non Staff? </label>
	                                    <div class="col-lg-4">
	                                        <div class="radio">
	                                            <label>
	                                                <input id="optionsRadios2" type="radio" name="type_of_sale" value="0"  onclick="get_type_of_sale(0)">
	                                                Non - staff
	                                            </label>
	                                        </div>
	                                    </div>
	                                    
	                                    <div class="col-lg-4">
	                                        <div class="radio">
	                                            <label>
	                                                <input id="optionsRadios2" type="radio" name="type_of_sale" value="1" onclick="get_type_of_sale(1)">
	                                                Staff 
	                                            </label>
	                                        </div>
	                                    </div>
									</div>
                                <div class="row" id="staff_div" style="display: none;">
                                	<div class="col-lg-12 col-md-12 col-sm-12">
								 		<div class="form-group" >
											<label class="col-lg-4 control-label">Staff: </label>					
											<div class="col-lg-6">
												<select name="staff_id" id="staff_id" class="form-control">
													<option value="">----Select a Staff----</option>
													<?php
																			
														if($all_personnel->num_rows() > 0){

															foreach($all_personnel->result() as $row):
																$personnel_fname = $row->personnel_fname;
																$personnel_id = $row->personnel_id;
																$personnel_onames = $row->personnel_onames;

																if($personnel_id == set_value('personnel_id'))
																{
																	echo "<option value='".$personnel_id."' selected='selected'>".$personnel_fname." ".$personnel_onames."</option>";
																}
																
																else
																{
																	echo "<option value='".$personnel_id."'>".$personnel_fname." ".$personnel_onames."</option>";
																}
															endforeach;
														}
													?>
												</select>
											</div>
										</div>
									</div>
								</div>
								 	<!-- end of personnel detail -->

                                <div class="row" id="normal_div" style="display: none;">
                                	<div class="col-lg-12 col-md-12 col-sm-12">

                                		<div class="form-group">
											<label class="col-lg-4 control-label">Order Number: </label>
										  
											<div class="col-lg-6">
												<input type="text" class="form-control" name="customer_name" id="customer_name" placeholder="" autocomplete="off" readonly="readonly">
											</div>
										</div>
                                	</div>
                                </div>
                                <br>
                                <div class="row" id="date_div" style="display: none;">
	                                <div class="form-group">
	                                    <label class="col-lg-4 control-label">Date: </label>
	                                    
	                                    <div class="col-lg-6">
	                                        <div class="input-group">
	                                            <span class="input-group-addon">
	                                                <i class="fa fa-calendar"></i>
	                                            </span>
	                                            <input data-format="yyyy-MM-dd" type="text" data-plugin-datepicker class="form-control" name="sale_date" placeholder="Date" value="<?php echo date('Y-m-d');?>">
	                                        </div>
	                                    </div>
	                                </div>
	                            </div>
                                
                                <br>
								<div class="row" id="submit_button" style="display: none;">
									<div class="col-lg-12 col-md-12 col-sm-12">
										<div class="center-align">
											<button class="btn btn-info btn-sm" type="submit">CREATE ORDER</button>
										</div>
									</div>
								</div>
								<?php echo form_close();?>

							</div>
							<div class="modal-footer">
								<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
							</div>
						</div>
					</div>
				</div>

        </header>

        <!-- Widget content -->
          <div class="panel-body">
          <div class="padd">
          <?php
            	$error = $this->session->userdata('error_message');
				$success = $this->session->userdata('success_message');
				
				if(!empty($error))
				{
					echo '<div class="alert alert-danger">'.$error.'</div>';
					$this->session->unset_userdata('error_message');
				}
				
				if(!empty($success))
				{
					echo '<div class="alert alert-success">'.$success.'</div>';
					$this->session->unset_userdata('success_message');
				}
			?>
<?php
		$search = $this->session->userdata('visit_accounts_search');
		
		if(!empty($search))
		{
			echo '<a href="'.site_url().'accounts/close_queue_search/'.$type_links.'" class="btn btn-warning btn-sm">Close Search</a>';
		}
		
		if($module == 0)
		{
			$data['onSubmit'] = 'return confirm(\'Are you sure you want to end multiple visits?\');';
			$result = form_open('pos/bulk_close_visits',$data);
		}
		
		else
		{
			$result = '';
		}
		
		//if users exist display them
		if ($query->num_rows() > 0)
		{
			$count = $page;
			
			//accounts bulk close visit
			
			$result .= '
				<div class="center-align">
					<button type="submit" class="btn btn-sm btn-danger">End Visits</button>
				</div>
				<table class="table table-hover table-bordered ">
				  <thead>
					<tr>
					  <th>#</th>
					  <th></th>
					  <th>Customer Name</th>
					  <th>From Date</th>
					  <th>To Date</th>
					  <th>Invoice</th>
					  <th>Payments</th>
					  <th>Balance</th>';

					  if($type_links == 3){
						  $result .=  '<th colspan="2">Actions</th>';
					  }
					  else{
						  $result .= '<th colspan="5">Actions</th>';
					  }
			$result .= 	'</tr>
				  </thead>
				  <tbody>
			';
		
			
			$personnel_query = $this->personnel_model->retrieve_personnel();
			
			foreach ($query->result() as $row)
			{
				$visit_date = date('jS M Y',strtotime($row->visit_date));
				$visit_end_date = date('jS M Y',strtotime($row->visit_end_date));
				
				$visit_id = $row->visit_id;
				$customer_id = $row->customer_id;
				$customer_surname = $row->customer_surname;
				$customer_first_name = $row->customer_first_name;
				$customer_phone = $row->customer_phone;
				$customer_email = $row->customer_email;
				
			
				
				$checkbox_data = array(
								  'name'        => 'visit[]',
								  'id'          => 'checkbox'.$visit_id,
								  'class'          => 'css-checkbox lrg',
								  'value'       => $visit_id
								);
				
				//creators and editors
			
				
				$count++;
				
				if($module != 1)
				{
					$to_doctor = '<td><a href="'.site_url().'nurse/send_to_doctor/'.$visit_id.'" class="btn btn-sm btn-warning" onclick="return confirm(\'Send to doctor?\');">To Doctor</a></td>';
				}
				
				else
				{
					$to_doctor = '';
				}
				
				$payments_value = $this->booking_model->total_payments($visit_id);

				$invoice_total = $this->booking_model->total_invoice($visit_id);

				$balance = $this->booking_model->balance($payments_value,$invoice_total);
				
				$result .= 
					'
						<tr>
							<td>'.$count.'</td>';
				
				//bulk close visits
				if($module == 0)
				{
					$result .= 
					'
							<td>'.form_checkbox($checkbox_data).'<label for="checkbox'.$visit_id.'" name="checkbox79_lbl" class="css-label lrg klaus"></label>'.'</td>';
				}
				
				$result .= 
					'
							<td>'.$customer_surname.' '.$customer_first_name.'</td>
							<td>'.$visit_date.'</td>
							<td>'.$visit_end_date.'</td>
							<td>'.$invoice_total.'</td>
							<td>'.$payments_value.'</td>
							<td>'.$balance.'</td>
							<td><a href="'.site_url().'booking/print_invoice_new/'.$visit_id.'" target="_blank" class="btn btn-sm btn-success">Invoice </a></td>';
							if($type_links == 3){

							}else{
							$result .='
							<td><a href="'.site_url().'pos/billing/'.$visit_id.'" class="btn btn-sm btn-primary" >Billing</a></td>
							<td><a href="'.site_url().'pos/close_sale/'.$visit_id.'" class="btn btn-sm btn-danger" onclick="return confirm(\' Do you want to close sale ?\');">Close Sale</a></td>';
							}

						$result .='</tr> 
					';
			}
			
			$result .= 
				'
							  </tbody>
							</table>
				';
		
			if($module == 0)
			{
				$result .= '
				<br>
				<div class="center-align">
					<button type="submit" class="btn btn-sm btn-danger">End Visits</button>
				</div>
				'.form_close();
			}
		}
		
		else
		{
			$result .= "There are no orders";
		}
		
		echo $result;
?>
          </div>
          
          <div class="widget-foot">
                                
				<?php if(isset($links)){echo $links;}?>
            
                <div class="clearfix"></div> 
            
            </div>
        </div>
        <!-- Widget ends -->

</section>
<script type="text/javascript">
	$(document).ready(function(){

		var url = "<?php echo site_url();?>pos/get_next_customer_number";
	
      	$.get(url, function( data ) 
			{
				$( "#customer_name" ).val(data);
			});
      
  	});
	function get_type_of_sale(value)
	{
		// alert(value);
		if(value == '1')
		{
			$('#staff_div').css('display', 'block');
			$('#submit_button').css('display', 'block');
			$('#date_div').css('display', 'block');
			$('#normal_div').css('display', 'none');
		}
		else
		{
			$('#staff_div').css('display', 'none');
			$('#submit_button').css('display', 'block');
			$('#date_div').css('display', 'block');
			$('#normal_div').css('display', 'block');
		}
	}

</script>