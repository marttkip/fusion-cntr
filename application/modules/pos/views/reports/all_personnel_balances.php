<!-- search -->
<?php echo $this->load->view('search/all_staff_transactions', '', TRUE);?>
<!-- end search -->
<?php //echo $this->load->view('transaction_statistics', '', TRUE);?>
 
<div class="row">
    <div class="col-md-12">

        <section class="panel panel-featured panel-featured-info">
            <header class="panel-heading">
            	 <h2 class="panel-title"><?php echo $title;?></h2>
            </header>             

          <!-- Widget content -->
                <div class="panel-body">
          <h5 class="center-align"><?php echo $this->session->userdata('search_title');?></h5>
<?php
		$result = '';
		$search = $this->session->userdata('all_staff_sales_search');
		if(!empty($search))
		{
			echo '<a href="'.site_url().'pos/reports/close_staff_sales_search" class="btn btn-sm btn-warning">Close Search</a>';
		}
		
		//if users exist display them
		//if users exist display them
		if ($query->num_rows() > 0)
		{
			$count = $page;
			
			//accounts bulk close visit
			
			$result .= '
				
				<table class="table table-bordered ">
				  <thead>
					<tr>
					  <th>#</th>
					  <th>Customer Name</th>
					  <th>Customer Number</th>
					  <th>Invoice</th>
					  <th>Payments</th>
					  <th>Balance</th>';
			$result .= 	'</tr>
				  </thead>
				  <tbody>
			';
		
			
			$personnel_query = $this->personnel_model->retrieve_personnel();
			$total_payments =0;
			$total_invoice = 0;
			$total_balance = 0;
			foreach ($query->result() as $row)
			{
				
				$customer_id = $row->customer_id;
				$customer_surname = $row->customer_surname;
				$customer_first_name = $row->customer_first_name;
				$customer_phone = $row->customer_phone;
				$customer_email = $row->customer_email;
				$customer_number = $row->customer_number;
				$personnel_id = $row->personnel_id;

				$payments_value = $row->amount_paid;
				$invoice_total = $row->total_invoice;
				$balance = $row->balance;


				if($personnel_id > 0)
				{
					$checked = 'success';
				}
				else
				{
					$checked = 'default';
				}
				
			
			
				
				//creators and editors
			
				
				$count++;
				
				
				
				$total_invoice += $invoice_total;
				$total_payments += $payments_value;
				$total_balance += $balance;

				// $balance = $this->booking_model->balance($payments_value,$invoice_total);
				
				$result .= 
					'
						<tr>
							<td>'.$count.'</td>';
				
				//bulk close visits

				
			
				
				$result .= 
					'
							<td class="'.$checked.'">'.$customer_surname.' '.$customer_first_name.'</td>
							<td>'.$customer_number.'</td>
							<td>'.$invoice_total.'</td>
							<td>'.$payments_value.'</td>
							<td>'.$balance.'</td>
							';

						$result .='</tr> 
					';
			}
			$result .= 
						'
						<tr>
							<td colspan="3"><strong>Total</strong></td>
							<td><strong>'.number_format($total_invoice,2).'</strong></td>
							<td><strong>'.number_format($total_payments,2).'</strong></td>
							<td><strong>'.number_format($total_balance,2).'</strong></td>
						</tr> ';

			
			$result .= 
				'
							  </tbody>
							</table>
				';
		
			if($module == 0)
			{
				$result .= '
				
				'.form_close();
			}
		}
		
		else
		{
			$result .= "There are no orders";
		}
		
		echo $result;
?>
          </div>
          
          <div class="widget-foot">
                                
				<?php if(isset($links)){echo $links;}?>
            
                <div class="clearfix"></div> 
            
            </div>
        
		</section>
    </div>
  </div>