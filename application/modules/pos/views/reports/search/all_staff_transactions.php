        <section class="panel panel-featured panel-featured-info">
            <header class="panel-heading">
            	<h2 class="panel-title pull-right">Sales Search:</h2>
            	<h2 class="panel-title">Search</h2>
            </header>             

          <!-- Widget content -->
                <div class="panel-body">
			<?php
            echo form_open("pos/reports/search_all_staff_transactions", array("class" => "form-horizontal"));
            ?>
            <div class="row">
                <div class="col-md-6">
                    <div class="form-group" >
						<label class="col-lg-4 control-label">Staff: </label>					
						<div class="col-lg-8">
							<select name="staff_id" id="staff_id" class="form-control">
								<option value="">----Select a Staff----</option>
								<?php
														
									if($all_personnel->num_rows() > 0){

										foreach($all_personnel->result() as $row):
											$personnel_fname = $row->personnel_fname;
											$personnel_id = $row->personnel_id;
											$personnel_onames = $row->personnel_onames;

											if($personnel_id == set_value('personnel_id'))
											{
												echo "<option value='".$personnel_id."' selected='selected'>".$personnel_fname." ".$personnel_onames."</option>";
											}
											
											else
											{
												echo "<option value='".$personnel_id."'>".$personnel_fname." ".$personnel_onames."</option>";
											}
										endforeach;
									}
								?>
							</select>
						</div>
					</div>
                </div>
                
                <div class="col-md-4">
                    
                   
                     <div class="form-group">
                        <div class="col-lg-8 col-lg-offset-4">
                        	<div class="center-align">
                           		<button type="submit" class="btn btn-info">Search</button>
            				</div>
                        </div>
                    </div>
                    
                    
                </div>
                
                <!-- <div class="col-md-4"> -->
                    
                    <!--<div class="form-group">
                        <label class="col-lg-4 control-label">Patient number: </label>
                        
                        <div class="col-lg-8">
                            <input type="text" class="form-control" name="patient_number" placeholder="Patient number">
                        </div>
                    </div>-->
                   <!--  <div class="form-group">
                        <label class="col-lg-4 control-label"> Date To: </label>
                        
                        <div class="col-lg-8">
                        	<div class="input-group">
                                <span class="input-group-addon">
                                    <i class="fa fa-calendar"></i>
                                </span>
                                <input data-format="yyyy-MM-dd" type="text" data-plugin-datepicker class="form-control" name="date_to" placeholder=" Date To" autocomplete="off">
                            </div>
                        </div>
                    </div> -->
                    
                    
                    
                   
                <!-- </div> -->
            </div>
            
            
            <?php
            echo form_close();
            ?>
          </div>
		</section>