<?php

$item_invoiced_rs = $this->booking_model->get_patient_visit_charge_items($visit_id);
$credit_note_amount = $this->booking_model->get_sum_credit_notes($visit_id);
$debit_note_amount = $this->booking_model->get_sum_debit_notes($visit_id);
$payments_rs = $this->booking_model->payments($visit_id);

?>
<?php


$consumble_where = 'service_charge_delete = 0 AND service.service_id = service_charge.service_id AND service_charge.visit_type_id = visit_type.visit_type_id AND service.service_name = "Cafeteria" AND service_charge.visit_type_id = (SELECT visit_type_id FROM visit_type WHERE visit_type_name = "POS") ';
$account_order = 'service_charge.service_charge_name';
$account_table = 'service_charge,service,visit_type';
$join = FALSE;

$account_query = $this->booking_model->get_items_list($account_table, $consumble_where, $account_order,$join);
$rs8 = $account_query->result();
$procedure = '';
foreach ($rs8 as $account_rs) :


$service_charge_id = $account_rs->service_charge_id;
$service_charge_name = $account_rs->service_charge_name;

$service_charge_amount = $account_rs->service_charge_amount;

$procedure .="<option value='".$service_charge_id."'> ".$service_charge_name."  - Kes. ".$service_charge_amount."</option>";

endforeach;


// get the stores assigned 


$store_where = 'personnel_store.store_id = store.store_id AND personnel_store.personnel_id =  '.$this->session->userdata('personnel_id');
$store_order = 'store.store_name';
$store_table = 'personnel_store,store';

$store_query = $this->booking_model->get_items_list($store_table, $store_where, $store_order);
$rs8 = $store_query->result();
$stores = '';
foreach ($rs8 as $store_rs) :
$store_id = $store_rs->store_id;
$store_name = $store_rs->store_name;
$stores .="<option value='".$store_id."'> ".$store_name."</option>";

endforeach;



$total = 0;
?>
 <section class="panel">
	<header class="panel-heading">
		<div class="well well-sm info">
			<h5 style="margin:0;">
				 <div class="row">
                                <div class="col-md-6">
                                    <div class="row">
                                        <div class="col-md-4">
                                            <strong>Name:</strong>
                                        </div>
                                        <div class="col-md-8">
                                            <?php echo $customer_surname.' '.$customer_first_name;?>
                                        </div>
                                    </div>
                                </div>
                            </div>
			</h5>
		</div>
	</header>
	
	<!-- Widget content -->
	
	<div class="panel-body">
		
		 
		

		<div class="row">
            <div class="col-md-12">
            <?php
                $error = $this->session->userdata('error_message');
                $success = $this->session->userdata('success_message');
                
                if(!empty($error))
                {
                  echo '<div class="alert alert-danger">'.$error.'</div>';
                  $this->session->unset_userdata('error_message');
                }
                
                if(!empty($success))
                {
                  echo '<div class="alert alert-success">'.$success.'</div>';
                  $this->session->unset_userdata('success_message');
                }
              
             ?>
             </div>
          <?php
           if($close_page != 1)
           {
           		?>
           		<div class="col-md-12 center-align">
             	
	               <a href="<?php echo site_url().'pos/close_sale/'.$visit_id;?>" class="btn btn-danger" onclick="return confirm('Do you want to close sale?');"><i class="fa fa-times"></i> Close Sale</a>

	               <a href="<?php echo site_url().'point-of-sale/pos';?>" class="btn btn-warning"><i class="fa fa-arrow-left"></i> Back to sales</a>
	                
	             </div>
           		<?php
           }
           else
           {
           	?>
           		<div class="col-md-12 center-align">
             	

	               <a href="<?php echo site_url().'point-of-sale/pos-report';?>" class="btn btn-warning"><i class="fa fa-arrow-left"></i> Back to reports</a>
	                
	             </div>
           		<?php
           }
           ?>
             
             
          </div>
       
		<div class="row">
			<div class="col-md-12">
				<div class="col-md-6">
					
					<div class="row">
						<div class="col-md-12">
							<section class="panel panel-featured panel-featured-info">
								<header class="panel-heading">
									
									<h2 class="panel-title">Invoices Charges</h2>
								</header>
								<div class="panel-body">
                                	<div class="row">
                                    	<div class="col-md-12">
                                    		<button type="button" class="btn btn-sm btn-warning" data-toggle="modal" data-target="#edit_account"><i class="fa fa-pencil"></i> Add Billing</button>
                                        	<a href="<?php echo site_url();?>booking/print_invoice_new/<?php echo $visit_id;?>" target="_blank" class="btn btn-sm btn-primary pull-right" style="margin-bottom:10px;" >Print Invoice</a>

													<div class="modal fade" id="edit_account" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
														<div class="modal-dialog modal-xl" role="document">
															<div class="modal-content">
																<div class="modal-header">
																	<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
																	<h4 class="modal-title" id="myModalLabel">Billing Sheet</h4>
																</div>
																<div class="modal-body">
																	 <div class="row">
								                                    		<div class="col-lg-8 col-md-8 col-sm-8">
															                  <div class="form-group">
															                    <select id='procedure_id' name='procedure_id' class='form-control custom-select '>
															                    <!-- <select class="form-control custom-select " id='procedure_id' name='procedure_id'> -->
															                      <option value=''>None - Please Select</option>
															                      <?php echo $procedure;?>
															                    </select>
															                  </div>
															                
															                </div>
															                
															                <div class="col-lg-4 col-md-4 col-sm-4">
															                  <div class="form-group">
															                      <button type='submit' class="btn btn-sm btn-success"  onclick="parse_procedures(<?php echo $visit_id;?>,1);"> Add Bill</button>
															                  </div>
															                </div>
								                                    </div>
								                                    <div class="row">
								                                    	           
															           <!-- visit Procedures from java script -->
															            <div id="procedures"></div>
															            <!-- end of visit procedures -->
								                                    </div>

																</div>
																<div class="modal-footer">
																	<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
																</div>
															</div>
														</div>
													</div>
                                        </div>
                                    </div>
                                     <div class="row">
                                     	<div id="customer_bill"></div>
										
									</div>
								</div>
							</section>
						</div>
					</div>
					<!-- END OF THIRD ROW -->
				</div>
				<!-- END OF THE SPAN 7 -->
				  
				<!-- START OF THE SPAN 5 -->
				<div class="col-md-6">
					<div class="row">
						<div class="col-md-12">
							<section class="panel panel-featured panel-featured-info">
								<header class="panel-heading">
									<h2 class="panel-title">Receipts</h2>
								</header>
								
								<div class="panel-body">
                                	<div class="row">
                                    	<div class="col-md-12">
                                    		<button type="button" class="btn btn-sm btn-warning" data-toggle="modal" data-target="#receipting"><i class="fa fa-pencil"></i> Add Payment</button>
                                        	<a href="<?php echo site_url();?>booking/print_receipt_new/<?php echo $visit_id;?>" target="_blank" class="btn btn-sm btn-primary pull-right" style="margin-bottom:10px;" >Print all Receipts</a>
                                        </div>
                                    </div>

                                    <div class="modal fade" id="receipting" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
										<div class="modal-dialog" role="document">
											<div class="modal-content">
												<div class="modal-header">
													<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
													<h4 class="modal-title" id="myModalLabel">Billing Sheet</h4>
												</div>
												<div class="modal-body">
													
													
									<?php echo form_open("booking/make_payments/".$visit_id.'/'.$close_page, array("class" => "form-horizontal"));?>
                                    
										<div class="form-group">
											<div class="col-lg-4">
                                            	<div class="radio">
                                                    <label>
                                                        <input id="optionsRadios2" type="radio" name="type_payment" value="1" checked="checked" onclick="getservices(1)"> 
                                                        Normal
                                                    </label>
                                                </div>
											</div>
											<div class="col-lg-4">
                                            	<div class="radio">
                                                    <label>
                                                        <input id="optionsRadios2" type="radio" name="type_payment" value="2" onclick="getservices(2)"> 
                                                        Debit Note
                                                    </label>
                                                </div>
											</div>
											<div class="col-lg-4">
                                            	<div class="radio">
                                                    <label>
                                                        <input id="optionsRadios2" type="radio" name="type_payment" value="3" onclick="getservices(3)"> 
                                                        Credit Note
                                                    </label>
                                                </div>
											</div>
										</div>
                                        
										<div id="service_div2" class="form-group">
											<label class="col-lg-4 control-label">Service: </label>
										  
											<div class="col-lg-8">
                                            	<select name="service_id" class="form-control">
                                                	<option value="">All services</option>
                                            	<?php
												if(count($item_invoiced_rs) > 0)
												{
													$s=0;
													foreach ($item_invoiced_rs as $key_items):
														$s++;
														$service_id = $key_items->service_id;
														$service_name = $key_items->service_name;
														?>
                                                        <option value="<?php echo $service_id;?>"><?php echo $service_name;?></option>
														<?php
													endforeach;
												}
													
												//display DN & CN services
												if(count($payments_rs) > 0)
												{
													foreach ($payments_rs as $key_items):
														$payment_type = $key_items->payment_type;
														
														if(($payment_type == 2) || ($payment_type == 3))
														{
															$payment_service_id = $key_items->payment_service_id;
															
															if($payment_service_id > 0)
															{
																$service_associate = $this->booking_model->get_service_detail($payment_service_id);
																?>
																<option value="<?php echo $payment_service_id;?>"><?php echo $service_associate;?></option>
																<?php
															}
														}
														
													endforeach;
												}
												?>
                                                </select>
											</div>
										</div>
                                        
                                    	<div id="service_div" class="form-group" style="display:none;">
                                            <label class="col-lg-4 control-label"> Services: </label>
                                            
                                            <div class="col-lg-8">
                                                <select class="form-control" name="payment_service_id" >
                                                	<option value="">--Select a service--</option>
													<?php
                                                    $service_rs = $this->booking_model->get_all_service();
                                                    $service_num_rows = count($service_rs);
                                                    if($service_num_rows > 0)
                                                    {
														foreach($service_rs as $service_res)
														{
															$service_id = $service_res->service_id;
															$service_name = $service_res->service_name;
															
															echo '<option value="'.$service_id.'">'.$service_name.'</option>';
														}
                                                    }
                                                    ?>
                                                </select>
                                            </div>
                                        </div>

										<div class="form-group">
											<label class="col-lg-4 control-label">Amount: </label>
										  
											<div class="col-lg-8">
												<input type="text" class="form-control" name="amount_paid" placeholder="" autocomplete="off">
											</div>
										</div>
										
										<div class="form-group" id="payment_method">
											<label class="col-lg-4 control-label">Payment Method: </label>
											  
											<div class="col-lg-8">
												<select class="form-control" name="payment_method" onchange="check_payment_type(this.value)">
                                                	<?php
													  $method_rs = $this->booking_model->get_payment_methods();
													  $num_rows = count($method_rs);
													 if($num_rows > 0)
													  {
														
														foreach($method_rs as $res)
														{
														  $payment_method_id = $res->payment_method_id;
														  $payment_method = $res->payment_method;
														  
															echo '<option value="'.$payment_method_id.'">'.$payment_method.'</option>';
														  
														}
													  }
												  ?>
												</select>
											  </div>
										</div>
										<div id="mpesa_div" class="form-group" style="display:none;" >
											<label class="col-lg-4 control-label"> Mpesa TX Code: </label>

											<div class="col-lg-8">
												<input type="text" class="form-control" name="mpesa_code" placeholder="">
											</div>
										</div>
									  
										<div id="insuarance_div" class="form-group" style="display:none;" >
											<label class="col-lg-4 control-label"> Insurance Number: </label>
											<div class="col-lg-8">
												<input type="text" class="form-control" name="insuarance_number" placeholder="">
											</div>
										</div>
									  
										<div id="cheque_div" class="form-group" style="display:none;" >
											<label class="col-lg-4 control-label"> Cheque Number: </label>
										  
											<div class="col-lg-8">
												<input type="text" class="form-control" name="cheque_number" placeholder="">
											</div>
										</div>
									  
										<div id="username_div" class="form-group" style="display:none;" >
											<label class="col-lg-4 control-label"> Username: </label>
										  
											<div class="col-lg-8">
												<input type="text" class="form-control" name="username" placeholder="">
											</div>
										</div>
									  
										<div id="password_div" class="form-group" style="display:none;" >
											<label class="col-lg-4 control-label"> Password: </label>
										  
											<div class="col-lg-8">
												<input type="password" class="form-control" name="password" placeholder="">
											</div>
										</div>
										
										<div class="center-align">
											<button class="btn btn-info btn-sm" type="submit">Add Payment Information</button>
										</div>
										<?php echo form_close();?>
												</div>
												<div class="modal-footer">
													<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
												</div>
											</div>
										</div>
									</div>
									<table class="table table-hover table-bordered col-md-12">
										<thead>
											<tr>
												<th>#</th>
												<th>Time</th>
												<th>Method</th>
												<th>Amount</th>
												<th colspan="2"></th>
											</tr>
										</thead>
										<tbody>
											<?php
											$total_payments = 0;
											$total_amount = ($total + $debit_note_amount) - $credit_note_amount;
											
											if(count($payments_rs) > 0)
											{
												$x=0;

												foreach ($payments_rs as $key_items):
													$x++;
													$payment_method = $key_items->payment_method;

													$time = $key_items->time;
													$payment_type = $key_items->payment_type;
													$payment_id = $key_items->payment_id;
													$payment_status = $key_items->payment_status;
													$payment_service_id = $key_items->payment_service_id;
													$service_name = '';
													
													if($payment_type == 1 && $payment_status == 1)
													{
														$amount_paid = $key_items->amount_paid;
														$amount_paidd = number_format($amount_paid,2);
														
														if(count($item_invoiced_rs) > 0)
														{
															foreach ($item_invoiced_rs as $key_items):
															
																$service_id = $key_items->service_id;
																
																if($service_id == $payment_service_id)
																{
																	$service_name = $key_items->service_name;
																	break;
																}
															endforeach;
														}
													
														//display DN & CN services
														if((count($payments_rs) > 0) && ($service_name == ''))
														{
															foreach ($payments_rs as $key_items):
																$payment_type = $key_items->payment_type;
																
																if(($payment_type == 2) || ($payment_type == 3))
																{
																	$payment_service_id2 = $key_items->payment_service_id;
																	
																	if($payment_service_id2 == $payment_service_id)
																	{
																		$service_name = $this->booking_model->get_service_detail($payment_service_id);
																		break;
																	}
																}
																
															endforeach;
														}
														?>
														<tr>
															<td><?php echo $x;?></td>
															<td><?php echo $time;?></td>
															<td><?php echo $payment_method;?></td>
															<td><?php echo $amount_paidd;?></td>
															<td><a href="<?php echo site_url().'booking/print_single_receipt/'.$payment_id;?>" class="btn btn-small btn-warning" target="_blank"><i class="fa fa-print"></i></a></td>
															<td>
                                                            	<button type="button" class="btn btn-small btn-default" data-toggle="modal" data-target="#refund_payment<?php echo $payment_id;?>"><i class="fa fa-times"></i></button>
																<!-- Modal -->
																<div class="modal fade" id="refund_payment<?php echo $payment_id;?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
																    <div class="modal-dialog" role="document">
																        <div class="modal-content">
																            <div class="modal-header">
																            	<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
																            	<h4 class="modal-title" id="myModalLabel">Cancel payment</h4>
																            </div>
																            <div class="modal-body">
																            	<?php echo form_open("booking/cancel_payment/".$payment_id.'/'.$visit_id, array("class" => "form-horizontal"));?>
																                <div class="form-group">
																                    <label class="col-md-4 control-label">Action: </label>
																                    
																                    <div class="col-md-8">
																                        <select class="form-control" name="cancel_action_id">
																                        	<option value="">-- Select action --</option>
																                            <?php
																                                if($cancel_actions->num_rows() > 0)
																                                {
																                                    foreach($cancel_actions->result() as $res)
																                                    {
																                                        $cancel_action_id = $res->cancel_action_id;
																                                        $cancel_action_name = $res->cancel_action_name;
																                                        
																                                        echo '<option value="'.$cancel_action_id.'">'.$cancel_action_name.'</option>';
																                                    }
																                                }
																                            ?>
																                        </select>
																                    </div>
																                </div>
																                
																                <div class="form-group">
																                    <label class="col-md-4 control-label">Description: </label>
																                    
																                    <div class="col-md-8">
																                        <textarea class="form-control" name="cancel_description"></textarea>
																                    </div>
																                </div>
																                
																                <div class="row">
																                	<div class="col-md-8 col-md-offset-4">
																                    	<div class="center-align">
																                        	<button type="submit" class="btn btn-primary">Save action</button>
																                        </div>
																                    </div>
																                </div>
																                <?php echo form_close();?>
																            </div>
																            <div class="modal-footer">
																                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
																            </div>
																        </div>
																    </div>
																</div>
                                                            </td>
														</tr>
														<?php
														$total_payments =  $total_payments + $amount_paid;
													}
												endforeach;

												?>
												<tr>
													<td colspan="4"><strong>Total : </strong></td>
													<td><strong> <?php echo number_format($total_payments,2);?></strong></td>
												</tr>
												<?php
											}
											
											else
											{
												?>
												<tr>
													<td colspan="4"> No payments made yet</td>
												</tr>
												<?php
											}
											?>
										</tbody>
									</table>
								</div>
							</section>
						</div>
					</div>
					
			</div>
			</div>
		</div>
		 <div class="row">
			<div class="col-md-12">
				<div class="col-md-4">
				</div>
				<div class="col-md-6">
					<div class="row">
						<div class="col-md-12">
							<section class="panel panel-featured panel-featured-info">
								
								<div class="panel-body">
									<div class="row">
										<div class="col-md-12">
											<div class="form-group">
												<?php
												if($customer_type == 1)
												{
													?>
													<div class="col-lg-4">
		                                            	<div class="radio">
		                                                    <label>
		                                                        <input id="optionsRadios2" type="radio" name="type_payment" value="1"  onclick="getcustomertype(1)"> 
		                                                        POS
		                                                    </label>
		                                                </div>
													</div>
													<div class="col-lg-4">
		                                            	<div class="radio">
		                                                    <label>
		                                                        <input id="optionsRadios2" type="radio" name="type_payment" value="2" checked="checked" onclick="getcustomertype(2)"> 
		                                                        Delivery
		                                                    </label>
		                                                </div>
													</div>
													<?php

												}
												else
												{
													?>
													<div class="col-lg-4">
		                                            	<div class="radio">
		                                                    <label>
		                                                        <input id="optionsRadios2" type="radio" name="type_payment" value="1" checked="checked" onclick="getcustomertype(1)"> 
		                                                        POS
		                                                    </label>
		                                                </div>
													</div>
													<div class="col-lg-4">
		                                            	<div class="radio">
		                                                    <label>
		                                                        <input id="optionsRadios2" type="radio" name="type_payment" value="2"  onclick="getcustomertype(2)"> 
		                                                        Delivery
		                                                    </label>
		                                                </div>
													</div>
													<?php
												}
												?>
												
											</div>
										</div>
									</div>
								</div>
							</section>
						</div>
					</div>

				</div>
			</div>
		</div>
		<?php echo form_open("pos/delivery_details/".$customer_id."/".$visit_id, array("class" => "form-horizontal"));?>
                                    
		<div class="row" id="delivery" style="display: none;">
			<section class="panel panel-featured panel-featured-info">
				<div class="panel-body">
		        	<div class="col-lg-12 col-md-12 col-sm-12">

		        		<div class="col-md-6">
		            		<div class="form-group">
								<label class="col-lg-4 control-label">Customer: </label>
							  
								<div class="col-lg-8">
									<input type="text" class="form-control" name="customer_name" placeholder="" value="<?php echo $customer_name?>" autocomplete="off">
								</div>
							</div>
							
							<div class="form-group">
								<label class="col-lg-4 control-label">Phone Number: </label>
							  
								<div class="col-lg-8">
									<input type="text" class="form-control" name="phone_number" placeholder="" value="<?php echo $customer_phone?>" autocomplete="off">
								</div>
							</div>
						</div>
						<div class="col-md-6">
							<div class="form-group">
								<label class="col-lg-4 control-label">Physcal Location: </label>
							  
								<div class="col-lg-8">
									<textarea class="form-control" cols="4" name="location"><?php echo $location;?></textarea>
									
								</div>
							</div>
							<div class="form-group">
								<label class="col-lg-4 control-label">Transported By: </label>
							  
								<div class="col-lg-8">
									<textarea class="form-control" cols="4" name="transported_by"><?php echo $transported_by;?></textarea>
								</div>
							</div>
						</div>
		        	</div>
		        	<div class="row">
		        		<div class="form-group">
						  	<div class="center-align">
								<button class="btn btn-info btn-sm" type="submit">Add Delivery Details</button>
							</div>
						</div>
		        	</div>
		        </div>
		     </section>
        </div>
        <?php echo form_close();?>
</section>
  <!-- END OF ROW -->
<script type="text/javascript">

	$(function() {
	    $("#procedure_id").customselect();
	    $("#store_id").customselect();

	});
	$(document).ready(function(){
	  	display_procedure(<?php echo $visit_id;?>);
	  	display_invoices(<?php echo $visit_id;?>);
		$(function() {
			$("#procedure_id").customselect();
			$("#store_id").customselect();
		});

		var customer_type = <?php echo $customer_type;?>;
		if(customer_type == 1)
		{
			$('#delivery').css('display', 'block');

			
		}
		else
		{
			$('#delivery').css('display', 'none');
		}

	});

	function parse_procedures(visit_id,suck)
    {

      var procedure_id = document.getElementById("procedure_id").value;
      var store_id = 1;//document.getElementById("store_id").value;

       procedures(procedure_id, visit_id, suck,store_id);
      
    }

	function procedures(id, v_id, suck,store_id){
       
        var XMLHttpRequestObject = false;
            
        if (window.XMLHttpRequest) {
        
            XMLHttpRequestObject = new XMLHttpRequest();
        } 
            
        else if (window.ActiveXObject) {
            XMLHttpRequestObject = new ActiveXObject("Microsoft.XMLHTTP");
        }
        
        var url = "<?php echo site_url();?>booking/procedure/"+id+"/"+v_id+"/"+suck+"/"+store_id;
    
         if(XMLHttpRequestObject) {
                    
            XMLHttpRequestObject.open("GET", url);
                    
            XMLHttpRequestObject.onreadystatechange = function(){
                
                if (XMLHttpRequestObject.readyState == 4 && XMLHttpRequestObject.status == 200) {

                    document.getElementById("procedures").innerHTML=XMLHttpRequestObject.responseText;
                    display_invoices(v_id);
                }
            }
                    
            XMLHttpRequestObject.send(null);
        }

    }
    function display_procedure(visit_id){

	    var XMLHttpRequestObject = false;
	        
	    if (window.XMLHttpRequest) {
	    
	        XMLHttpRequestObject = new XMLHttpRequest();
	    } 
	        
	    else if (window.ActiveXObject) {
	        XMLHttpRequestObject = new ActiveXObject("Microsoft.XMLHTTP");
	    }
	    
	    var config_url = document.getElementById("config_url").value;
	    var url = config_url+"booking/view_procedure/"+visit_id;
	    if(XMLHttpRequestObject) {
	                
	        XMLHttpRequestObject.open("GET", url);
	                
	        XMLHttpRequestObject.onreadystatechange = function(){
	            
	            if (XMLHttpRequestObject.readyState == 4 && XMLHttpRequestObject.status == 200) {

	                document.getElementById("procedures").innerHTML=XMLHttpRequestObject.responseText;
	                 display_invoices(v_id);
	            }
	        }
	                
	        XMLHttpRequestObject.send(null);
	    }
	}





	 function display_invoices(visit_id){

	    var XMLHttpRequestObject = false;
	        
	    if (window.XMLHttpRequest) {
	    
	        XMLHttpRequestObject = new XMLHttpRequest();
	    } 
	        
	    else if (window.ActiveXObject) {
	        XMLHttpRequestObject = new ActiveXObject("Microsoft.XMLHTTP");
	    }
	    
	    var config_url = document.getElementById("config_url").value;
	    var url = config_url+"booking/dislay_invoices/"+visit_id;
	    // alert(url);
	    if(XMLHttpRequestObject) {
	                
	        XMLHttpRequestObject.open("GET", url);
	                
	        XMLHttpRequestObject.onreadystatechange = function(){
	            
	            if (XMLHttpRequestObject.readyState == 4 && XMLHttpRequestObject.status == 200) {

	                document.getElementById("customer_bill").innerHTML=XMLHttpRequestObject.responseText;
	            }
	        }
	                
	        XMLHttpRequestObject.send(null);
	    }
	}

	//Calculate procedure total


	 function calculatetotal(amount, id, procedure_id, v_id){
       
	    var units = document.getElementById('units'+id).value;  
	   
	    grand_total(id, units, amount, v_id);
	  }
	  function grand_total(procedure_id, units, amount, v_id){
	    var XMLHttpRequestObject = false;
	        
	    if (window.XMLHttpRequest) {
	    
	        XMLHttpRequestObject = new XMLHttpRequest();
	    } 
	        
	    else if (window.ActiveXObject) {
	        XMLHttpRequestObject = new ActiveXObject("Microsoft.XMLHTTP");
	    }
	     var config_url = document.getElementById("config_url").value;
	    var url = config_url+"booking/procedure_total/"+procedure_id+"/"+units+"/"+amount;
	
	    if(XMLHttpRequestObject) {
	                
	        XMLHttpRequestObject.open("GET", url);
	                
	        XMLHttpRequestObject.onreadystatechange = function(){
	            
	            if (XMLHttpRequestObject.readyState == 4 && XMLHttpRequestObject.status == 200) 
				{
	    			display_procedure(v_id);
	    			display_invoices(v_id);
	            }
	        }
	                
	        XMLHttpRequestObject.send(null);
	    }
	  }

	    function delete_procedure(id, visit_id){
		    var XMLHttpRequestObject = false;
		        
		    if (window.XMLHttpRequest) {
		    
		        XMLHttpRequestObject = new XMLHttpRequest();
		    } 
		        
		    else if (window.ActiveXObject) {
		        XMLHttpRequestObject = new ActiveXObject("Microsoft.XMLHTTP");
		    }
		     var config_url = document.getElementById("config_url").value;
		    var url = config_url+"booking/delete_procedure/"+id;
		    
		    if(XMLHttpRequestObject) {
		                
		        XMLHttpRequestObject.open("GET", url);
		                
		        XMLHttpRequestObject.onreadystatechange = function(){
		            
		            if (XMLHttpRequestObject.readyState == 4 && XMLHttpRequestObject.status == 200) {

		                display_procedure(visit_id);
	    				display_invoices(visit_id);
		            }
		        }
		                
		        XMLHttpRequestObject.send(null);
		    }
		}
	
  function getservices(id){

        var myTarget1 = document.getElementById("service_div");
        var myTarget2 = document.getElementById("username_div");
        var myTarget3 = document.getElementById("password_div");
        var myTarget4 = document.getElementById("service_div2");
        var myTarget5 = document.getElementById("payment_method");
		
        if(id == 1)
        {
          myTarget1.style.display = 'none';
          myTarget2.style.display = 'none';
          myTarget3.style.display = 'none';
          myTarget4.style.display = 'block';
          myTarget5.style.display = 'block';
        }
        else
        {
          myTarget1.style.display = 'block';
          myTarget2.style.display = 'block';
          myTarget3.style.display = 'block';
          myTarget4.style.display = 'none';
          myTarget5.style.display = 'none';
        }
        
  }
  function check_payment_type(payment_type_id){
   
    var myTarget1 = document.getElementById("cheque_div");

    var myTarget2 = document.getElementById("mpesa_div");

    var myTarget3 = document.getElementById("insuarance_div");

    if(payment_type_id == 1)
    {
      // this is a check
     
      myTarget1.style.display = 'block';
      myTarget2.style.display = 'none';
      myTarget3.style.display = 'none';
    }
    else if(payment_type_id == 2)
    {
      myTarget1.style.display = 'none';
      myTarget2.style.display = 'none';
      myTarget3.style.display = 'none';
    }
    else if(payment_type_id == 3)
    {
      myTarget1.style.display = 'none';
      myTarget2.style.display = 'none';
      myTarget3.style.display = 'block';
    }
    else if(payment_type_id == 4)
    {
      myTarget1.style.display = 'none';
      myTarget2.style.display = 'none';
      myTarget3.style.display = 'none';
    }
    else if(payment_type_id == 5)
    {
      myTarget1.style.display = 'none';
      myTarget2.style.display = 'block';
      myTarget3.style.display = 'none';
    }
    else
    {
      myTarget2.style.display = 'none';
      myTarget3.style.display = 'block';  
    }

  }
 



	function getcustomertype(visit_type_id)
	{
		if(visit_type_id == 1)
		{
			$('#delivery').css('display', 'none');

			
		}
		else if(visit_type_id == 2)
		{
			$('#delivery').css('display', 'block');
		}
		else
		{
			$('#delivery').css('display', 'none');
		}
	}
</script>