<?php
class Reports_model extends CI_Model 
{


	public function get_all_personnel_orders($table, $where, $per_page, $page, $order = 'customer_first_name', $order_method = 'ASC')
	{
		//retrieve all users
		$this->db->from($table);
		$this->db->select('*');
		$this->db->where($where);
		$this->db->order_by($order, $order_method);
		$query = $this->db->get('', $per_page, $page);
		
		return $query;
	}
	public function get_all_ongoing_visits($table, $where, $per_page, $page, $order = NULL)
	{
		//retrieve all users
		$this->db->from($table);
		$this->db->select('visit_pos.*, customer.*');
		$this->db->where($where);
		$this->db->order_by('visit_pos.visit_date','ASC');
		$query = $this->db->get('', $per_page, $page);
		
		return $query;
	}



	public function get_all_personnel()
	{
		$this->db->select('personnel.*');
		$this->db->where('personnel_job.personnel_id = personnel.personnel_id');
		$this->db->order_by('personnel_fname');
		$query = $this->db->get('personnel,personnel_job');
		
		return $query;
	}
}
?>