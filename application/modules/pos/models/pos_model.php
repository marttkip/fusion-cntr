<?php
class Pos_model extends CI_Model 
{

	/*
	*	Count all items from a table
	*	@param string $table
	* 	@param string $where
	*
	*/
	public function count_items($table, $where, $limit = NULL)
	{
		if($limit != NULL)
		{
			$this->db->limit($limit);
		}
		$this->db->from($table);
		$this->db->where($where);
		return $this->db->count_all_results();
	}
	public function get_all_service_charges($table, $where)
	{
		//retrieve all users
		$this->db->from($table);
		$this->db->select('service.service_name, service_charge.*, product.category_id');
		$this->db->where($where);
		$this->db->order_by('service_charge.service_charge_name','ASC');
		$query = $this->db->get();
		
		return $query;
	}

	public function get_all_personnel_orders($table, $where, $per_page, $page, $order = 'customer_first_name', $order_method = 'ASC')
	{
		//retrieve all users
		$this->db->from($table);
		$this->db->select('*');
		$this->db->where($where);
		$this->db->order_by($order, $order_method);
		$query = $this->db->get('', $per_page, $page);
		
		return $query;
	}
	
	public function get_product_categories($table, $where)
	{
		//retrieve all users
		$this->db->from($table);
		$this->db->select('product_category.product_category_id, product_category.product_category_name');
		$this->db->where($where);
		$this->db->group_by('product_category_name');
		$this->db->order_by('product_category.product_category_name','ASC');
		$query = $this->db->get();
		
		return $query;
	}
	
	public function create_visit_id()
	{
		$visit_data = array(
			"branch_code" => $this->session->userdata('branch_code'),
			"visit_date" => date('Y-m-d'),
			"visit_end_date" => date('Y-m-d'),
			"visit_type_id" => 0,
			"customer_id" => 0,
			"time_start" => date('H:i:s'),
			"customer_id" => 0,
			'created'=>date('Y-m-d H:i:s'),
			'created_by'=>$this->session->userdata('personnel_id'),
			'modified_by'=>$this->session->userdata('personnel_id')
		);
		if($this->db->insert('visit_pos', $visit_data))
		{
			$visit_id = $this->db->insert_id();
			
			return $visit_id;
		}
		
		else
		{
			return FALSE;
		}
	}
	public function create_new_customer()
	{
		$customer_first_name = $this->input->post('customer_name');

		$type_of_sale = $this->input->post('type_of_sale');

		if($type_of_sale == 1)
		{
			// this is a staff
			$staff_id = $this->input->post('staff_id');


				$where = 'personnel_id ='.$staff_id;
				$table = 'personnel';
				$this->db->where($where);
				$query = $this->db->get($table);

				if($query->num_rows() == 1 )
				{
					foreach ($query->result() as $key => $value) {
						# code...
						$personnel_fname = $value->personnel_fname;
						$personnel_onames = $value->personnel_onames;
						$personnel_id = $value->personnel_id;

						$customer_name = $personnel_fname.' '.$personnel_onames;
					}
				}
				else
				{
					$customer_name = '';
				}
		}
		else
		{
			// this is a normal 
			$customer_name = $this->input->post('customer_name');
			$personnel_id = null;
		}




		$data = array
			(
				'customer_first_name'=>$customer_name,
				'customer_number'=>$this->create_customer_number(),
				'customer_created'=>date('Y-m-d H:i:s'),
				// 'created_by'=>$this->session->userdata('personnel_id'),
				'personnel_id'=>$personnel_id,
				'customer_type_id'=>$type_of_sale,
			);
			if($this->db->insert('customer', $data))
			{
				$customer_id = $this->db->insert_id();

				$where = 'visit_type_name = "POS" ';
				$table = 'visit_type';
				$this->db->where($where);
				$query_type = $this->db->get($table);
				$visit_type_id = 17;
				if($query_type->num_rows() > 0)
				{
					foreach ($query_type->result() as $key) {
						# code...
						$visit_type_id = $key->visit_type_id;
					}
				}

				$visit_data = array(
					"branch_code" => $this->session->userdata('branch_code'),
					"visit_date" => $this->input->post('sale_date'),
					"visit_end_date" => date('Y-m-d'),
					"visit_type_id" => $visit_type_id,
					"close_card" => 0,
					"time_start" => date('H:i:s'),
					"customer_id" => $customer_id,
					'created'=>date('Y-m-d H:i:s'),
					'created_by'=>$this->session->userdata('personnel_id'),
					'modified_by'=>$this->session->userdata('personnel_id')
				);
				if($this->db->insert('visit_pos', $visit_data))
				{
					$visit_id = $this->db->insert_id();
					
					return $visit_id;
				}
				
				else
				{
					return FALSE;
				}
			}
			else
			{
				return FALSE;
			}

			
	}

	public function update_customer_delivery_details($customer_id,$visit_id)
	{
		
		$data = array
		(
			'customer_first_name'=>$this->input->post('customer_name'),
			'customer_phone'=>$this->input->post('phone_number'),
			'customer_location' => $this->input->post('location'),
			'customer_type_id'=>1
		);
		$this->db->where('customer_id',$customer_id);
		$this->db->update('customer', $data);
		
		$where = 'visit_type_name = "POS" ';
		$table = 'visit_type';
		$this->db->where($where);
		$query_type = $this->db->get($table);
		if($query_type->num_rows() > 0)
		{
			foreach ($query_type->result() as $key) {
				# code...
				$visit_type_id = $key->visit_type_id;
			}
		}

		$visit_data = array(
			"customer_id" => $customer_id,
			'location' => $this->input->post('location'),
			'transported_by' => $this->input->post('transported_by')
		);
		$this->db->where('visit_id',$visit_id);
		$this->db->update('visit_pos', $visit_data);
		return TRUE;
		
		
			
	}
	public function create_customer_number()
	{
		//select product code
		$preffix = "".$this->session->userdata('branch_code')."-";
		$this->db->from('customer');
		$this->db->where("customer_number LIKE '%".$preffix."%'");
		$this->db->select('MAX(customer_number) AS number');

		$query = $this->db->get();
		
		if($query->num_rows() > 0)
		{
			$result = $query->result();
			$number =  $result[0]->number;
			$real_number = str_replace($preffix, "", $number) * 1;
			$real_number++;
			$number = $preffix.sprintf('%04d', $real_number);
		}
		else
		{
			$number = $preffix.sprintf('%04d', 1);
		}
		return $number;
	}
	public function customer_names($visit_id)
	{
		
		$table = "customer, visit_pos";
		$where = "customer.customer_id = visit_pos.customer_id AND visit_pos.visit_id = ".$visit_id;
		$items = "customer.*,visit_pos.close_card,visit_pos.location,visit_pos.transported_by";
		$order = "customer_surname";
		
		
		$result = $this->database->select_entries_where($table, $where, $items, $order);
		
		foreach ($result as $row)
		{
			$customer_id = $row->customer_id;
			$customer_first_name = $row->customer_first_name;
			$customer_surname = $row->customer_surname;
			$customer_phone = $row->customer_phone;
			$close_card = $row->close_card;
			$customer_type_id = $row->customer_type_id;
			$location = $row->location;
			$transported_by = $row->transported_by;
		}
		// calculate patient balance
		$account_balance = $this->booking_model->patient_account_balance($customer_id);
		// end of patient balance
		$patient['customer_id'] = $customer_id;
		$patient['account_balance'] = $account_balance;
		$patient['customer_first_name'] = $customer_first_name;
		$patient['customer_surname'] = $customer_surname;
		$patient['close_card'] = $close_card;
		$patient['customer_type'] = $customer_type_id;
		$patient['customer_phone'] = $customer_phone;
		$patient['location'] = $location;
		$patient['transported_by'] = $transported_by;
		return $patient;
	}
	public function get_visit_types()
	{
		$this->db->from('visit_type');
		$this->db->select('*');
		$this->db->where('visit_type_status = 1');
		$query = $this->db->get();
		
		return $query;
	}
	public function get_product_color($product_id)
	{//echo $product_id;die();
		$product_color = '';
		$this->db->select('lab_test_class_name');
		$this->db->where('product.color_id = lab_test_class.lab_test_class_id AND product.product_id = '.$product_id);
		$query = $this->db->get('product, lab_test_class');
		
		if($query->num_rows() > 0)
		{
			$query_row = $query->row();
			$product_color = $query_row->lab_test_class_name;
		}
		return $product_color;
	}
	
}
?>