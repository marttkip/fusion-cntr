<?php   if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Pos extends MX_Controller 
{
	function __construct()
	{
		parent:: __construct();
		
		$this->load->model('pos_model');
		$this->load->model('auth/auth_model');
		$this->load->model('admin/users_model');
		$this->load->model('booking/booking_model');
		$this->load->model('booking/database');
		$this->load->model('site/site_model');
		$this->load->model('admin/sections_model');
		$this->load->model('admin/admin_model');
		$this->load->model('hr/personnel_model');
		$this->load->model('booking/booking_model');
	}
	
	public function index()
	{
		$branch_code = $this->session->userdata('search_branch_code');
		$visit_id = $this->session->userdata('pos_visit_id');
		
		if(empty($visit_id))
		{
			$visit_id = $this->pos_model->create_visit_id();
			$this->session->set_userdata('pos_visit_id', $visit_id);
		}
		// redirect('booking/billing/'.$visit_id.'/1');
		$v_data['visit_id'] = $visit_id;
		
		$this->db->where('branch_code', $branch_code);
		$query = $this->db->get('branch');
		
		$v_data['branch'] = $query;


		$where = 'service.service_id = service_charge.service_charge_id AND service_charge.service_charge_delete = 0  AND service.service_name = \'Cafeteria\' ';
		
		$table = 'service, service_charge';
		
		$pos_search = $this->session->userdata('pos_search');
		
		if(!empty($pos_search))
		{
			$where .= $pos_search;
		}
		
		$query = $this->pos_model->get_all_service_charges($table, $where);
		$v_data['categories'] = $this->pos_model->get_product_categories($table, $where);
		$v_data['query'] = $query;
		
		$v_data['item_invoiced_rs'] = $this->booking_model->get_patient_visit_charge_items($visit_id);
		$v_data['credit_note_amount'] = $this->booking_model->get_sum_credit_notes($visit_id);
		$v_data['debit_note_amount'] = $this->booking_model->get_sum_debit_notes($visit_id);
		$v_data['payments_rs'] = $this->booking_model->payments($visit_id);
		$v_data['cancel_actions'] = $this->booking_model->get_cancel_actions();
		
		$data['title'] = 'POS';
		$v_data['title'] = $data['title'];
		$data['content'] = $this->load->view('pos', $v_data, true);
		
		$this->load->view('admin/templates/general_page', $data);
		// end of it
		
	}


	public function my_orders()
	{
		$branch_code = $this->session->userdata('search_branch_code');
		$visit_id = $this->session->userdata('pos_visit_id');

		$this->db->where('branch_code', $branch_code);
		$query = $this->db->get('branch');
		$personnel_id = $this->session->userdata('personnel_id');
		// var_dump($personnel_id); die();
			
		$where = 'visit_pos.customer_id = customer.customer_id AND visit_pos.close_card = 0 AND visit_pos.created_by = '.$personnel_id;
		
		$table = 'visit_pos, customer';

		
		$segment = 3;
		//pagination
		$this->load->library('pagination');
		$config['base_url'] = site_url().'point-of-sale/pos';
		$config['total_rows'] = $this->users_model->count_items($table, $where);
		$config['uri_segment'] = $segment;
		$config['per_page'] = 40;
		$config['num_links'] = 5;
		
		$config['full_tag_open'] = '<ul class="pagination pull-right">';
		$config['full_tag_close'] = '</ul>';
		
		$config['first_tag_open'] = '<li>';
		$config['first_tag_close'] = '</li>';
		
		$config['last_tag_open'] = '<li>';
		$config['last_tag_close'] = '</li>';
		
		$config['next_tag_open'] = '<li>';
		$config['next_link'] = 'Next';
		$config['next_tag_close'] = '</span>';
		
		$config['prev_tag_open'] = '<li>';
		$config['prev_link'] = 'Prev';
		$config['prev_tag_close'] = '</li>';
		
		$config['cur_tag_open'] = '<li class="active"><a href="#">';
		$config['cur_tag_close'] = '</a></li>';
		
		$config['num_tag_open'] = '<li>';
		$config['num_tag_close'] = '</li>';
		$this->pagination->initialize($config);
		
		$page = ($this->uri->segment($segment)) ? $this->uri->segment($segment) : 0;
        $v_data["page"] = $page;
        $v_data["links"] = $this->pagination->create_links();
		$v_data['query'] = $this->pos_model->get_all_personnel_orders($table, $where, $config["per_page"], $page);
		$v_data['all_personnel'] = $this->booking_model->get_all_personnel();
		$data['title'] = $v_data['title'] = 'My Orders';
		$v_data['module'] = 0;
		$v_data['close_page'] = 1;
		$v_data['type_links'] = 1;
		// var_dump($personnel_id); die();
		$data['content'] = $this->load->view('orders', $v_data, TRUE);
		
		$this->load->view('admin/templates/general_page', $data);
	
		// end of it
		
	}
	public function search_pos($pager)
	{
		$product_code = $this->input->post('product_code');
		
		if(!empty($product_code))
		{
			$product_code = ' AND product.product_code = '.$product_code.' ';
		}
		
		$search = $product_code;
		$this->session->unset_userdata('pos_search');
		$this->session->set_userdata('pos_search', $search);
		redirect('point-of-sale/pos');
		
		
	}
	public function close_queue_search($pager)
	{
		$this->session->unset_userdata('pos_search');
		redirect('point-of-sale/pos');
	}
	
	public function shopping_cart($visit_id, $service_charge_id)
	{
		if($this->pos_model->add_visit_charge($visit_id, $service_charge_id))
		{
			$this->session->set_userdata('success_message', 'Customer added successfully');
		}
		
		else
		{
			$this->session->set_userdata('success_message', 'Customer added successfully');
		}
		redirect('point-of-sale/pos');
	}
	
	public function close_sale($visit_id)
	{
		$visit_data = array('close_card'=>1);
		$this->db->where('visit_id',$visit_id);
		$this->db->update('visit_pos',$visit_data);
		redirect('point-of-sale/pos');
	}
	public function bulk_close_visits()
	{
		$total_visits = sizeof($_POST['visit']);
		
		//check if any checkboxes have been ticked
		if($total_visits > 0)
		{	
			for($r = 0; $r < $total_visits; $r++)
			{	
				$visit = $_POST['visit'];
				$visit_id = $visit[$r]; 
				//check if card is held
			
				$visit_data = array('close_card'=>1);
				$this->db->where('visit_id',$visit_id);
				$this->db->update('visit',$visit_data);
			}
				$this->session->set_userdata('success_message', 'Sales closed successfully');
		}
		
		else
		{
			$this->session->set_userdata('error_message', 'Please select visits to terminate first');
		}
		
		redirect('point-of-sale/pos');
	}
	public function create_new_customer()
	{
		//form validation rules

		$this->form_validation->set_rules('type_of_sale', 'Type of sale', 'required|xss_clean');
		$this->form_validation->set_rules('sale_date', 'Sale Date', 'required|xss_clean');

		$type_of_sale = $this->input->post('type_of_sale');

		if($type_of_sale == 1)
		{
			// this is a staff
			$this->form_validation->set_rules('staff_id', 'Staff', 'required|xss_clean');
		}
		else
		{
			// this is a normal 
			$this->form_validation->set_rules('customer_name', 'Table Name', 'required|xss_clean');
		}
		
		
		//if form conatins invalid data
		if ($this->form_validation->run())
		{
			$visit_id = $this->pos_model->create_new_customer();
			if($visit_id)
			{
				$this->session->set_userdata("success_message","Order Created successfully");

				redirect('pos/billing/'.$visit_id.'');
			}
			
			else
			{
				$this->session->set_userdata("error_message","Could not create a new order");
				redirect('point-of-sale/pos');
			}
		}
		else
		{
			$this->session->set_userdata("error_message","Please try again");

		}
	}
	public function delivery_details($customer_id,$visit_id)
	{
		$this->form_validation->set_rules('customer_name', 'Customer Name', 'requireed|xss_clean');
		$this->form_validation->set_rules('phone_number', 'Phone Number', 'requireed|xss_clean');
		
		//if form conatins invalid data
		if ($this->form_validation->run())
		{
			if($this->pos_model->update_customer_delivery_details($customer_id,$visit_id))
			{
				$this->session->set_userdata("success_message","Delivery Details was added successfully");

				redirect('pos/billing/'.$visit_id.'');
			}
			
			else
			{
				$this->session->set_userdata("error_message","Could not add Delivery Details was added successfully");
				redirect('pos/billing/'.$visit_id.'');
			}
		}
		else
		{
			$this->session->set_userdata("error_message","Please try again");
			redirect('pos/billing/'.$visit_id.'');

		}
	}
	public function billing($visit_id, $close_page = NULL)
	{
		$v_data = array('visit_id'=>$visit_id);
		
		$v_data['cancel_actions'] = $this->booking_model->get_cancel_actions();
		
		$patient = $this->pos_model->customer_names($visit_id);
		$v_data['customer_first_name'] = $patient['customer_first_name'];

		$v_data['customer_surname'] = $patient['customer_surname'];
		$v_data['account_balance'] = $patient['account_balance'];
		$v_data['customer_id'] = $patient['customer_id'];
		$v_data['customer_type'] = $patient['customer_type'];
		$v_data['customer_phone'] = $patient['customer_phone'];
		$v_data['location'] = $patient['location'];
		$v_data['transported_by'] = $patient['transported_by'];
		if($v_data['customer_type'] != 1)
		{
			
			$v_data['customer_name'] = '';	
		}
		else
		{
			$v_data['customer_name'] = $v_data['customer_first_name'];
		}
		$primary_key = $patient['customer_id'];
		$v_data['close_page'] = $close_page;
		$data['content'] = $this->load->view('front_office', $v_data, true);
		
		$data['title'] = 'Payments';
		$this->load->view('admin/templates/general_page', $data);
	}
	public function get_next_customer_number()
	{
		$number = $this->pos_model->create_customer_number();

		echo $number;
	}
}


?>