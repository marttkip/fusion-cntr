<?php   if ( ! defined('BASEPATH')) exit('No direct script access allowed');

require_once "./application/modules/pos/controllers/pos.php";

class Reports extends pos
{	
	function __construct()
	{
		parent:: __construct();
		$this->load->model('pos/pos_model');
		$this->load->model('reports_model');
	}
	public function all_sales()
	{



		$where = 'visit_pos.customer_id = customer.customer_id AND visit_pos.close_card = 0 ';
		$table = 'visit_pos, customer';
		
		$visit_search = $this->session->userdata('all_sales_search');
		if(!empty($visit_search))
		{
			$where .= $visit_search;
		}
		$segment = 3;
		//pagination
		$this->load->library('pagination');
		$config['base_url'] = site_url().'point-of-sale/pos-report';
		$config['total_rows'] = $this->users_model->count_items($table, $where);
		$config['uri_segment'] = $segment;
		$config['per_page'] = 40;
		$config['num_links'] = 5;
		
		$config['full_tag_open'] = '<ul class="pagination pull-right">';
		$config['full_tag_close'] = '</ul>';
		
		$config['first_tag_open'] = '<li>';
		$config['first_tag_close'] = '</li>';
		
		$config['last_tag_open'] = '<li>';
		$config['last_tag_close'] = '</li>';
		
		$config['next_tag_open'] = '<li>';
		$config['next_link'] = 'Next';
		$config['next_tag_close'] = '</span>';
		
		$config['prev_tag_open'] = '<li>';
		$config['prev_link'] = 'Prev';
		$config['prev_tag_close'] = '</li>';
		
		$config['cur_tag_open'] = '<li class="active"><a href="#">';
		$config['cur_tag_close'] = '</a></li>';
		
		$config['num_tag_open'] = '<li>';
		$config['num_tag_close'] = '</li>';
		$this->pagination->initialize($config);
		
		$page = ($this->uri->segment($segment)) ? $this->uri->segment($segment) : 0;
        $v_data["page"] = $page;
        $v_data["links"] = $this->pagination->create_links();
		$v_data['query'] = $this->reports_model->get_all_personnel_orders($table, $where, $config["per_page"], $page);
		$v_data['all_personnel'] = $this->booking_model->get_all_personnel();
		$data['title'] = $v_data['title'] = 'Reports';
		$v_data['module'] = 0;
		$v_data['close_page'] = 1;
		$v_data['type_links'] = 1;
		// var_dump($personnel_id); die();
		$data['content'] = $this->load->view('reports/all_sales', $v_data, true);
		
		$this->load->view('admin/templates/general_page', $data);
	}

	public function search_transactions()
	{
		$personnel_id = $this->input->post('staff_id');
		$date_from = $this->input->post('date_from');
		$date_to = $this->input->post('date_to');
		
		$search_title = 'Showing reports for: ';
		
		
		
		if(!empty($personnel_id))
		{
			$personnel_id = ' AND customer.personnel_id = '.$personnel_id.' ';
			
			$this->db->where('personnel_id', $personnel_id);
			$query = $this->db->get('personnel');
			
			if($query->num_rows() > 0)
			{
				$row = $query->row();
				$search_title .= $row->personnel_fname.' '.$row->personnel_onames.' ';
			}
		}
		
		//date filter for cash report
		$prev_search = '';
		$prev_table = '';
		
		if(!empty($date_from) && !empty($date_to))
		{
			$visit_date = ' AND visit_pos.visit_date BETWEEN \''.$date_from.'\' AND \''.$date_to.'\'';
			$search_title .= 'Date from '.date('jS M Y', strtotime($date_from)).' to '.date('jS M Y', strtotime($date_to)).' ';
		}
		
		else if(!empty($date_from))
		{
			$visit_date = ' AND visit_pos.visit_date = \''.$date_from.'\'';
			$search_title .= 'Date of '.date('jS M Y', strtotime($date_from)).' ';
		}
		
		else if(!empty($date_to))
		{
			$visit_date = ' AND visit_pos.visit_date = \''.$date_to.'\'';
			$search_title .= 'Date of '.date('jS M Y', strtotime($date_to)).' ';
		}
		
		else
		{
			$visit_date = '';
		}
		
		
		$search = $visit_date.$personnel_id;
	
		$this->session->set_userdata('all_sales_search', $search);
		$this->session->set_userdata('search_title', $search_title);
		
		redirect('point-of-sale/pos-report');
	}
	public function close_sales_search()
	{
		$this->session->unset_userdata('all_sales_search');
		$this->session->unset_userdata('search_title', $search_title);
		redirect('point-of-sale/pos-report');
	}

	public function all_staff_sales()
	{
		$where = 'v_customer_balances.personnel_id = customer.personnel_id AND customer.personnel_id > 0';
		$table = 'customer,v_customer_balances';
		
		$visit_search = $this->session->userdata('all_staff_sales_search');
		if(!empty($visit_search))
		{
			$where .= $visit_search;
		}
		$segment = 3;
		//pagination
		$this->load->library('pagination');
		$config['base_url'] = site_url().'point-of-sale/staff-report';
		$config['total_rows'] = $this->users_model->count_items($table, $where);
		$config['uri_segment'] = $segment;
		$config['per_page'] = 40;
		$config['num_links'] = 5;
		
		$config['full_tag_open'] = '<ul class="pagination pull-right">';
		$config['full_tag_close'] = '</ul>';
		
		$config['first_tag_open'] = '<li>';
		$config['first_tag_close'] = '</li>';
		
		$config['last_tag_open'] = '<li>';
		$config['last_tag_close'] = '</li>';
		
		$config['next_tag_open'] = '<li>';
		$config['next_link'] = 'Next';
		$config['next_tag_close'] = '</span>';
		
		$config['prev_tag_open'] = '<li>';
		$config['prev_link'] = 'Prev';
		$config['prev_tag_close'] = '</li>';
		
		$config['cur_tag_open'] = '<li class="active"><a href="#">';
		$config['cur_tag_close'] = '</a></li>';
		
		$config['num_tag_open'] = '<li>';
		$config['num_tag_close'] = '</li>';
		$this->pagination->initialize($config);
		
		$page = ($this->uri->segment($segment)) ? $this->uri->segment($segment) : 0;
        $v_data["page"] = $page;
        $v_data["links"] = $this->pagination->create_links();
		$v_data['query'] = $this->reports_model->get_all_personnel_orders($table, $where, $config["per_page"], $page);
		$v_data['all_personnel'] = $this->booking_model->get_all_personnel();
		$data['title'] = $v_data['title'] = 'Reports';
		$v_data['module'] = 0;
		$v_data['close_page'] = 1;
		$v_data['type_links'] = 1;
		// var_dump($personnel_id); die();
		$data['content'] = $this->load->view('reports/all_personnel_balances', $v_data, true);
		
		$this->load->view('admin/templates/general_page', $data);
	}
	public function search_all_staff_transactions()
	
	{
		$personnel_id = $this->input->post('staff_id');
		
		$search_title = 'Showing reports for: ';
		
		
		
		if(!empty($personnel_id))
		{
			$personnel_id = ' AND customer.personnel_id = '.$personnel_id.' ';
			
			$this->db->where('personnel_id', $personnel_id);
			$query = $this->db->get('personnel');
			
			if($query->num_rows() > 0)
			{
				$row = $query->row();
				$search_title .= $row->personnel_fname.' '.$row->personnel_onames.' ';
			}
		}
		
		//date filter for cash report
		
		$search = $personnel_id;
	
		$this->session->set_userdata('all_staff_sales_search', $search);
		$this->session->set_userdata('search_title', $search_title);
		
		redirect('point-of-sale/staff-report');
	}
	public function close_staff_sales_search()
	{
		$this->session->unset_userdata('all_staff_sales_search');
		$this->session->unset_userdata('search_title', $search_title);
		redirect('point-of-sale/staff-report');
	}

}
?>