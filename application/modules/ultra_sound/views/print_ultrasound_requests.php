<?php

//patient details
$visit_type = $patient['visit_type'];
$patient_type = $patient['patient_type'];
$patient_othernames = $patient['patient_othernames'];
$patient_surname = $patient['patient_surname'];
$patient_surname = $patient['patient_surname'];
$patient_number = $patient['patient_number'];
$gender = $patient['gender'];
$patient_insurance_number = $patient['patient_insurance_number'];
$inpatient = $patient['inpatient'];
$visit_type_name = $patient['visit_type_name'];
$patient_age = $patient['patient_age'];
$patient_date_of_birth = $patient['patient_date_of_birth'];

$today = date('jS F Y H:i a',strtotime(date("Y:m:d h:i:s")));
$visit_date = date('jS F Y',strtotime($this->accounts_model->get_visit_date($visit_id)));

//doctor
$doctor = $this->accounts_model->get_att_doctor($visit_id);

//served by
$served_by = $this->accounts_model->get_personnel($this->session->userdata('personnel_id'));

if($gender == 1)
{
    $gender = 'Female';
}
else
{
    $gender = 'Male';
}

?>

<!DOCTYPE html>
<html lang="en">
    <head>
        <title><?php echo $contacts['company_name'];?> | Lab Request Form</title>
        <!-- For mobile content -->
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <!-- IE Support -->
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <!-- Bootstrap -->
        <link rel="stylesheet" href="<?php echo base_url()."assets/themes/porto-admin/1.4.1/";?>assets/vendor/bootstrap/css/bootstrap.css" media="all"/>
        <link rel="stylesheet" href="<?php echo base_url()."assets/themes/porto-admin/1.4.1/";?>assets/stylesheets/theme-custom.css" media="all"/>
        <style type="text/css">
			.receipt_spacing{letter-spacing:0px; font-size: 12px;}
			.center-align{margin:0 auto; text-align:center;}
			
			.receipt_bottom_border{border-bottom: #888888 medium solid;}
			.row .col-md-12 table {
				border:solid #000 !important;
				border-width:1px 0 0 1px !important;
				font-size:10px;
			}
			.row .col-md-12 th, .row .col-md-12 td {
				border:solid #000 !important;
				border-width:0 1px 1px 0 !important;
			}
			.table thead > tr > th, .table tbody > tr > th, .table tfoot > tr > th, .table thead > tr > td, .table tbody > tr > td, .table tfoot > tr > td
			{
				 padding: 2px;
			}
			
			.row .col-md-12 .title-item{float:left;width: 130px; font-weight:bold; text-align:right; padding-right: 20px;}
			.title-img{float:left; padding-left:30px;}
			img.logo{max-height:70px; margin:0 auto;}
		</style>
    </head>
    <body class="receipt_spacing">
    	<div class="row">
        	<div class="col-xs-12">
            	<img src="<?php echo base_url().'assets/logo/'.$contacts['logo'];?>" alt="<?php echo $contacts['company_name'];?>" class="img-responsive logo"/>
            </div>
        </div>
    	<div class="row">
        	<div class="col-md-12 center-align receipt_bottom_border">
            	<strong>
                	<?php echo $contacts['company_name'];?><br/>
                    P.O. Box <?php echo $contacts['address'];?> <?php echo $contacts['post_code'];?>, <?php echo $contacts['city'];?><br/>
                    E-mail: <?php echo $contacts['email'];?>. Tel : <?php echo $contacts['phone'];?><br/>
                    <?php echo $contacts['location'];?>, <?php echo $contacts['building'];?>, <?php echo $contacts['floor'];?><br/>
                </strong>
            </div>
        </div>
        
      <div class="row receipt_bottom_border" >
        	<div class="col-md-12 center-align">
            	<strong>RADIOLOGY TEST REQUEST</strong>
            </div>
        </div>
        
        <!-- Patient Details -->
    	<div class="row receipt_bottom_border" style="margin-bottom: 10px;">
        	<div class="col-md-4 pull-left">
            	<div class="row">
                	<div class="col-md-12">
                    	
                    	<div class="title-item">Patient Name:</div>
                        
                    	<?php echo $patient_surname.' '.$patient_othernames; ?>
                    </div>
                </div>
            	<div class="row">
                    <div class="col-md-12">
                        <div class="title-item">Gender:</div> 
                        
                        <?php echo $gender; ?>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <div class="title-item">Age:</div> 
                        
                        <?php echo $patient_age; ?>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <div class="title-item">Patient Number:</div> 
                        
                        <?php echo $patient_number; ?>
                    </div>
                </div>
            
            </div>
            
        	
            
        	<div class="col-md-4 pull-right">
            	<div class="row">
                	<div class="col-md-12">
                    	<div class="title-item">Date of admission:</div>
                        
                    	<?php echo $visit_date; ?>
                    </div>
                </div>
                <?php if($visit_type != 1){?>
            	<div class="row">
                	<div class="col-md-12">
                    	<div class="title-item">Insurance:</div>
                        
                    	<?php echo $visit_type_name; ?>
                    </div>
                </div>
                <?php }?>
            </div>
        </div>
        
    	
        <div class="row receipt_bottom_border">
        	<div class="col-md-12">
	       		<?php
	       		//  check if visit exists

					$rs = $this->nurse_model->check_visit_type($visit_id);

					if(count($rs) >0){
						foreach ($rs as $rs1):
							# code...
							$visit_t = $rs1->visit_type;

							// get the visit charge

						endforeach;

						$rs2 = $this->nurse_model->visit_charge_per_service($visit_id,'Ultrasound');

						


					}

					// var_dump($rs2);die();

					echo "
					<table align='center' class='table table-striped table-hover table-condensed'>
						<tr>
							
							<th align='center'>Procedure Name</th>
						</tr>		
					";                     
							$total= 0;  
							if(count($rs2) >0){
								foreach ($rs2 as $rs3):
									# code...
									$v_procedure_id = $rs3->visit_charge_id;
									$procedure_id = $rs3->service_charge_id;
									$visit_charge_amount = $rs3->visit_charge_amount;
									$units = $rs3->visit_charge_units;

									//  get the service charge

									$rs4 = $this->nurse_model->get_service_charge($procedure_id);
									if(count($rs4) >0){
										foreach ($rs4 as $rs5):
							
											$procedure_name = $rs5->service_charge_name;
											$service_id = $rs5->service_id;
											// if($service_id==12){
												$total= $total +($units * $visit_charge_amount);
												echo"
														<tr> 
												 			<td>".$procedure_name."</td>
															
														</tr>	
												";					
											// } 
											endforeach;
									}
									endforeach;

							}
				
				?>
			</div>
        	
		</div>
        
	
    	
    	<div class="row" style="font-style:italic; font-size:11px;">
        	<div class="col-md-10 pull-left">
                <div class="col-md-6 pull-left">
                    Prepared by: <?php echo $served_by;?> 
                </div>
          	</div>
        	<div class="col-md-6 pull-right">
            	<?php echo date('jS M Y H:i a'); ?> Thank you
            </div>
        </div>
    </body>
    
</html>