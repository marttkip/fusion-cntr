
<?php echo $this->load->view('search/search_branch_expenses', '', TRUE);?>

<div class="row">
    <div class="col-md-12">

        <section class="panel ">
            <header class="panel-heading">

                <h2 class="panel-title"><?php echo strtoupper($title);?></h2>
                <a href="<?php echo site_url();?>export-expenses-report" target="_blank"  class="btn btn-sm btn-info pull-right" style="margin-top:-25px;margin-left:5px" > Export </a>
                <a href="<?php echo base_url().'print-expenses-report';?>" target="_blank" class="btn btn-sm btn-warning pull-right" style="margin-top: -25px;"><i class="fa fa-cancel" ></i> Print Expenses</a>
            </header>

            <div class="panel-body">


			<?php
			$ledger_search = $this->session->userdata('export_payment_search');
			if(!empty($ledger_search))
			{
				?>
                <a href="<?php echo base_url().'close-expense-search-report';?>" class="btn btn-sm btn-danger"><i class="fa fa-cancel"></i> Close Search</a>
                <?php
			}
			$error = $this->session->userdata('error_message');
			$success = $this->session->userdata('success_message');

			if(!empty($error))
			{
				echo '<div class="alert alert-danger">'.$error.'</div>';
				$this->session->unset_userdata('error_message');
			}

			if(!empty($success))
			{
				echo '<div class="alert alert-success">'.$success.'</div>';
				$this->session->unset_userdata('success_message');
			}

			// echo $result;
			// var_dump($query);die();
			$result ='';

			if ($query->num_rows() > 0)
			{
				$count = $page;
				
				
					$result .= 
					'
						<table class="table table-condensed table-hover table-bordered ">
						  <thead>
							<tr>
							  <th>#</th>
							  <th>Transaction Date</th>
							  <th>Account Name</th>
							  <th>Transaction Category</th>
							  <th>Description</th>
							  <th>Reference Code</th>
							  <th>Amount</th>
							</tr>
						  </thead>
						  <tbody>
					';
				
				
				
				foreach ($query->result() as $row)
				{

					$total_amount = $row->dr_amount;
					$accountName = $row->accountName;
					$referenceCode = $row->referenceCode;
					$account_id = $row->accountId;
				    $transactionDescription = $row->transactionDescription;
				    $transactionClassification = $row->transactionClassification;
				    $transactionDate = $row->transactionDate;
				    $transactionCode = $row->transactionCode;

					if(empty($referenceCode))
					{
						$referenceCode = $transactionCode;
					}

					if($transactionClassification == "Creditors Invoices Payments")
					{
						$total_amount = $row->cr_amount;
						$accountName = 'Creditor Payments';
					}

					$total_operational_amount += $total_amount;
					$count++;
					$result .='<tr>
									<td class="text-left">'.$count.'</td>
	                          		<td class="text-left">'.strtoupper($transactionDate).'</td>
	                          		<td class="text-left">'.strtoupper($accountName).'</td>
	                          		<td class="text-left">'.strtoupper($transactionClassification).'</td>
	            					<td class="text-left">'.strtoupper($transactionDescription).'</td>
	                          		<td class="text-left">'.strtoupper($referenceCode).'</td>
	            					<td class="text-right">'.number_format($total_amount,2).'</td>
	            				 </tr>';
				}
				$result .='<tr>
								<th class="text-left" colspan="6">Total</th>
            					<th class="text-right">'.number_format($total_operational_amount,2).'</th>
            				 </tr>';
				
				$result .= 
				'
							  </tbody>
							</table>
				';
			}

			else
			{
				$result .= "There are no patients";
			}
			echo $result;
?>			
			

          	</div>

          	<div class="widget-foot">
                                
				<?php if(isset($links)){echo $links;}?>
            
                <div class="clearfix"></div> 
            
            </div>
		</section>
<?php

$payments_result = $this->reports_model->get_expenses_summary();
// var_dump($payments_result);die();
$results ='';
$total_visit_report ='';
$amount_paid_totals = 0;
if($payments_result->num_rows() > 0)
{
    $x = 0;
    foreach ($payments_result->result() as $key => $value) {
        # code...
        $accountName = $value->accountName;
        $total_amount = $value->total_amount;
        $accountsclassfication = $value->accountsclassfication;
        $total_cr_amount = $value->total_cr_amount;

        if($accountsclassfication == "Bank")
        {
        	$total_amount = $total_cr_amount;
        	$accountName = 'Creditor Payments';
        }

        $x++;
        $color = $this->reception_model->random_color();                           
        $total_visit_report .= '{
                                    label: " '.$accountName.' ('.number_format($total_amount,2).')",
                                    data: [
                                        ['.$x.', '.$total_amount.']
                                    ],
                                    color: "'.$color.'",
                                },';
        $amount_paid_totals += $total_amount;

        $results .= '
                    <tr>
                        <th>'.$accountName.'</th>
                        <td>'.number_format($total_amount,2).'</td>
                    </tr>';


    }
}

// var_dump($total_visit_report);die();
?>

		<section class="panel">
		    <header class="panel-heading">
		          <h4 class="pull-left"><i class="icon-reorder"></i>Transactions Report <?php echo $title;?></h4>
		          <div class="widget-icons pull-right">
		               
		          </div>
		          <div class="clearfix"></div>
		    </header>
		    <div class="panel-body">
		        <div class="padd">
		            <div class="row">
		                <div class="col-md-6">

		                    <h3>Per Expense Account </h3>
		                    <table class="table table-striped table-hover table-condensed">
		                        <thead>
		                            <tr>
		                                <th>Transaction Category Name</th>
		                                <th>Amount Used</th>
		                            </tr>
		                        </thead>
		                        <tbody>
		                           
		                            <?php echo $results;?>
		                            <tr>
		                                <th>TOTALS</th>
		                                <td><?php echo number_format($amount_paid_totals,2);?></td>
		                            </tr>
		                        </tbody>
		                    </table>
		                </div>
		                <div class="col-md-6">
		                    <!-- Flot: Pie -->
		                    <div class="chart chart-md" id="flotPie2"></div>
		                    <script type="text/javascript">
		                        var flotPieData2 = [<?php echo $total_visit_report?>];

		                        // See: js/examples/examples.charts.js for more settings.
		                    </script>
		                </div>
		            </div>
		        </div>
		    </div>
</section>
    </div>
</div>